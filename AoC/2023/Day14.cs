﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2023;

public class Day14(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value; var m = lines.Length; var n = lines[0].Length;
     char[] parse()
     {
        var res = new char[m * n];
        for (var r = 0; r < m; r++)
        for (var c = 0; c < n; c++)
           res[r * n + c] = lines[r][c];
        return res;
     }

     // TODO REFACTORING tilt()
     const char ROUND = 'O', EMPTY = '.';
     void tiltNorth(char[] a)
     {
        for (var c = 0; c < n; c++)
        {
           int firstMoved;
           do
           {
              firstMoved = -1;
              for (var r = firstMoved + 1; r < m - 1; r++)
                 if (a[r * n + c] == EMPTY && a[(r + 1) * n + c] == ROUND)
                 {
                    a[r * n + c] = ROUND; a[(r + 1) * n + c] = EMPTY;
                    if (firstMoved == -1)
                       firstMoved = r;
                 }
           } while (firstMoved != -1);
        }
     }
     void tiltWest(char[] a)
     {
        for (var r = 0; r < m; r++)
        {
           int firstMoved;
           do
           {
              firstMoved = -1;
              for (var c = firstMoved + 1; c < n - 1; c++)
                 if (a[r * n + c] == EMPTY && a[r * n + c + 1] == ROUND)
                 {
                    a[r * n + c] = ROUND; a[r * n + c + 1] = EMPTY;
                    if (firstMoved == -1)
                       firstMoved = c;
                 }
           } while (firstMoved != -1);
        }
     }
     void tiltSouth(char[] a)
     {
        for (var c = 0; c < n; c++)
        {
           int firstMoved;
           do
           {
              firstMoved = m;
              for (var r = firstMoved - 1; r > 0; r--)
                 if (a[r * n + c] == EMPTY && a[(r - 1) * n + c] == ROUND)
                 {
                    a[r * n + c] = ROUND; a[(r - 1) * n + c] = EMPTY;
                    if (firstMoved == m)
                       firstMoved = r;
                 }
           } while (firstMoved != m);
        }
     }
     void tiltEast(char[] a)
     {
        for (var r = 0; r < m; r++)
        {
           int firstMoved;
           do
           {
              firstMoved = n;
              for (var c = firstMoved - 1; c > 0; c--)
                 if (a[r * n + c] == EMPTY && a[r * n + c - 1] == ROUND)
                 {
                    a[r * n + c] = ROUND; a[r * n + c - 1] = EMPTY;
                    if (firstMoved == n)
                       firstMoved = c;
                 }
           } while (firstMoved != n);
        }
     }
     void doCycle(char[] a) { tiltNorth(a); tiltWest(a); tiltSouth(a); tiltEast(a); }
     int load(char[] a) => Enumerable.Range(0, m * n).Where(i => a[i] == ROUND).Sum(i => m - i / n);

     int part1()
     {
        var area = parse();
        tiltNorth(area);
        return load(area);
     }
     int part2()
     {
        var area = parse();
        var prevAreas = new List<char[]>();
        const int TARGET = 1000000000;
        for (var periodFound = false; !periodFound; doCycle(area))
        {
           for (var prev = 0; prev < prevAreas.Count; prev++)
              if (prevAreas[prev].SequenceEqual(area))
              {
                 area = prevAreas[prev + (TARGET - prev) % (prevAreas.Count - prev) - 1];
                 periodFound = true;
                 break;
              }
           prevAreas.Add((char[])area.Clone());
        }
        return load(area);
     }
     Part1 = part1().ToString(); Part2 = part2().ToString();
  }
}
