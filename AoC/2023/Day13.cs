﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2023;

public class Day13(string input) : AoC(input)
{
  public override void Solve()
  {
     static (int, int) solve(bool[][] p)
     {
        var m = p.Length; var n = p[0].Length;
        int value(int avoid = 0)
        {
           var ver = new bool[n - 1]; var hor = new bool[m - 1];
           int res()
           {
              int val(bool[] arr, int mult)
              {
                 for (var i = 0; i < arr.Length; i++)
                    if (!arr[i])
                    {
                       var v = mult * (i + 1);
                       if (avoid != v)
                          return v;
                    }
                 return 0;
              }
              return new (bool[] arr, int mult)[] { (ver, 1), (hor, 100) }
                 .Select(el => val(el.arr, el.mult))
                 .FirstOrDefault(v => v > 0);
           }
           for (var r = 0; r < m; r++)
              for (var c = 0; c < n; c++)
              {
                 for (var vd = c; vd < n - 1; vd++)
                    if (vd + 1 + vd - c < n && p[r][c] != p[r][vd + 1 + vd - c])
                       ver[vd] = true;
                 for (var hd = r; hd < m - 1; hd++)
                    if (hd + 1 + hd - r < m && p[r][c] != p[hd + 1 + hd - r][c])
                       hor[hd] = true;
              }
           return res();
        }
        void flip(int r, int c) { p[r][c] = !p[r][c]; }
        var el1 = value(); var el2 = 0;
        for (var smudge = 0; el2 == 0; smudge++)
        {
           var r = smudge / n; var c = smudge % n;
           flip(r, c); el2 = value(avoid: el1); flip(r, c);
        }
        return (el1, el2);
     }

     var p1 = 0; var p2 = 0;
     var lines = InputLines.Value.ToList(); lines.Add(string.Empty);
     var pattern = new List<bool[]>();
     foreach (var l in lines)
        if (string.Empty == l)
        {
           var (el1, el2) = solve([.. pattern]);
           p1 += el1; p2 += el2;
           pattern = [];
        }
        else
           pattern.Add(l.Select(ch => ch == '#').ToArray());

     Part1 = p1.ToString(); Part2 = p2.ToString();
  }
}
