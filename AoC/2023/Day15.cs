﻿using System.Linq;
using System.Collections.Generic;

using Lens = (string label, int value);

namespace AoC._2023;

public class Day15(string input) : AoC(input)
{
  public override void Solve()
  {
     var hashmaps = Input.Split(',');
     const int N = 256;
     var boxes = new List<Lens>[N];

     static int hash(string s) => s.Aggregate(0, (a, c) => (a + c) * 17 % N);
     List<Lens> getBox(string label)
     {
        var ind = hash(label);
        return boxes[ind] ?? (boxes[ind] = []);
     }

     foreach (var map in hashmaps)
        if (map[^1] == '-')
        {
           var label = map[..^1];
           var box = getBox(label);
           for (var i = 0; i < box.Count; i++)
              if (box[i].label == label)
              {
                 for (var j = i; j < box.Count - 1; j++)
                    box[j] = box[j + 1];
                 box.RemoveAt(box.Count - 1);
                 break;
              }
        }
        else
        {
           var parts = map.Split('=');
           var label = parts[0]; var value = int.Parse(parts[1]);
           var box = getBox(label);
           var found = false;
           for (var i = 0; i < box.Count; i++)
              if (box[i].label == label)
              {
                 box[i] = (label, value);
                 found = true;
                 break;
              }
           if (!found)
              box.Add((label, value));
        }

     Part1 = hashmaps.Sum(hash).ToString();
     Part2 = boxes.Select((box, i) => box == null ? 0 : box.Select(
        (item, j) => (i + 1) * (j + 1) * item.value).Sum()).Sum().ToString();
  }
}
