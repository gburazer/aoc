﻿using System;
using System.Collections.Generic;

namespace AoC._2023;

public class Day11(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value; var m = lines.Length; var n = lines[0].Length;
     var stars = new List<(int r, int c)>();
     var rowHasStar = new bool[m]; var colHasStar = new bool[n];
     for (var r = 0; r < m; r++)
        for (var c = 0; c < n; c++)
           if (lines[r][c] == '#')
           {
              stars.Add((r, c));
              rowHasStar[r] = colHasStar[c] = true;
           }
     int[,] calcGaps(int dim, bool[] hasStar)
     {
        var gaps = new int[dim, dim];
        for (var i = 0; i < dim - 1; i++)
        {
           var val = 0;
           for (var j = i + 1; j < dim; j++)
           {
              if (!hasStar[j])
                 val++;
              gaps[i, j] = gaps[j, i] = val;
           }
        }
        return gaps;
     }
     var rowGaps = calcGaps(m, rowHasStar); var colGaps = calcGaps(n, colHasStar);
     var p1 = 0L; var p2 = 0L;
     void accDist((int r, int c) s1, (int r, int c) s2)
     {
        var dist = Math.Abs(s1.c - s2.c) + Math.Abs(s1.r - s2.r);
        var totalGap = rowGaps[s1.r, s2.r] + colGaps[s1.c, s2.c];
        p1 += dist + totalGap;
        p2 += dist + totalGap * (1000000 - 1);
     }
     for (var i = 0; i < stars.Count - 1; i++)
        for (var j = i + 1; j < stars.Count; j++)
           accDist(stars[i], stars[j]);
     Part1 = p1.ToString(); Part2 = p2.ToString();
  }
}
