﻿using System;
using System.Globalization;
using System.Collections.Generic;

namespace AoC._2023;

public class Day18(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value;
     var dirs = new Dictionary<char, (int dx, int dy)>
     {
        {'U', ( 0, -1) },
        {'D', ( 0 , 1) },
        {'L', (-1,  0) },
        {'R', ( 1,  0) },
     };
     long calc(Func<string[], (char dir, int amt)> parse)
     {
        var res = 0L;
        var (x, y) = (0, 0);
        var xs = new SortedSet<int>(); var ys = new SortedSet<int>();
        foreach (var line in lines)
        {
           var (dir, amt) = parse(line.Split(' '));
           var (dx, dy) = dirs[dir];
           xs.Add(x); ys.Add(y);
           x += amt * dx; y += amt * dy;
        }
        int[] copyAndFix(SortedSet<int> set)
        {
           var res = new int[set.Count];
           set.CopyTo(res); res[^1]++;
           return res;
        }
        var sxs = copyAndFix(xs); var sys = copyAndFix(ys);
        bool isInside((int x, int y) p1, (int x, int y) p2)
        {
           /* TODO IMPL
              how many vertical or horizontal lines does line drawn from center of rectangle to one side cross?
              even number -> outside
              odd number -> inside
              does it need to be in center?
              handle off-by-one on edges
              what if it matches horizontal line? (nothing, only vertical are important)
              where does vertical begin (does intersection of vertical and horizontal count as both?)

              illustration:
#######      #######
#     #      #     #
#   ###      #     #
# X>#>>>>>>>>#>>>>>#>>>>
#   ##########     #####
#                      #
#       ################
#       #
####    #
#    #  
#    ####
#       #
###       #
#         #
###########
           */
           return true;
        }
        for (var i = 0; i < sxs.Length - 1; i++)
           for (var j = 0; j < sys.Length - 1; j++)
           {
              var (x1, y1) = (sxs[i], sys[j]); var (x2, y2) = (sxs[i + 1], sys[j + 1]);
              if (isInside((x1, y1), (x2, y2)))
                 res += ((long)x2 - x1) * ((long)y2 - y1);
           }
        return res;
     }

     Part1 = calc(parts =>
     (
        dir: parts[0][0],
        amt: int.Parse(parts[1])
     )).ToString();

     Part2 = calc(parts =>
     (
        dir: parts[2][7] switch
        {
           '0' => 'R',
           '1' => 'D',
           '2' => 'L',
           '3' => 'U',
           _ => throw new NotImplementedException()
        },
        amt: int.Parse(parts[2].Substring(2, 5), NumberStyles.HexNumber))
     ).ToString();
  }
}
