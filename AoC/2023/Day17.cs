﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2023;

public class Day17(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value; var m = lines.Length; var n = lines[0].Length;
     var val = new int[m * n];
     for (var pos = 0; pos < m * n; pos++)
        val[pos] = int.Parse(lines[pos / n][pos % n].ToString());
     const int DIR_UP = 0, DIR_DOWN = 1, DIR_LEFT = 2, DIR_RIGHT = 3, NDIR = 4;
     var dirs = new (int dr, int dc, int d)[NDIR][];
     dirs[DIR_UP  ] = dirs[DIR_DOWN ] = [( 0, -1, DIR_LEFT), (0, 1, DIR_RIGHT)];
     dirs[DIR_LEFT] = dirs[DIR_RIGHT] = [(-1,  0, DIR_UP  ), (1, 0, DIR_DOWN )];
     int calc(int min, int max)
     {
        var best = new int[m * n][];
        for (var pos = 0; pos < m * n; pos++)
           best[pos] = Enumerable.Repeat(int.MaxValue, NDIR).ToArray();
        best[0][DIR_UP] = best[0][DIR_LEFT] = 0;
        var q = new PriorityQueue<(int v, int pos, int d), int>();
        q.Enqueue((0, 0, DIR_UP), 0); q.Enqueue((0, 0, DIR_LEFT), 0);
        while (q.Count > 0)
        {
           var (v, pos, d) = q.Dequeue();
           foreach (var (dr, dc, nd) in dirs[d])
           {
              var rr = pos / n; var cc = pos % n; var nv = v;
              for (var i = 0; i < max; i++)
              {
                 rr += dr; if (rr < 0 || rr == m) break;
                 cc += dc; if (cc < 0 || cc == n) break;
                 var npos = rr * n + cc;
                 nv += val[npos];
                 if (i + 1 < min || nv >= best[npos][nd]) continue;
                 best[npos][nd] = nv;
                 q.Enqueue((nv, npos, nd), nv);
              }
           }
        }
        return best[(m - 1) * n + n - 1].Min();
     }
     Part1 = calc(1, 3).ToString();
     Part2 = calc(4, 10).ToString();
  }
}
