﻿using System;
using System.Collections.Generic;

namespace AoC._2023;

public class Day21(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value; var M = lines.Length; var N = lines[0].Length;
     var map = new bool[M, N]; (int r, int c) start = (-1, -1);
     for (var r = 0; r < M; r++)
        for (var c = 0; c < N; c++)
           if (lines[r][c] == 'S')
              start = (r, c);
           else if (lines[r][c] == '#')
              map[r, c] = true;

     long calc(int max)
     {
        var res = 1L; // start

        var visited = new HashSet<(int r, int c)>() { start };
        var q = new Queue<((int r, int c) pos, int cnt)>();
        long normalize(int val, int limit) => val > 0 ? val % limit : (1000000L * limit - (-val)) % limit;
        bool isRock(int r, int c) => map[normalize(r, M), normalize(c, N)];
        void tryEnqueue(int r, int c, int cnt)
        {
           if (cnt > max || isRock(r, c) || visited.Contains((r, c)))
              return;
           if (cnt % 2 == max % 2)
              res++;
           if (res % 10000000 == 0) Console.WriteLine($"{cnt} {res} {q.Count}");
           visited.Add((r, c));
           q.Enqueue(((r, c), cnt));
        }

        q.Enqueue((start, 0));
        while (q.Count > 0)
        {
           var (pos, cnt) = q.Dequeue();
           tryEnqueue(pos.r - 1, pos.c, cnt + 1);
           tryEnqueue(pos.r + 1, pos.c, cnt + 1);
           tryEnqueue(pos.r, pos.c - 1, cnt + 1);
           tryEnqueue(pos.r, pos.c + 1, cnt + 1);
        }

        // TODO IMPL
        // track "borders" only, so only spreading outwards
        // keeping track of every visted piece ever is not feasible
        // keeping track of every place feasible to visit may be better?

        return res;
     }

     Part1 = calc(64).ToString();
     //Part2 = calc(26501365).ToString();
  }
}
