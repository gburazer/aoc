﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Rule = (string condition, string outcome);
using Part = System.Collections.Generic.Dictionary<char, int>;

namespace AoC._2023;

public class Day19(string input) : AoC(input)
{
  public override void Solve()
  {
     var allRules = new Dictionary<string, List<Rule>>();
     void parseRule(string line)
     {
        var lineSplit = line.Split('{');
        var name = lineSplit[0];
        var rules = new List<Rule>();
        foreach (var ruleStr in lineSplit[1][..^1].Split(','))
        {
           var ruleSplit = ruleStr.Split(':');
           var isFinal = ruleSplit.Length == 1;
           rules.Add((
              isFinal ? string.Empty : ruleSplit[0],
              isFinal ? ruleSplit[0] : ruleSplit[1]));
        }
        allRules[name] = rules;
     }

     var parts = new List<Part>();
     void parsePart(string line)
     {
        var lineSplit = line[1..^1].Split(',');
        var res = new Part();
        foreach (var p in lineSplit.Select(p => p.Split('=')))
           res[p[0].Single()] = int.Parse(p[1]);
        parts.Add(res);
     }

     void parse()
     {
        var parseParts = false;
        foreach (var line in InputLines.Value)
           if (line == string.Empty)
              parseParts = true;
           else if (!parseParts)
              parseRule(line);
           else
              parsePart(line);
     }

     List<Rule> start() => allRules["in"];
     bool isAccepted(Part p)
     {
        (string, bool) eval(IEnumerable<Rule> rules)
        {
           (string, bool) result(string outcome) => outcome switch
           {
              "A" => (string.Empty, true),
              "R" => (string.Empty, false),
              _ => (outcome, false),
           };

           foreach (var (condition, outcome) in rules)
              if (condition == string.Empty)
                 return result(outcome);
              else
              {
                 var v1 = p[condition[0]]; var v2 = int.Parse(condition[2..]);
                 if ((condition[1] == '>' && v1 > v2) || (condition[1] == '<' && v1 < v2))
                    return result(outcome);
              }

           throw new InvalidOperationException();
        }

        string next;
        for (var cur = start(); ; cur = allRules[next])
        {
           (next, var res) = eval(cur);
           if (next == string.Empty)
              return res;
        }
     }

     long part2()
     {
        Rule opposite(Rule rule)
        {
           var oppositeCondition = new StringBuilder();
           oppositeCondition.Append(rule.condition[0]);
           oppositeCondition.Append(rule.condition[1] == '>' ? '<' : '>');
           var v = int.Parse(rule.condition[2..]);
           if (rule.condition[1] == '>')
              oppositeCondition.Append(v + 1);
           else
              oppositeCondition.Append(v - 1);
           return (oppositeCondition.ToString(), rule.outcome);
        }

        var rulePaths = new List<List<Rule>>();
        void appendRulePaths(List<Rule> rules, List<Rule> cur)
        {
           foreach (var (condition, outcome) in rules)
              if (condition == string.Empty)
                 if (outcome == "R")
                    return;
                 else if (outcome == "A")
                    rulePaths.Add(cur);
                 else
                    appendRulePaths(allRules[outcome], cur);
              else
              {
                 if (outcome != "R")
                 {
                    var curCopy = new List<Rule>(cur) { (condition, outcome) };
                    if (outcome == "A")
                       rulePaths.Add(curCopy);
                    else
                       appendRulePaths(allRules[outcome], curCopy);
                 }
                 cur.Add(opposite((condition, outcome)));
              }
        }

        appendRulePaths(start(), []);

        const int N = 4000;
        void intersect(bool[] arr, Rule rule)
        {
           var (oppositeCondition, _) = opposite(rule);
           var v = int.Parse(oppositeCondition[2..]);
           if (oppositeCondition[1] == '>')
              for (var i = v; i < N; i++)
                 arr[i] = false;
           else
              for (var i = 0; i < v - 1; i++)
                 arr[i] = false;
        }
        bool[] init() => Enumerable.Repeat(true, N).ToArray();
        long count(bool[] arr) => arr.Count(v => v);
        var res = 0L;
        foreach (var rp in rulePaths)
        {
           var xs = init(); var ms = init(); var @as = init(); var ss = init();
           foreach (var rule in rp)
              switch (rule.condition[0])
              {
                 case 'x': intersect(xs, rule); break;
                 case 'm': intersect(ms, rule); break;
                 case 'a': intersect(@as, rule); break;
                 case 's': intersect(ss, rule); break;
                 default: throw new InvalidOperationException();
              }
           res += count(xs) * count(ms) * count(@as) * count(ss);
        }
        return res;
     }

     parse();
     Part1 = parts.Where(isAccepted).Sum(p => p.Values.Sum()).ToString();
     Part2 = part2().ToString();
  }
}
