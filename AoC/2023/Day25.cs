﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2023;

public class Day25(string input) : AoC(input)
{
  public override void Solve()
  {
     var graph = new Dictionary<string, HashSet<string>>();
     void addSingle(string src, string dst)
     {
        if (!graph.ContainsKey(src)) graph[src] = new();
        graph[src].Add(dst);
     }
     void add(string src, string dst) { addSingle(src, dst); addSingle(dst, src); }
     void removeSingle(string src, string dst)
     {
        if (graph.ContainsKey(src))
           graph[src].Remove(dst);
        if (graph[src].Count == 0)
           graph.Remove(src);
     }
     void remove(string src, string dst) { removeSingle(src, dst); removeSingle(dst, src); }
     foreach (var line in InputLines.Value)
     {
        var parts = line.Split(": ");
        foreach (var part in parts[1].Split(' '))
           add(parts[0], part);
     }
     var allConnections = new HashSet<(string src, string dst)>();
     foreach (var key in graph.Keys)
        foreach (var value in graph[key])
           allConnections.Add((key, value));
     var finalConnections = new HashSet<(string src, string dst)>(allConnections);
     foreach (var (src, dst) in allConnections)
        if (finalConnections.Contains((dst, src)))
        {
           finalConnections.Remove((dst, src));
           finalConnections.Add((src, dst));
        }
     var N = graph.Keys.Count;
     bool isSplit(out int c1, out int c2)
     {
        c1 = 0; c2 = 0;
        var visited = new HashSet<string>();
        void visit(string node)
        {
           visited.Add(node);
           foreach (var adj in graph[node])
              if (!visited.Contains(adj))
              {
                 visit(adj);
                 if (visited.Count == N)
                    return;
              }
        }
        visit(graph.Keys.First());
        if (visited.Count < graph.Keys.Count)
        {
           c1 = visited.Count; c2 = graph.Keys.Count - c1;
           return true;
        }
        else
           return false;
     }
     var candidates = finalConnections.OrderBy(c => -graph[c.src].Count).ToArray();
     int calc()
     {
        for (var i = 0; i < candidates.Length - 2; i++)
        {
           remove(candidates[i].src, candidates[i].dst);
           for (var j = i + 1; j < candidates.Length - 1; j++)
           {
              Console.WriteLine($"{i} {j} / {candidates.Length}");
              remove(candidates[j].src, candidates[j].dst);
              for (var k = j + 1; k < candidates.Length; k++)
              {
                 remove(candidates[k].src, candidates[k].dst);
                 if (isSplit(out var c1, out var c2))
                    return c1 * c2;
                 add(candidates[k].src, candidates[k].dst);
              }
              add(candidates[j].src, candidates[j].dst);
           }
           add(candidates[i].src, candidates[i].dst);
        }
        return 0;
     }
     Part1 = calc().ToString();
     Part2 = NoSolution;
  }
}
