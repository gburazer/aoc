﻿using System;
using System.Linq;

namespace AoC._2023;

public class Day12(string input) : AoC(input)
{
  public override void Solve()
  {
     const char GOOD = '.', DAMAGED = '#', UNKNOWN = '?';
     long combine(string recordTemplate, string damagedTemplate, int repeat)
     {
        string join(char ch, string s, int n) => string.Join(ch, Enumerable.Repeat(s, n));
        var record = join(UNKNOWN, recordTemplate, repeat).ToCharArray();
        var n = record.Length;
        var damaged = join(',', damagedTemplate, repeat).Split(',').Select(int.Parse).ToArray();
        var res = 0L;
        var damagedFrom = new int[n]; var goodFrom = new int[n]; var unknownFrom = new int[n];
        for (var i = 0; i < n; i++)
           for (var pos = i; pos < n; pos++)
              switch (record[pos])
              {
                 case GOOD: goodFrom[i]++; break;
                 case DAMAGED: damagedFrom[i]++; break;
                 case UNKNOWN: unknownFrom[i]++; break;
              }
        void countAllStartingAt(int i, int ind, int len, int remaining)
        {
           if (i == n)
           {
              if ((len == 0 && ind == damaged.Length) || (ind == damaged.Length - 1 && damaged[ind] == len))
              {
                 res++;
                 //Console.WriteLine($"Valid: {new string(record)} {join(',', damagedTemplate, repeat)}");
              }
              return;
           }
           if (damagedFrom[i] > remaining) return;
           switch (record[i])
           {
              case GOOD:
                 if (len > 0)
                 {
                    if (damaged[ind] != len) return;
                    countAllStartingAt(i + 1, ind + 1, 0, remaining);
                 }
                 else
                    countAllStartingAt(i + 1, ind, len, remaining);
                 break;
              case DAMAGED:
                 if (ind == damaged.Length) return;
                 countAllStartingAt(i + 1, ind, len + 1, remaining - 1);
                 break;
              case UNKNOWN:
                 record[i] = DAMAGED; countAllStartingAt(i, ind, len, remaining);
                 record[i] = GOOD; countAllStartingAt(i, ind, len, remaining);
                 record[i] = UNKNOWN;
                 break;
           }
        }
        countAllStartingAt(0, 0, 0, damaged.Sum());
        return res;
     }

     //const int N = 5;
     var p1 = 0L; var p2 = 0L;
     var lines = InputLines.Value; var n = lines.Length;
     //for (var i = 0; i < n; i++)
     var res = Enumerable.Range(0, n).AsParallel().Select(i =>
     {
        var parts = lines[i].Split(' ');
        //Console.WriteLine($"{i + 1}/{n}: {lines[i]} - {parts[0].Count(ch => ch == UNKNOWN)} UNKNOWN .. ");
        //p1 += combine(parts[0], parts[1], 1);
        //p2 += combine(parts[0], parts[1], N);
        return (el1: combine(parts[0], parts[1], 1), el2: 0L/* TODO combine(parts[0], parts[1], N)*/);
        //Console.WriteLine($"{p1} {p2}");
     }).ToArray();
     foreach (var (el1, el2) in res)
     {
        p1 += el1; p2 += el2;
     }
     Part1 = p1.ToString(); Part2 = p2.ToString();
  }
}
