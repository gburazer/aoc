﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2023;

public class Day05(string input) : AoC(input)
{
  public override void Solve()
  {
     const int SRC = 0, DST = 1, LEN = 2;
     var lines = InputLines.Value;
     var ns = lines[0].Split(": ")[1].Split(' ').Select(long.Parse).ToArray();
     var allMappings = new List<List<long[]>>();
     for (var ind = 2; ind < lines.Length; ind++)
     {
        var mappings = new List<long[]>();
        ind++; // skip header
        while (ind < lines.Length && lines[ind] != string.Empty)
           mappings.Add(lines[ind++].Split(' ').Select(long.Parse).ToArray());
        allMappings.Add(mappings);
     }

     long map(long n, List<long[]> mappings)
     {
        foreach (var mapping in mappings)
           if (n >= mapping[DST] && n < mapping[DST] + mapping[LEN])
              return mapping[SRC] + (n - mapping[DST]);
        return n;
     }
     long mapAll(long s) => allMappings.Aggregate(s, map);

     var p2 = long.MaxValue;
     for (var i = 0; i < ns.Length / 2; i++)
     {
        var from = ns[2 * i]; var to = from + ns[2 * i + 1];
        for (var n = from; n < to; n++)
           p2 = Math.Min(p2, mapAll(n));
     }

     Part1 = ns.Min(mapAll).ToString(); Part2 = p2.ToString();
  }
}
