﻿using System.Collections.Generic;
using System.Linq;

namespace AoC._2023;

public class Day04(string input) : AoC(input)
{
  public override void Solve()
  {
     static List<int> parse(string l) => l.Split(" ").Where(s => s != string.Empty).Select(int.Parse).ToList();

     var p1 = 0;
     var lines = InputLines.Value; var n = lines.Length;
     var count = Enumerable.Repeat(1, n).ToArray();
     for (var i = 0; i < n; i++)
     {
        var numbers = lines[i].Split(": ")[1].Split(" | ");
        var winning = parse(numbers[0]); var mine = parse(numbers[1]);
        var matches = mine.Count(winning.Contains);
        for (var j = 0; j < matches; j++)
           count[i + 1 + j] += count[i];
        p1 += matches > 0 ? 1 << (matches - 1) : 0;
     }

     Part1 = p1.ToString(); Part2 = count.Sum().ToString();
  }
}
