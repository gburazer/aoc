﻿using System.Linq;

namespace AoC._2023;

public class Day23(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value; var m = lines.Length; var n = lines[0].Length;
     var isWall = Enumerable.Range(0, m * n).Select(i => lines[i / n][i % n] == '#').ToArray();
     const int START = 1; var end = (m - 1) * n + n - 2; // explicitly read from input
     int calc(bool useSlopes)
     {
        var best = new int[m * n]; var visited = new bool[m * n];
        void visit(int pos, int from, int cnt)
        {
           if (pos < 0 || pos >= m * n || isWall[pos] || visited[pos]) return;
           if (useSlopes)
           {
              var r = pos / n; var c = pos % n; var dr = r - from / n; var dc = c - from % n;
              if ((dr == -1 && lines[r][c] == 'v') || (dr == 1 && lines[r][c] == '^') ||
                  (dc == -1 && lines[r][c] == '>') || (dc == 1 && lines[r][c] == '<'))
                 return;
           }
           if (best[pos] < cnt) best[pos] = cnt;
           visited[pos] = true;
           visit(pos - n, pos, cnt + 1); visit(pos + n, pos, cnt + 1);
           visit(pos - 1, pos, cnt + 1); visit(pos + 1, pos, cnt + 1);
           visited[pos] = false;
        }
        visit(START, START - n, 0);
        return best[end];
     }
     Part1 = calc(useSlopes: true).ToString(); Part2 = calc(useSlopes: false).ToString();
  }
}
