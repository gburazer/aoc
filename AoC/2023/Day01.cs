﻿using System.Linq;

namespace AoC._2023;

public class Day01(string input) : AoC(input)
{
  public override void Solve()
  {
     var digits = new[] { "", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
     int digitOrZero(bool useLetters, string s, int ind) => char.IsDigit(s[ind]) ? s[ind] - '0'
        : useLetters
           ? Enumerable.Range(1, 9).FirstOrDefault(i =>
              ind + digits[i].Length <= s.Length && s.Substring(ind, digits[i].Length) == digits[i], default)
           : default;
     int firstDigit(bool useLetters, string s, int ind, int step)
     {
        for (var i = 0; ; i++)
        {
           var d = digitOrZero(useLetters, s, ind + i * step);
           if (d > 0)
              return d;
        }
     }
     int value(string line, bool useLetters) =>
        10 * firstDigit(useLetters, line, 0, 1) + firstDigit(useLetters, line, line.Length - 1, -1);
     string sumAll(bool useLetters = false) => InputLines.Value.Sum(l => value(l, useLetters)).ToString();
     Part1 = sumAll(); Part2 = sumAll(useLetters: true);
  }
}
