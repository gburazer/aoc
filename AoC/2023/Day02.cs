﻿using System;

namespace AoC._2023;

public class Day02(string input) : AoC(input)
{
  struct ColorValues { public int R, G, B; }

  public override void Solve()
  {
     var max = new ColorValues { R = 12, G = 13, B = 14 };
     var min = new ColorValues();
     var p1 = 0; var p2 = 0;
     var lines = InputLines.Value;
     for (var id = 1; id <= lines.Length; id++)
     {
        var isValid = true;
        min.R = min.G = min.B = 0;
        foreach (var round in lines[id - 1].Split(": ")[1].Split("' "))
           foreach (var count in round.Split(", "))
           {
              var parts = count.Split(' ');
              var n = int.Parse(parts[0]);
              switch (parts[1])
              {
                 case "red": if (n > max.R) isValid = false; min.R = Math.Max(min.R, n); break;
                 case "green": if (n > max.G) isValid = false; min.G = Math.Max(min.G, n); break;
                 case "blue": if (n > max.B) isValid = false; min.B = Math.Max(min.B, n); break;
              }
           }
        if (isValid) p1 += id;
        p2 += min.R * min.G * min.B;
     }
     Part1 = p1.ToString(); Part2 = p2.ToString();
  }
}
