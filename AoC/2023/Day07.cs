﻿using System;
using System.Linq;
using Cnt = System.Collections.Generic.Dictionary<char, int>;

namespace AoC._2023;

public class Day07(string input) : AoC(input)
{
    class Card
    {
        public Card(string hand, int bid)
        {
            Hand = hand; Bid = bid;

            _cnt = []; Init(_cnt, hand); _maxCnt = _cnt.Values.Max();
            _jCount = hand.Count(c => c == 'J');
            var cntNoJ = new Cnt(); Init(cntNoJ, hand.Replace("J", ""));
            if (cntNoJ.Values.Count > 0)
            {
                _maxCntNoJ = cntNoJ.Values.Max();
                _maxCntNoJCount = cntNoJ.Values.Count(c => c == _maxCnt);
            }
            else
                _maxCntNoJ = _maxCntNoJCount = 0;

            _type = CalcType(useJ: false);
            _typeJ = CalcType(useJ: true);
        }

        public readonly string Hand;
        public readonly int Bid;

        public int Type(bool useJ) => useJ ? _typeJ : _type;

        const int Fok = 7, Poker = 6, FullHouse = 5, Three = 4, TwoPair = 3, OnePair = 2, High = 1;

        int CalcType(bool useJ) => !useJ || _jCount == 0
            ? _maxCnt switch
            {
                5 => Fok,
                4 => Poker,
                3 => _cnt.Values.Any(v => v == 2) ? FullHouse : Three,
                2 => _cnt.Values.Count(v => v == 2) == 2 ? TwoPair : OnePair,
                _ => High
            }
            : _jCount == 5
                ? Fok
                : (_maxCntNoJ + _jCount) switch
                {
                    5 => Fok,
                    4 => Poker,
                    3 => _maxCntNoJ == 2 && _maxCntNoJCount == 2 ? FullHouse : Three,
                    _ => OnePair
                };

        readonly Cnt _cnt;
        readonly int _jCount, _maxCnt, _maxCntNoJ, _maxCntNoJCount, _type, _typeJ;

        static void Init(Cnt cnt, string hand)
        {
            foreach (var c in hand)
            {
                cnt.TryAdd(c, 0);
                cnt[c]++;
            }
        }
    }

    public override void Solve()
    {
        var lines = InputLines.Value; var n = lines.Length;
        var cards = new Card[n];
        for (var i = 0; i < n; i++)
        {
            var parts = lines[i].Split(' ');
            cards[i] = new(hand: parts[0], bid: int.Parse(parts[1]));
        }

        int cmp(Card a, Card b, bool useJ)
        {
            var t1 = a.Type(useJ); var t2 = b.Type(useJ);
            int cmpVal(string h1, string h2)
            {
                int val(char c) => c switch
                {
                    'A' => 14,
                    'K' => 13,
                    'Q' => 12,
                    'J' => useJ ? 1 : 11,
                    'T' => 10,
                    _ => c - '0'
                };
                for (var i = 0; i < h1.Length; i++)
                {
                    var v1 = val(h1[i]); var v2 = val(h2[i]);
                    if (v1 == v2) continue;
                    return v1 > v2 ? 1 : -1;
                }
                return 0;
            }
            if (t1 == t2) return cmpVal(a.Hand, b.Hand);
            return t1 > t2 ? 1 : -1;
        }

        string calc(bool useJ)
        {
            Array.Sort(cards, (a, b) => cmp(a, b, useJ));
            return Enumerable.Range(1, n).Aggregate(0, (res, rank) => res + rank * cards[rank - 1].Bid).ToString();
        }

        Part1 = calc(useJ: false); Part2 = calc(useJ: true);
    }
}
