﻿using System.Linq;

namespace AoC._2023;

public class Day08(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value;
     var dir = lines[0]; var ind = 0; void reset() { ind = 0; }
     var nodes = lines.Skip(2).ToDictionary(l => l[..3], l => (l: l.Substring(7, 3), r: l.Substring(12, 3)));
     char next() => dir[ind++ % dir.Length];
     string move(string node) => next() == 'L' ? nodes[node].l : nodes[node].r;

     var p1 = 0;
     for (var cur = "AAA"; cur != "ZZZ"; cur = move(cur), p1++) ;
     Part1 = p1.ToString();

     long gcd(long a, long b) => b == 0 ? a : gcd(b, a % b);
     long lcm(long[] ns) => ns.Aggregate((a, n) => a * n / gcd(a, n));
     Part2 = lcm(nodes.Keys.Where(n => n[2] == 'A').Select(start =>
     {
        var res = 0L; reset();
        for (var cur = start; cur[2] != 'Z'; cur = move(cur), res++) ;
        return res;
     }).ToArray()).ToString();
  }
}
