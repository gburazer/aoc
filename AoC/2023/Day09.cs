﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2023;

public class Day09(string input) : AoC(input)
{
  public override void Solve()
  {
     static int next(List<int> history)
     {
        var ss = new List<List<int>>(); var cur = history; var allZeros = false;
        while (!allZeros)
        {
           ss.Add(cur); var @new = new List<int>(); allZeros = true;
           for (var i = 0; i < cur.Count - 1; i++)
           {
              var sum = cur[i + 1] - cur[i]; @new.Add(sum);
              if (sum != 0) allZeros = false;
           }
           cur = @new;
        }
        for (var i = ss.Count - 2; i >= 0; i--)
           ss[i].Add(ss[i + 1][^1] + ss[i][^1]);
        return ss[0][^1];
     }
     var p1 = 0; var p2 = 0;
     foreach (var l in InputLines.Value)
     {
        var ns = l.Split(' ').Select(int.Parse);
        p1 += next(ns.ToList()); p2 += next(ns.Reverse().ToList());
     }
     Part1 = p1.ToString(); Part2 = p2.ToString();
  }
}
