﻿using System;
using System.Collections.Generic;

namespace AoC._2023;

public class Day16(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value; var m = lines.Length; var n = lines[0].Length;
     var visited = new HashSet<(int r, int c, int dr, int dc)>();
     var energized = new HashSet<(int r, int c)>();
     const char EMPTY = '.', MIRROR_NW = '/', MIRROR_NE = '\\', SPLITTER_WE = '-', SPLITTER_NS = '|';
     void beam(int r, int c, int dr, int dc)
     {
        while (r >= 0 && r < m && c >= 0 && c < n && !visited.Contains((r, c, dr, dc)))
        {
           visited.Add((r, c, dr, dc)); energized.Add((r, c));
           switch (lines[r][c])
           {
              case EMPTY:
                 break;
              case MIRROR_NW:
                 if (dr == -1) { dr = 0; dc = 1; }
                 else if (dr == 1) { dr = 0; dc = -1; }
                 else if (dc == -1) { dr = 1; dc = 0; }
                 else if (dc == 1) { dr = -1; dc = 0; }
                 break;
              case MIRROR_NE:
                 if (dr == -1) { dr = 0; dc = -1; }
                 else if (dr == 1) { dr = 0; dc = 1; }
                 else if (dc == -1) { dr = -1; dc = 0; }
                 else if (dc == 1) { dr = 1; dc = 0; }
                 break;
              case SPLITTER_WE:
                 if (dr != 0)
                 {
                    beam(r, c - 1, 0, -1);
                    beam(r, c + 1, 0, 1);
                    return;
                 }
                 break;
              case SPLITTER_NS:
                 if (dc != 0)
                 {
                    beam(r - 1, c, -1, 0);
                    beam(r + 1, c, 1, 0);
                    return;
                 }
                 break;
           }
           r += dr; c += dc;
        }
     }
     int part1()
     {
        beam(0, 0, 0, 1);
        return energized.Count;
     }
     int part2()
     {
        var res = 0;
        void init() { visited.Clear(); energized.Clear(); }
        void update() { res = Math.Max(res, energized.Count); }
        for (var r = 0; r < m; r++)
        {
           init(); beam(r, 0, 0, 1); update(); // -> right
           init(); beam(r, n - 1, 0, -1); update(); // -> left
        }
        for (var c = 0; c < n; c++)
        {
           init(); beam(0, c, 1, 0); update(); // -> down
           init(); beam(m - 1, c, -1, 0); update(); // -> up
        }
        return res;
     }
     Part1 = part1().ToString(); Part2 = part2().ToString();
  }
}
