﻿using System.Linq;

namespace AoC._2023;

public class Day06(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value;
     static int[] parse(string s) =>
        s.Split(':')[1].Split(' ').Where(s => s != string.Empty).Select(int.Parse).ToArray();
     var ts = parse(lines[0]); var ds = parse(lines[1]);
     static long winCount(long t, long d)
     {
        var min = 1L; 
        for (; min < t && min * (t - min) <= d; min++) 
           ;
        var max = t - 1; 
        for (; max > min && max * (t - max) <= d; max--) 
           ;
        return max - min + 1;
     }
     static long join(int[] ns) => long.Parse(string.Join("", ns));
     Part1 = Enumerable.Range(0, ts.Length).Aggregate(1L, (res, i) => res * winCount(ts[i], ds[i])).ToString();
     Part2 = winCount(join(ts), join(ds)).ToString();
  }
}
