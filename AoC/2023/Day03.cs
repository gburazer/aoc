﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2023;

public class Day03(string input) : AoC(input)
{
  public override void Solve()
  {
     const int N = 140;
     const char VOID = '.', GEAR = '*';
     // TODO REFACTORING TYPES
     var numByPos = new Dictionary<(int y, int x), ((int y, int x) pos, int n, int len)>();
     var isSymbol = new bool[N, N];
     var gears = new List<(int y, int x)>();

     // TODO PERF
     void parse()
     {
        var lines = InputLines.Value;
        for (var y = 0; y < lines.Length; y++)
        {
           var l = lines[y];
           for (var x = 0; x < l.Length; x++)
              if (char.IsDigit(l[x]))
              {
                 var n = 0; var len = 0;
                 do n = 10 * n + l[x + len++] - '0'; while (x + len < l.Length && char.IsDigit(l[x + len]));
                 for (var i = x; i < x + len; i++) numByPos[(y, i)] = ((y, x), n, len);
                 x += len - 1;
              }
              else if (l[x] != VOID)
              {
                 isSymbol[y, x] = true;
                 if (l[x] == GEAR) gears.Add((y, x));
              }
        }
     }

     bool checkAdjacent((int y, int x) p, int len, Func<int, int, bool> predicate)
     {
        var miny = Math.Max(0, p.y - 1); var maxy = Math.Min(N - 1, p.y + 1);
        var minx = Math.Max(0, p.x - 1); var maxx = Math.Min(N - 1, p.x + len);
        for (var y = miny; y <= maxy; y++)
           for (var x = minx; x <= maxx; x++)
              if (predicate(y, x))
                 return true;
        return false;
     }
     bool isPart((int y, int x) p, int len) => checkAdjacent(p, len, (y, x) => isSymbol[y, x]);
     int gearRatio((int y, int x) g)
     {
        var ns = new HashSet<((int y, int x) pos, int n, int len)>();
        // TODO REFACTORING DUPLICATION
        var miny = Math.Max(0, g.y - 1); var maxy = Math.Min(N - 1, g.y + 1);
        var minx = Math.Max(0, g.x - 1); var maxx = Math.Min(N - 1, g.x + 1);
        for (var y = miny; y <= maxy; y++)
           for (var x = minx; x <= maxx; x++)
              if (numByPos.ContainsKey((y, x)))
                 ns.Add(numByPos[(y, x)]);
        if (ns.Count != 2) return 0;
        var res = 1; foreach (var (_, n, _) in ns) res *= n; return res;
     }

     parse();
     Part1 = numByPos.Values.Distinct().Where(num => isPart(num.pos, num.len)).Sum(num => num.n).ToString();
     Part2 = gears.Sum(gearRatio).ToString();
  }
}
