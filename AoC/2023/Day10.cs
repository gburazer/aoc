﻿using System;
using System.Linq;
using System.Collections.Generic;

using Move = System.Func<int, int>;

namespace AoC._2023;

public class Day10(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value; var mm = lines.Length; var nn = lines[0].Length;
     int n(int p) => p - nn; int s(int p) => p + nn; int e(int p) => p + 1; int w(int p) => p - 1;
     int ne(int p) => n(e(p)); int nw(int p) => n(w(p)); int se(int p) => s(e(p)); int sw(int p) => s(w(p));
     const int NPOS = -1;
     const char NS = '|', WE = '-', NE = 'L', NW = 'J', SW = '7', SE = 'F';
     char ch(int p) => lines[p / nn][p % nn];
     int next(int p, int from) => ch(p) switch
     {
        NS => from == n(p) ? s(p) : from == s(p) ? n(p) : NPOS,
        WE => from == w(p) ? e(p) : from == e(p) ? w(p) : NPOS,
        NE => from == n(p) ? e(p) : from == e(p) ? n(p) : NPOS,
        NW => from == n(p) ? w(p) : from == w(p) ? n(p) : NPOS,
        SW => from == s(p) ? w(p) : from == w(p) ? s(p) : NPOS,
        SE => from == s(p) ? e(p) : from == e(p) ? s(p) : NPOS,
        _ => NPOS,
     };
     const char P = 'o', Q = '?', L = 'L', R = 'R';
     var cover = new char[mm * nn]; for (var p = 0; p < mm * nn; p++) cover[p] = Q;
     int findStart()
     {
        for (var p = 0; p < mm * nn; p++)
           if (lines[p / nn][p % nn] == 'S')
              return p;
        throw new InvalidOperationException();
     }
     var start = findStart();
     cover[start] = P;
     int findFirst()
     {
        for (var r = start / nn - 1; r <= start / nn + 1; r++)
           for (var c = start % nn - 1; c <= start % nn + 1; c++)
              if (r >= 0 && r < mm && c >= 0 && c < nn)
              {
                 var p = r * nn + c;
                 if (p != start && next(p, start) != NPOS)
                    return p;
              }
        throw new InvalidOperationException();
     }
     int len(int p, int from)
     {
        var res = 1;
        while (p != start)
        {
           cover[p] = P;
           var newFrom = p; p = next(p, from); from = newFrom;
           res++;
        }
        return res;
     }
     int part1()
     {
        var res = len(findFirst(), start);
        return res / 2 + res % 2;
     }
     Part1 = part1().ToString();

     void flood(char c, int pos)
     {
        var q = new Queue<int>();
        void visit(int p)
        {
           if (p / nn < 0 || p / nn >= mm || p % nn < 0 || cover[p] != Q) return;
           cover[p] = c; q.Enqueue(p);
        }
        visit(pos);
        while (q.Count > 0)
        {
           pos = q.Dequeue();
           visit(pos - nn); visit(pos + nn); visit(pos - 1); visit(pos + 1);
        }
     }
     void floodAll(int from)
     {
        // where I'm now (pos) compared to where I was (from)
        // for each of pipe chars that could have led here
        // where to try and flood left, where to try and flood right
        var dirMoves = new (Move dir, (char pipe, Move[] l, Move[] r)[] ps)[]
        {
           (n, [
              (NS, [w], [e]),
              (NE, [w, sw, s], [ne]),
              (NW, [nw], [s, se, e])
           ]),
           (s, [
              (NS, [e], [w]),
              (SW, [n, ne, e], [sw]),
              (SE, [se], [w, nw, n])
           ]),
           (w, [
              (WE, [s], [n]),
              (NW, [s, se, e], [nw]),
              (SW, [sw], [n, ne, e])
           ]),
           (e, [
              (WE, [n], [s]),
              (NE, [ne], [w, sw, s]),
              (SE, [w, nw, n], [se])
           ]),
        };
        for (var pos = findFirst(); pos != start; (from, pos) = (pos, next(pos, from)))
        {
           if (ch(from) == 'S') continue;
           var (_, ps) = dirMoves.Single(dm => dm.dir(from) == pos);
           var (_, leftMoves, rightMoves) = ps.Single(p => p.pipe == ch(from));
           foreach (var move in leftMoves) flood(L, move(from));
           foreach (var move in rightMoves) flood(R, move(from));
        }
     }

     floodAll(start);

     int count(char c) => Enumerable.Range(0, mm * nn).Count(p => cover[p] == c);
     int part2()
     {
        // inside = opposite color of first color found on the edge
        // wouldn't work if all edge would be covered with main pipe
        for (var r = 0; r < mm; r++)
        {
           if (cover[r * nn] == L || cover[r * nn + nn - 1] == L) return count(R);
           if (cover[r * nn] == R || cover[r * nn + nn - 1] == R) return count(L);
        }
        for (var c = 0; c < nn; c++)
        {
           if (cover[c] == L || cover[(mm - 1) * nn + c] == L) return count(R);
           if (cover[c] == R || cover[(mm - 1) * nn + c] == R) return count(L);
        }
        throw new InvalidOperationException();
     }
     Part2 = part2().ToString();
  }
}
