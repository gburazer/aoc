﻿using System;

namespace AoC._2022;

public class Day03(string input) : AoC(input)
{
  public override void Solve()
  {
     int val(char c) => (c >= 'a' ? c - 'a' : c - 'A' + 26) + 1;
     char intersect(ReadOnlySpan<char> s1, ReadOnlySpan<char> s2, ReadOnlySpan<char> s3)
     {
        foreach (var el in s1)
           if (s2.IndexOf(el) != -1 && (s3.IsEmpty || s3.IndexOf(el) != -1))
              return el;
        return default;
     }
     var p1 = 0; var p2 = 0; var lines = InputLines.Value;
     for (var i = 0; i < lines.Length; i++)
     {
        ref var l = ref lines[i]; var mid = l.Length / 2;
        p1 += val(intersect(l.AsSpan(0, mid), l.AsSpan(mid, mid), null));
        if (i % 3 == 0) p2 += val(intersect(l, lines[i + 1], lines[i + 2]));
     }
     Part1 = p1.ToString(); Part2 = p2.ToString();
  }
}
