﻿using System.Collections.Generic;

namespace AoC._2022;

public class Day13(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value; var items = new List<Item>();

     static Item parseItem(string s) { var pos = 0; return Item.Parse(s, ref pos); }
     var p1 = 0;
     for (var lind = 0; lind < lines.Length; lind += 3)
     {
        var first = parseItem(lines[lind]); var second = parseItem(lines[lind + 1]);
        if (first.CompareTo(second) < 0) p1 += lind / 3 + 1;
        items.Add(first); items.Add(second);
     }
     var d1 = parseItem("[[2]]"); items.Add(d1);
     var d2 = parseItem("[[6]]"); items.Add(d2);
     items.Sort((a, b) => a.CompareTo(b));
     var p2 = 1;
     for (var ind = 0; ind < items.Count; ind++)
        if (items[ind] == d1 || items[ind] == d2)
           p2 *= ind + 1;
     Part1 = p1.ToString(); Part2 = p2.ToString();
  }

  class Item
  {
     public static Item Parse(string s, ref int pos)
     {
        if (s[pos] == ',') pos++;
        var res = new Item();
        if (s[pos] == '[')
        {
           res._items = [];
           pos++; // '['
           while (s[pos] != ']') res._items.Add(Parse(s, ref pos));
           pos++; // ']'
        }
        else
        {
           static int val(char ch) => ch - '0';
           res._el = val(s[pos++]);
           if (char.IsDigit(s[pos])) { res._el = 10 * res._el + val(s[pos++]); }
        }
        return res;
     }
     public int CompareTo(Item other)
     {
        if (_items == null && other._items == null) return _el.CompareTo(other._el);
        var first = AsList(); var second = other.AsList();
        for (var i = 0; i < first.Count; i++)
           if (i >= second.Count) return 1;
           else
           {
              var res = first[i].CompareTo(second[i]);
              if (res != 0) return res;
           }
        return first.Count == second.Count ? 0 : -1;
     }

     List<Item> AsList() => _items ?? [..new[] { new Item { _el = _el } }];

     int _el;
     List<Item> _items;
  }
}
