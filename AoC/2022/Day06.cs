﻿namespace AoC._2022;

public class Day06(string input) : AoC(input)
{
  public override void Solve()
  {
     int calc(int start, int len)
     {
        for (var pos = start + len - 1; pos < Input.Length; pos++)
        {
           for (var i = 0; i < len; i++)
              for (var j = i + 1; j < len; j++)
                 if (Input[pos - i] == Input[pos - j])
                 {
                    pos = pos - j + len - 1;
                    goto _next;
                 }
           return pos + 1;
        _next:
           ;
        }
        return -1;
     }
     var p1 = calc(0, 4); Part1 = p1.ToString(); Part2 = calc(p1, 14).ToString();
  }
}
