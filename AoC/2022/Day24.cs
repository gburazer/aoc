﻿using System.Collections.Generic;

namespace AoC._2022;

public class Day24(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value;
     var m = lines.Length - 2; var n = lines[0].Length - 2;
     var left = new HashSet<int>[m]; var right = new HashSet<int>[m];
     for (var r = 0; r < m; r++) { left[r] = []; right[r] = []; }
     var up = new HashSet<int>[n]; var down = new HashSet<int>[n];
     for (var c = 0; c < n; c++) { up[c] = []; down[c] = []; }
     for (var r = 0; r < m; r++)
        for (var c = 0; c < n; c++)
           switch (lines[r + 1][c + 1])
           {
              case '^':    up[c].Add(r); break;
              case 'v':  down[c].Add(r); break;
              case '<':  left[r].Add(c); break;
              case '>': right[r].Add(c); break;
           }
     var q = new Queue<(int t, int r, int c)>(); var visited = new HashSet<(int t, int r, int c)>();
     void visit(int t, int r, int c)
     {
        if (visited.Contains((t, r, c))) return;
        q.Enqueue((t, r, c)); visited.Add((t, r, c));
     }
     var (startr, startc) = (-1, 0); var (targetr, targetc) = (m, n - 1);
     visit(0, startr, startc);
     void check(int t, int r, int c)
     {
        var isStart = r == startr && c == startc; var isTarget = r == targetr && c == targetc;
        if ((r < 0 || r >= m || c < 0 || c >= n) && !(isStart || isTarget)) return;
        if (!(isStart || isTarget))
        {
           if ( left[r].Contains(          (c + t) % n)) return;
           if (right[r].Contains((100 * n + c - t) % n)) return;
           if (   up[c].Contains(          (r + t) % m)) return;
           if ( down[c].Contains((100 * m + r - t) % m)) return;
        }
        visit(t, r, c);
     }
     var waypoints = new List<(int r, int c)> { (targetr, targetc), (startr, startc), (targetr, targetc) };
     var waypoint = 0; var p1 = 0; var p2 = 0;
     while (q.Count > 0)
     {
        var (t, r, c) = q.Dequeue();
        if (r == waypoints[waypoint].r && c == waypoints[waypoint].c)
        {
           if (waypoint == 0) p1 = t;
           q.Clear(); visited.Clear(); visit(t, waypoints[waypoint].r, waypoints[waypoint].c);
           if (++waypoint == waypoints.Count) { p2 = t; break; }
        }
        t++;
        check(t, r + 1, c); check(t, r - 1, c); check(t, r, c + 1); check(t, r, c - 1);
        check(t, r, c);
     }
     Part1 = p1.ToString(); Part2 = p2.ToString();
  }
}
