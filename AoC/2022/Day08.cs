﻿using System;

namespace AoC._2022;

public class Day08(string input) : AoC(input)
{
  public override void Solve()
  {
     var l = InputLines.Value;
     var m = l.Length; var n = l[0].Length;

     var counted = new bool[m, n];
     var p1 = 2 * (n + m) - 4; // perimeter
     void check(int r, int c, ref char max)
     {
        if (l[r][c] <= max) return;
        max = l[r][c];
        if (counted[r, c]) return;
        counted[r, c] = true; p1++;
     }
     for (var row = 1; row < m - 1; row++)
     {
        var maxl = l[row][0]; var maxr = l[row][n - 1];
        for (var d = 0; d < n - 2; d++)
        {
           check(row, 1 + d, ref maxl);
           check(row, n - 1 - d, ref maxr);
        }
     }
     for (var col = 1; col < n - 1; col++)
     {
        var maxu = l[0][col]; var maxd = l[m - 1][col];
        for (var d = 0; d < m - 2; d++)
        {
           check(1 + d, col, ref maxu);
           check(m - 1 - d, col, ref maxd);
        }
     }

     int s(int r, int c, int dr, int dc)
     {
        var res = 0; var pivot = l[r][c];
        do { res++; r += dr; c += dc; }
        while (r > 0 && r < n - 1 && c > 0 && c < m - 1 && l[r][c] < pivot);
        return res;
     }
     var p2 = 0;
     for (var r = 1; r < m - 1; r++)
        for (var c = 1; c < n - 1; c++)
           p2 = Math.Max(p2, s(r, c, 0, -1) * s(r, c, 0, 1) * s(r, c, -1, 0) * s(r, c, 1, 0));

     Part1 = p1.ToString(); Part2 = p2.ToString();
  }
}
