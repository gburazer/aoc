﻿namespace AoC._2022;

public class Day02(string input) : AoC(input)
{
  public override void Solve()
  {
     int val(char c, char @ref) => c - @ref + 1;
     const int
        ROCK = 1, PAPER = 2, SCISSORS = 3,
        LOSS = 0, DRAW = 3, WIN = 6;
     var beats = new[] { 0, PAPER, SCISSORS, ROCK };
     var isBeatenBy = new[] { 0, SCISSORS, ROCK, PAPER };
     int outcome(int v1, int v2) => v2 == v1 ? DRAW : v2 == beats[v1] ? WIN : LOSS;
     var p1 = 0; var p2 = 0;
     foreach (var l in InputLines.Value)
     {
        var v1 = val(l[0], 'A'); var c2 = l[2]; var v2 = val(c2, 'X');
        p1 += v2 + outcome(v1, v2);
        p2 += c2 switch
        {
           'X' => LOSS + isBeatenBy[v1],
           'Y' => DRAW + v1,
           _ => WIN + beats[v1]
        };
     }
     Part1 = p1.ToString(); Part2 = p2.ToString();
  }
}
