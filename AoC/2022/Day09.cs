﻿using System;
using System.Collections.Generic;

namespace AoC._2022;

public class Day09(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value;
     var ms = new (int x, int y)[lines.Length * 100]; var nms = 0;
     foreach (var l in lines)
     {
        var dir = l[0]; var amt = int.Parse(l[2..]);
        for (var i = 0; i < amt; i++, nms++)
           ms[nms] = (dir switch { 'L' => -1, 'R' => 1, _ => 0 }, dir switch { 'U' => -1, 'D' => 1, _ => 0 });
     }

     const int N = 10; var rope = new int[N, 2];
     void move(int r, (int x, int y) m) { rope[r, 0] += m.x; rope[r, 1] += m.y; }
     bool isAdj(int r) => Math.Abs(rope[r, 0] - rope[r - 1, 0]) <= 1 && Math.Abs(rope[r, 1] - rope[r - 1, 1]) <= 1;
     int step(int r, int dim) => rope[r, dim] < rope[r - 1, dim] ? 1 : -1;
     (int, int) calcMove(int r)
     {
        if (rope[r, 0] == rope[r - 1, 0] && Math.Abs(rope[r, 1] - rope[r - 1, 1]) == 2) return (0, step(r, 1));
        if (rope[r, 1] == rope[r - 1, 1] && Math.Abs(rope[r, 0] - rope[r - 1, 0]) == 2) return (step(r, 0), 0);
        return (step(r, 0), step(r, 1));
     }
     var v1 = new HashSet<int>(); var v2 = new HashSet<int>();
     const int MAX = 1000; int val(int r) => (rope[r, 1] + MAX) * MAX / 2 + rope[r, 0] + MAX;
     for (var mind = 0; mind < nms; mind++)
     {
        move(0, ms[mind]);
        for (var r = 1; r < N && !isAdj(r); r++)
           move(r, calcMove(r));
        v1.Add(val(1)); v2.Add(val(N - 1));
     }

     string sol(HashSet<int> v) => v.Count.ToString();
     Part1 = sol(v1); Part2 = sol(v2);
  }
}
