﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2022;

public class Day11(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value; var lind = 0; var mcount = 0;
     const int MAX = 10;
     var items = new Queue<long>[MAX, 2];
     var op = new (bool add, int arg)[MAX];
     var test = new int[MAX]; var eqClass = 1;
     var ifTrue = new int[MAX]; var ifFalse = new int[MAX];
     int lastInt() => int.Parse(lines[lind++].Split(' ').Last());
     while (lind++ < lines.Length)
     {
        var content = lines[lind++].Split(": ")[1].Split(", ").Select(long.Parse).ToList();
        for (var i = 0; i < 2; i++) items[mcount, i] = new Queue<long>(content);
        var opparts = lines[lind++].Split(" = ")[1].Split(' ');
        op[mcount] = (opparts[1][0] == '+', opparts[2] == "old" ? 0 : int.Parse(opparts[2]));
        test[mcount] = lastInt(); eqClass *= test[mcount];
        ifTrue[mcount] = lastInt(); ifFalse[mcount] = lastInt();
        lind++; mcount++;
     }
     long arg(long n, int m) => op[m].arg == 0 ? n : op[m].arg;
     long doOp(long n, int m) => op[m].add ? n + arg(n, m) : n * arg(n, m);
     long solve(int p, int rcount)
     {
        var icount = new int[mcount];
        for (var r = 1; r <= rcount; r++)
           for (var m = 0; m < mcount; m++)
           {
              icount[m] += items[m, p].Count;
              while (items[m, p].Count > 0)
              {
                 var item = doOp(items[m, p].Dequeue(), m);
                 if (p == 0)
                    item /= 3;
                 else
                    item %= eqClass;
                 items[item % test[m] == 0 ? ifTrue[m] : ifFalse[m], p].Enqueue(item);
              }
           }
        return icount.OrderDescending().Take(2).Aggregate(1L, (res, el) => res * el);
     }
     Part1 = solve(0, 20).ToString(); Part2 = solve(1, 10000).ToString();
  }
}
