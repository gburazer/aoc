﻿using System.Collections.Generic;

namespace AoC._2022;

public class Day01(string input) : AoC(input)
{
  public override void Solve()
  {
     var v = new List<int>(); var cur = 0;
     foreach (var l in InputLines.Value)
        if (l == string.Empty) { v.Add(cur); cur = 0; }
        else cur += int.Parse(l);
     v.Add(cur); v.Sort((a, b) => b.CompareTo(a));
     Part1 = $"{v[0]}";
     Part2 = $"{v[0] + v[1] + v[2]}";
  }
}
