﻿using System;
using System.Collections.Generic;

namespace AoC._2022;

public class Day17(string input) : AoC(input)
{
  public override void Solve()
  {
     var jind = 0; var jet = Input;
     var rind = 0; var rocks = new[]
     {
        new [] { "....", "....", "....", "####" },
        new [] { "....", ".#..", "###.", ".#.." },
        new [] { "....", "..#.", "..#.", "###." },
        new [] { "#...", "#...", "#...", "#..." },
        new [] { "....", "....", "##..", "##.." },
     };
     var h = new[] { 1, 3, 3, 4, 2 };
     const char EMPTY = '.', ROCK = '#';
     const int MAXY = 10000, X = 7, D = 4;
     var playground = new char[MAXY, X];
     for (var y = 0; y < MAXY; y++)
        for (var x = 0; x < X; x++)
           playground[y, x] = EMPTY;
     var maxy = 0;
     bool test(int x, int y)
     {
        for (var dy = 0; dy < D; dy++)
           for (var dx = 0; dx < D; dx++)
           {
              var tx = x + dx; var ty = y + dy;
              var t = rocks[rind][D - dy - 1][dx];
              if (tx < 0 || (tx >= X && t == ROCK) || ty < 0) return false; // out of bounds
              if (t == ROCK && playground[ty, tx] == ROCK) return false; // clashing
           }
        return true;
     }
     void place(int x, int y)
     {
        for (var dy = 0; dy < D; dy++)
           for (var dx = 0; dx < D; dx++)
              if (rocks[rind][D - dy - 1][dx] == ROCK)
                 playground[y + dy, x + dx] = ROCK;
     }
     var history = new Dictionary<(int jind, int rind), List<(int i, int maxy)>>();
     const int TARGET1 = 2022; const long TARGET2 = 1000000000000L;
     var p1 = 0; var p2 = 0L;
     var limit = 10000; // set by observation, will be enough
     for (var i = 0; i <= limit; i++, rind = (rind + 1) % rocks.Length)
     {
        if (i == TARGET1) p1 = maxy;
        if (!history.ContainsKey((jind, rind))) history[(jind, rind)] = [];
        var hlist = history[(jind, rind)]; hlist.Add((i, maxy));
        if (hlist.Count > 2) // check for period
        {
           var cur = hlist.Count - 1; var prev = hlist.Count - 2; var prevprev = hlist.Count - 3;
           var (dy, di) = (hlist[cur].maxy - hlist[prev].maxy, hlist[cur].i - hlist[prev].i);
           if (hlist[prev].maxy - hlist[prevprev].maxy == dy) // bingo
           {
              p2 = maxy + dy * ((TARGET2 - i) / di);                    // zoom in on p2
              limit = Math.Max(TARGET1, i + (int)((TARGET2 - i) % di)); // if mod > 0 we still need to count some
           }
        }
        var (x, y) = (2, maxy + 3);
        while (true)
        {
           var dx = jet[jind++] == '>' ? 1 : -1; jind %= jet.Length;
           if (test(x + dx, y)) x += dx;
           if (test(x, y - 1)) y--;
           else
           {
              place(x, y);
              var newy = y + h[rind]; if (newy > maxy) maxy = newy;
              break;
           }
        }
     }

     Part1 = p1.ToString(); Part2 = p2.ToString();
  }
}
