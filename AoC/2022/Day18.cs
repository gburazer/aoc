﻿using System.Collections.Generic;

namespace AoC._2022;

public class Day18(string input) : AoC(input)
{
  public override void Solve()
  {
     const int MAX = 20; var cubelets = new bool[MAX, MAX, MAX];
     var lines = InputLines.Value;
     foreach (var l in lines)
     {
        var parts = l.Split(',');
        cubelets[int.Parse(parts[0]), int.Parse(parts[1]), int.Parse(parts[2])] = true;
     }

     int surfaceArea(bool[,,] set)
     {
        var res = 0;
        for (var x = 0; x < MAX; x++)
           for (var y = 0; y < MAX; y++)
              for (var z = 0; z < MAX; z++)
                 if (set[x, y, z])
                    for (var d = -1; d <= 1; d++)
                       if (d != 0)
                       {
                          if (x + d < 0 || x + d == MAX || !set[x + d, y, z]) res++;
                          if (y + d < 0 || y + d == MAX || !set[x, y + d, z]) res++;
                          if (z + d < 0 || z + d == MAX || !set[x, y, z + d]) res++;
                       }
        return res;
     }
     var p1 = surfaceArea(cubelets);

     var q = new Queue<(int x, int y, int z)>();
     void visit(int x, int y, int z) { cubelets[x, y, z] = true; q.Enqueue((x, y, z)); }
     for (var i = 0; i < MAX; i++)
     {
        visit(i, 0, 0); visit(0, i, 0); visit(0, 0, i);
        visit(i, MAX - 1, MAX - 1); visit(MAX - 1, i, MAX - 1); visit(MAX - 1, MAX - 1, i);
        visit(i, MAX - 1, 0); visit(MAX - 1, i, 0); visit(MAX - 1, 0, i);
        visit(i, 0, MAX - 1); visit(0, i, MAX - 1); visit(0, MAX - 1, i);
     }
     while (q.Count > 0)
     {
        var (x, y, z) = q.Dequeue();
        for (var d = -1; d <= 1; d++)
           if (d != 0)
           {
              var nx = x + d; var ny = y + d; var nz = z + d;
              if (nx is >= 0 and < MAX && !cubelets[nx, y, z]) visit(nx, y, z);
              if (ny is >= 0 and < MAX && !cubelets[x, ny, z]) visit(x, ny, z);
              if (nz is >= 0 and < MAX && !cubelets[x, y, nz]) visit(x, y, nz);
           }
     }
     var air = new bool[MAX, MAX, MAX];
     for (var x = 0; x < MAX; x++)
        for (var y = 0; y < MAX; y++)
           for (var z = 0; z < MAX; z++)
              if (!cubelets[x, y, z])
                 air[x, y, z] = true;

     Part1 = p1.ToString(); Part2 = (p1 - surfaceArea(air)).ToString();
  }
}
