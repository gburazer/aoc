﻿using System.Collections.Generic;

namespace AoC._2022;

public class Day07(string input) : AoC(input)
{
  public override void Solve()
  {
     var root = new Dir(); var cur = root;
     Dir cd(string name) => name switch
     {
        "/" => root,
        ".." => cur.Parent,
        _ => cur.Dirs[name]
     };
     foreach (var l in InputLines.Value)
     {
        var parts = l.Split(' ');
        if (parts[1] == "cd") cur = cd(parts[2]);
        else if (int.TryParse(parts[0], out var val)) cur.Add(val);
        else if (parts[0] == "dir") cur.Add(parts[1]);
     }

     const long THRESHOLD = 100000, TOTAL_AVAILABLE = 70000000L, TOTAL_NEEDED = 30000000L;
     var toFree = TOTAL_NEEDED - (TOTAL_AVAILABLE - root.TotalSize);
     var p1 = 0L; var p2 = TOTAL_NEEDED;
     void calc(Dir d)
     {
        if (d.TotalSize < THRESHOLD) p1 += d.TotalSize;
        if (d.TotalSize < p2 && d.TotalSize >= toFree) p2 = d.TotalSize;
        foreach (var c in d.Dirs.Values) calc(c);
     }
     calc(root);

     Part1 = p1.ToString(); Part2 = p2.ToString();
  }

  public class Dir(Dir parent = null)
  {
     public void Add(int fSize)
     {
        for (var cur = this; cur != null; cur = cur.Parent)
           cur.TotalSize += fSize;
     }
     public void Add(string name) { Dirs[name] = new Dir(this); }

     public readonly Dir Parent = parent;
     public readonly Dictionary<string, Dir> Dirs = [];
     public int TotalSize;
  }
}
