﻿using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace AoC._2022;

public class Day05(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value;
     var stacksLimit = 0;
     for (; lines[stacksLimit] != string.Empty; stacksLimit++)
        ;
     var stackCount = int.Parse(lines[stacksLimit - 1].Trim().Split(' ').Last());
     Stack<char>[] createStacks()
     {
        var res = new Stack<char>[stackCount];
        for (var i = 0; i < stackCount; i++)
           res[i] = new();
        return res;
     }
     var s1 = createStacks(); var s2 = createStacks();

     for (var i = stacksLimit - 2; i >= 0; i--)
        for (var j = 0; j < stackCount; j++)
        {
           var c = lines[i][j * 4 + 1];
           if (c != ' ') { s1[j].Push(c); s2[j].Push(c); }
        }

     var chars = new char[stackCount * stackCount];
     for (var i = stacksLimit + 1; i < lines.Length; i++)
     {
        var parts = lines[i].Split(' ');
        var amt = int.Parse(parts[1]); var from = int.Parse(parts[3]); var to = int.Parse(parts[5]);

        // p1
        for (var j = 0; j < amt; j++) s1[to - 1].Push(s1[from - 1].Pop());

        // p2
        for (var j = 0; j < amt; j++) chars[j] = s2[from - 1].Pop();
        for (var j = amt - 1; j >= 0; j--) s2[to - 1].Push(chars[j]);
     }

     string sol(Stack<char>[] ss)
     {
        var sb = new StringBuilder();
        foreach (var s in ss) if (s.Count > 0) sb.Append(s.Pop());
        return sb.ToString();
     }
     Part1 = sol(s1); Part2 = sol(s2);
  }
}
