﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2022;

public class Day16(string input) : AoC(input)
{
  public override void Solve()
  {
     // TODO PERF work with arrays and indices not dictionaries and strings
     // TODO PERF use p1 in p2 (divide & conquer)
     var directs = new Dictionary<string, List<string>>();
     var flow = new Dictionary<string, int>();
     foreach (var parts in InputLines.Value.Select(l => l.Split(" to ")))
     {
        var parts1 = parts[0].Split(' ');
        var valve = parts1[1];
        directs[valve] = []; flow[valve] = int.Parse(parts1[4][5..^1]);
        foreach (var target in string.Join(' ', parts[1].Split(' ').Skip(1)).Split(", "))
           directs[valve].Add(target);
     }
     var valves = directs.Keys.ToList();
     var nodes = valves.Where(v => flow[v] > 0).ToList(); // only valves with flow are interesting

     // calc distances between nodes (target graph), using Floyd algo
     const int INFINITY = int.MaxValue / 3;
     var ds = new Dictionary<(string, string), int>();
     for (var from = 0; from < valves.Count; from++)
        for (var to = 0; to < valves.Count; to++)
           ds[(valves[from], valves[to])] = from == to ? 0
              : directs[valves[from]].Contains(valves[to]) ? 1 : INFINITY;
     foreach (var via in valves)
        foreach (var from in valves)
           foreach (var to in valves)
           {
              var test = ds[(from, via)] + ds[(via, to)];
              if (test < ds[(from, to)]) ds[(from, to)] = test;
           }

     // p1, BFS suffices to find optimal node traversal
     const string START = "AA";
     const int MAXT1 = 30;
     var q = new Queue<(int t, string pos, HashSet<string> closed, int total)>();
     foreach (var node in nodes)
        if (ds[(START, node)] < INFINITY)
           q.Enqueue((ds[(START, node)], node, [..nodes], 0));

     var p1 = 0;
     while (q.Count > 0)
     {
        var (t, pos, closed, total) = q.Dequeue();
        if (t > MAXT1) continue;
        closed.Remove(pos); t++;
        total += flow[pos] * (MAXT1 - t);
        if (total > p1) p1 = total;
        foreach (var to in closed)
           if (ds[(pos, to)] < INFINITY)
              q.Enqueue((t + ds[(pos, to)], to, [..closed], total));
     }

     // p2, same BFS but with more complex state .. improve by pre-partitioning to give 1/2 to each and use p1?
     const int MAXT2 = 26;
     var q2 = new Queue<(int[] ts, string[] ps, HashSet<string> closed, int total)>();
     for (var i = 0; i < nodes.Count - 1; i++)
        for (var j = i + 1; j < nodes.Count; j++)
           if (ds[(START, nodes[i])] < INFINITY && ds[(START, nodes[j])] < INFINITY)
              q2.Enqueue((
                 [ds[(START, nodes[i])], ds[(START, nodes[j])]],
                 [nodes[i], nodes[j]],
                 [..nodes],
                 0));
     var p2 = 0;
     while (q2.Count > 0)
     {
        var (ts, ps, closed, total) = q2.Dequeue();
        for (var i = 0; i < 2; i++)
        {
           closed.Remove(ps[i]); ts[i]++;
           total += flow[ps[i]] * (MAXT2 - ts[i]);
        }
        if (total > p2) p2 = total;
        var l = closed.ToList();
        for (var i = 0; i < l.Count; i++)
           for (var j = 0; j < l.Count; j++)
              if (i != j)
              {
                 var test0 = ts[0] + ds[(ps[0], l[i])]; if (test0 >= MAXT2) continue;
                 var test1 = ts[1] + ds[(ps[1], l[j])]; if (test1 >= MAXT2) continue;
                 q2.Enqueue(([test0, test1], [l[i], l[j]], [..closed], total));
              }
     }

     Part1 = p1.ToString(); Part2 = p2.ToString();
  }
}
