﻿using System;
using System.Linq;
using System.Text;

namespace AoC._2022;

public class Day25(string input) : AoC(input)
{
  public override void Solve()
  {
     long toDecimal(string snafu)
     {
        long val(char c) => c switch
        {
           '2' => 2L,
           '1' => 1L,
           '0' => 0L,
           '-' => -1L,
           '=' => -2L,
           _ => throw new InvalidOperationException()
        };
        var res = 0L; var unit = 1L;
        for (var i = snafu.Length - 1; i >= 0; i--, unit *= 5)
           res += unit * val(snafu[i]);
        return res;
     }
     const int MAX = 24;
     var powers = new long[MAX]; powers[0] = 1; for (var i = 1; i < MAX; i++) powers[i] = 5L * powers[i - 1];
     string toSnafu(long n)
     {
        var res = new StringBuilder();
        var pow = 0; while (2 * powers[pow] < n) pow++;
        long val(char digit) => (digit - '0') * powers[pow];
        for (; pow >= 0; pow--)
        {
           var digit = powers[pow] >= Math.Abs(n) ? '1' : '2';
           if (n > 0) { res.Append(digit); n -= val(digit); }
           else if (n < 0) { res.Append(digit == '1' ? '-' : '='); n += val(digit); }
           else res.Append('0');
        }
        return res.ToString();
     }
     Part1 = toSnafu(InputLines.Value.Sum(toDecimal)); Part2 = NoSolution;
  }
}
