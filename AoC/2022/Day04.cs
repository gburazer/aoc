﻿namespace AoC._2022;

public class Day04(string input) : AoC(input)
{
  public override void Solve()
  {
     var p1 = 0; var p2 = 0;
     foreach (var l in InputLines.Value)
     {
        var parts = l.Split(',', '-');
        var min1 = int.Parse(parts[0]); var max1 = int.Parse(parts[1]);
        var min2 = int.Parse(parts[2]); var max2 = int.Parse(parts[3]);
        if ((min1 <= min2 && max1 >= max2) || (min2 <= min1 && max2 >= max1)) p1++;
        if ((min1 <= min2 && max1 >= min2) || (min2 <= min1 && max2 >= min1)) p2++;
     }
     Part1 = p1.ToString(); Part2 = p2.ToString();
  }
}
