﻿using System;
using System.Linq;

namespace AoC._2022;

public class Day14(string input) : AoC(input)
{
  public override void Solve()
  {
     var (minx, maxx) = (int.MaxValue, 0); var (miny, maxy) = (minx, maxx);
     const char EMPTY = '\0', WALL = '#', SAND = 'o';
     const int MAXX = 700, MAXY = 175; var field = new char[MAXY, MAXX];
     foreach (var l in InputLines.Value)
     {
        var (prevx, prevy) = (-1, -1);
        foreach (var (x, y) in l.Split(" -> ").Select(point => point.Split(','))
           .Select(cs => (int.Parse(cs[0]), int.Parse(cs[1]))))
        {
           if (x == prevx)
              for (var i = 0; i <= Math.Abs(prevy - y); i++)
                 field[Math.Min(prevy, y) + i, x] = WALL;
           else if (y == prevy)
              for (var i = 0; i <= Math.Abs(prevx - x); i++)
                 field[y, Math.Min(prevx, x) + i] = WALL;
           prevx = x; prevy = y;
           if (x < minx) minx = x; if (x > maxx) maxx = x;
           if (y < miny) miny = y; if (y > maxy) maxy = y;
        }
     }
     bool sandSettled(int y, int x)
     {
        while (true)
           if (y == maxy + 3 || field[y, x] == SAND) return false;
           else if (field[y + 1, x] == EMPTY) y++;
           else if (field[y + 1, x - 1] == EMPTY) { y++; x--; }
           else if (field[y + 1, x + 1] == EMPTY) { y++; x++; }
           else { field[y, x] = SAND; return true; }
     }
     const int STARTX = 500, STARTY = 0;
     var p1 = 0; while (sandSettled(STARTY, STARTX)) p1++;
     for (var i = 0; i < MAXX; i++) field[maxy + 2, i] = WALL;
     var p2 = p1; while (sandSettled(STARTY, STARTX)) p2++;
     Part1 = p1.ToString(); Part2 = p2.ToString();
  }
}
