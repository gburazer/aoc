﻿using System.Collections.Generic;

namespace AoC._2022;

public class Day23(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value;
     var cur = new HashSet<(int r, int c)>();
     for (var r = 0; r < lines.Length; r++)
        for (var c = 0; c < lines[r].Length; c++)
           if (lines[r][c] == '#')
              cur.Add((r, c));
     (bool n, bool s, bool w, bool e) check(int r, int c) =>
        (cur.Contains((r - 1, c - 1)) || cur.Contains((r - 1, c)) || cur.Contains((r - 1, c + 1)),
        cur.Contains((r + 1, c - 1)) || cur.Contains((r + 1, c)) || cur.Contains((r + 1, c + 1)),
        cur.Contains((r - 1, c - 1)) || cur.Contains((r, c - 1)) || cur.Contains((r + 1, c - 1)),
        cur.Contains((r - 1, c + 1)) || cur.Contains((r, c + 1)) || cur.Contains((r + 1, c + 1)));
     const int N = 0, S = 1, W = 2, E = 3; var dir = 0;
     int propdir(bool n, bool s, bool w, bool e)
     {
        for (var test = 0; test < 4; test++)
        {
           var tdir = (dir + test) % 4;
           if ((tdir == N && !n) || (tdir == S && !s) || (tdir == W && !w) || (tdir == E && !e))
              return tdir;
        }
        return -1;
     }
     var round = 1; var p1 = 0;
     var stay = new HashSet<(int r, int c)>(); var prop = new Dictionary<(int r, int c), List<(int r, int c)>>();
     for (; ; round++, dir++)
     {
        stay.Clear(); prop.Clear();
        foreach (var (r, c) in cur)
        {
           var (n, s, w, e) = check(r, c);
           if (!(n || s || w || e))
              stay.Add((r, c));
           else
           {
              var (propr, propc) = propdir(n, s, w, e) switch
              {
                 N => (r - 1, c),
                 S => (r + 1, c),
                 W => (r, c - 1),
                 E => (r, c + 1),
                 _ => (r, c)
              };
              if ((propr, propc) == (r, c))
                 stay.Add((r, c));
              else
              {
                 if (!prop.ContainsKey((propr, propc)))
                    prop[(propr, propc)] = [];
                 prop[(propr, propc)].Add((r, c));
              }
           }
        }
        if (stay.Count == cur.Count)
           break;
        cur = [..stay];
        foreach (var p in prop)
           if (p.Value.Count == 1)
              cur.Add(p.Key);
           else
              foreach (var elf in p.Value)
                 cur.Add(elf);
        if (round != 10) continue;
        var (minr, minc) = (int.MaxValue, int.MaxValue); var (maxr, maxc) = (int.MinValue, int.MinValue);
        foreach (var (r, c) in cur)
        {
           if (r < minr) minr = r; if (r > maxr) maxr = r;
           if (c < minc) minc = c; if (c > maxc) maxc = c;
        }
        p1 = (maxr - minr + 1) * (maxc - minc + 1) - cur.Count;
     }
     Part1 = p1.ToString(); Part2 = round.ToString();
  }
}
