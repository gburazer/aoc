﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2022;

public class Day12(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value; var m = lines.Length; var n = lines[0].Length;
     var field = new int[m, n];
     (int r, int c) start = (-1, -1), end = (-1, -1);
     var @as = new List<(int r, int c)>();
     var dist = new int[m, n];
     const int BOTTOM = 'z' - 'a';
     for (var r = 0; r < m; r++)
        for (var c = 0; c < n; c++)
        {
           dist[r, c] = int.MaxValue;
           if (lines[r][c] == 'S') { field[r, c] = BOTTOM; start = (r, c); @as.Add(start); }
           else if (lines[r][c] == 'E') { field[r, c] = BOTTOM; end = (r, c); }
           else
           {
              if (lines[r][c] == 'a') @as.Add((r, c));
              field[r, c] = lines[r][c] - 'a';
           }
        }
     var q = new Queue<(int count, int r, int c)>();
     void visit(int d, int r, int c) { q.Enqueue((d, r, c)); dist[r, c] = d; }
     visit(0, end.r, end.c);
     while (q.Count > 0)
     {
        var (d, r, c) = q.Dequeue();
        d++;
        void test(int nr, int nc)
        {
           if (nr >= 0 && nr < m && nc >= 0 && nc < n && d < dist[nr, nc] && field[r, c] - field[nr, nc] <= 1)
              visit(d, nr, nc);
        }
        test(r, c - 1); test(r, c + 1); test(r - 1, c); test(r + 1, c);
     }
     Part1 = dist[start.r, start.c].ToString(); Part2 = @as.Min(s => dist[s.r, s.c]).ToString();
  }
}
