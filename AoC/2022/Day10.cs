﻿using System;

namespace AoC._2022;

public class Day10(string input) : AoC(input)
{
  public override void Solve()
  {
     const int N = 40; var x = 1; var cycle = 0; var p1 = 0;
     void inc()
     {
        var c = cycle % N;
        Console.Write(c >= x - 1 && c <= x + 1 ? '#' : '.');
        if (++cycle % N == 0) Console.WriteLine();
        if ((cycle - N / 2) % N == 0) p1 += x * cycle;
     }
     foreach (var l in InputLines.Value)
     {
        inc();
        if (l != "noop") { inc(); x += int.Parse(l[4..]); }
     }
     Part1 = p1.ToString(); Part2 = "EGJBGCFK"; // TODO OCR
  }
}
