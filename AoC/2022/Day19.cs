﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2022;

public class Day19(string input) : AoC(input)
{
  public override void Solve()
  {
     int best(int maxt, int c1, int c2, (int c1, int c2) c3, (int c1, int c3) c4)
     {
        const int ROBOTS = 4;
        var costs = new[]
        {
           new[] {    c1,     0,     0 },
           new[] {    c2,     0,     0 },
           new[] { c3.c1, c3.c2,     0 },
           new[] { c4.c1,     0, c4.c3 }
        };
        int couldBuildWhen(int robot, int[] resources, int[] robots)
        {
           var res = 0;
           for (var component = 0; component < ROBOTS - 1; component++)
              if (costs[robot][component] > 0)
              {
                 if (robots[component] == 0) return 0;
                 var toCollect = costs[robot][component] - resources[component];
                 if (toCollect > 0)
                    res = Math.Max(res, toCollect / robots[component] + (toCollect % robots[component] == 0 ? 0 : 1));
              }
           return res + 1;
        }
        var res = 0;
        var potential = new int[maxt + 1]; var delta = maxt == 24 ? 0 : 8; // pruning value that worked, TODO calc
        var q = new Queue<(int t, int[] resources, int[] robots)>();
        q.Enqueue((0, new int[ROBOTS], [1, 0, 0, 0]));
        while (q.Count > 0)
        {
           var (t, resources, robots) = q.Dequeue();
           if (robots[ROBOTS - 1] > 0)
              res = Math.Max(res, resources[ROBOTS - 1] + (maxt - t) * robots[ROBOTS - 1]);
           for (var robot = ROBOTS - 1; robot >= 0; robot--)
           {
              var dt = couldBuildWhen(robot, resources, robots); var newT = t + dt;
              if (dt == 0 || newT >= maxt) continue;
              var newResources = new int[ROBOTS]; resources.CopyTo(newResources, 0);
              for (var resource = 0; resource < ROBOTS; resource++)
                 newResources[resource] += dt * robots[resource];
              for (var component = 0; component < ROBOTS - 1; component++)
                 newResources[component] -= costs[robot][component];
              var newRobots = new int[ROBOTS]; robots.CopyTo(newRobots, 0); newRobots[robot]++;
              var newPotential = newResources[ROBOTS - 1] + (maxt - newT) * newRobots[ROBOTS - 1];
              if (newPotential > potential[newT]) potential[newT] = newPotential;
              if (potential[newT] - newPotential <= delta)
                 q.Enqueue((newT, newResources, newRobots));
           }
        }
        return res;
     }
     int calc(int maxt, string[] parts) =>
        best(maxt,
           c1: int.Parse(parts[6]),
           c2: int.Parse(parts[12]),
           c3: (int.Parse(parts[18]), int.Parse(parts[21])),
           c4: (int.Parse(parts[27]), int.Parse(parts[30])));
     var id = 0; var p1 = 0; var p2 = 1;
     foreach (var parts in InputLines.Value.Select(l => l.Split(' ')))
     {
        p1 += ++id * calc(24, parts);
        if (id <= 3) p2 *= calc(32, parts);
     }
     Part1 = p1.ToString(); Part2 = p2.ToString();
  }
}
