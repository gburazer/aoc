﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2022;

public class Day21(string input) : AoC(input)
{
  public override void Solve()
  {
     var expr = InputLines.Value
        .Select(l => l.Split(": "))
        .ToDictionary(parts => parts[0], parts => parts[1]);
     long eval(long el1, string op, long el2) => op[0] switch
     {
        '+' => el1 + el2,
        '-' => el1 - el2,
        '*' => el1 * el2,
        '/' => el1 / el2,
        _ => throw new InvalidOperationException()
     };
     var val = new Dictionary<string, long>();
     long value(string el)
     {
        if (val.TryGetValue(el, out var v))
           return v;
        if (long.TryParse(expr[el], out var num))
        {
           val[el] = num;
           return num;
        }
        var parts = expr[el].Split(' ');
        val[el] = eval(value(parts[0]), parts[1], value(parts[2]));
        return val[el];
     }
     const string ROOT = "root", HUMAN = "humn";
     bool containsHuman(string where)
     {
        if (HUMAN == where) return true;
        var parts = expr[where].Split(' ');
        if (parts.Length == 1) return false;
        return containsHuman(parts[0]) || containsHuman(parts[2]);
     }
     string inverse(string op) => op switch
     {
        "+" => "-",
        "-" => "+",
        "*" => "/",
        "/" => "*",
        _ => throw new InvalidOperationException()
     };
     long update(string candidate, long toWhat)
     {
        if (candidate == HUMAN) return toWhat;
        var parts = expr[candidate].Split(' ');
        var el1 = parts[0]; var op = parts[1]; var el2 = parts[2];
        var v1 = val[el1]; var v2 = val[el2];
        return containsHuman(el1) 
           ? update(el1, eval(toWhat, inverse(op), v2)) 
           : update(el2, op is "-" or "/" 
              ? eval(v1, op, toWhat) 
              : eval(toWhat, inverse(op), v1));
     }
     long equalize(string[] parts)
     {
        var el1 = parts[0]; var el2 = parts[2];
        return containsHuman(el1) ? update(el1, val[el2]) : update(el2, val[el1]);
     }
     Part1 = value(ROOT).ToString(); Part2 = equalize(expr[ROOT].Split(' ')).ToString();
  }
}
