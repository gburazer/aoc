﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2022;

public class Day15(string input) : AoC(input)
{
  public override void Solve()
  {
     var sensors = new List<(int x, int y)>(); var rs = new List<int>();
     var used = new HashSet<(int x, int y)>();
     var minx = int.MaxValue; var maxx = 0;
     int manhattan((int x, int y) p1, (int x, int y) p2) => Math.Abs(p1.x - p2.x) + Math.Abs(p1.y - p2.y);
     foreach (var parts in InputLines.Value.Select(l => l.Split(' ')))
     {
        int parse(int pind, int tail = 1) => int.Parse(parts[pind][2..^tail]);
        var (sx, sy) = (parse(2), parse(3)); var (bx, by) = (parse(8), parse(9, 0));
        sensors.Add((sx, sy)); used.Add((bx, by)); used.Add((sx, sy));
        var r = manhattan((sx, sy), (bx, by)); rs.Add(r);
        if (sx - r < minx) minx = sx - r; if (sx + r > maxx) maxx = sx + r;
     }
     for (var i = 0; i < sensors.Count - 1; i++)
        for (var j = i + 1; j < sensors.Count; j++)
           if (sensors[i].x - rs[i] > sensors[j].x - rs[j]) // sort by left edge
           {
              (sensors[j], sensors[i]) = (sensors[i], sensors[j]);
              (rs[j], rs[i]) = (rs[i], rs[j]);
           }

     const int SEARCH1 = 2000000; var p1 = 0;
     for (var x = minx; x <= maxx; x++)
        for (var i = 0; i < sensors.Count; i++)
           if (manhattan(sensors[i], (x, SEARCH1)) <= rs[i] && !used.Contains((x, SEARCH1)))
           {
              p1++; break;
           }

     const long SEARCH2 = 4000000L; var p2 = 0L;
     for (var sx = 0; sx <= SEARCH2 && p2 == 0; sx++)
        for (var sy = 0; sy <= SEARCH2 && p2 == 0; sy++)
        {
           for (var i = 0; i < sensors.Count; i++)
           {
              var d = manhattan(sensors[i], (sx, sy));
              if (d > rs[i]) continue;
              sy += rs[i] - d;
              if (sy >= SEARCH2) { sx++; sy = 0; }
              goto next;
           }
           p2 = SEARCH2 * sx + sy;
        next:;
        }

     Part1 = p1.ToString(); Part2 = p2.ToString();
  }
}
