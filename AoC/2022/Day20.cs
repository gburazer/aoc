﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2022;

public class Day20(string input) : AoC(input)
{
  public override void Solve()
  {
     var l1 = new CircularList<long>(); var l2 = new CircularList<long>();
     var order1 = new List<LinkedListNode<long>>(); var order2 = new List<LinkedListNode<long>>();
     foreach (var n in InputLines.Value.Select(int.Parse))
     {
        order1.Add(l1.AddLast(n));
        order2.Add(l2.AddLast(n * 811589153L));
     }
     void mix(CircularList<long> l, List<LinkedListNode<long>> order)
     {
        foreach (var el in order)
        {
           var v = el.Value; if (v == 0) continue;
           var adv = Math.Abs(v) % (l.Count - 1);
           var to = l.Advance(el, v > 0 ? adv : -adv); var prev = v > 0 ? to : l.Previous(to);
           if (prev == el) continue;
           l.Remove(el); l.AddAfter(prev, el);
        }
     }
     mix(l1, order1);
     for (var i = 0; i < 10; i++)
        mix(l2, order2);
     string res(CircularList<long> l) => 
        Enumerable.Range(1, 3).Select(n => l.Advance(l.Find(0), n * 1000).Value).Sum().ToString();
     Part1 = res(l1); Part2 = res(l2);
  }
}
