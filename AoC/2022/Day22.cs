﻿using System;
using System.Linq;

namespace AoC._2022;

public class Day22(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value; var m = lines.Length - 2; var n = lines.Max(l => l.Length);
     const char EMPTY = '.', WALL = '#', VOID = (char)0;
     var field = new char[m, n];
     var (sr, sc) = (-1, -1);
     for (var r = 0; r < m; r++)
        for (var c = 0; c < lines[r].Length; c++)
        {
           field[r, c] = lines[r][c] == ' ' ? VOID : lines[r][c];
           if (sr == -1 && field[r, c] == EMPTY) (sr, sc) = (r, c);
        }

     const int RIGHT = 0, DOWN = 1, LEFT = 2, UP = 3;
     var dir1 = RIGHT; var dir2 = RIGHT;
     (int dr, int dc) d(int dir) => dir switch
     {
        RIGHT => (0, 1),
        DOWN => (1, 0),
        LEFT => (0, -1),
        UP => (-1, 0),
        _ => throw new InvalidOperationException()
     };

     (int cr, int cc) next1(int cr, int cc, int dr, int dc)
     {
        do (cr, cc) = ((cr + m + dr) % m, (cc + n + dc) % n); while (field[cr, cc] == VOID);
        return (cr, cc);
     }

     (int cr, int cc) next2(int cr, int cc, int dr, int dc)
     {
        return (cr, cc); // TODO, cube and all

        // figure out dimension of cube (each row can have min 1 and max 4 sides, altogether 6 sides are needed)
        //    find true width of each row then how many of those rows are repeating etc
        // figure out how its folded (which section is where in the map and how it connects to next)
        //    each section can have section on the right and section below
        // establish a mapping from cube coordinates to map coordinates and vice-versa
        //    cube coordinates x, y, z will always have one dimension "extreme" (0 or DIM-1)
        //    each cube side will be represented on the map with its corner and orientations
        // walk around the cube using the mapping -> next2() should be easy to implement
     }

     void turn(char ch, ref int dir) { dir = (dir + 4 + (ch == 'R' ? 1 : -1)) % 4; }
     void move(ref int r, ref int c, int amt, ref int dir, Func<int, int, int, int, (int, int)> next)
     {
        var (dr, dc) = d(dir); var (cr, cc) = (r, c);
        for (var i = 0; i < amt; i++)
        {
           (cr, cc) = next(cr, cc, dr, dc);
           if (field[cr, cc] == WALL) break;
           (r, c) = (cr, cc);
        }
     }
     var amt = 0;
     var (r1, c1) = (sr, sc); void m1() { move(ref r1, ref c1, amt, ref dir1, next1); }
     var (r2, c2) = (sr, sc); void m2() { move(ref r2, ref c2, amt, ref dir2, next2); }
     void mv() { if (amt > 0) { m1(); m2(); amt = 0; } }
     foreach (var ch in lines[^1])
     {
        if (char.IsDigit(ch)) amt = 10 * amt + (ch - '0');
        else { mv(); turn(ch, ref dir1); turn(ch, ref dir2); }
     }
     mv();

     string score(int r, int c, int dir) => (1000 * (r + 1) + 4 * (c + 1) + dir).ToString();
     Part1 = score(r1, c1, dir1); Part2 = score(r2, c2, dir2);
  }
}
