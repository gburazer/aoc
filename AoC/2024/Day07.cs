﻿using System.Linq;

namespace AoC._2024
{
    public class Day07(string input) : AoC(input)
    {
        public override void Solve()
        {
            static long concat(long a, long b)
            {
                var m = 1L;
                while (m <= b) m *= 10;
                return a * m + b;
            }
            static bool canBeTrue(long t, long cur, long[] ns, int pos, bool tryConcat)
            {
                if (pos == ns.Length) return cur == t;
                if (cur > t) return false;
                if (canBeTrue(t, cur + ns[pos], ns, pos + 1, tryConcat)) return true;
                if (canBeTrue(t, cur * ns[pos], ns, pos + 1, tryConcat)) return true;
                return tryConcat && canBeTrue(t, concat(cur, ns[pos]), ns, pos + 1, true);
            }
            var p1 = 0L; var p2 = 0L;
            foreach (var parts in InputLines.Value.Select(line => line.Split(": ")))
            {
                var res = long.Parse(parts[0]);
                var ns = parts[1].Split(' ').Select(long.Parse).ToArray();
                if (canBeTrue(res, ns[0], ns, 1, tryConcat: false)) p1 += res;
                if (canBeTrue(res, ns[0], ns, 1, tryConcat:  true)) p2 += res;
            }
            Part1 = p1.ToString(); Part2 = p2.ToString();
        }
    }
}