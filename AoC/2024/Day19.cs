﻿using System;
using System.Collections.Generic;

namespace AoC._2024;

public class Day19 : AoC
{
    public Day19(string input) : base(input)
    {
        _lines = InputLines.Value;
        _patterns = _lines[0].Split(", ");
    }

    readonly string[] _lines, _patterns;
    readonly Dictionary<int, long> _cache = new(); // start -> number of ways from there

    public override void Solve()
    {
        var p1 = 0L; var p2 = 0L;
        for (var i = 2; i < _lines.Length; i++)
        {
            var n = WaysPossible(_lines[i].AsSpan());
            if (n > 0) p1++;
            p2 += n;
        }
        Part1 = p1.ToString(); Part2 = p2.ToString();
    }

    private long WaysPossible(ReadOnlySpan<char> design)
    {
        _cache.Clear();
        return CountWays(design, 0);
    }

    private long CountWays(ReadOnlySpan<char> design, int start)
    {
        if (start == design.Length) return 1;
        if (_cache.TryGetValue(start, out var cached)) return cached;
        var res = 0L;
        foreach (var p in _patterns)
        {
            var ps = p.AsSpan();
            if (start + ps.Length <= design.Length && design.Slice(start, ps.Length).SequenceEqual(ps))
                res += CountWays(design, start + ps.Length);
        }
        _cache[start] = res;
        return res;
    }
}