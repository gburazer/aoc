﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2024;

public class Day17(string input) : AoC(input)
{
    public override void Solve()
    {
        static List<long> run(long a, long b, long c, List<long> program)
        {
            long combo(long operand) => operand switch
            {
                0 or 1 or 2 or 3 => operand,
                4 => a, 5 => b, 6 => c,
                _ => throw new InvalidOperationException()
            };
            var res = new List<long>();
            for (var ip = 0; ip < program.Count; ip += 2)
            {
                var operand = program[ip + 1];
                switch (program[ip])
                {
                    case 0: a /= 1 << (int)combo(operand); break;
                    case 1: b ^= operand; break;
                    case 2: b = combo(operand) % 8; break;
                    case 3: if (a != 0) ip = (int)operand - 2; break;
                    case 4: b ^= c; break;
                    case 5: res.Add(combo(operand) % 8); break;
                    case 6: b = a / (1 << (int)combo(operand)); break;
                    case 7: c = a / (1 << (int)combo(operand)); break;
                }
            }
            return res;
        }
        
        var lines = InputLines.Value;
        string content(int lineInd) => lines[lineInd].Split(": ")[1];
        long register(int lineInd) => long.Parse(content(lineInd));
        var program = content(4).Split(',').Select(long.Parse).ToList();
        Part1 = string.Join(',', run(a: register(0), b: register(1), c: register(2), program));
        
        /*
         * program: 2,4,1,2,7,5,4,3,0,3,1,7,5,5,3,0
         * b = a % 8
         * b ˆ= 2
         * c = a / 2 ^ b
         * b ˆ= c
         * a /= 8 (shift right 3 bits)
         * b ˆ= 7
         * output b % 8
         * repeat all if still smth in a!
         *
         * 3 least significant bit of a are determining next output!
         * work program backwards, figure out bits needed for output and shift
         * could be combinatorics involved -> work with all possibilities
         */
        var @as = new List<long> { 0 };
        for (var i = 1; i <= program.Count; i++)
        {
            var nextas = new List<long>();
            foreach (var a in @as)
                for (var j = 0; j < 8; j++)
                {
                    var nexta = (a << 3) + j;
                    if (run(nexta, 0, 0, program)[0] == program[^i])
                        nextas.Add(nexta);
                }
            @as = nextas;
        }
        Part2 = @as.Min().ToString();
    }
}