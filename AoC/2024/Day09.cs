﻿using System;
using System.Linq;

namespace AoC._2024;

public class Day09(string input) : AoC(input)
{
    public override void Solve()
    {
        static int val(char ch) => ch - '0';
        var size = Input.Sum(val); var disk = new int[size];
        const int EMPTY = -1; var id = 0; var pos = 0;
        for (var i = 0; i < Input.Length; i++)
        {
            var isEmpty = i % 2 == 1;
            var v = val(Input[i]);
            for (var j = 0; j < v; j++) disk[pos++] = isEmpty ? EMPTY : id;
            if (isEmpty) id++;
        }
        void part1(int[] d)
        {
            var (left, right) = (0, size - 1);
            while (true)
            {
                while (d[left] != EMPTY) left++;
                while (d[right] == EMPTY) right--;
                if (left > right) break;
                (d[left], d[right]) = (d[right--], d[left++]);
            }
        }
        void part2(int[] d) // TODO PERF track available empty 1s, 2s, ..
        {
            bool findEmpty(int len, int to, out int start)
            {
                for (var i = 0; i <= to - len; i++)
                    if (d[i] == EMPTY)
                    {
                        var elen = 1; while (elen < len && d[i + elen] == EMPTY) elen++;
                        if (elen == len) { start = i; return true; }
                        i += elen;
                    }
                start = 0; return false;
            }
            var right = size - 1;
            while (true)
            {
                while (d[right] == EMPTY) right--;
                var curId = d[right]; if (curId == 0) break; 
                var len = 0; while (d[right] == curId) { len++; right--; }
                if (!findEmpty(len, right + 1, out var start)) continue;
                for (var i = 0; i < len; i++)
                    (d[start + i], d[right + 1 + i]) = (d[right + 1 + i], d[start + i]);
            }
        }
        long checksum(int[] d)
        {
            var res = 0L;
            for (var i = 0; i < d.Length; i++)
                if (d[i] != EMPTY)
                    res += i * d[i];
            return res;
        }
        string solve(Action<int[]> fn)
        {
            var newDisk = new int[size];
            disk.CopyTo(newDisk, 0);
            fn(newDisk);
            return checksum(newDisk).ToString();
        }
        Part1 = solve(part1); Part2 = solve(part2);
    }
}