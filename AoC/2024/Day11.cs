﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2024;

public class Day11(string input) : AoC(input)
{
    public override void Solve()
    {
        var cur = new Dictionary<long, long>(); // n, count
        static void add(Dictionary<long, long> dict, long n, long count)
        {
            if (!dict.TryAdd(n, count))
                dict[n] += count;
        }
        foreach (var n in InputLines.Value.Single().Split(' ').Select(long.Parse))
            add(cur, n, 1);
        static long sum(Dictionary<long, long> dict) => dict.Keys.Sum(k => dict[k]);
        var p1 = 0L;
        for (var i = 0; i < 75; i++)
        {
            if (i == 25) 
                p1 = sum(cur);
            var next = new Dictionary<long, long>();
            foreach (var (n, count) in cur.Select(kv => (n: kv.Key, count: kv.Value)))
                if (n == 0)
                    add(next, 1, count);
                else
                {
                    var s = n.ToString();
                    if (s.Length % 2 == 0)
                    {
                        add(next, long.Parse(s[..(s.Length / 2)]), count);
                        add(next, long.Parse(s[(s.Length / 2)..]), count);
                    }
                    else
                        add(next, n * 2024, count);
                }
            cur = next;
        }
        Part1 = p1.ToString(); Part2 = sum(cur).ToString();
    }
}