﻿using System.Linq;

namespace AoC._2024;

public class Day05(string input) : AoC(input)
{
    public override void Solve()
    {
        var mustComeBefore = new bool[100, 100]; // all are 2-digit, 1-99
        var lines = InputLines.Value; var lpos = 0;
        for (; lines[lpos] != string.Empty; lpos++)
        {
            var parts = lines[lpos].Split('|');
            mustComeBefore[int.Parse(parts[0]), int.Parse(parts[1])] = true;
        }
        lpos++; // skip empty line (input divider)
        bool checkAndOrder(int[] ns)
        {
            var res = true;
            for (var i = 0; i < ns.Length - 1; i++)
                for (var j = i + 1; j < ns.Length; j++)
                    if (mustComeBefore[ns[j], ns[i]])
                    {
                        (ns[i], ns[j]) = (ns[j], ns[i]);
                        res = false;
                    }
            return res;
        }
        var p1 = 0; var p2 = 0;
        for (; lpos < lines.Length; lpos++)
        {
            var ns = lines[lpos].Split(',').Select(int.Parse).ToArray();
            int mid() => ns[ns.Length / 2];
            if (checkAndOrder(ns)) p1 += mid();else p2 += mid();
        }
        Part1 = p1.ToString(); Part2 = p2.ToString();
    }
}