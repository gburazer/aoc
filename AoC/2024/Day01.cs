﻿using System;
using System.Collections.Generic;

namespace AoC._2024;

public class Day01(string input) : AoC(input)
{
    public override void Solve()
    {
        var l1 = new List<int>(); var l2 = new List<int>();
        foreach (var line in InputLines.Value)
        {
            var parts = line.Split("   ");
            l1.Add(int.Parse(parts[0])); l2.Add(int.Parse(parts[1]));
        }
        l1.Sort(); l2.Sort();
        var pos = 0;
        int count(int n)
        {
            var res = 0;
            for (; l2[pos] < n; pos++) ;
            for (; l2[pos] == n; pos++) res++;
            return res;
        }
        var p1 = 0; var p2 = 0;
        for (var i = 0; i < l1.Count; i++)
        {
            p1 += Math.Abs(l1[i] - l2[i]);
            p2 += l1[i] * count(l1[i]);
        }
        Part1 = p1.ToString(); Part2 = p2.ToString();
    }
}