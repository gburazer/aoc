﻿using System.Linq;

namespace AoC._2024;

public class Day03(string input) : AoC(input)
{
    public override void Solve()
    {
        var pos = 0;
        char peek() => Input[pos];
        bool matchc(char c) => Input[pos++] == c;
        bool matchs(string s) => s.All(matchc);
        bool matchn(out int n)
        {
            n = 0;
            while (n < 100) // max 3 digits
                if (char.IsDigit(Input[pos]))
                    n = 10 * n + (Input[pos++] - '0');
                else
                    return n > 0;
            return true;
        }
        int nextMul() => 
            matchs("mul") && matchc('(') && matchn(out var a) && matchc(',') && matchn(out var b) && matchc(')')
            ? a * b : 0;
        var p1 = 0; var p2 = 0; 
        var @do = true;
        while (pos < Input.Length)
        {
            if (peek() == 'd')
                if (@do)
                {
                    if (matchs("don't()"))
                        @do = false;
                }
                else if (matchs("do()"))
                    @do = true;
            var n = nextMul(); p1 += n; if (@do) p2 += n;
        }
        Part1 = p1.ToString(); Part2 = p2.ToString();
    }
}