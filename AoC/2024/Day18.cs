﻿using System.Collections.Generic;

namespace AoC._2024;

public class Day18(string input) : AoC(input)
{
    public override void Solve()
    {
        const int MAX = 70, TAKE = 1024;
        var isWall = new bool[MAX + 1, MAX + 1];
        void parse(int ind)
        {
            var parts = InputLines.Value[ind].Split(',');
            isWall[int.Parse(parts[0]), int.Parse(parts[1])] = true;
        }
        for (var i = 0; i < TAKE; i++)
            parse(i);
        int solve()
        {
            var best = new int[MAX + 1, MAX + 1];
            for (var x = 0; x <= MAX; x++)
                for (var y = 0; y <= MAX; y++)
                    best[x, y] = int.MaxValue;
            var q = new Queue<(int x, int y)>(); q.Enqueue((0, 0)); best[0, 0] = 0;
            while (q.Count > 0)
            {
                var (x, y) = q.Dequeue();
                var nxt = best[x, y] + 1;
                for (var dx = -1; dx <= 1; dx++)
                    for (var dy = -1; dy <= 1; dy++)
                        if ((dx == 0 && dy != 0) || (dx != 0 && dy == 0))
                        {
                            var xx = x + dx; var yy = y + dy;
                            if (xx is < 0 or > MAX || yy is < 0 or > MAX 
                                                   || isWall[xx, yy] 
                                                   || best[xx, yy] <= nxt) 
                                continue;
                        q.Enqueue((xx, yy)); best[xx, yy] = nxt;
                    }
            }
            return best[MAX, MAX];            
        }
        string p2()
        {
            for (var i = TAKE;; i++)
            {
                parse(i);
                if (solve() == int.MaxValue)
                    return InputLines.Value[i];
            }
        }
        Part1 = solve().ToString(); Part2 = p2();
    }
}