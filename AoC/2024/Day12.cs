﻿using System;
using System.Collections.Generic;

namespace AoC._2024;

public class Day12(string input) : AoC(input)
{
    public override void Solve()
    {
        var lines = InputLines.Value; var m = lines.Length; var n = lines[0].Length; 
        var visited = new bool[m, n];
        (int area, int perimeter, int sides) fill(int row, int col)
        {
            var (area, perimeter) = (0, 0);
            var hSegments = new List<(int r, int c)>(); var vSegments = new List<(int r, int c)>();
            var q = new Queue<(int r, int c)>();
            void visit(int r, int c) { visited[r, c] = true; q.Enqueue((r, c)); }
            visit(row, col);
            void tryVisit(char color, (int r, int c) pos, int dr, int dc)
            {
                var (rr, cc) = (pos.r + dr, pos.c + dc);
                const int MULT = 1000; // to distinguish inner/outer
                if (rr < 0 || rr >= m || cc < 0 || cc >= n || lines[rr][cc] != color)
                {
                    perimeter++;
                    if (dr == 0)
                        vSegments.Add((rr, dc < 0 ? MULT * pos.c : cc));
                    else
                        hSegments.Add((dr < 0 ? MULT * pos.r : rr, cc));
                }
                else if (!visited[rr, cc]) 
                    visit(rr, cc);
            }
            while (q.Count > 0)
            {
                var pos = q.Dequeue(); area++;
                var color = lines[pos.r][pos.c];
                tryVisit(color, pos, 1, 0); tryVisit(color, pos, -1, 0);
                tryVisit(color, pos, 0, 1); tryVisit(color, pos, 0, -1);
            }
            hSegments.Sort((s1, s2) => s1.r.CompareTo(s2.r) == 0 ? s1.c.CompareTo(s2.c) : s1.r.CompareTo(s2.r));
            vSegments.Sort((s1, s2) => s1.c.CompareTo(s2.c) == 0 ? s1.r.CompareTo(s2.r) : s1.c.CompareTo(s2.c));
            int countSides(List<(int r, int c)> segments, Func<(int r, int c), int, int, bool> cnd)
            {
                var res = 0;
                var (r, c) = (-1, -1);
                foreach (var s in segments)
                {
                    if (cnd(s, r, c))
                        res++;
                    (r, c) = s;
                }
                return res;
            }
            return (area, perimeter,
                countSides(hSegments, (s, r, c) => s.r != r || s.c != c + 1) +
                countSides(vSegments, (s, r, c) => s.c != c || s.r != r + 1));
        }
        var p1 = 0; var p2 = 0;
        for (var r = 0; r < m; r++)
            for (var c = 0; c < n; c++)
                if (!visited[r, c])
                {
                    var (area, perimeter, sides) = fill(r, c);
                    p1 += area * perimeter; p2 += area * sides;
                }
        Part1 = p1.ToString(); Part2 = p2.ToString();
    }
}