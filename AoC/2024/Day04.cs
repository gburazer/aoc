﻿namespace AoC._2024;

public class Day04(string input) : AoC(input)
{
    public override void Solve()
    {
        var field = InputLines.Value; var m = field.Length; var n = field[0].Length;
        const string WORD = "XMAS";
        bool isMatch(int ind, int r, int c, int dr, int dc)
        {
            if (r < 0 || r >= m || c < 0 || c >= n) return false;
            if (field[r][c] != WORD[ind]) return false;
            return ind == WORD.Length - 1 || isMatch(ind + 1, r + dr, c + dc, dr, dc);
        }
        bool isMidP2(int r, int c)
        {
            if (field[r][c] != 'A') return false;
            if (r == 0 || c == 0 || r == m - 1 || c == n - 1) return false;
            bool check(char c1, char c2) => (c1 == 'M' && c2 == 'S') || (c1 == 'S' && c2 == 'M');
            return check(field[r - 1][c - 1], field[r + 1][c + 1]) && check(field[r + 1][c - 1], field[r - 1][c + 1]);
        }
        var p1 = 0; var p2 = 0;
        for (var r = 0; r < m; r++)
            for (var c = 0; c < n; c++)
            {
                if (isMidP2(r, c))
                    p2++;
                for (var dr = -1; dr <= 1; dr++)
                    for (var dc = -1; dc <= 1; dc++)
                        if ((dr != 0 || dc != 0) && isMatch(0, r, c, dr, dc))
                            p1++;
            }
        Part1 = p1.ToString(); Part2 = p2.ToString();
    }
}