﻿namespace AoC._2024;

public class Day13(string input) : AoC(input)
{
    public override void Solve()
    {
        var lines = InputLines.Value;
        static (int x, int y) parseCoords(string s, char del)
        {
            var parts = s.Split(": ")[1].Split(", ");
            int parse(int pt) => int.Parse(parts[pt].Split(del)[1]);
            return (parse(0), parse(1));
        }
        const int MAX = 100, A = 3, B = 1;
        const long D = 10000000000000L;
        static int part1((int x, int y) da, (int x, int y) db, (int x, int y) prize)
        {
            var minScore = int.MaxValue;
            for (var m = 0; m <= MAX; m++)
                for (var n = 0; n <= MAX; n++)
                {
                    if ((m * da.x + n * db.x, m * da.y + n * db.y) != prize)
                        continue;
                    var score = m * A + n * B;
                    if (score < minScore)
                        minScore = score;
                }
            return minScore != int.MaxValue ? minScore : 0;
        }
        static long part2((int x, int y) da, (int x, int y) db, (long x, long y) prize)
        {
            // m * da.x + n * db.x == prize.x && m * da.y + n * db.y == prize.y .. score = A * m + B * n
            var det = (long)da.x * db.y - (long)da.y * db.x;
            if (det == 0) return 0;
            var nm = prize.x * db.y - prize.y * db.x;
            var nn = da.x * prize.y - da.y * prize.x;
            if (nm % det != 0 || nn % det != 0) return 0;
            return A * nm / det + B * nn / det;
        }
        var p1 = 0; var p2 = 0L;
        for (var lineInd = 0; lineInd < (lines.Length + 1) / 4; lineInd++)
        {
            var da = parseCoords(lines[4 * lineInd], '+');
            var db = parseCoords(lines[4 * lineInd + 1], '+');
            var prize1 = parseCoords(lines[4 * lineInd + 2], '=');
            var prize2 = (X: prize1.x + D, y: prize1.y + D);
            p1 += part1(da, db, prize1); p2 += part2(da, db, prize2);
        }
        Part1 = p1.ToString(); Part2 = p2.ToString();
    }
}