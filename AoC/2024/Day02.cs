﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2024;

public class Day02(string input) : AoC(input)
{
    public override void Solve()
    {
        bool isSafe(List<int> ns)
        {
            var diff0 = ns[0] - ns[1];
            for (var i = 1; i < ns.Count; i++)
            {
                var diff = ns[i - 1] - ns[i];
                if (diff0 * diff > 0 && Math.Abs(diff) is 1 or 2 or 3) continue;
                return false;
            }
            return true;
        }
        var p1 = 0; var p2 = 0;
        foreach (var ns in InputLines.Value.Select(line => line.Split(' ').Select(int.Parse).ToList()))
            if (isSafe(ns)) { p1++; p2++; }
            else if (ns.Where((_, i) => isSafe(ns.Where((_, ind) => ind != i).ToList())).Any()) p2++;
        Part1 = p1.ToString(); Part2 = p2.ToString();
    }
}