﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace AoC._2024;

public class Day15(string input) : AoC(input)
{
    public override void Solve()
    {
        var lines = InputLines.Value; 
        var m = Array.IndexOf(lines, string.Empty); var n = lines[0].Length;
        var field1 = new char[m, n]; var field2 = new char[m, 2 * n];
        var pos1 = (r: 0, c: 0); var pos2 = (r: 0, c: 0);
        const char ROBOT = '@', EMPTY = '.', WALL = '#', BOX = 'O', BOXL = '[', BOXR = ']',
            LEFT = '<', RIGHT = '>', UP = '^', DOWN = 'v';
        string wall2 = $"{WALL}{WALL}", empty2 = $"{EMPTY}{EMPTY}", robot2 = $"{ROBOT}{EMPTY}", box2 = $"{BOXL}{BOXR}";
        for (var lind = 0; lind < m; lind++)
            for (var i = 0; i < n; i++)
            {
                field1[lind, i] = lines[lind][i];
                var s = field1[lind, i] switch
                {
                    WALL => wall2, EMPTY => empty2, ROBOT => robot2, BOX => box2,
                    _ => throw new InvalidOperationException()
                };
                field2[lind, 2 * i] = s[0]; field2[lind, 2 * i + 1] = s[1];
                if (field1[lind, i] != ROBOT) continue;
                pos1 = (lind, i); pos2 = (lind, 2 * i);
            }
        var moves = Enumerable.Range(m, lines.Length - m)
            .Aggregate(new StringBuilder(), (sb, i) => sb.Append(lines[i])).ToString();
        (int dr, int dc) d(char ch) => ch switch
        {
            UP    => (-1,  0),
            RIGHT => ( 0,  1),
            DOWN  => ( 1,  0),
            LEFT  => ( 0, -1),
            _ => throw new InvalidOperationException()
        };
        void moveRobot(char[,] field, ref (int r, int c) pos, int dr, int dc)
        {
            field[pos.r, pos.c] = EMPTY; 
            pos.r += dr; pos.c += dc; 
            field[pos.r, pos.c] = ROBOT;
        }
        void move1(int dr, int dc)
        {
            var boxCount = 0;
            for (var (peekr, peekc) = (pos1.r + dr, pos1.c + dc); field1[peekr, peekc] == BOX; peekr += dr, peekc += dc)
                boxCount++;
            var finish = (r: pos1.r + (boxCount + 1) * dr, c: pos1.c + (boxCount + 1) * dc);
            if (field1[finish.r, finish.c] == WALL) return; // no move
            if (boxCount > 0)
                field1[finish.r, finish.c] = BOX;
            moveRobot(field1, ref pos1, dr, dc);
        }
        void moveBigBoxUpOrDown(int dr)
        {
            var toMove = new Stack<(int r, int c)>();
            var allThatMove = new HashSet<(int r, int c)>();
            bool tryMove(int r, int c)
            {
                switch (field2[r, c])
                {
                    case WALL: return false;
                    case EMPTY: return true;
                    case BOXL:
                        toMove.Push((r, c)); toMove.Push((r, c + 1));
                        return tryMove(r + dr, c) && tryMove(r + dr, c + 1);
                    case BOXR:
                        toMove.Push((r, c)); toMove.Push((r, c - 1));
                        return tryMove(r + dr, c) && tryMove(r + dr, c - 1);
                    default: throw new InvalidOperationException();
                }
            }
            if (!tryMove(pos2.r + dr, pos2.c)) return; // no move
            while (toMove.Count > 0)
            {
                var (r, c) = toMove.Pop();
                if (!allThatMove.Add((r, c))) continue;
                (field2[r, c], field2[r + dr, c]) = (field2[r + dr, c], field2[r, c]);
            }
            moveRobot(field2, ref pos2, dr, 0);
        }
        void move2(int dr, int dc)
        {
            if (dr == 0) // left or right, similar as move1 but actual moving is needed
            {
                var boxCount = 0;
                for (var pc = pos2.c + dc; field2[pos2.r, pc] == BOXL || field2[pos2.r, pc] == BOXR; pc += dc)
                    boxCount++;
                var finishc = pos2.c + (boxCount + 1) * dc;
                if (field2[pos2.r, finishc] == WALL) return; // no move
                for (var i = boxCount; i > 0; i--)
                {
                    var c1 = pos2.c + i * dc; var c2 = c1 + dc;
                    (field2[pos2.r, c1], field2[pos2.r, c2]) = (field2[pos2.r, c2], field2[pos2.r, c1]);
                }
                moveRobot(field2, ref pos2, dr, dc);
            }
            else
                moveBigBoxUpOrDown(dr);
        }
        foreach (var (dr, dc) in moves.Select(d)) { move1(dr, dc); move2(dr, dc); }
        int gps(char[,] field, char target)
        {
            var res = 0;
            for (var r = 0; r < field.GetLength(0); r++)
                for (var c = 0; c < field.GetLength(1); c++)
                    if (field[r, c] == target)
                        res += 100 * r + c;
            return res;
        }
        Part1 = gps(field1, BOX).ToString(); Part2 = gps(field2, BOXL).ToString();
    }
}