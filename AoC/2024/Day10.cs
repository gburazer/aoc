﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2024;

public class Day10(string input) : AoC(input)
{
    public override void Solve()
    {
        var lines = InputLines.Value; var m = lines.Length; var n = lines[0].Length;
        var field = new int[m, n]; var starts = new List<(int r, int c)>();
        for (var r = 0; r < m; r++)
            for (var c = 0; c < n; c++)
            {
                field[r, c] = lines[r][c] - '0';
                if (field[r, c] == 0) starts.Add((r, c));
            }
        int calc((int r, int c) start, bool visitOnce)
        {
            var res = 0;
            var visited = new bool[m, n];
            var q = new Queue<(int r, int c)>();
            void visit((int r, int c) pos) { visited[pos.r, pos.c] = true; q.Enqueue(pos); }
            visit(start);
            void tryVisit(int val, (int r, int c) pos)
            {
                if (pos.r < 0 || pos.r == m || pos.c < 0 || pos.c == n) return;
                if ((visitOnce && visited[pos.r, pos.c]) || field[pos.r, pos.c] != val + 1) return;
                visit(pos);
            }
            while (q.Count > 0)
            {
                var pos = q.Dequeue(); var val = field[pos.r, pos.c];
                if (val == 9) res++;
                else
                {
                    tryVisit(val, (pos.r - 1, pos.c)); tryVisit(val, (pos.r + 1, pos.c));
                    tryVisit(val, (pos.r, pos.c - 1)); tryVisit(val, (pos.r, pos.c + 1));
                }
            }
            return res;
        }
        Part1 = starts.Sum(s => calc(s, visitOnce: true)).ToString(); 
        Part2 = starts.Sum(s => calc(s, visitOnce: false)).ToString();
    }
}