﻿using System;
using System.Linq;
using System.Collections.Generic;

using State = ((int r, int c) pos, int dir);

namespace AoC._2024;

public class Day16(string input) : AoC(input)
{
    public override void Solve()
    {
        var field = InputLines.Value; var m = field.Length; var n = field[0].Length;
        var best = new int[m, n, 4]; var bestOrigins = new HashSet<State>[m, n, 4];
        var start = (r: 0, c: 0); var end = (r: 0, c: 0);
        for (var r = 0; r < m; r++)
            for (var c = 0; c < n; c++)
            {
                for (var d = 0; d < 4; d++)
                    best[r, c, d] = int.MaxValue;
                switch (field[r][c])
                {
                    case 'S': start = (r, c); break;
                    case 'E': end   = (r, c); break;
                }
            }
        int  cw(int d) => (d + 1) % 4;
        int ccw(int d) => (d + 3) % 4;
        (int dr, int dc) move(int dir) => dir switch
        {
            0 => (-1,  0), // N
            1 => ( 0,  1), // E
            2 => ( 1,  0), // S
            3 => ( 0, -1), // W
            _ => throw new InvalidOperationException()
        };
        const int FORWARD = 1, TURN = 1000;
        var q = new Queue<(State state, int score, State origin)>();
        void tryEnqueue(State s, int score, State origin)
        {
            if (score > best[s.pos.r, s.pos.c, s.dir]) return;
            q.Enqueue((s, score, origin));
        }
        tryEnqueue((start, dir: 1), score: 0, default);
        while (q.Count > 0)
        {
            var (state, score, origin) = q.Dequeue();
            ref var b = ref best[state.pos.r, state.pos.c, state.dir];
            ref var bo = ref bestOrigins[state.pos.r, state.pos.c, state.dir];
            if (score <= b)
            {
                if (score == b) bo.Add(origin); else bo = [origin];
                b = score;
            }
            if (state.pos == end) continue;
            tryEnqueue((state.pos, ccw(state.dir)), score + TURN, state);
            tryEnqueue((state.pos,  cw(state.dir)), score + TURN, state);
            var dd = move(state.dir); var fpos = (r: state.pos.r + dd.dr, c: state.pos.c + dd.dc);
            if ('#' != field[fpos.r][fpos.c])
                tryEnqueue((fpos, state.dir), score + FORWARD, state);
        }
        var p1 = Enumerable.Range(0, 4).Min(d => best[end.r, end.c, d]);
        var allBest = new HashSet<(int r, int c)>(); var bestQ = new Queue<State>();
        for (var d = 0; d < 4; d++)
            if (best[end.r, end.c, d] == p1)
                bestQ.Enqueue((end, d));
        while (bestQ.Count > 0)
        {
            var (pos, dir) = bestQ.Dequeue();
            allBest.Add(pos);
            if (start == pos) continue;
            foreach (var s in bestOrigins[pos.r, pos.c, dir])
                bestQ.Enqueue(((s.pos.r, s.pos.c), s.dir));
        }
        Part1 = p1.ToString(); Part2 = allBest.Count.ToString();
    }
}