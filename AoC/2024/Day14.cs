﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2024;

public class Day14(string input) : AoC(input)
{
    public override void Solve()
    {
        static (int, int) parsePair(string pair)
        {
            var ps = pair.Split('=').Last().Split(',');
            return (int.Parse(ps[0]), int.Parse(ps[1]));
        }
        const int W = 101, H = 103, SECONDS = 100;
        var p1 = new int[2, 2];
        static int wrap(int val, int dim) => (val % dim + dim) % dim;
        var ps = new List<(int x, int y)>(); var vs = new List<(int x, int y)>();
        var field = new bool[W, H];
        foreach (var parts in InputLines.Value.Select(l => l.Split(' ').Select(p => p.Trim()).ToArray()))
        {
            var (px, py) = parsePair(parts[0]); var (vx, vy) = parsePair(parts[1]);
            ps.Add((px, py)); field[px, py] = true; vs.Add((vx, vy));
            px = wrap(px + SECONDS * vx, W); py = wrap(py + SECONDS * vy, H); // apply v and wrap to final position
            if (px != W / 2 && py != H / 2) // middle ones are not counted
                p1[px < W / 2 ? 0 : 1, py < H / 2 ? 0 : 1]++;
        }
        var p2 = 0;
        bool checkShape()
        {
            /* initially have output to file then inspected, realised it had a frame so it can be automated
            for (var y = 0; y < H; y++)
            {
                for (var x = 0; x < W; x++)
                    Console.Write(field[x, y] ? '#' : '.');
                Console.WriteLine();
            }
            Console.WriteLine($"After {p2} ˆ");
            return p2 == 10000;
            */
            for (var y = 0; y < H; y++)
            {
                var consecutive = 0;
                for (var x = 0; x < W; x++) // look for frame, 10 consecutive will do
                    if (field[x, y])
                    {
                        consecutive++;
                        if (consecutive == 10)
                            return true;
                    }
                    else
                        consecutive = 0;
            }
            return false;
        }
        while (!checkShape())
        {
            field = new bool[W, H];
            var newps = new List<(int x, int y)>();
            for (var i = 0; i < ps.Count; i++)
            {
                var (newx, newy) = (wrap(ps[i].x + vs[i].x, W), wrap(ps[i].y + vs[i].y, H));
                field[newx, newy] = true; newps.Add((newx, newy));
            }
            ps = newps; p2++;
        }
        Part1 = (p1[0, 0] * p1[0, 1] * p1[1, 0] * p1[1, 1]).ToString();
        Part2 = p2.ToString();
    }
}