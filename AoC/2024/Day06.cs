﻿using System;
using System.Collections.Generic;

namespace AoC._2024;

public class Day06(string input) : AoC(input)
{
    public override void Solve()
    {
        var field = InputLines.Value; var m = field.Length; var n = field[0].Length;
        bool isWithin(int pos) => pos >= 0 && pos < m * n;
        int move(int p, int d) => d switch
        {
            0 => p - n,
            1 => p % n == n - 1 ? m * n : p + 1,
            2 => p + n,
            3 => p % n == 0 ? -1 : p - 1,
            _ => throw new InvalidOperationException()
        };
        int turn(int d) => (d + 1) % 4; // 0123 <=> NESW
        var isWall = new bool[m * n];
        int init()
        {
            var res = -1;
            for (var r = 0; r < m; r++)
                for (var c = 0; c < n; c++)
                    switch (field[r][c])
                    {
                        case '^': res = r * n + c; break;
                        case '#': isWall[r * n + c] = true; break;
                    }
            return res;
        }
        bool walk(int cur, int dir, Func<int, int, int, bool> check)
        {
            for (var next = move(cur, dir); isWithin(next); next = move(cur, dir))
            {
                var prev = cur;
                if (isWall[next])
                    dir = turn(dir);
                else
                    cur = next;
                if (check(prev, cur, dir))
                    return true;
            }
            return false;
        }
        bool isLoop(int pos, int dir)
        {
            var v = new HashSet<(int pos, int dir)>();
            return walk(pos, dir, (_, to, d) => !v.Add((to, d)));
        }
        var start = init();
        var unique = new HashSet<int>(); var loopObstacles = new HashSet<int>();
        walk(start, 0, (from, to, dir) => // _ return
        {
            if (isWall[to] || !unique.Add(to)) return false;
            isWall[to] = true;
            if (isLoop(from, dir) && to != start)
                loopObstacles.Add(to);
            isWall[to] = false;
            return false;
        });
        Part1 = unique.Count.ToString(); Part2 = loopObstacles.Count.ToString();
    }
}