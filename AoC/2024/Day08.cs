﻿using System.Collections.Generic;

namespace AoC._2024;

public class Day08(string input) : AoC(input)
{
    public override void Solve()
    {
        var field = InputLines.Value; var m = field.Length; var n = field[0].Length;
        var antennas = new Dictionary<char, List<(int r, int c)>>();
        for (var r = 0; r < m; r++)
        {
            var l = field[r];
            for (var c = 0; c < n; c++)
            {
                var ch = l[c]; if (ch == '.') continue;
                if (!antennas.TryGetValue(ch, out var positions)) { positions = []; antennas[ch] = positions; }
                positions.Add((r, c));
            }
        }
        var antinodes1 = new HashSet<(int r, int c)>(); var antinodes2 = new HashSet<(int r, int c)>();
        void addLine(int startR, int dr, int rsig, int startC, int dc, int csig)
        {
            for (var k = 0;; k++)
            {
                var rr = startR + k * rsig * dr; var cc = startC + k * csig * dc;
                if ((uint)rr >= (uint)m || (uint)cc >= (uint)n) break;
                if (k == 1) antinodes1.Add((rr, cc));
                antinodes2.Add((rr, cc));
            }
        }
        foreach (var kvp in antennas)
        {
            var @as = kvp.Value; var count = @as.Count;
            for (var i = 0; i < count - 1; i++)
                for (var j = i + 1; j < count; j++)
                {
                    var a1 = @as[i]; var a2 = @as[j];
                    int r1 = a1.r, c1 = a1.c; int r2 = a2.r, c2 = a2.c;
                    var dr = r1 > r2 ? r1 - r2 : r2 - r1; var dc = c1 > c2 ? c1 - c2 : c2 - c1;
                    int minr, maxr, minc, maxc;
                    if (r1 < r2) { minr = r1; maxr = r2; } else { minr = r2; maxr = r1; }
                    if (c1 < c2) { minc = c1; maxc = c2; } else { minc = c2; maxc = c1; }
                    if ((r1 == minr && c1 == minc) || (r2 == minr && c2 == minc))
                    {
                        addLine(minr, dr, -1, minc, dc, -1);
                        addLine(maxr, dr,  1, maxc, dc,  1);
                    }
                    else
                    {
                        addLine(minr, dr, -1, maxc, dc, 1);
                        addLine(maxr, dr, 1, minc, dc, -1);
                    }
                }
        }
        Part1 = antinodes1.Count.ToString(); Part2 = antinodes2.Count.ToString();
    }
}