﻿namespace AoC._2024;

public class Day23(string input) : AoC(input)
{
    public override void Solve()
    {
        Part1 = NoSolution;
        Part2 = NoSolution;
    }
}