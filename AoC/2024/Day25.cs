﻿using System.Collections.Generic;
using System.Linq;

namespace AoC._2024;

public class Day25(string input) : AoC(input)
{
    public override void Solve()
    {
        const int W = 5, H = 7; const char PIN = '#', EMPTY = '.';
        var lines = InputLines.Value; var n = (lines.Length + 1) / (H + 1);
        var keys = new List<int[]>(); var locks = new List<int[]>();
        int[] parseKey(int i)
        {
            var res = new int[W];
            for (var j = 0; j < W; j++)
                for (var k = 1; k <= W; k++)
                    if (lines[(H + 1) * i + k][j] == PIN) { res[j] = W - k + 1; break; }
            return res;
        }
        int[] parseLock(int i)
        {
            var res = new int[W];
            for (var j = 0; j < W; j++)
                for (var k = W; k >= 0; k--)
                    if (lines[(H + 1) * i + k][j] == PIN) { res[j] = k; break; }
            return res;
        }
        for (var i = 0; i < n; i++)
            if (lines[(H + 1) * i][0] == EMPTY)
                keys.Add(parseKey(i));
            else
                locks.Add(parseLock(i));
        bool match(int[] key, int[] @lock) => Enumerable.Range(0, W).All(i => key[i] + @lock[i] <= W);
        Part1 = keys.Sum(key => locks.Sum(@lock => match(key, @lock) ? 1 : 0)).ToString();
        Part2 = NoSolution; // Day 25
    }
}