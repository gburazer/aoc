﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2024;

public class Day20(string input) : AoC(input)
{
    public override void Solve()
    {
        var grid = InputLines.Value.Select(line => line.ToCharArray()).ToArray();
        var m = grid.Length; var n = grid[0].Length;
        (int r, int c) start = (-1, -1), end = (-1, -1);
        for (var i = 0; i < m; i++)
            for (var j = 0; j < n; j++)
            {
                if (grid[i][j] == 'S') start = (i, j);
                if (grid[i][j] == 'E')   end = (i, j);
            }
        int[,] bfs(int targetr, int targetc)
        {
            var res = new int[m, n];
            for (var r = 0; r < m; r++)
                for (var c = 0; c < n; c++)
                    res[r, c] = int.MaxValue;
            var q = new Queue<(int r, int c)>(); res[targetr, targetc] = 0; q.Enqueue((targetr, targetc));
            int[] dr = [-1, 1, 0, 0], dc = [0, 0, -1, 1];
            while (q.Count > 0)
            {
                var (r, c) = q.Dequeue(); var nd = res[r, c] + 1;
                for (var d = 0; d < 4; d++)
                {
                    var (nr, nc) = (r + dr[d], c + dc[d]);
                    if (nr < 0 || nr >= m || nc < 0 || nc >= n || grid[nr][nc] == '#' || res[nr, nc] <= nd) 
                        continue;
                    res[nr, nc] = nd; q.Enqueue((nr, nc));
                }
            }
            return res;
        }
        var distFromS = bfs(start.r, start.c); var distToE = bfs(end.r, end.c);
        var originalTime = distFromS[end.r, end.c];
        int countValidCheats(int cheatDuration)
        {
            var res = 0;
            for (var r1 = 0; r1 < m; r1++)
                for (var c1 = 0; c1 < n; c1++)
                    if (grid[r1][c1] != '#' && distFromS[r1, c1] != int.MaxValue)
                        for (var r2 = 0; r2 < m; r2++)
                            for (var c2 = 0; c2 < n; c2++)
                                if (grid[r2][c2] != '#' && distToE[r2, c2] != int.MaxValue)
                                {
                                    var distance = Math.Abs(r1 - r2) + Math.Abs(c1 - c2);
                                    if (distance > cheatDuration) continue;
                                    var newTime = distFromS[r1, c1] + distance + distToE[r2, c2];
                                    if (originalTime - newTime >= 100)
                                        res++;
                                }
            return res;
        }
        Part1 = countValidCheats(2).ToString(); Part2 = countValidCheats(20).ToString();
    }
}