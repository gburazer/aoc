﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace AoC;

public static class KnotHash
{
    public static string CalcHash(string input)
    {
        var list = Enumerable.Range(0, 256).Select(i => (byte)i).ToArray();
        var lengths = input.ToCharArray().SelectMany(c => Encoding.ASCII.GetBytes(c.ToString()))
            .Concat(new byte[] { 17, 31, 73, 47, 23 })
            .ToList();
        var cur = 0; var skipSize = 0;
        for (var round = 1; round <= 64; round++)
            DoRound(list, lengths, ref cur, ref skipSize);
        for (var i = 0; i < 16; i++)
        {
            list[i] = list[16 * i];
            for (var j = 1; j < 16; j++)
                list[i] ^= list[16 * i + j];
        }
        return Convert.ToHexStringLower(list.Take(16).ToArray());
    }

    public static void DoRound(byte[] list, IList<byte> lengths, ref int cur, ref int skipSize)
    {
        var listSize = list.Length;
        foreach (var length in lengths)
        {
            for (var j = 0; j < length / 2; j++)
            {
                var src = (cur + j) % listSize;
                var dest = cur + length - 1 - j;
                if (dest < 0) dest += listSize;
                dest %= listSize;
                list[src] ^= list[dest]; list[dest] ^= list[src]; list[src] ^= list[dest];
            }
            cur += length + skipSize++; cur %= listSize;
        }
    }
}