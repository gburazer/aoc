﻿using System;
using System.Linq;

namespace AoC._2019;

public class Day04 : AoC
{
    readonly int _from, _to;

    public Day04(string input) : base(input)
    {
        var parts = input.Split('-');
        _from = int.Parse(parts[0]); _to = int.Parse(parts[1]);
    }

    public override void Solve()
    {
        Part1 = Count(IsValid1);
        Part2 = Count(IsValid2);
    }

    string Count(Func<string, bool> isValid) =>
        Enumerable.Range(_from, _to - _from + 1).Count(pwd => isValid(pwd.ToString())).ToString();

    static bool IsValid1(string pwd)
    {
        var isAdjSame = false;
        for (var i = 1; i < pwd.Length; i++)
            if (pwd[i - 1] > pwd[i])
                return false;
            else if (pwd[i - 1] == pwd[i])
                isAdjSame = true;
        return isAdjSame;
    }

    static bool IsValid2(string pwd)
    {
        var is2 = false;
        var repeats = 1;
        for (var i = 1; i < pwd.Length; i++)
            if (pwd[i - 1] > pwd[i])
                return false;
            else if (pwd[i - 1] == pwd[i])
                repeats++;
            else
            {
                if (repeats == 2)
                    is2 = true;
                repeats = 1;
            }

        return is2 || repeats == 2;
    }
}
