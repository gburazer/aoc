﻿using System;
using System.Linq;

namespace AoC._2019;

public class Day07(string input) : AoC(input)
{
  public override void Solve()
  {
     var max = long.MinValue;
     for (var ps1 = 0L; ps1 <= 4; ps1++)
        for (var ps2 = 0L; ps2 <= 4; ps2++)
           if (ps2 != ps1)
              for (var ps3 = 0L; ps3 <= 4; ps3++)
                 if (ps3 != ps2 && ps3 != ps1)
                    for (var ps4 = 0L; ps4 <= 4; ps4++)
                       if (ps4 != ps3 && ps4 != ps2 && ps4 != ps1)
                          for (var ps5 = 0L; ps5 <= 4; ps5++)
                             if (ps5 != ps4 && ps5 != ps3 && ps5 != ps2 && ps5 != ps1)
                                max = Math.Max(max, new Intcode(Input).Run([ps5,
                                   new Intcode(Input)
                                   .Run([ps4,
                                      new Intcode(Input)
                                      .Run([ps3,
                                         new Intcode(Input)
                                         .Run([ps2,
                                            new Intcode(Input)
                                            .Run([ps1, 0])])])])]));
     Part1 = max.ToString();
     Part2 = SolvePart2();
  }

  string SolvePart2()
  {
     var max = long.MinValue;
     for (var p1 = 5L; p1 <= 9; p1++)
        for (var p2 = 5; p2 <= 9; p2++)
           if (p2 != p1)
              for (var p3 = 5L; p3 <= 9; p3++)
                 if (p3 != p2 && p3 != p1)
                    for (var p4 = 5L; p4 <= 9; p4++)
                       if (p4 != p3 && p4 != p2 && p4 != p1)
                          for (var p5 = 5L; p5 <= 9; p5++)
                             if (p5 != p4 && p5 != p3 && p5 != p2 && p5 != p1)
                             {
                                var A = new Intcode(Input);
                                var B = new Intcode(Input);
                                var C = new Intcode(Input);
                                var D = new Intcode(Input);
                                var E = new Intcode(Input);
                                var isHalted = false;
                                var i1 = p1; var i2 = p2; var i3 = p3; var i4 = p4; var i5 = p5;
                                while (!isHalted)
                                {
                                   break; // TODO
                                }
                                //max = Math.Max(max, last);
                             }
     return max.ToString();
  }

  class Intcode(string input)
  {
     public long Run(long[] input)
     {
        var ip = 0L; var inputPos = 0;
        var lastOutput = 0L; var isHalted = false;
        while (!isHalted && ip >= 0 && ip < _mem.Length)
        {
           var m = _mem[ip]; var op = m % 100; var rem = m / 100;
           var isImm = new[] { rem % 10 == 1, rem / 10 % 10 == 1, rem / 100 % 10 == 1 };
           var p1 = new[] { 1L, 2, 4, 5, 6, 7, 8 }.Contains(op) ? _mem[isImm[0] ? ip + 1 : _mem[ip + 1]] : 0;
           var p2 = new[] { 1L, 2, 5, 6, 7, 8 }.Contains(op) ? _mem[isImm[1] ? ip + 2 : _mem[ip + 2]] : 0;
           var skip = 0;
           switch (op)
           {
              case 1: _mem[_mem[ip + 3]] = p1 + p2; skip = 4; break;
              case 2: _mem[_mem[ip + 3]] = p1 * p2; skip = 4; break;
              case 3: _mem[_mem[ip + 1]] = input[inputPos++]; skip = 2; break;
              case 4: lastOutput = p1; skip = 2; break;
              case 5: if (p1 != 0) ip = p2; else skip = 3; break;
              case 6: if (p1 == 0) ip = p2; else skip = 3; break;
              case 7: _mem[_mem[ip + 3]] = p1 < p2 ? 1 : 0; skip = 4; break;
              case 8: _mem[_mem[ip + 3]] = p1 == p2 ? 1 : 0; skip = 4; break;
              case 99: isHalted = true; break;
              default: throw new InvalidOperationException();
           }
           if (!isHalted)
              ip += skip;
        }
        return lastOutput;
     }

     readonly long[] _mem = input.Split(',').Select(long.Parse).ToArray();
  }
}
