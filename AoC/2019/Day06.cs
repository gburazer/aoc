﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2019;

public class Day06 : AoC
{
    readonly Dictionary<string, HashSet<string>> _d1 = [], _d2 = [];

    public Day06(string input) : base(input)
    {
        foreach (var parts in InputLines.Value.Select(l => l.Split(')')))
        {
            static void add(Dictionary<string, HashSet<string>> d, string key, string value)
            {
                if (!d.ContainsKey(key))
                    d[key] = [];
                d[key].Add(value);
            }
            var p1 = parts[0]; var p2 = parts[1];
            add(_d1, p1, p2); add(_d2, p2, p1);
        }
    }

    public override void Solve()
    {
        Part1 = _d1.Keys.Aggregate(0, (res, key) => res + CountAll(key)).ToString();
        Part2 = Shortest(_d2["YOU"].Single(), _d2["SAN"].Single()).ToString();
    }

    int CountAll(string key) => _d1[key]
        .Where(k => _d1.ContainsKey(k))
        .Aggregate(_d1[key].Count, (res, el) => res + CountAll(el));

    readonly HashSet<string> _visited = [];

    int Shortest(string from, string to)
    {
        if (from == to) return 0;
        _visited.Add(from);
        var min = int.MaxValue - 1;
        int visitDict(Dictionary<string, HashSet<string>> d) => d.TryGetValue(from, out var value)
            ? value.Where(k => !_visited.Contains(k)).Select(key => 1 + Shortest(key, to)).Prepend(min).Min()
            : min;
        min = Math.Min(min, visitDict(_d1));
        min = Math.Min(min, visitDict(_d2));
        _visited.Remove(from);
        return min;
    }
}
