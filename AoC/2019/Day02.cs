﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2019;

public class Day02 : AoC
{
   readonly int[] _program, _copy;

  public Day02(string input) : base(input)
  {
     _program = InputLines.Value.Single().Split(',').Select(int.Parse).ToArray();
     _copy = new int[_program.Length];
  }

  public override void Solve()
  {
     Part1 = Run(12, 2).ToString();
     Part2 = SolvePart2(target: 19690720);
  }

  string SolvePart2(int target)
  {
     const int MIN = 0, MAX = 99;
     for (var noun = MIN; noun <= MAX; noun++)
        for (var verb = MIN; verb <= MAX; verb++)
           if (Run(noun, verb) == target)
              return (100 * noun + verb).ToString();
     return NoSolution;
  }

  int Run(int noun, int verb)
  {
     _program.CopyTo(_copy, 0);
     _copy[1] = noun; _copy[2] = verb;
     void exec(int pos, Func<int, int, int> op) =>
        _copy[_copy[pos + 3]] = op(_copy[_copy[pos + 2]], _copy[_copy[pos + 1]]);
     for (var pos = 0; ; pos += 4)
        switch (_copy[pos])
        {
           case 1: exec(pos, (a, b) => a + b); break;
           case 2: exec(pos, (a, b) => a * b); break;
           case 99: return _copy[0];
           default: throw new InvalidOperationException();
        }
  }
}
