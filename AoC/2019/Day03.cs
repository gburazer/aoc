﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2019;

public class Day03 : AoC
{
  public Day03(string input) : base(input)
  {
     static Move[] parseWire(string w) => w.Split(',').Select(Move.Parse).ToArray();
     var lines = InputLines.Value;
     _w1 = parseWire(lines[0]); _w2 = parseWire(lines[1]);
     _grid[(0, 0)] = (Cp, 0, 0);
  }

  public override void Solve()
  {
     Run(_w1, W1);
     Run(_w2, W2);
  }

  void Run(Move[] moves, char w)
  {
     var stepCount = 0;
     (char ch, int s1, int s2) @new(char newch, (char ch, int s1, int s2) cur) =>
        (newch, w == W1 ? stepCount + 1 : cur.s1, w == W2 ? stepCount + 1 : cur.s2);

     static (int r, int c) doStep((int r, int c) pos, char dir) => dir switch
     {
        'R' => (pos.r, pos.c + 1),
        'L' => (pos.r, pos.c - 1),
        'U' => (pos.r - 1, pos.c),
        'D' => (pos.r + 1, pos.c),
        _ => throw new InvalidOperationException()
     };

     var bestp1 = int.MaxValue; var bestp2 = int.MaxValue;
     var (r, c) = (0, 0);

     foreach (var move in moves)
        for (var i = 0; i < move.Steps; i++, stepCount++)
        {
           var (newr, newc) = doStep((r, c), move.Dir);

           if (_grid.TryGetValue((newr, newc), out var cur) && cur.ch != Cp)
           {
              if (w == W2 && cur.ch == W1)
              {
                 var (ch, s1, s2) = @new(Intersection, cur);
                 _grid[(newr, newc)] = (ch, s1, s2);
                 bestp1 = Math.Min(bestp1, Math.Abs(newr) + Math.Abs(newc));
                 bestp2 = Math.Min(bestp2, s1 + s2);
              }
           }
           else
              _grid[(newr, newc)] = @new(w, cur);

           (r, c) = (newr, newc);
        }

     if (w != W2) return;
     Part1 = bestp1.ToString();
     Part2 = bestp2.ToString();
  }

  class Move
  {
     public static Move Parse(string s) => new() { Dir = s[0], Steps = int.Parse(s[1..]) };

     public char Dir;
     public int Steps;
  }

  readonly Move[] _w1, _w2;
  readonly Dictionary<(int r, int c), (char ch, int s1, int s2)> _grid = [];

  const char Cp = 'o', W1 = '1', W2 = '2', Intersection = 'X';
}
