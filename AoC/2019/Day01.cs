﻿using System.Linq;

namespace AoC._2019;

public class Day01 : AoC
{
   readonly int[] _ns;
   public Day01(string input) : base(input) => _ns = InputLines.Value.Select(int.Parse).ToArray();

   public override void Solve()
   {
     static int fuel(int n) => (n / 3) - 2;
     Part1 = _ns.Sum(fuel).ToString();

     static int totalFuel(int n)
     {
        var res = 0;
        while (true)
        {
           var cur = fuel(n);
           if (cur <= 0) break;
           res += cur;
           n = cur;
        }
        return res;
     }
     Part2 = _ns.Sum(totalFuel).ToString();
   }
}
