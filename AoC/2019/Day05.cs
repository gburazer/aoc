﻿using System;
using System.Linq;

namespace AoC._2019;

public class Day05(string input) : AoC(input)
{
  public override void Solve()
  {
     string run(int param) => new Intcode(Input).Run(param).ToString();
     Part1 = run(1); Part2 = run(5);
  }

  class Intcode(string input)
  {
     public int Run(int input)
     {
        var ip = 0;
        var lastOutput = 0; var isHalted = false;
        while (!isHalted && ip >= 0 && ip < _mem.Length)
        {
           var m = _mem[ip]; var op = m % 100; var rem = m / 100;
           var isImm = new[] { rem % 10 == 1, rem / 10 % 10 == 1, rem / 100 % 10 == 1 };
           var p1 = P1Src.Contains(op) ? _mem[isImm[0] ? ip + 1 : _mem[ip + 1]] : 0;
           var p2 = P2Src.Contains(op) ? _mem[isImm[1] ? ip + 2 : _mem[ip + 2]] : 0;
           var skip = 0;
           switch (op)
           {
              case 1: _mem[_mem[ip + 3]] = p1 + p2; skip = 4; break;
              case 2: _mem[_mem[ip + 3]] = p1 * p2; skip = 4; break;
              case 3: _mem[_mem[ip + 1]] = input; skip = 2; break;
              case 4: lastOutput = p1; skip = 2; break;
              case 5: if (p1 != 0) ip = p2; else skip = 3; break;
              case 6: if (p1 == 0) ip = p2; else skip = 3; break;
              case 7: _mem[_mem[ip + 3]] = p1 < p2 ? 1 : 0; skip = 4; break;
              case 8: _mem[_mem[ip + 3]] = p1 == p2 ? 1 : 0; skip = 4; break;
              case 99: isHalted = true; break;
              default: throw new InvalidOperationException();
           }
           if (!isHalted)
              ip += skip;
        }
        return lastOutput;
     }

     readonly int[] _mem = input.Split(',').Select(int.Parse).ToArray();
     static readonly int[] P1Src = [1, 2, 4, 5, 6, 7, 8], P2Src = [1, 2, 5, 6, 7, 8];
  }
}
