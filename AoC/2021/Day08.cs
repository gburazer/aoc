﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2021;

public class Day08(string input) : AoC(input)
{
  public override void Solve()
  {
     var digits = new Dictionary<string, int>
     {
        { "abcefg", 0 },
        { "cf", 1 },
        { "acdeg", 2 },
        { "acdfg", 3 },
        { "bcdf", 4 },
        { "abdfg", 5 },
        { "abdefg", 6 },
        { "acf", 7 },
        { "abcdefg", 8 },
        { "abcdfg", 9 },
     };

     var possibleByLen = new Dictionary<int, HashSet<char>>();
     foreach (var (digit, len) in digits.Keys.Select(d => (new HashSet<char>(d.ToCharArray()), d.Length)))
        if (!possibleByLen.TryGetValue(len, out var value))
           possibleByLen[len] = digit;
        else
           value.UnionWith(digit);

     var chars = Enumerable.Range(0, 7).Select(n => (char)('a' + n)).ToList();

     static string mapped(string s, Dictionary<char, char> map) => new(s.Select(c => map[c]).OrderBy(c => c).ToArray());

     Dictionary<char, char> decode(string[] patterns)
     {
        var possibleByChar = chars.ToDictionary(c => c, _ => new HashSet<char>(chars));
        foreach (var pattern in patterns)
           foreach (var c in pattern)
              possibleByChar[c].IntersectWith(possibleByLen[pattern.Length]);

        const int COUNT = 7;
        var used = new bool[COUNT];
        var res = new Dictionary<char, char>();
        bool rek(int level)
        {
           if (level == COUNT)
           {
              if (patterns.All(p => digits.ContainsKey(mapped(p, res))))
                 return true;
           }
           else
           {
              var source = (char)('a' + level);
              foreach (var c in possibleByChar[source])
              {
                 var ind = c - 'a';
                 if (used[ind]) continue;
                 used[ind] = true;
                 res[source] = c;
                 if (rek(level + 1))
                    return true;
                 used[ind] = false;
              }
           }
           return false;
        }
        rek(0);
        return res;
     }

     var p1Count = 0; var p1Digits = new HashSet<int> { 1, 4, 7, 8 };
     var p2Sum = 0;
     foreach (var line in InputLines.Value)
     {
        var parts = line.Split(" | ");
        var patterns = parts[0].Split(' ').ToArray();
        var decoded = decode(patterns);
        var n = 0;
        foreach (var output in parts[1].Split(' '))
        {
           var digit = digits[mapped(output, decoded)];
           if (p1Digits.Contains(digit))
              p1Count++;
           n = 10 * n + digit;
        }
        p2Sum += n;
     }

     Part1 = p1Count.ToString();
     Part2 = p2Sum.ToString();
  }
}
