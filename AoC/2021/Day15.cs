﻿namespace AoC._2021;

public class Day15(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value; var n = lines.Length;
     var risk = new int[5 * n, 5 * n];
     for (var r = 0; r < n; r++)
        for (var c = 0; c < n; c++)
           risk[r, c] = lines[r][c] - '0';
     for (var i = 0; i < 5; i++)
        for (var j = 0; j < 5; j++)
           for (var r = 0; r < n; r++)
              for (var c = 0; c < n; c++)
                 if (j > 0)
                    risk[i * n + r, j * n + c] = valFrom(risk[i * n + r, (j - 1) * n + c]);
                 else if (i > 0)
                    risk[i * n + r, c] = valFrom(risk[(i - 1) * n + r, c]);
     static int valFrom(int source) => source % 9 + 1;

     int calc(int size)
     {
        var best = new int[size, size];
        for (var r = 0; r < size; r++)
           for (var c = 0; c < size; c++)
              best[r, c] = int.MaxValue / 10;
        best[0, 0] = 0;
        bool improved;
        do
        {
           improved = false;
           for (var r = 0; r < size; r++)
              for (var c = 0; c < size; c++)
                 for (var dr = -1; dr <= 1; dr++)
                    for (var dc = -1; dc <= 1; dc++)
                       if (dr == 0 ^ dc == 0)
                       {
                          var rr = r + dr; var cc = c + dc;
                          if (rr < 0 || rr >= size || cc < 0 || cc >= size) continue;
                          var alt = best[rr, cc] + risk[r, c];
                          if (alt >= best[r, c]) continue;
                          best[r, c] = alt;
                          if (r == size - 1 && c == size - 1)
                             improved = true;
                       }
        } while (improved);
        return best[size - 1, size - 1];
     }

     Part1 = calc(n).ToString();
     Part2 = calc(5 * n).ToString();
  }
}
