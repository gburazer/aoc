﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2021;

public class Day19(string input) : AoC(input)
{
  public override void Solve()
  {
     var scanners = new List<Scanner>();
     Variant variant = null;
     foreach (var line in InputLines.Value)
     {
        if (line.StartsWith("---")) { variant = new(); }
        else if (line == "") addScanner(variant);
        else
        {
           var parts = line.Split(',');
           variant.Add(new(int.Parse(parts[0]), int.Parse(parts[1]), int.Parse(parts[2])));
        }
     }
     addScanner(variant);

     void addScanner(Variant v)
     {
        var s = new Scanner
        {
           v,
           t(v, p => new(-p.x, p.y, p.z)) // +
        };
        // TOO add all transformations (23 added + 1 originally loaded)

        scanners.Add(s);
     }

     Variant t(Variant source, Func<Point, Point> morph)
     {
        var res = new Variant();
        res.UnionWith(source.Select(p => morph(p)));
        return res;
     }

     var world = new HashSet<Point>(scanners.First().First());
     var used = new HashSet<Scanner> { scanners.First() };
     var available = new HashSet<Scanner>(scanners.Skip(1));
     while (available.Count > 0)
     {
        Scanner availableScannerFound = null;
        foreach (var usedScanner in used)
        {
           var usedVariant = usedScanner.First();
           foreach (var availableScanner in available)
           {
              foreach (var availableVariant in availableScanner)
                 if (canMap(usedVariant, availableVariant, out Point d))
                 {
                    availableScannerFound = availableScanner;
                    foreach (var p in availableVariant)
                       world.Add(new(p.x + d.x, p.y + d.y, p.z + d.z));
                    break;
                 }
              if (availableScannerFound != null)
                 break;
           }
        }
        if (availableScannerFound == null) { Console.WriteLine("No scanner maps!"); break; }
        Console.WriteLine("Mapped a scanner!");
        used.Add(availableScannerFound); available.Remove(availableScannerFound);
     }

     bool canMap(Variant known, Variant unknown, out Point d)
     {
        foreach (var source in known)
           foreach (var dest in unknown)
           {
              d = new(source.x - dest.x, source.y - dest.y, source.z - dest.z);
              if (doesMap(known, unknown, d))
                 return true;
           }
        d = new(0, 0, 0);
        return false;
     }

     bool doesMap(Variant known, Variant unknown, Point d)
     {
        var count = 0;
        foreach (var p in unknown)
        {
           if (known.Contains(new(p.x + d.x, p.y + d.y, p.z + d.z)))
              count++;
           if (count == 12)
              return true;
        }
        return false;
     }

     foreach (var p in world)
        Console.WriteLine($"{p.x},{p.y},{p.z}");

     Part1 = world.Count.ToString();
     Part2 = NoSolution;
  }

  struct Point(int x, int y, int z) { public int x = x, y = y, z = z; }
  class Variant : HashSet<Point> { }
  class Scanner : List<Variant> { }
}
