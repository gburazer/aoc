﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2021;

public class Day16(string input) : AoC(input)
{
  public override void Solve()
  {
     var bin = string.Join(string.Empty, Input.Select(c => Convert.ToString(
        Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')));

     var pos = 0; var packet = Packet.Parse(bin, ref pos);

     Part1 = packet.SumVersions().ToString();
     Part2 = packet.Value.ToString();
  }

  class Packet
  {
     public long Value;

     long _version, _typeId;
     List<Packet> _subpackets;

     public static Packet Parse(string bin, ref int pos)
     {
        var res = new Packet
        {
           _version = FromBin(bin, 3, ref pos),
           _typeId = FromBin(bin, 3, ref pos),
           _subpackets = []
        };
        if (res._typeId == 4)
           res.ParseLiteral(bin, ref pos);
        else
        {
           res.ParseSubpackets(bin, ref pos);
           res.CalcValue();
        }
        return res;
     }

     void ParseLiteral(string bin, ref int pos)
     {
        var binValue = new StringBuilder();
        var done = false;
        while (!done)
        {
           binValue.Append(bin.AsSpan(pos + 1, 4));
           done = bin[pos] == '0';
           pos += 5;
        }
        Value = FromBin(binValue.ToString());
     }

     void ParseSubpackets(string bin, ref int pos)
     {
        var lengthTypeId = bin[pos++] - '0';
        var length = (int)FromBin(bin, lengthTypeId == 0 ? 15 : 11, ref pos);
        if (lengthTypeId == 0)
        {
           var subbin = bin.Substring(pos, length);
           var subpos = 0;
           do
           {
              _subpackets.Add(Parse(subbin, ref subpos));
           } while (subpos < length - 1);
           pos += length;
        }
        else
           for (var i = 0; i < length; i++)
              _subpackets.Add(Parse(bin, ref pos));
     }

     void CalcValue()
     {
        Value = _typeId switch
        {
           0 => _subpackets.Sum(sp => sp.Value),
           1 => _subpackets.Aggregate(1L, (res, sp) => res * sp.Value),
           2 => _subpackets.Min(sp => sp.Value),
           3 => _subpackets.Max(sp => sp.Value),
           5 => _subpackets.First().Value > _subpackets.Last().Value ? 1 : 0,
           6 => _subpackets.First().Value < _subpackets.Last().Value ? 1 : 0,
           7 => _subpackets.First().Value == _subpackets.Last().Value ? 1 : 0,
           _ => Value
        };
     }

     static long FromBin(string s, int len, ref int pos)
     {
        var res = FromBin(s.Substring(pos, len));
        pos += len;
        return res;
     }

     static long FromBin(string s) => Convert.ToInt64(s, 2);

     public long SumVersions() => _subpackets.Aggregate(_version, (res, sp) => res + sp.SumVersions());
  }
}
