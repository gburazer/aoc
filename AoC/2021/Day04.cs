﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2021;

public class Day04(string input) : AoC(input)
{
    public override void Solve()
    {
        var lines = InputLines.Value;
        for (var offset = 2; offset < lines.Length; offset += 6)
        {
            var board = new int[N, N];
            for (var row = 0; row < N; row++)
                for (var col = 0; col < N; col++)
                    board[row, col] = int.Parse(lines[offset + row].Substring(col * 3, 2).Trim());
            Boards.Add(board);
            Marked.Add(new bool[N, N]);
        }
        foreach (var n in lines[0].Split(',').Select(int.Parse))
        {
            Mark(n); CheckWinner(n);
            if (_last > 0 && _first == 0)
                _first = _last;
        }
        Part1 = _first.ToString(); Part2 = _last.ToString();
    }

    const int N = 5;
    static readonly List<int[,]> Boards = [];
    static readonly List<bool[,]> Marked = [];
    static readonly HashSet<int> ProcessedIndices = [];
    static int _last, _first; // TODO get rid of static

    static void Mark(int n)
    {
        for (var ind = 0; ind < Boards.Count; ind++)
            if (!ProcessedIndices.Contains(ind))
            {
                for (var row = 0; row < N; row++)
                    for (var col = 0; col < N; col++)
                        if (Boards[ind][row, col] == n)
                            Marked[ind][row, col] = true;
            }
    }

    static void CheckWinner(int n)
    {
        for (var ind = 0; ind < Boards.Count; ind++)
            if (!ProcessedIndices.Contains(ind))
                for (var i = 0; i < N; i++)
                    if (Check(ind, i, -1, n) || Check(ind, -1, i, n))
                    {
                        ProcessedIndices.Add(ind);
                        break;
                    }
    }

    static bool Check(int ind, int row, int col, int n)
    {
        for (var other = 0; other < N; other++)
            if (!Marked[ind][row != -1 ? row : other, col != -1 ? col : other])
                return false;
        _last = n * Enumerable.Range(0, N).Sum(r => Enumerable.Range(0, N)
            .Sum(c => !Marked[ind][r, c] ? Boards[ind][r, c] : 0));
        return true;
    }
}
