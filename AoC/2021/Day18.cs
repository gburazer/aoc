﻿using System;
using System.Linq;
using System.Text;

namespace AoC._2021;

public class Day18(string input) : AoC(input)
{
    public override void Solve()
    {
        var lines = InputLines.Value;
        Part1 = solvePart1(); Part2 = solvePart2();

        string solvePart1()
        {
            var res = new StringBuilder(lines[0]);
            res = lines.Skip(1).Select(l => new StringBuilder(l)).Aggregate(res, add);
            var pos = 0;
            return magnitude(res, ref pos).ToString();
        }

        string solvePart2()
        {
            var max = 0L;
            for (var ind1 = 0; ind1 < lines.Length; ind1++)
                for (var ind2 = 0; ind2 < lines.Length; ind2++)
                    if (ind1 != ind2)
                    {
                        var pos = 0;
                        max = Math.Max(max, 
                            magnitude(add(new StringBuilder(lines[ind1]), new StringBuilder(lines[ind2])), ref pos));
                    }
            return max.ToString();
        }

        //testAll();

        static StringBuilder add(StringBuilder sf1, StringBuilder sf2)
        {
            var res = new StringBuilder($"[{sf1},{sf2}]");
            while (tryReduce(res))
                ;
            return res;
        }

        static bool tryReduce(StringBuilder sf) => tryExplode(sf) || trySplit(sf);

        static bool tryExplode(StringBuilder sf)
        {
            var level = 0;
            for (var pos = 0; pos < sf.Length; pos++)
            {
                if (level == 5)
                {
                    explode(sf, ref pos);
                    return true;
                }
                if (sf[pos] == '[') level++;
                if (sf[pos] == ']') level--;
            }
            return false;
        }

        static void explode(StringBuilder sf, ref int pos)
        {
            var first = parseNumber(sf, ref pos);
            var firstLen = first.ToString().Length;
            match(sf, ',', ref pos);
            var second = parseNumber(sf, ref pos);
            var secondLen = second.ToString().Length;
            match(sf, ']', ref pos);
            var fullLen = firstLen + secondLen + 3; // [x,y]
            pos -= fullLen;

            // replace number to the left with sum
            var toLeft = pos - 1;
            while (toLeft >= 0 && !isDigit(sf[toLeft])) toLeft--;
            while (toLeft >= 0 && isDigit(sf[toLeft])) toLeft--;
            toLeft++;
            if (toLeft >= 0 && isDigit(sf[toLeft]))
            {
                var left = parseNumber(sf, ref toLeft);
                var leftLen = left.ToString().Length;
                var sum = first + left;
                var sumLen = sum.ToString().Length;
                replace(sf, toLeft - leftLen, leftLen, sum.ToString());
                pos += sumLen - leftLen; // compensate for different widths of left and sum
            }

            // replace number to the right with sum
            var toRight = pos + fullLen;
            while (toRight < sf.Length && !isDigit(sf[toRight])) toRight++;
            if (toRight < sf.Length && isDigit(sf[toRight]))
            {
                var right = parseNumber(sf, ref toRight);
                var rightLen = right.ToString().Length;
                replace(sf, toRight - rightLen, rightLen, (second + right).ToString());
            }

            // replace entire pair with zero
            replace(sf, pos, fullLen, "0");
        }

        static bool trySplit(StringBuilder sf)
        {
            for (var pos = 0; pos < sf.Length - 1; pos++)
                if (isDigit(sf[pos]) && isDigit(sf[pos + 1]))
                {
                    split(sf, pos);
                    return true;
                }
            return false;
        }

        static void split(StringBuilder sf, int pos)
        {
            var value = 10 * toInt(sf[pos]) + toInt(sf[pos + 1]);
            var left = value / 2;
            var right = value / 2 + value % 2;
            replace(sf, pos, 2, $"[{left},{right}]");
        }

        static int parseNumber(StringBuilder sb, ref int pos)
        {
            var d1 = toInt(sb[pos++]);
            return isDigit(sb[pos]) ? 10 * d1 + toInt(sb[pos++]) : d1;
        }

        static void match(StringBuilder sb, char ch, ref int pos)
        {
            if (sb[pos] != ch)
                throw new InvalidOperationException($"'{ch}' not matched, got {sb[pos]} instead!");
            pos++;
        }

        static void replace(StringBuilder sb, int pos, int count, string what)
        {
            sb.Remove(pos, count);
            sb.Insert(pos, what);
        }

        static bool isDigit(char ch) => char.IsDigit(ch);
        static int toInt(char ch) => ch - '0';

        static long magnitude(StringBuilder sf, ref int pos)
        {
            if (sf[pos] == '[')
            {
                match(sf, '[', ref pos);
                var left = 3 * magnitude(sf, ref pos);
                match(sf, ',', ref pos);
                var right = 2 * magnitude(sf, ref pos);
                match(sf, ']', ref pos);

                return left + right;
            }
            if (isDigit(sf[pos]))
                return parseNumber(sf, ref pos);
            throw new InvalidOperationException($"Invalid character in {sf} at {pos}: {sf[pos]}");
        }

        /*
        void testAll()
        {
           testExplode("[[[[[9,8],1],2],3],4]", "[[[[0,9],2],3],4]");
           testExplode("[7,[6,[5,[4,[3,2]]]]]", "[7,[6,[5,[7,0]]]]");
           testExplode("[[6,[5,[4,[3,2]]]],1]", "[[6,[5,[7,0]]],3]");
           testExplode("[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]", "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]");
           testExplode("[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]", "[[3,[2,[8,0]]],[9,[5,[7,0]]]]");
           testExplode("[[[[0,7],4],[7,[[8,4],9]]],[1,1]]", "[[[[0,7],4],[15,[0,13]]],[1,1]]");

           testSplit("10", "[5,5]");
           testSplit("11", "[5,6]");
           testSplit("12", "[6,6]");
           testSplit("[34,51]", "[[17,17],51]");

           testAdd("34", new[] { "51" }, "[[[8,9],[8,9]],[[[6,6],[6,7]],[[6,7],[6,7]]]]");
           testAdd("[[[[4,3],4],4],[7,[[8,4],9]]]", new[] { "[1,1]" }, "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]");
           testAdd("[1,1]", new[] { "[2,2]", "[3,3]", "[4,4]" }, "[[[[1,1],[2,2]],[3,3]],[4,4]]");
           testAdd("[1,1]", new[] { "[2,2]", "[3,3]", "[4,4]", "[5,5]" }, "[[[[3,0],[5,3]],[4,4]],[5,5]]");
           testAdd("[1,1]", new[] { "[2,2]", "[3,3]", "[4,4]", "[5,5]", "[6,6]" }, "[[[[5,0],[7,4]],[5,5]],[6,6]]");
           testAdd("[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]", new[]
           {
              "[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]",
              "[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]",
              "[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]",
              "[7,[5,[[3,8],[1,4]]]]",
              "[[2,[2,2]],[8,[8,1]]]",
              "[2,9]",
              "[1,[[[9,3],9],[[9,0],[0,7]]]]",
              "[[[5,[7,4]],7],1]",
              "[[[[4,2],2],6],[8,7]]"
           }, "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]");

           testMagnitude("[9,1]", 29);
           testMagnitude("[1,9]", 21);
           testMagnitude("[[9,1],[1,9]]", 129);
           testMagnitude("[[1,2],[[3,4],5]]", 143);
           testMagnitude("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]", 1384);
           testMagnitude("[[[[1,1],[2,2]],[3,3]],[4,4]]", 445);
           testMagnitude("[[[[3,0],[5,3]],[4,4]],[5,5]]", 791);
           testMagnitude("[[[[5,0],[7,4]],[5,5]],[6,6]]", 1137);
           testMagnitude("[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]", 3488);
           testMagnitude("[[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]", 4140);
        }

        void testExplode(string source, string expected)
        {
           var sf = new StringBuilder(source);
           if (!tryExplode(sf))
              Console.WriteLine($"Didn't explode: {source}");
           test(sf, expected);
        }

        void testSplit(string source, string expected)
        {
           var sf = new StringBuilder(source);
           if (!trySplit(sf))
              Console.WriteLine($"Didn't split: {source}");
           test(sf, expected);
        }

        void testAdd(string source, string[] others, string expected)
        {
           var sf = new StringBuilder(source);
           foreach (var other in others.Select(o => new StringBuilder(o)))
              sf = add(sf, other);
           test(sf, expected);
        }

        void test(StringBuilder sf, string expected)
        {
           var actual = sf.ToString();
           if (actual != expected)
              Console.WriteLine($"{actual} != {expected}");
        }

        static void testMagnitude(string sf, long expected)
        {
           int pos = 0;
           var m = magnitude(new StringBuilder(sf), ref pos);
           if (m != expected)
              Console.WriteLine($"Invalid magnitude of {sf} ({m} != {expected}).");
        }
        */
    }
}
