﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2021;

public class Day13(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value;
     var lineInd = 0;
     var coords = new HashSet<(int x, int y)>();
     for (; lines[lineInd] != ""; lineInd++)
     {
        var c = lines[lineInd].Split(',').Select(int.Parse).ToArray();
        coords.Add((c[0], c[1]));
     }

     var p1 = 0;
     for (lineInd++; lineInd < lines.Length; lineInd++)
     {
        var parts = lines[lineInd].Split(' ')[2].Split('=');
        fold(parts[0].Single() == 'x', int.Parse(parts[1]));
        if (p1 == 0) p1 = coords.Count;
     }
     display();
     var p2 = "FJAHJGAH"; // TODO: "OCR" :)

     void display()
     {
        var maxx = coords.Max(c => c.x) + 1; var maxy = coords.Max(c => c.y) + 1;
        for (var y = 0; y < maxy; y++)
        {
           for (var x = 0; x < maxx; x++)
              Console.Write(coords.Contains((x, y)) ? '#' : ' ');
           Console.WriteLine();
        }
     }

     void fold(bool isX, int amount)
     {
        var toAdd = new HashSet<(int, int)>();
        foreach (var (x, y) in coords.Where(c => isX ? c.x > amount : c.y > amount))
           toAdd.Add((isX ? amount - (x - amount) : x, !isX ? amount - (y - amount) : y));
        coords.RemoveWhere(c => isX ? c.x > amount : c.y > amount);
        coords.UnionWith(toAdd);
     }

     Part1 = p1.ToString(); Part2 = p2;
  }
}
