﻿using System;
using System.Linq;

namespace AoC._2021;

public class Day07(string input) : AoC(input)
{
  public override void Solve()
  {
     var ns = InputLines.Value.Single().Split(',').Select(int.Parse).ToArray();
     var min1 = int.MaxValue; var min2 = int.MaxValue; var max = ns.Max();
     for (var pos = ns.Min(); pos <= max; pos++)
     {
        var sum1 = 0; var sum2 = 0;
        foreach (var n in ns)
        {
           var d = Math.Abs(pos - n);
           sum1 += d; sum2 += d * (d + 1) / 2;
        }
        if (sum1 < min1) min1 = sum1;
        if (sum2 < min2) min2 = sum2;
     }
     Part1 = min1.ToString(); Part2 = min2.ToString();
  }
}
