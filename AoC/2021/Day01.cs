﻿using System.Linq;

namespace AoC._2021;

public class Day01(string input) : AoC(input)
{
  public override void Solve()
  {
     var prev2 = int.MaxValue / 3; var prev1 = prev2;
     var a = int.MaxValue;
     var p1 = 0; var p2 = 0;
     foreach (var n in InputLines.Value.Select(int.Parse))
     {
        if (n > prev1) p1++;

        var b = n + prev1 + prev2;
        if (b > a) p2++;

        prev2 = prev1; prev1 = n; a = b;
     }
     Part1 = p1.ToString(); Part2 = p2.ToString();
  }
}
