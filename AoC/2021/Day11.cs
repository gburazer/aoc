﻿using System;

namespace AoC._2021;

public class Day11(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value;
     const int N = 10, FLASHED = -1;
     var state = new int[N, N];
     forEach((r, c) => state[r, c] = lines[r][c] - '0');
     var p1 = 0; var p2 = 0;
     var step = 1;
     do
     {
        var p2Done = true;
        forEach((r, c) => state[r, c]++);
        forEach(flash);
        forEach((r, c) =>
        {
           if (state[r, c] == FLASHED)
              state[r, c] = 0;
           else
              p2Done = false;
        });
        if (p2Done)
           p2 = step;
        step++;
     } while (p2 == 0);

     void forEach(Action<int, int> action)
     {
        for (var r = 0; r < N; r++)
           for (var c = 0; c < N; c++)
              action(r, c);
     }

     void flash(int r, int c)
     {
        if (state[r, c] <= 9) return;
        state[r, c] = FLASHED;
        if (step <= 100) p1++;
        for (var dr = -1; dr <= 1; dr++)
           for (var dc = -1; dc <= 1; dc++)
           {
              var nr = r + dr; var nc = c + dc;
              if (!(dr == 0 && dc == 0)
                 && nr >= 0 && nr < N && nc >= 0 && nc < N
                 && state[nr, nc] != FLASHED)
              {
                 state[nr, nc]++;
                 flash(nr, nc);
              }
           }
     }

     Part1 = p1.ToString(); Part2 = p2.ToString();
  }
}
