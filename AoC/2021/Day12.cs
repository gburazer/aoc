﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2021;

public class Day12(string input) : AoC(input)
{
  public override void Solve()
  {
     const string START = "start", END = "end";
     var map = new Dictionary<string, List<string>>();
     foreach (var parts in InputLines.Value.Select(l => l.Split('-')))
     {
        add(parts[0], parts[1]);
        if (parts[0] != START)
           add(parts[1], parts[0]);
     }
     void add(string from, string to)
     {
        if (!map.ContainsKey(from))
           map[from] = [];
        map[from].Add(to);
     }

     var pathsFound = new HashSet<string>();

     traverse();
     var p1 = pathsFound.Count;

     foreach (var single in allLower())
        traverse(single, 2);
     var p2 = pathsFound.Count;

     void traverse(string single = "", int maxCount = 1)
     {
        var q = new Queue<(string, string)>([(START, START)]);
        while (q.Count > 0)
        {
           var (cur, path) = q.Dequeue();
           if (cur == END)
              pathsFound.Add(path);
           else if (map.TryGetValue(cur, out var value))
           {
              var all = path.Split(' ');
              foreach (var next in value)
              {
                 var count = all.Count(s => s == next);
                 if (isUpper(next) || next == END || count == 0 || (next == single && count < maxCount))
                    q.Enqueue((next, $"{path} {next}"));
              }
           }
        }
     }

     bool isUpper(string s) => s.All(char.IsUpper);
     IEnumerable<string> allLower() => map.Keys.Where(key => !isUpper(key) && key != START && key != END);

     Part1 = p1.ToString(); Part2 = p2.ToString();
  }
}
