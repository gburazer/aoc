﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2021;

public class Day14(string input) : AoC(input)
{
  public override void Solve()
  {
     var t = InputLines.Value[0].Select(ctoi).ToArray();
     const int N = 26;
     var rules = new int[N * N];
     var allRules = new List<int>();
     foreach (var (key, value) in InputLines.Value.Skip(2)
        .Select(l => l.Split(" -> ")).Select(parts => (key: parts[0], value: parts[1])))
     {
        var rule = ctoi(key[0]) * N + ctoi(key[1]);
        rules[rule] = ctoi(value.Single());
        allRules.Add(rule);
     }
     static int ctoi(char c) => c - 'A' + 1;
     var cur = new long[N * N];
     for (var i = 0; i < t.Length - 1; i++)
        cur[t[i] * N + t[i + 1]]++;
     var p1 = 0L;
     for (var step = 1; step <= 40; step++)
     {
        var next = new long[N * N];
        foreach (var rule in allRules)
        {
           next[rule / N * N + rules[rule]] += cur[rule];
           next[rules[rule] * N + rule % N] += cur[rule];
        }
        Array.Copy(next, cur, next.Length);
        if (step == 10) p1 = count();
     }
     long count()
     {
        var counts = new long[N];
        for (var i = 0; i < N * N; i++)
           counts[i / N] += cur[i];
        counts[t.Last()]++;
        var res = counts.Where(n => n > 0).OrderByDescending(n => n).ToList();
        return res[0] - res[^1];
     }
     Part1 = p1.ToString(); Part2 = count().ToString();
  }
}
