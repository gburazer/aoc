﻿using System;
using System.Linq;

namespace AoC._2021;

public class Day05(string input) : AoC(input)
{
  public override void Solve()
  {
     const int MAX = 1000;
     var coverNoDiags = new int[MAX, MAX]; var cover = new int[MAX, MAX];
     var sumNoDiags = 0; var sum = 0;
     static int calcd(int dist, int first, int second) => dist == 0 ? 0 : first < second ? 1 : -1;
     static ((int x, int y) p1, (int x, int y) p2) parseLine(string line)
     {
        static (int, int) parse(string s)
        {
           var parts = s.Split(',');
           return (int.Parse(parts[0]), int.Parse(parts[1]));
        }
        var parts = line.Split(" -> ");
        return (parse(parts[0]), parse(parts[1]));
     }
     foreach (var ((x1, y1), (x2, y2)) in InputLines.Value.Select(parseLine))
     {
        var distx = Math.Abs(x2 - x1); var disty = Math.Abs(y2 - y1);
        var dx = calcd(distx, x1, x2); var dy = calcd(disty, y1, y2);
        var posx = x1; var posy = y1;
        for (var i = 0; i <= Math.Max(distx, disty); i++, posx += dx, posy += dy)
        {
           if (distx == 0 || disty == 0)
           {
              coverNoDiags[posx, posy]++;
              if (coverNoDiags[posx, posy] == 2) sumNoDiags++;
           }
           cover[posx, posy]++;
           if (cover[posx, posy] == 2) sum++;
        }
     }
     Part1 = sumNoDiags.ToString(); Part2 = sum.ToString();
  }
}
