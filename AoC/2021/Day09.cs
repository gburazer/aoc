﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2021;

public class Day09(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value;
     var rc = lines.Length; var cc = lines[0].Length;
     var field = new int[rc, cc];
     for (var r = 0; r < rc; r++)
        for (var c = 0; c < cc; c++)
           field[r, c] = lines[r][c] - '0';

     var p1 = 0;
     var colored = new bool[rc, cc]; var colorCount = new List<int>();
     for (var r = 0; r < rc; r++)
        for (var c = 0; c < cc; c++)
           if (isLowPoint(r, c))
           {
              p1 += field[r, c] + 1;
              colorCount.Add(color(r, c));
           }

     bool isLowPoint(int r, int c) => !(
        (r > 0 && check(r, c, -1, 0)) ||
        (r < rc - 1 && check(r, c, 1, 0)) ||
        (c > 0 && check(r, c, 0, -1)) ||
        (c < cc - 1 && check(r, c, 0, 1)));
     bool check(int r, int c, int dr, int dc) => field[r + dr, c + dc] <= field[r, c];

     int color(int r, int c)
     {
        var res = 0;
        var q = new Queue<(int r, int c)>(); q.Enqueue((r, c));
        while (q.Count > 0)
        {
           var cur = q.Dequeue();
           if (colored[cur.r, cur.c])
              continue;
           colored[cur.r, cur.c] = true;
           res++;
           if (cur.r > 0) colorCheck(cur.r - 1, cur.c);
           if (cur.r < rc - 1) colorCheck(cur.r + 1, cur.c);
           if (cur.c > 0) colorCheck(cur.r, cur.c - 1);
           if (cur.c < cc - 1) colorCheck(cur.r, cur.c + 1);
        }
        void colorCheck(int newr, int newc)
        {
           if (field[newr, newc] != 9 && !colored[newr, newc])
              q.Enqueue((newr, newc));
        }
        return res;
     }

     Part1 = p1.ToString();
     Part2 = colorCount.OrderByDescending(n => n).Take(3).Aggregate(1, (res, n) => res * n).ToString();
  }
}
