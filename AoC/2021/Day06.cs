﻿using System;
using System.Linq;

namespace AoC._2021;

public class Day06(string input) : AoC(input)
{
  public override void Solve()
  {
     const int MAX = 9;
     var curState = new long[MAX]; var newState = new long[MAX];
     foreach (var val in Input.Split(',').Select(int.Parse))
        curState[val]++;

     string sum() => curState.Sum().ToString();

     for (var i = 0; i < 256; i++)
     {
        if (i == 80) Part1 = sum();
        var zeros = curState[0];
        for (var ind = 1; ind < MAX; ind++)
           newState[ind - 1] = curState[ind];
        newState[6] += zeros; newState[MAX - 1] = zeros;
        Array.Copy(newState, curState, MAX);
     }

     Part2 = sum();
  }
}
