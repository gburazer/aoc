﻿using System.Linq;

namespace AoC._2021;

public class Day02(string input) : AoC(input)
{
  public override void Solve()
  {
     var x = 0; var y = 0; var aim = 0;
     foreach (var (dir, amount) in InputLines.Value.Select(l => l.Split(' ')).Select(p => (p[0], int.Parse(p[1]))))
     {
        switch (dir)
        {
           case "forward": x += amount; y += aim * amount; break;
           case "down": aim += amount; break;
           case "up": aim -= amount; break;
        }
     }
     Part1 = (x * aim).ToString();
     Part2 = (x * y).ToString();
  }
}
