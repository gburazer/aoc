﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2021;

public class Day03(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value; var lineLen = lines[0].Length;
     var gamma = new StringBuilder(); var epsilon = new StringBuilder();
     for (var ind = 0; ind < lineLen; ind++)
     {
        var (gbit, ebit) = MostAndLeast(ind, lines);
        gamma.Append(gbit); epsilon.Append(ebit);
     }
     Part1 = (ToDec(gamma.ToString()) * ToDec(epsilon.ToString())).ToString();
     Part2 = (SingleRemaining(true, lines) * SingleRemaining(false, lines)).ToString();
  }

  static int ToDec(string s) => Convert.ToInt32(s, 2);

  static (char, char) MostAndLeast(int ind, IEnumerable<string> arr)
  {
     var zeros = 0; var ones = 0;
     foreach (var line in arr)
        if (line[ind] == '0')
           zeros++;
        else
           ones++;
     var moreOnes = zeros <= ones;
     return (moreOnes ? '1' : '0', moreOnes ? '0' : '1');
  }

  static int SingleRemaining(bool most, IEnumerable<string> arr)
  {
     var remaining = new HashSet<string>(arr);
     for (var ind = 0; remaining.Count > 1; ind++)
     {
        var (m, l) = MostAndLeast(ind, remaining);
        remaining.RemoveWhere(r => r[ind] != (most ? m : l));
     }
     return ToDec(remaining.Single());
  }
}
