﻿using System.Linq;
using System.Numerics;
using System.Collections.Generic;

namespace AoC._2021;

public class Day10(string input) : AoC(input)
{
  public override void Solve()
  {
     var p1 = 0; var stack = new Stack<char>(); var scores = new List<BigInteger>();
     foreach (var line in InputLines.Value)
     {
        var err = false;
        foreach (var c in line)
        {
           switch (c)
           {
              case ')': if (stack.Pop() != '(') { err = true; p1 += 3; } break;
              case ']': if (stack.Pop() != '[') { err = true; p1 += 57; } break;
              case '}': if (stack.Pop() != '{') { err = true; p1 += 1197; } break;
              case '>': if (stack.Pop() != '<') { err = true; p1 += 25137; } break;
              default: stack.Push(c); break;
           }
           if (err)
              break;
        }
        if (err || stack.Count == 0) continue;
        var score = new BigInteger();
        while (stack.Count > 0)
        {
           score *= 5;
           switch (stack.Pop())
           {
              case '(': score += 1; break;
              case '[': score += 2; break;
              case '{': score += 3; break;
              case '<': score += 4; break;
           }
        }
        scores.Add(score);
     }
     Part1 = p1.ToString();
     Part2 = scores.OrderBy(s => s).Skip(scores.Count / 2).First().ToString();
  }
}
