﻿using System;
using System.Linq;

namespace AoC._2021;

public class Day17(string input) : AoC(input)
{
  public override void Solve()
  {
     var parts = string.Join(' ', Input.Split(' ').Skip(2))
        .Split(", ").Select(part => part.Split('=').Last()).ToArray();
     var (minx, maxx) = extract(parts[0]); var (miny, maxy) = extract(parts[1]);
     static (int min, int max) extract(string s)
     {
        var parts = s.Split("..");
        return (int.Parse(parts[0]), int.Parse(parts[1]));
     }

     const int MIN_STEPS = 120, BOUND_COEFF = 12; // TODO replace with properly calculated boundaries
     var p1 = 0; var p2 = 0;
     var boundx = BOUND_COEFF * Math.Abs(maxx - minx); var boundy = BOUND_COEFF * Math.Abs(maxy - miny);
     for (var vx = -boundx; vx <= boundx; vx++)
        for (var vy = -boundy; vy <= boundy; vy++)
        {
           var (hit, hiy) = fire(vx, vy);
           if (hit) { p1 = Math.Max(p1, hiy); p2++; }
        }

     (bool hit, int hiy) fire(int vx, int vy)
     {
        var x = 0; var y = 0;
        var (hit, hiy) = (false, int.MinValue);
        int d1(int some, int min, int max) => Math.Abs(some < min ? min - some : some > max ? max - some : 0);
        int d(int somex, int somey) => d1(somex, minx, maxx) + d1(somey, miny, maxy);
        for (var stepCount = 0; !hit; stepCount++)
        {
           var prevd = d(x, y);
           x += vx; y += vy; vx += vx == 0 ? 0 : vx < 0 ? 1 : -1; vy--;
           var curd = d(x, y);
           if (stepCount >= MIN_STEPS && curd > prevd)
              break;
           (hit, hiy) = (curd == 0, Math.Max(hiy, y));
        }
        return (hit, hiy);
     }

     Part1 = p1.ToString(); Part2 = p2.ToString();
  }
}
