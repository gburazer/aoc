﻿using System;
using System.Collections.Generic;

namespace AoC;

public class CircularList<T> : LinkedList<T>
{
  public LinkedListNode<T> Next(LinkedListNode<T> node) => node.Next ?? First;
  public LinkedListNode<T> Previous(LinkedListNode<T> node) => node.Previous ?? Last;

  public void Add(T value) { AddLast(value); }
  public LinkedListNode<T> Advance(LinkedListNode<T> node, long steps)
  {
     bool sign = steps > 0;
     steps = Math.Abs(steps) % Count;
     for (var i = 0; i < steps; i++)
        node = sign ? Next(node) : Previous(node);
     return node;
  }
}
