﻿using System;
using System.Text;
using System.Security.Cryptography;
using System.Runtime.InteropServices;

namespace AoC;

public class Md5Generator
{
    public string Md5Hex(string prefix, int n) =>
        ByteArrayToHexViaLookup32UnsafeDirect(_md5.ComputeHash(Encoding.ASCII.GetBytes(prefix + n)));

    readonly MD5 _md5 = MD5.Create();

    static readonly uint[] Lookup32Unsafe = CreateLookup32Unsafe();
    static readonly unsafe uint* Lookup32UnsafeP =
        (uint*)GCHandle.Alloc(Lookup32Unsafe, GCHandleType.Pinned).AddrOfPinnedObject();

    static uint[] CreateLookup32Unsafe()
    {
        var result = new uint[256];
        for (var i = 0; i < 256; i++)
        {
            var s = i.ToString("X2");
            result[i] = BitConverter.IsLittleEndian ? s[0] + ((uint)s[1] << 16) : s[1] + ((uint)s[0] << 16);
        }
        return result;
    }

    // https://stackoverflow.com/questions/311165/how-do-you-convert-a-byte-array-to-a-hexadecimal-string-and-vice-versa/24343727#24343727
    static unsafe string ByteArrayToHexViaLookup32UnsafeDirect(byte[] bytes)
    {
        var lookupP = Lookup32UnsafeP;
        var result = new string((char)0, bytes.Length * 2);
        fixed (byte* bytesP = bytes)
        fixed (char* resultP = result)
        {
            var resultP2 = (uint*)resultP;
            for (var i = 0; i < bytes.Length; i++)
                resultP2[i] = lookupP[bytesP[i]];
        }
        return result;
    }
}