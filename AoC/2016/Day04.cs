﻿using System.Text;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2016;

public class Day04(string input) : AoC(input)
{
  public override void Solve()
  {
     var reals = ParseAndCalc().Where(p => p.isReal).ToList();

     Part1 = reals.Sum(p => p.id).ToString();
     Part2 = reals.Single(p => p.name.Contains("northpole")).id.ToString();
  }

  (bool isReal, int id, string name)[] ParseAndCalc()
  {
     var lines = InputLines.Value;
     var res = new (bool isReal, int id, string name)[lines.Length];
     for (var lineInd = 0; lineInd < lines.Length; lineInd++)
     {
        var parts = lines[lineInd].Split('-');
        var lastPartSplit = parts[^1].Split('[');
        var id = int.Parse(lastPartSplit[0]);
        res[lineInd].id = id;
        var checksum = lastPartSplit[1][0..^1];

        var letters = new Dictionary<char, int>();
        var name = new StringBuilder();
        static char decode(char l, int id) => l == '-'
           ? ' '
           : (char)('a' + ((l - 'a' + id) % 26));
        static void addLetter(Dictionary<char, int> d, char l)
        {
           if (d.TryGetValue(l, out var value))
              d[l] = ++value;
           else
              d[l] = 1;
        }
        for (var i = 0; i < parts.Length - 1; i++)
        {
           if (i > 0)
              name.Append(' ');
           foreach (var l in parts[i])
           {
              addLetter(letters, l);
              name.Append(decode(l, id));
           }
        }
        res[lineInd].name = name.ToString();

        var sorted = letters.Keys.OrderByDescending(c => (letters[c] * 1000) - c);
        var cmp = new string(sorted.Take(5).ToArray());
        res[lineInd].isReal = cmp == checksum;
     }
     return res;
  }
}
