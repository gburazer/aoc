﻿using System;
using System.Linq;

namespace AoC._2016;

public class Day03(string input) : AoC(input)
{
  public override void Solve()
  {
     _triplets = InputLines.Value;
     _matrix = new int[_triplets.Length][];

     Part1 = SolvePart1();
     Part2 = SolvePart2();
  }

  string[] _triplets;
  int[][] _matrix;

  string SolvePart1()
  {
     var sol = 0;
     for (var i = 0; i < _matrix.Length; i++)
     {
        _matrix[i] = _triplets[i].Split(' ').Select(int.Parse).ToArray();
        if (IsTriangle(_matrix[i]))
           sol++;
     }
     return sol.ToString();
  }

  string SolvePart2()
  {
     var sol = 0;
     for (var i = 0; i < _matrix.Length / 3; i++)
        for (var j = 0; j < 3; j++)
           if (IsTriangle([_matrix[3 * i][j], _matrix[(3 * i) + 1][j], _matrix[(3 * i) + 2][j]]))
              sol++;
     return sol.ToString();
  }

  static bool IsTriangle(int[] v)
  {
     static (int min, int mid, int max) sort3(int[] v)
     {
        var min = Math.Min(v[0], Math.Min(v[1], v[2]));
        var max = Math.Max(v[0], Math.Max(v[1], v[2]));
        return (min, v[0] + v[1] + v[2] - min - max, max);
     }
     var (min, mid, max) = sort3(v);
     return min + mid > max;
  }
}
