﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2016;

public class Day11(string input) : AoC(input)
{
  public override void Solve()
  {
     return; // TODO
     /*
     _q.Enqueue(ParseInput());
     while (_q.Count > 0)
     {
        var state = _q.Dequeue();
        if (IsSolution(state))
        {
           var p1 = 0;
           for (; state.From != null; state = state.From)
              p1++;
           Part1 = p1.ToString();
           break;
        }
        MarkVisited(state);
        foreach (var newState in GenerateAllNew(state).Where(newState => !IsVisited(newState)))
           _q.Enqueue(newState);
     }

     Part2 = "bar";
     */
  }

  class Element
  {
     public string Name;
     public bool IsGenerator;
  };

  class State : List<HashSet<Element>>
  {
     public State() : base(Enumerable.Range(0, 5).Select(i => new HashSet<Element>()).ToList()) { }

     public int ElevatorFloor;
     public State From;
  }

  readonly Queue<State> _q = new();
  readonly List<string> _names = new();
  readonly HashSet<int> _visitedHashes = new();

  State ParseInput()
  {
     var res = new State { ElevatorFloor = 1, From = null };

     for (var floor = 1; floor <= 3; floor++)
     {
        var parts = InputLines.Value[floor - 1]
            .Replace(",", "").Replace(".", "")
            .Split(" ")
            .Skip(4)
            .Where(p => p is not "a" and not "and")
            .ToArray();

        for (var pair = 0; pair < parts.Length; pair++)
        {
           var isGenerator = parts[(2 * pair) + 1] == "generator";
           var name = isGenerator ? parts[2 * pair] : parts[2 * pair].Split("-")[0];

           res[floor].Add(new Element { Name = name, IsGenerator = isGenerator });

           _names.Add(name);
        }
     }

     return res;
  }

  bool IsSolution(State state) => state[4].Count == 2 * _names.Count;

  void MarkVisited(State state)
  {
     _visitedHashes.Add(CalcHashCode(state));
  }

  int CalcHashCode(State state)
  {
     var res = 0;
     for (var floor = 0; floor <= 4; floor++)
        foreach (var element in state[floor])
           res += (floor * 1000000) + (_names.IndexOf(element.Name) * 1000) + (element.IsGenerator ? 100 : 200);
     return res;
  }

  bool IsVisited(State state) => _visitedHashes.Contains(CalcHashCode(state));

  static IEnumerable<State> GenerateAllNew(State state)
  {
     _ = new State { From = state }; // TODO var newState = ..
     // do every possible exchange between elevator and current floor
     // elevator cant be empty
     // 
     // move elevator up or down
     // do every possible exchange between elevator and current floor
     return null; // TODO
  }
}
