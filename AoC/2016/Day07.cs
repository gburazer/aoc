﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2016;

public class Day07(string input) : AoC(input)
{
  public override void Solve()
  {
     var supportTls = 0; var supportSsl = 0;
     foreach (var line in InputLines.Value)
     {
        var (supportsTls, supportsSsl) = ScanLine(line);
        if (supportsSsl) supportSsl++;
        if (supportsTls) supportTls++;
     }
     Part1 = supportTls.ToString();
     Part2 = supportSsl.ToString();
  }

  static (bool supportsTls, bool supportsSsl) ScanLine(string s) // TODO use regex
  {
     var brackets = 0;
     var supportsTls = false;
     var tlsCanceled = false;
     HashSet<(char a, char b)> abas = [], babs = [];
     for (var i = 0; i < s.Length - 2; i++)
     {
        if (s[i] == '[') brackets++;
        else if (s[i] == ']') brackets--;
        else
        {
           if (i < s.Length - 3 && s[i] == s[i + 3] && s[i + 1] == s[i + 2] && s[i] != s[i + 1])
              if (brackets > 0)
                 tlsCanceled = true;
              else
                 supportsTls = true;
           if (s[i] != s[i + 2] || s[i] == s[i + 1]) continue;
           if (brackets == 0)
              abas.Add((s[i], s[i + 1]));
           else
              babs.Add((s[i + 1], s[i]));
        }
     }
     return (supportsTls && !tlsCanceled, abas.Intersect(babs).Any());
  }
}
