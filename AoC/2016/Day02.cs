﻿using System;
using System.Text;

namespace AoC._2016;

public class Day02(string input) : AoC(input)
{
  public override void Solve()
  {
     Part1 = Solve(dial:
     [
            "123",
            "456",
            "789"
     ], x: 1, y: 1,
     d: (_, _, _) => true,
     r: (_, _, _) => true,
     u: (_, _, _) => true,
     l: (_, _, _) => true);

     Part2 = Solve(dial:
     [
            "00100",
            "02340",
            "56789",
            "0ABC0",
            "00D00"
     ], x: 0, y: 2,
     d: (dial, x, y) => y <= 3 && dial[y + 1].ToCharArray()[x] != '0',
     r: (dial, x, y) => x <= 3 && dial[y].ToCharArray()[x + 1] != '0',
     u: (dial, x, y) => y >= 1 && dial[y - 1].ToCharArray()[x] != '0',
     l: (dial, x, y) => x >= 1 && dial[y].ToCharArray()[x - 1] != '0');
  }

  string Solve(string[] dial, int x, int y,
      Func<string[], int, int, bool> d,
      Func<string[], int, int, bool> r,
      Func<string[], int, int, bool> u,
      Func<string[], int, int, bool> l)
  {
     var sol = new StringBuilder();
     foreach (var walk in InputLines.Value)
     {
        foreach (var step in walk)
        {
           switch (step)
           {
              case 'D': if (d(dial, x, y)) y++; break;
              case 'R': if (r(dial, x, y)) x++; break;
              case 'U': if (u(dial, x, y)) y--; break;
              case 'L': if (l(dial, x, y)) x--; break;
           }
           x = Math.Min(Math.Max(x, 0), dial.Length - 1);
           y = Math.Min(Math.Max(y, 0), dial.Length - 1);
        }
        sol.Append(dial[y].ToCharArray()[x]);
     }
     return sol.ToString();
  }
}
