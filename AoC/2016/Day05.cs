﻿using System;
using System.Linq;

namespace AoC._2016;

public class Day05(string input) : AoC(input)
{
  public override void Solve()
  {
     Part1 = Fill((hex, filledCount, res) =>
     {
        res[filledCount] = hex[Prefix.Length];
        return true;
     });

     Part2 = Fill((hex, _, res) =>
     {
        if (int.TryParse(hex.AsSpan(Prefix.Length, 1), out var pos)
               && pos < N
               && res[pos] == ' ')
        {
           res[pos] = hex[Prefix.Length + 1];

           return true;
        }

        return false;
     });
  }

  const string Prefix = "00000";
  const int N = 8;
  readonly Md5Generator _md5Generator = new();

  string Fill(Func<string, int, char[], bool> processHash)
  {
     var res = Enumerable.Repeat(' ', N).ToArray();
     var n = 0; var filledCount = 0;
     while (filledCount < N)
     {
        var hex = _md5Generator.Md5Hex(Input, ++n);
        if (hex.StartsWith(Prefix) && processHash(hex, filledCount, res))
           filledCount++;
     }
     return new string(res).ToLower();
  }
}
