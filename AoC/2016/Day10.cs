﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2016;

public class Day10(string input) : AoC(input)
{
  public override void Solve()
  {
     ParseInput();
     Init();
     ProcessAll();

     Part1 = _targetBot.ToString();
     Part2 = (_outputs[0] * _outputs[1] * _outputs[2]).ToString();
  }

  void ParseInput()
  {
     _instructions = []; _maxBot = 0; _maxOutput = 0;
     foreach (var @in in InputLines.Value.Select(l => l.Split(' ')))
        switch (@in[0])
        {
           case "value": _instructions.Add(ParseValue(@in)); break;
           case "bot": _instructions.Add(ParseBot(@in)); break;
        }
  }

  List<(int value, int bot, (int val, bool isOutput) lo, (int val, bool isOutput) hi)> _instructions;
  int _maxBot, _maxOutput;

  (int, int, (int, bool), (int, bool)) ParseValue(string[] @in)
  {
     var value = int.Parse(@in[1]);
     var bot = GetBot(@in[5]);
     return (value, bot, (0, false), (0, false));
  }

  int GetBot(string s)
  {
     var bot = int.Parse(s);
     if (bot > _maxBot)
        _maxBot = bot;
     return bot;
  }

  (int, int, (int, bool), (int, bool)) ParseBot(string[] @in)
  {
     var bot = GetBot(@in[1]);
     var lo = GetLoHi(@in[5], @in[6]);
     var hi = GetLoHi(@in[10], @in[11]);
     return (0, bot, lo, hi);
  }

  (int val, bool isOutput) GetLoHi(string output, string num)
  {
     var isOutput = output == "output";
     var loHi = int.Parse(num);
     if (isOutput && loHi > _maxOutput)
        _maxOutput = loHi;
     return (loHi, isOutput);
  }

  void Init()
  {
     _outputs = new int[_maxOutput + 1];
     _bots = new int[_maxBot + 1, 2];
     _rules = new ((int val, bool isOutput) lo, (int val, bool isOutput) hi)[_maxBot + 1];

     foreach (var (value, bot, lo, hi) in _instructions)
        if (value > 0)
           SetBotValue(bot, value);
        else
           _rules[bot] = (lo, hi);

     _targetBot = -1;
  }

  int[] _outputs;
  int[,] _bots;
  ((int val, bool isOutput) lo, (int val, bool isOutput) hi)[] _rules;

  void SetBotValue(int bot, int value)
  {
     var ind = _bots[bot, 0] > 0 ? 1 : 0;
     _bots[bot, ind] = value;
  }

  int _targetBot;

  // do main processing - while there are candidates (bots with 2 values), process them
  // when value needs to be given to bot with "full hands", it needs to be processed first
  void ProcessAll()
  {
     while (true)
     {
        var next = FindNext();
        if (next >= 0)
           Process(next);
        else
           break;
     }
  }

  int FindNext()
  {
     for (var bot = 0; bot <= _maxBot; bot++)
        if (ShouldProcess(bot))
           return bot;
     return -1;
  }

  bool ShouldProcess(int bot) => _bots[bot, 1] > 0;

  void Process(int bot)
  {
     static bool areEqual((int n1, int n2) v1, (int n1, int n2) v2) =>
         Math.Min(v1.n1, v1.n2) == Math.Min(v2.n1, v2.n2) && Math.Max(v1.n1, v1.n2) == Math.Max(v2.n1, v2.n2);
     if (areEqual((_bots[bot, 0], _bots[bot, 1]), _target))
        _targetBot = bot;

     var (lo, hi) = _rules[bot];
     ProcessSide(lo, Math.Min(_bots[bot, 0], _bots[bot, 1]));
     ProcessSide(hi, Math.Max(_bots[bot, 0], _bots[bot, 1]));

     _bots[bot, 0] = _bots[bot, 1] = 0;
  }

  readonly (int n1, int n2) _target = (61, 17);

  void ProcessSide((int val, bool isOutput) side, int value)
  {
     if (side.isOutput)
        _outputs[side.val] = value;
     else
     {
        if (_bots[side.val, 1] > 0)
           Process(side.val);
        SetBotValue(side.val, value);
     }
  }
}
