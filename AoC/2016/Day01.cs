﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2016;

public class Day01(string input) : AoC(input)
{
  public override void Solve()
  {
     (int x, int y) = (0, 0); var cur = (x, y);
     var dir = 0; var p2 = -1;
     var visited = new HashSet<(int, int)>() { (0, 0) };
     static int manhattan(int a, int b) => Math.Abs(a) + Math.Abs(b);
     foreach (var instruction in InputLines.Value)
     {
        dir += instruction[0] == 'L' ? -1 : 1;
        if (dir < 0) dir += 4;
        dir %= 4;

        var n = int.Parse(new string(instruction.Skip(1).ToArray()));
        var (stepx, stepy) = (0, 0);
        switch (dir)
        {
           case 0: stepy = -1; break;
           case 1: stepx = 1; break;
           case 2: stepy = 1; break;
           case 3: stepx = -1; break;
        }

        static (int x, int y) step((int x, int y) pair, int stepx, int stepy) => (pair.x + stepx, pair.y + stepy);
        for (var i = 0; i < n; i++)
        {
           cur = step(cur, stepx, stepy);
           if (!visited.Add(cur) && p2 < 0)
              p2 = manhattan(cur.x, cur.y);
        }
     }

     Part1 = manhattan(cur.x, cur.y).ToString();
     Part2 = p2.ToString();
  }
}
