﻿using System;
using System.Linq;
using System.Text;

namespace AoC._2016;

public class Day08(string input) : AoC(input)
{
  public override void Solve()
  {
     _grid = new bool[6, 50];
     foreach (var cmd in InputLines.Value.Select(l => l.Split(' ')))
        switch (cmd[0])
        {
           case "rect": Rect(cmd[1]); break;
           case "rotate":
              switch (cmd[1])
              {
                 case "row": Rotate(RotateParams(cmd), row: true); break;
                 case "column": Rotate(RotateParams(cmd), row: false); break;
              }
              break;
        }

     Part1 = CountLit().ToString();
     Part2 = string.Join(InputLineSeparator, Display().Split(Environment.NewLine)); // TODO "OCR"
  }

  bool[,] _grid;

  void Rect(string arg)
  {
     var args = arg.Split('x');
     var rows = int.Parse(args[1]);
     var cols = int.Parse(args[0]);
     for (var i = 0; i < rows; i++)
        for (var j = 0; j < cols; j++)
           _grid[i, j] = true;
  }

  void Rotate((int pos, int by) @params, bool row)
  {
     ref bool el(int pos) => ref _grid[row ? @params.pos : pos, row ? pos : @params.pos];
     var dim = _grid.GetLength(row ? 1 : 0);
     var count = 0;
     for (var start = 0; count < dim; start++)
     {
        var cur = start;
        var prev = el(start);
        do
        {
           var next = (cur + @params.by) % dim;
           (prev, el(next)) = (el(next), prev);
           cur = next;
           count++;
        } while (start != cur);
     }
  }
  static (int pos, int by) RotateParams(string[] cmd) => (int.Parse(cmd[2].Split('=')[1]), int.Parse(cmd[4]));

  int CountLit()
  {
     var cols = _grid.GetLength(1);
     return Enumerable.Range(0, _grid.GetLength(0) * cols).Count(i => _grid[i / cols, i % cols]);
  }
  string Display()
  {
     var rows = _grid.GetLength(0);
     var cols = _grid.GetLength(1);
     var sb = new StringBuilder();
     for (var i = 0; i < rows; i++)
     {
        for (var j = 0; j < cols; j++)
           sb.Append(_grid[i, j] ? '#' : '.');
        sb.AppendLine();
     }
     return sb.ToString();
  }
}
