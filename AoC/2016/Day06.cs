﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2016;

public class Day06(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value;
     var n = lines[0].Length;
     var fs = Enumerable.Repeat(0, n).Select(_ => new Dictionary<char, int>()).ToArray();
     foreach (var line in lines)
        for (var i = 0; i < n; i++)
           if (fs[i].TryGetValue(line[i], out int value))
              fs[i][line[i]] = ++value;
           else
              fs[i][line[i]] = 1;

     var p1 = new char[n]; var p2 = new char[n];
     for (var i = 0; i < n; i++)
     {
        var min = lines.Length; var max = 0;
        var f = fs[i];
        foreach (var c in f.Keys)
        {
           if (f[c] > max) { max = f[c]; p1[i] = c; }
           if (f[c] < min) { min = f[c]; p2[i] = c; }
        }
     }

     Part1 = new string(p1);
     Part2 = new string(p2);
  }
}
