﻿using System.Text;

namespace AoC._2016;

public class Day09(string input) : AoC(input)
{
  public override void Solve()
  {
     Part1 = Solve(Input, 0, Input.Length, recurse: false).ToString();
     Part2 = Solve(Input, 0, Input.Length, recurse: true).ToString();
  }

  static long Solve(string str, int start, int len, bool recurse)
  {
     var res = 0L; var end = start + len;
     for (var pos = start; pos < end; pos++)
        if (str[pos] == '(')
        {
           // move until end of marker, parsing content length and times
           var markerSb = new StringBuilder();
           for (var markerPos = pos + 1; str[markerPos] != ')'; markerPos++)
              markerSb.Append(str[markerPos]);
           var marker = markerSb.ToString();
           var parts = marker.Split('x');
           var contentLen = int.Parse(parts[0]);
           var times = int.Parse(parts[1]);
           var markerLen = marker.Length + 2;

           // multiply or recurse
           res += times * (recurse ? Solve(str, pos + markerLen, contentLen, true) : contentLen);

           // move past content
           pos += markerLen + contentLen - 1;
        }
        else
           res++;
     return res;
  }
}
