﻿using System;
using System.Linq;

namespace AoC._2020;

public class Day12 : AoC
{
    readonly (char what, int value)[] _commands;

    public Day12(string input) : base(input) => _commands = InputLines.Value
        .Select(l => (what: l[0], value: int.Parse(l[1..]))).ToArray();

    public override void Solve()
    {
        Part1 = SolvePart1();
        Part2 = SolvePart2();
    }

    string SolvePart1()
    {
        var (x, y) = (0, 0);
        var dir = 0; // East
        foreach (var (what, value) in _commands)
            switch (what)
            {
                case 'N': y -= value; break;
                case 'S': y += value; break;
                case 'E': x += value; break;
                case 'W': x -= value; break;
                case 'L':
                    var al = value / 90;
                    dir = (dir + 4 - al) % 4;
                    break;
                case 'R':
                    var ar = value / 90;
                    dir = (dir + ar) % 4;
                    break;
                case 'F':
                    switch (dir)
                    {
                        case 0: x += value; break;
                        case 1: y += value; break;
                        case 2: x -= value; break;
                        case 3: y -= value; break;
                        default: throw new InvalidOperationException();
                    }

                    break;
                default: throw new InvalidOperationException();
            }

        return (Math.Abs(x) + Math.Abs(y)).ToString();
    }

    string SolvePart2()
    {
        var (x, y) = (0, 0);
        var (dx, dy) = (10, -1);

        void translate(int a)
        {
            void swap(ref int m, ref int n)  { (m, n) = (n, m); }

            switch (a)
            {
                case 0: break;
                case 1: dy = -dy; swap(ref dx, ref dy); break;
                case 2: dx = -dx; dy = -dy; break;
                case 3: dx = -dx; swap(ref dx, ref dy); break;
                default: throw new InvalidOperationException();
            }
        }

        foreach (var (what, value) in _commands)
            switch (what)
            {
                case 'N': dy -= value; break;
                case 'S': dy += value; break;
                case 'E': dx += value; break;
                case 'W': dx -= value; break;
                case 'L': var al = value / 90; translate((4 - al) % 4); break;
                case 'R': var ar = value / 90; translate(ar); break;
                case 'F': x += value * dx; y += value * dy; break;
                default: throw new InvalidOperationException();
            }

        return (Math.Abs(x) + Math.Abs(y)).ToString();
    }
}
