﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2020;

public class Day07 : AoC
{
  public Day07(string input) : base(input)
  {
     foreach (var line in InputLines.Value)
     {
        var parts = line.Split(' ');
        string getColor(int i1, int i2) => $"{parts[i1]} {parts[i2]}";
        var color = getColor(0, 1);
        if (line.EndsWith("no other bags.")) continue;
        for (var ind = 4; ind < parts.Length; ind += 4)
           AddBag(color, getColor(ind + 1, ind + 2), int.Parse(parts[ind]));
     }
  }

  public override void Solve()
  {
     const string COLOR = "shiny gold";
     AddAllParents(_uniqueParents, COLOR);
     Part1 = _uniqueParents.Count.ToString();
     Part2 = (CountContents(COLOR) - 1).ToString();
  }

  void AddBag(string color, string containsColor, int count)
  {
     GetOrAdd(color).Contents.Add((containsColor, count));
     GetOrAdd(containsColor).ParentColors.Add(color);
  }

  Bag GetOrAdd(string color)
  {
     var exists = _bagsByColor.ContainsKey(color);
     var bag = exists ? _bagsByColor[color] : new Bag();
     if (!exists)
        _bagsByColor[color] = bag;
     return bag;
  }

  void AddAllParents(HashSet<string> parents, string color)
  {
     foreach (var pc in _bagsByColor[color].ParentColors)
     {
        parents.Add(pc);
        AddAllParents(parents, pc);
     }
  }

  int CountContents(string color) =>
     _bagsByColor[color].Contents.Aggregate(1, (res, c) => res + c.count * CountContents(c.color));

  class Bag
  {
     public readonly HashSet<string> ParentColors = [];
     public readonly List<(string color, int count)> Contents = [];
  }

  readonly Dictionary<string, Bag> _bagsByColor = [];
  readonly HashSet<string> _uniqueParents = [];
}
