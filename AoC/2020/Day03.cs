﻿using System.Linq;

namespace AoC._2020;

public class Day03 : AoC
{
    readonly int _rowCount, _colCount;
    readonly char[,] _grid;
    const char Tree = '#';

    public Day03(string input) : base(input)
    {
        var lines = InputLines.Value;
        _rowCount = lines.Length; _colCount = lines.First().Length;
        _grid = new char[_rowCount, _colCount];
        for (var row = 0; row < lines.Length; row++)
            for (var col = 0; col < _colCount; col++)
                _grid[row, col] = lines[row][col];
    }

    public override void Solve()
    {
        var p1 = Ride(1, 3);
        Part1 = p1.ToString();
        Part2 = (Ride(1, 1) * p1 * Ride(1, 5) * Ride(1, 7) * Ride(2, 1)).ToString();
    }

    int Ride(int dr, int dc)
    {
        var sol = 0;
        for (var (row, col) = (0, 0); row < _rowCount; row += dr, col = (col + dc) % _colCount)
            if (_grid[row, col] == Tree)
                sol++;
        return sol;
    }
}
