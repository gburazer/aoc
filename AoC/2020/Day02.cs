﻿using System;
using System.Linq;

namespace AoC._2020;

public class Day02 : AoC
{
    readonly Policy[] _policies;

    public Day02(string input) : base(input)
    {
        _policies = InputLines.Value.Select(Policy.Parse).ToArray();
    }

    public override void Solve()
    {
        string count(Func<Policy, bool> predicate) => _policies.Count(predicate).ToString();
        Part1 = count(p => p.IsValidPart1());
        Part2 = count(p => p.IsValidPart2());
    }

    class Policy
    {
        public static Policy Parse(string line)
        {
            var parts = line.Split(Separators, StringSplitOptions.None);
            return new Policy
            {
                _lo = int.Parse(parts[0]), 
                _hi = int.Parse(parts[1]),
                _char = parts[2].Single(),
                _password = parts[3]
            };
        }

        int _lo, _hi;
        char _char;
        string _password;
        static readonly string[] Separators = [": ", " ", "-"];

        public bool IsValidPart1()
        {
            var count = _password.Count(c => c == _char);
            return count >= _lo && count <= _hi;
        }

        public bool IsValidPart2() => _password[_lo - 1] == _char ^ _password[_hi - 1] == _char;
    }
}
