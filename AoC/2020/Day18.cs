﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2020;

public class Day18 : AoC
{
    readonly string[] _expressions;

    public Day18(string input) : base(input) =>
        _expressions = InputLines.Value.Select(l => l.Replace(" ", "")).ToArray();

    public override void Solve()
    {
        string solve(Func<string, long> eval) => _expressions.Aggregate(0L, (res, exp) => res + eval(exp)).ToString();
        Part1 = solve(Eval1);
        Part2 = solve(Eval2);
    }

    long Eval1(string expression)
    {
        var pos = 0;
        long parseTerm() => ParseTerm(expression, Eval1, ref pos);
        var res = parseTerm();
        while (pos < expression.Length)
        {
            var op = expression[pos++]; var t = parseTerm();
            if (op == '+') res += t; else res *= t;
        }
        return res;
    }

    long Eval2(string expression)
    {
        var pos = 0;
        long parseTerm() => ParseTerm(expression, Eval2, ref pos);
        var terms = new List<long> { parseTerm() };
        var ops = new List<char>();
        while (pos < expression.Length)
        {
            ops.Add(expression[pos++]);
            terms.Add(parseTerm());
        }

        var stack = new Stack<long>([terms.First()]);
        for (var opInd = 0; opInd < ops.Count; opInd++)
        {
            var t = terms[opInd + 1];
            stack.Push(ops[opInd] == '+' ? stack.Pop() + t : t);
        }

        return stack.Aggregate(1L, (res, el) => res * el);
    }

    static long ParseTerm(string expression, Func<string, long> eval, ref int pos)
    {
        var ch = expression[pos++];
        if (char.IsDigit(ch))
            return ch - '0';
        if (ch != '(') throw new InvalidOperationException();
        var lvl = 1; var start = pos;
        while (lvl > 0)
            switch (expression[pos++])
            {
                case '(': lvl++; break;
                case ')': lvl--; break;
            }
        return eval(expression.Substring(start, pos - start - 1));
    }
}
