﻿using System;
using System.Linq;

namespace AoC._2020;

public class Day09 : AoC
{
    readonly long[] _ns;
    
    public Day09(string input) : base(input) => _ns = InputLines.Value.Select(long.Parse).ToArray();

    public override void Solve()
    {
        const int N = 25;

        bool isValid(int i)
        {
            for (var i1 = i - N; i1 < i - 1; i1++)
                for (var i2 = i1 + 1; i2 < i; i2++)
                    if (_ns[i] == _ns[i1] + _ns[i2])
                        return true;
            return false;
        }
        long calcWeakness()
        {
            for (var ind = N; ind < _ns.Length; ind++)
                if (!isValid(ind))
                    return _ns[ind];
            throw new InvalidOperationException();
        }

        var weakness = calcWeakness();
        Part1 = weakness.ToString();

        long solvePart2()
        {
            for (var to = _ns.Length - 1;; to--)
            {
                var sum = _ns[to];
                if (sum > weakness)
                    continue;
                var min = sum; var max = sum;
                for (var from = to - 1; sum <= weakness; from--)
                {
                    sum += _ns[from];
                    min = Math.Min(min, _ns[from]); max = Math.Max(max, _ns[from]);
                    if (sum == weakness)
                        return min + max;
                }
            }
        }

        Part2 = solvePart2().ToString();
    }
}
