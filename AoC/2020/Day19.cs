﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2020;

public class Day19 : AoC
{
    readonly Dictionary<int, Rule> _rules = [];
    readonly List<string> _expressions = [];

    public Day19(string input) : base(input)
    {
        var lines = InputLines.Value;
        var lineInd = 0;
        while (lines[lineInd] != "")
        {
            var rule = Rule.Parse(lines[lineInd++], out var ind);
            _rules[ind] = rule;
        }

        lineInd++;
        while (lineInd < lines.Length)
            _expressions.Add(lines[lineInd++]);
    }

    public override void Solve()
    {
        string count() => _expressions.Count(exp => IsMatch(exp, _rules[0])).ToString();

        Part1 = count();

        _rules[8] = Rule.Parse("0: 42 | 42 8", out _);
        _rules[11] = Rule.Parse("0: 42 31 | 42 11 31", out _);
        Part2 = count();
    }

    bool IsMatch(string exp, Rule rule) => TryMatch(rule, exp, 0).Contains(exp.Length);

    IEnumerable<int> TryMatch(Rule rule, string exp, int pos) =>
        pos == exp.Length
            ? []
            : rule.Ch != 0
                ? rule.Ch == exp[pos]
                    ? Enumerable.Repeat(pos + 1, 1)
                    : []
                : rule.Groups.SelectMany(group => TryGroup(group, exp, pos));

    IEnumerable<int> TryGroup(IList<int> group, string exp, int pos) =>
        group.Count == 1
            ? TryMatch(_rules[group.Single()], exp, pos)
            : TryMatch(_rules[group.First()], exp, pos)
                .SelectMany(newPos => TryGroup(group.Skip(1).ToList(), exp, newPos));

    class Rule
    {
        public static Rule Parse(string s, out int ind)
        {
            var parts = s.Split(": ");
            ind = int.Parse(parts[0]);
            return parts[1][0] == '"'
                ? new Rule(parts[1][1])
                : new Rule
                {
                    Groups = parts[1].Split(" | ").Select(g => g.Split(' ').Select(int.Parse).ToList()).ToList()
                };
        }

        Rule() => Ch = (char)0;
        Rule(char ch) => Ch = ch;

        public List<List<int>> Groups;

        public readonly char Ch;
    }
}
