﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2020;

public class Day22 : AoC
{
    readonly Queue<int> _player1 = new(), _player2 = new();

    public Day22(string input) : base(input)
    {
        var lines = InputLines.Value;
        var q = _player1;
        for (var lineInd = 1; lineInd < lines.Length; lineInd++)
            if (lines[lineInd] == "")
            {
                q = _player2;
                lineInd++;
            }
            else
                q.Enqueue(int.Parse(lines[lineInd]));
    }

    public override void Solve()
    {
        Part1 = Solve(isPart2: false);
        Part2 = Solve(isPart2: true);
    }

    string Solve(bool isPart2)
    {
        var (_, winningDeck) = PlayGame(new Queue<int>(_player1), new Queue<int>(_player2), isPart2);
        var count = winningDeck.Count;
        var score = 0;
        for (var ind = 0; winningDeck.Count > 0; ind++)
            score += winningDeck.Dequeue() * (count - ind);
        return score.ToString();
    }

    static (bool p1Wins, Queue<int> winningDeck) PlayGame(Queue<int> p1, Queue<int> p2, bool isPart2)
    {
        var visited = new HashSet<string>();
        while (p1.Count > 0 && p2.Count > 0)
        {
            var key = string.Join(',', p1.Append(0).Concat(p2));
            if (visited.Contains(key) && isPart2)
                return (true, p1);
            visited.Add(key);
            var v1 = p1.Dequeue(); var v2 = p2.Dequeue();
            var p1Wins = v1 <= p1.Count && v2 <= p2.Count && isPart2
                ? PlayGame(new Queue<int>(p1.Take(v1)), new Queue<int>(p2.Take(v2)), true).p1Wins
                : v1 > v2;
            if (p1Wins) { p1.Enqueue(v1); p1.Enqueue(v2); } else { p2.Enqueue(v2); p2.Enqueue(v1); }
        }
        return p1.Count > 0 ? (true, p1) : (false, p2);
    }
}
