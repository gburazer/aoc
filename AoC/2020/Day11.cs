﻿using System;

namespace AoC._2020;

public class Day11 : AoC
{
    readonly char[,] _orig, _cur, _alt;
    readonly int _rowCount, _colCount;

    public Day11(string input) : base(input)
    {
        var lines = InputLines.Value;
        _rowCount = lines.Length; _colCount = lines[0].Length;
        char[,] alloc() => new char[_rowCount, _colCount];
        _orig = alloc(); _cur = alloc(); _alt = alloc();
        for (var row = 0; row < _rowCount; row++)
            for (var col = 0; col < _colCount; col++)
                _orig[row, col] = lines[row][col];
    }

    public override void Solve()
    {
        Part1 = Solve(4, CountOccupiedPart1);
        Part2 = Solve(5, CountOccupiedPart2);
    }

    string Solve(int threshold, Func<int, int, int> countOccupied)
    {
        Copy(_cur, _orig);
        while (DoRound(threshold, countOccupied))
            ;
        return CountTotalOccupied().ToString();
    }

    bool DoRound(int threshold, Func<int, int, int> countOccupied)
    {
        var res = false;
        for (var row = 0; row < _rowCount; row++)
            for (var col = 0; col < _colCount; col++)
            {
                var old = _cur[row, col];
                _alt[row, col] = old;
                if (Floor == old)
                    continue;
                var count = countOccupied(row, col);
                _alt[row, col] = old switch
                {
                    Empty when count == 0 => Occupied,
                    Occupied when count >= threshold => Empty,
                    _ => _alt[row, col]
                };
                if (_alt[row, col] != old)
                    res = true;
            }
        Copy(_cur, _alt);
        return res;
    }

    int CountOccupiedPart1(int row, int col)
    {
        var res = 0;
        if (row > 0 && col > 0 && _cur[row - 1, col - 1] == Occupied) res++;
        if (row > 0 && _cur[row - 1, col] == Occupied) res++;
        if (row > 0 && col < _colCount - 1 && _cur[row - 1, col + 1] == Occupied) res++;
        if (col > 0 && _cur[row, col - 1] == Occupied) res++;
        if (col < _colCount - 1 && _cur[row, col + 1] == Occupied) res++;
        if (row < _rowCount - 1 && col > 0 && _cur[row + 1, col - 1] == Occupied) res++;
        if (row < _rowCount - 1 && _cur[row + 1, col] == Occupied) res++;
        if (row < _rowCount - 1 && col < _colCount - 1 && _cur[row + 1, col + 1] == Occupied) res++;
        return res;
    }

    int CountOccupiedPart2(int row, int col)
    {
        var res = 0;

        bool checkSeen(int dr, int dc)
        {
            var r = row; var c = col;
            do
            {
                r += dr; c += dc;
                if (r < 0 || r == _rowCount || c < 0 || c == _colCount) return false;
                switch (_cur[r, c])
                {
                    case Occupied: return true;
                    case Empty: return false;
                }
            } while (true);
        }

        if (checkSeen(-1, -1)) res++;
        if (checkSeen(-1, 0)) res++;
        if (checkSeen(-1, 1)) res++;
        if (checkSeen(0, -1)) res++;
        if (checkSeen(0, 1)) res++;
        if (checkSeen(1, -1)) res++;
        if (checkSeen(1, 0)) res++;
        if (checkSeen(1, 1)) res++;
        return res;
    }

    void Copy(char[,] to, char[,] from)
    {
        for (var row = 0; row < _rowCount; row++)
            for (var col = 0; col < _colCount; col++)
                to[row, col] = from[row, col];
    }

    int CountTotalOccupied()
    {
        var res = 0;
        for (var row = 0; row < _rowCount; row++)
            for (var col = 0; col < _colCount; col++)
                if (Occupied == _cur[row, col])
                    res++;
        return res;
    }

    const char Floor = '.', Empty = 'L', Occupied = '#';
}
