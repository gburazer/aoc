﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2020;

public class Day04 : AoC
{
    readonly List<Passport> _passports = [];

    public Day04(string input) : base(input)
    {
        var p = new Passport();
        foreach (var line in InputLines.Value)
            if (line.Length == 0)
            {
                _passports.Add(p);
                p = new Passport();
            }
            else
                p.AddFields(line);
        if (p.Keys.Count > 0)
            _passports.Add(p);
    }

    public override void Solve()
    {
        Part1 = ValidPassportsCount(validateContent: false);
        Part2 = ValidPassportsCount(validateContent: true);
    }

    string ValidPassportsCount(bool validateContent) => _passports.Count(p => p.IsValid(validateContent)).ToString();

    class Passport : Dictionary<string, string>
    {
        static readonly string[] Required = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];

        public bool IsValid(bool validateContent)
        {
            if (Required.Any(f => !ContainsKey(f)))
                return false;

            if (!validateContent)
                return true;

            static bool isValid(string s, int min, int max) => int.TryParse(s, out var n) && n >= min && n <= max;
            if (new (string field, int min, int max)[]
                {
                    (this["byr"], 1920, 2002),
                    (this["iyr"], 2010, 2020),
                    (this["eyr"], 2020, 2030)
                }.Any(d => !isValid(d.field, d.min, d.max)))
            {
                return false;
            }

            if (!int.TryParse(this["hgt"][0..^2], out var hgt))
                return false;
            var h = this["hgt"];
            switch (h.Substring(h.Length - 2, 2))
            {
                case "in":
                    if (hgt is < 59 or > 76) return false;
                    break;
                case "cm":
                    if (hgt is < 150 or > 193) return false;
                    break;
                default: throw new InvalidOperationException();
            }

            // TODO use regex
            var hcl = this["hcl"];
            if (hcl.Length != 7 || hcl[0] != '#' || hcl[1..].Any(c => !Uri.IsHexDigit(c)))
                return false;

            if ("amb blu brn gry grn hzl oth".Split(' ').All(col => col != this["ecl"]))
                return false;

            return this["pid"].Length == 9 && int.TryParse(this["pid"], out _);
        }

        public void AddFields(string line)
        {
            foreach (var field in line.Split(' '))
            {
                var parts = field.Split(':');
                this[parts[0]] = parts[1];
            }
        }
    }
}
