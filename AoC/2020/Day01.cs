﻿using System.Linq;

namespace AoC._2020;

public class Day01 : AoC
{
    readonly int[] _ns;

    public Day01(string input) : base(input) => _ns = InputLines.Value.Select(int.Parse).ToArray();

    public override void Solve()
    {
        Part1 = Solve(2);
        Part2 = Solve(3);
    }

    string Solve(int targetDepth, int depth = 1, int lasti = -1, int lastSum = 0, int lastMult = 1)
    {
        var res = NoSolution;
        for (var i = lasti + 1; i < _ns.Length - (targetDepth - depth); i++)
            if (depth == targetDepth)
            {
                if (lastSum + _ns[i] == 2020)
                    return (lastMult * _ns[i]).ToString();
            }
            else
            {
                if (NoSolution != (res = Solve(targetDepth, depth + 1, i, lastSum + _ns[i], lastMult * _ns[i])))
                    break;
            }
        return res;
    }
}
