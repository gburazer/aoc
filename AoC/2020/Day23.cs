﻿using System.Text;
using System.Collections.Generic;

namespace AoC._2020;

public class Day23 : AoC
{
    readonly TheList _list1, _list2;
    readonly int _first;

    public Day23(string input) : base(input)
    {
        var n = input.Length;
        var prev = 0;
        for (var ind = 0; ind < n; ind++)
        {
            var el = int.Parse(input[ind].ToString());
            if (ind == 0)
            {
                _list1 = new TheList(n, el);
                _list2 = new TheList(1000000, el);
                _first = el;
            }
            else
            {
                _list1.AddAfter(prev, el);
                _list2.AddAfter(prev, el);
            }
            prev = el;
        }
        
        for (var el = input.Length + 1; el <= 1000000; el++)
        {
            _list2.AddAfter(prev, el);
            prev = el;
        }
    }

    public override void Solve()
    {
        Part1 = SolvePart1();
        Part2 = SolvePart2();
    }

    string SolvePart1()
    {
        Solve(_list1, 100);

        var p1 = new StringBuilder();
        var cur = _list1.Next[1];
        for (var ind = 0; ind < _list1.Count - 1; ind++, cur = _list1.Next[cur])
            p1.Append(cur);
        return p1.ToString();
    }

    string SolvePart2()
    {
        Solve(_list2, 10000000);

        return ((long)_list2.Next[1] * _list2.Next[_list2.Next[1]]).ToString();
    }

    void Solve(TheList list, int moveCount)
    {
        var cur = _first;
        var n = list.Count;
        const int PICK_COUNT = 3;
        var stack = new Stack<int>(PICK_COUNT);
        for (var moveInd = 1; moveInd <= moveCount; moveInd++, cur = list.Next[cur])
        {
            for (var pickInd = 0; pickInd < PICK_COUNT; pickInd++)
            {
                stack.Push(list.Next[cur]);
                list.RemoveAfter(cur);
            }

            int normalize(int pos) => (n + pos - 1) % n + 1;
            var dest = normalize(cur - 1);
            while (stack.Contains(dest))
                dest = normalize(dest - 1);

            while (stack.Count > 0)
                list.AddAfter(dest, stack.Pop());
        }
    }

    class TheList
    {
        public TheList(int capacity, int first)
        {
            Next = new int[capacity + 1];
            Next[first] = first;
            Count = 1;
        }

        public readonly int[] Next;
        public int Count { get; private set; }

        public void AddAfter(int n, int el)
        {
            var oldNext = Next[n];
            Next[n] = el;
            Next[el] = oldNext;
            Count++;
        }

        public void RemoveAfter(int n)
        {
            Next[n] = Next[Next[n]];
            Count--;
        }
    }
}
