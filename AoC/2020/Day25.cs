﻿namespace AoC._2020;

public class Day25 : AoC
{
    readonly long _pk1, _pk2;

    public Day25(string input) : base(input)
    {
        var lines = InputLines.Value;
        _pk1 = long.Parse(lines[0]); _pk2 = long.Parse(lines[1]);
    }

    public override void Solve()
    {
        Part1 = Transform(_pk2, InferLoopSize(_pk1)).ToString();
        Part2 = NoSolution;
    }

    static long InferLoopSize(long pk)
    {
        const long SUBJECT_NUMBER = 7;
        var loopSize = 0L;
        for (var value = 1L; value != pk; value = NewValue(value, SUBJECT_NUMBER))
            loopSize++;
        return loopSize;
    }

    static long Transform(long subjectNumber, long loopSize)
    {
        var res = 1L;
        for (var ind = 0; ind < loopSize; ind++)
            res = NewValue(res, subjectNumber);
        return res;
    }

    static long NewValue(long value, long subjectNumber) => value * subjectNumber % Mod;

    const long Mod = 20201227;
}
