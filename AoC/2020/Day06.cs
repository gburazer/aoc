﻿using System.Collections.Generic;

namespace AoC._2020;

public class Day06 : AoC
{
    int _sum1, _sum2;

    public Day06(string input) : base(input)
    {
        HashSet<char> group = [], intersection = null;

        void closeGroup()
        {
            _sum1 += group.Count; _sum2 += intersection.Count;
            group.Clear();
            intersection = null;
        }

        foreach (var line in InputLines.Value)
            if (line.Length == 0)
                closeGroup();
            else
            {
                var person = new HashSet<char>(line);
                group.UnionWith(person);
                if (intersection == null)
                    intersection = [..person];
                else
                    intersection.IntersectWith(person);
            }

        closeGroup(); // dangling group, because input doesn't end with an empty line
    }

    public override void Solve()
    {
        Part1 = _sum1.ToString();
        Part2 = _sum2.ToString();
    }
}
