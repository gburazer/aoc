﻿using System.Linq;
using System.Collections.Generic;

using Ticket = System.Collections.Generic.List<int>;

namespace AoC._2020;

public class Day16 : AoC
{
    readonly List<(string name, List<(int lo, int hi)> range)> _fields = [];
    readonly Ticket _own;
    readonly HashSet<Ticket> _nearby = [];

    readonly HashSet<int> _isValidSomewhere = [];
    readonly Dictionary<string, HashSet<int>> _validityByName;

    public Day16(string input) : base(input)
    {
        var l = InputLines.Value;
        var ind = 0;
        for (; l[ind] != ""; ind++)
        {
            var parts = l[ind].Split(": ");
            var (name, range) = (parts[0], new List<(int lo, int hi)>());
            foreach (var r in parts[1].Split(" or ").Select(p => p.Split('-')))
            {
                var lo = int.Parse(r[0]); var hi = int.Parse(r[1]);
                range.Add((lo, hi));
                for (var n = lo; n <= hi; n++)
                    _isValidSomewhere.Add(n);
            }
            _fields.Add((name, range));
        }

        static Ticket parseTicket(string s) => s.Split(',').Select(int.Parse).ToList();
        ind += 2;
        _own = parseTicket(l[ind++]);
        ind += 2;
        while (ind < l.Length)
            _nearby.Add(parseTicket(l[ind++]));

        _validityByName = _fields.Select(f =>
        (
            f.name,
            validity: new HashSet<int>(f.range.SelectMany(r => Enumerable.Range(r.lo, r.hi - r.lo + 1)))
        )).ToDictionary(x => x.name, x => x.validity);
    }

    public override void Solve()
    {
        // TODO solve with arrays only, should be faster than generic collections
        // ranges are always 2 so validation doesnt need hashset for example
        Part1 = SolvePart1();
        Part2 = SolvePart2();
    }

    string SolvePart1() => _nearby.Aggregate(0, (res, ticket) =>
        res + ticket.Aggregate(0, (s, value) => s + (_isValidSomewhere.Contains(value) ? 0 : value))).ToString();

    string SolvePart2()
    {
        _nearby.RemoveWhere(t => t.Any(v => !_isValidSomewhere.Contains(v)));
        var all = Enumerable.Range(0, _own.Count);
        var candidatesByName = _fields.Select(f => (f.name, candidates: new HashSet<int>(all)))
            .ToDictionary(x => x.name, x => x.candidates);
        bool isValidCandidate(string name, int pos) => _nearby.All(n => _validityByName[name].Contains(n[pos]));
        bool isDone() => candidatesByName.Values.All(c => c.Count == 1);
        while (!isDone())
            foreach (var (name, candidates) in _fields.Select(f => (f.name, candidates: candidatesByName[f.name])))
            {
                candidates.RemoveWhere(c => !isValidCandidate(name, c));
                if (candidates.Count != 1) continue;
                var toRemove = candidates.Single();
                foreach (var (_, others) in candidatesByName.Where(cbn => cbn.Value.Count > 1))
                    others.RemoveWhere(c => c == toRemove);
            }

        return _fields
            .Where(f => f.name.StartsWith("departure"))
            .Select(f => f.name)
            .Aggregate(1L, (res, name) => res * _own[candidatesByName[name].Single()])
            .ToString();
    }
}
