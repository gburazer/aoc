﻿using System;
using System.Linq;

namespace AoC._2020;

public class Day10 : AoC
{
    readonly int[] _ns;

    public Day10(string input) : base(input)
    {
        _ns = InputLines.Value.Select(int.Parse).ToArray();
        Array.Sort(_ns);
    }

    public override void Solve()
    {
        Part1 = SolvePart1();
        Part2 = SolvePart2();
    }

    string SolvePart1()
    {
        var d1 = 0; var d3 = 1;
        var last = 0;
        foreach (var n in _ns)
        {
            if (n - last == 1) d1++;
            if (n - last == 3) d3++;
            last = n;
        }
        return (d1 * d3).ToString();
    }

    string SolvePart2()
    {
        var device = _ns[^1] + 3;
        var waysToReach = new long[device + 1];
        waysToReach[0] = 1;
        long single(int j) => j >= 0 ? waysToReach[j] : 0;
        long count(int j) => single(j - 3) + single(j - 2) + single(j - 1);
        foreach (var n in _ns)
            waysToReach[n] = count(n);
        waysToReach[device] = count(device);
        return waysToReach[device].ToString();
    }
}
