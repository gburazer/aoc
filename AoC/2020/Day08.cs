﻿using System;
using System.Linq;

namespace AoC._2020;

public class Day08 : AoC
{
    readonly Hhc _hhc;

    public Day08(string input) : base(input) => _hhc = new(InputLines.Value);

    public override void Solve()
    {
        string solution() => _hhc.Acc.ToString();
        _hhc.Run(); Part1 = solution();
        var ind = InputLines.Value.Length - 1; while (!_hhc.RunWithChange(ind--)) ; Part2 = solution();
    }

    class Hhc(string[] lines)
    {
        public bool Run()
        {
            _ip = Acc = 0;
            _isRan = new bool[_ops.Length];
            while (_ip < _ops.Length && !_isRan[_ip])
            {
                _isRan[_ip] = true;
                switch (_ops[_ip].op)
                {
                    case "acc": Acc += _ops[_ip++].val; break;
                    case "jmp": _ip += _ops[_ip].val; break;
                    case "nop": _ip++; break;
                    default: throw new InvalidOperationException();
                }
            }

            return _ip == _ops.Length;
        }

        public bool RunWithChange(int ind)
        {
            var oldOp = _ops[ind];
            if (_ops[ind].op == "jmp") _ops[ind] = ("nop", oldOp.val);
            else if (_ops[ind].op == "nop") _ops[ind] = ("jmp", oldOp.val);
            else return false;
            var res = Run();
            _ops[ind] = oldOp;
            return res;
        }

        public int Acc;

        readonly (string op, int val)[] _ops = lines.Select(l =>
        {
            var parts = l.Split(' ');
            return (op: parts[0], val: int.Parse(parts[1]));
        }).ToArray();

        bool[] _isRan;
        int _ip;
    }
}
