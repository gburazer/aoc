﻿using System;
using System.Linq;

namespace AoC._2020;

public class Day13 : AoC
{
    long _n;
    readonly long[] _mult;

    readonly string[] _buses;
    readonly (long id, int ind)[] _values;
    readonly long _time;

    public Day13(string input) : base(input)
    {
        var lines = InputLines.Value;
        _time = long.Parse(lines[0]);
        _buses = [..lines[1].Split(',')];
        var busesParsed = _buses.Select(x => x[0] == 'x' ? 0L : long.Parse(x)).ToArray();

        var count = busesParsed.Count(bp => bp > 0);
        _values = new (long id, int ind)[count];
        var valInd = 0;
        for (var ind = 0; ind < busesParsed.Length; ind++)
            if (busesParsed[ind] > 0)
                _values[valInd++] = (busesParsed[ind], ind);

        Array.Sort(_values, (v1, v2) => -v1.id.CompareTo(v2.id));

        _mult = new long[_values.Length];
        _mult[0] = _values[0].id;
        for (var i = 1; i < _values.Length; i++)
            _mult[i] = _mult[i - 1] * _values[i].id;
    }

    public override void Solve()
    {
        Part1 = SolvePart1();
        Part2 = SolvePart2();
    }

    string SolvePart1()
    {
        var minBus = long.MaxValue; var minWait = long.MaxValue;
        foreach (var bus in _buses.Where(b => b[0] != 'x').Select(long.Parse))
        {
            var wait = bus - _time % bus;
            if (wait >= minWait) continue;
            minBus = bus; minWait = wait;
        }
        return (minWait * minBus).ToString();
    }

    string SolvePart2()
    {
        var threshold = 100000000000000L / _values[0].id;
        _n = threshold * _values[0].id - _values[0].ind;
        Solve(0);
        return _n.ToString();
    }

    bool Solve(int i)
    {
        if (i == _values.Length)
            return true;
        for (; (_n + _values[i].ind) % _values[i].id == 0; _n += _mult[i])
            if (Solve(i + 1))
                return true;
        return false;
    }
}
