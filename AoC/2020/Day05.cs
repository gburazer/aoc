﻿using System;
using System.Linq;

namespace AoC._2020;

public class Day05 : AoC
{
    readonly (string row, string col)[] _bps;

    public Day05(string input) : base(input) => _bps =
        InputLines.Value.Select(line => (row: line[..7], col: line.Substring(line.Length - 3, 3))).ToArray();

    public override void Solve()
    {
        static int convert(string s, char zero, char one) => Convert.ToInt32(s.Replace(zero, '0').Replace(one, '1'), 2);
        static int sum(int from, int to) => (from + to) * (to - from + 1) / 2;

        var min = int.MaxValue;
        var max = int.MinValue;
        var total = 0;
        foreach (var (row, col) in _bps)
        {
            var id = convert(row, 'F', 'B') * 8 + convert(col, 'L', 'R');
            min = Math.Min(min, id);
            max = Math.Max(max, id);
            total += id;
        }

        Part1 = max.ToString();
        Part2 = (sum(min, max) - total).ToString();
    }
}
