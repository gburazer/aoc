﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2020;

public class Day14 : AoC
{
    readonly List<(char[] mask, List<(long addr, long value)> statements)> _data = [];

    public Day14(string input) : base(input)
    {
        var lines = InputLines.Value;
        for (var lineInd = 0; lineInd < lines.Length;)
        {
            var mask = lines[lineInd].Split(" = ")[1].ToCharArray();
            var statements = new List<(long addr, long value)>();
            while (++lineInd < lines.Length && lines[lineInd].StartsWith("mem"))
            {
                var parts2 = lines[lineInd].Split(" = ");
                statements.Add((addr: int.Parse(parts2[0][4..^1]), value: long.Parse(parts2[1])));
            }
            _data.Add((mask, statements));
        }
    }

    public override void Solve()
    {
        Part1 = Solve((mask, addr, value, mem) =>
        {
            var res = new char[Length];
            var valueBin = ToBinStr(value);
            for (var i = 0; i < Length; i++)
                res[i] = mask[i] != Mask ? mask[i] : valueBin[i];
            mem[addr] = ToNum(res);
        });
        Part2 = Solve((mask, addr, value, mem) =>
        {
            var addrBin = ToBinStr(addr);
            var addrMask = (char[])mask.Clone();
            for (var i = 0; i < Length; i++)
                if (addrMask[i] == '0')
                    addrMask[i] = addrBin[i];
            var addrs = new List<char[]>();
            ProduceAddrs(addrMask, 0, addrs);
            foreach (var a in addrs)
                mem[ToNum(a)] = value;
        });
    }

    string Solve(Action<char[], long, long, Dictionary<long, long>> processStatement)
    {
        var mem = new Dictionary<long, long>();
        foreach (var (mask, statements) in _data)
            foreach (var (addr, value) in statements)
                processStatement(mask, addr, value, mem);
        return mem.Values.Sum().ToString();
    }

    static char[] ToBinStr(long n) => Convert.ToString(n, 2).PadLeft(Length, '0').ToCharArray();
    static long ToNum(char[] a) => Convert.ToInt64(new string(a), 2);

    static void ProduceAddrs(char[] addrMask, int ind, List<char[]> addrs)
    {
        void cloneAndAdd(int i, char ch)
        {
            var @new = (char[])addrMask.Clone();
            @new[i] = ch;
            ProduceAddrs(@new, i + 1, addrs);
        }
        for (var i = ind; i < Length; i++)
            if (addrMask[i] == Mask)
            {
                cloneAndAdd(i, '0'); cloneAndAdd(i, '1');
                return;
            }
        addrs.Add(addrMask);
    }

    const char Mask = 'X';
    const int Length = 36;
}
