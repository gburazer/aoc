﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2020;

public class Day24(string input) : AoC(input)
{
  public override void Solve()
  {
     Part1 = SolvePart1();
     Part2 = SolvePart2();
  }

  string SolvePart1()
  {
     foreach (var l in InputLines.Value)
        _floor.Flip(l);
     return _floor.BlackTilesCount().ToString();
  }

  string SolvePart2()
  {
     for (var day = 1; day <= 100; day++)
     {
        var newFloor = new Floor(_floor);

        var candidates = _floor.BlackTiles().SelectMany(tile =>
           Floor.Directions.Select(dir => (tile.x + dir.move.dx, tile.y + dir.move.dy, tile.z + dir.move.dz))
              .Append(tile))
           .ToHashSet();

        foreach (var tile in candidates)
        {
           var count = _floor.BlackNeighboursCount(tile);
           var isBlack = _floor.IsBlack(tile);
           if ((isBlack && count is 0 or > 2) || !isBlack && count == 2)
              newFloor.Flip(tile);
        }
        _floor = newFloor;
     }
     return _floor.BlackTilesCount().ToString();
  }

  Floor _floor = new();

  class Floor : Dictionary<(int x, int y, int z), int>
  {
     public Floor() { }
     public Floor(Floor other) : base(other) { }

     public IEnumerable<(int x, int y, int z)> BlackTiles() => Keys.Where(IsBlack);

     public int BlackTilesCount() => Values.Count(IsBlack);
     public int BlackNeighboursCount((int x, int y, int z) tile) =>
        Directions.Count(d => IsBlack((tile.x + d.move.dx, tile.y + d.move.dy, tile.z + d.move.dz)));

     public bool IsBlack((int x, int y, int z) tile) => ContainsKey(tile) && IsBlack(this[tile]);
     static bool IsBlack(int v) => v % 2 == 1;

     public void Flip(string moves)
     {
        var movesPos = 0;
        var (x, y, z) = (0, 0, 0);
        while (movesPos < moves.Length)
        {
           var (dx, dy, dz) = ParseMove(moves, ref movesPos);
           x += dx; y += dy; z += dz;
        }
        Flip((x, y, z));
     }

     public void Flip((int x, int y, int z) tile)
     {
        if (ContainsKey(tile))
           this[tile]++;
        else
           this[tile] = 1;
     }

     static (int dx, int dy, int dz) ParseMove(string moves, ref int pos)
     {
        foreach (var (dir, move) in Directions)
           if (moves.Length >= pos + dir.Length && moves.Substring(pos, dir.Length) == dir)
           {
              pos += dir.Length;
              return move;
           }
        throw new InvalidOperationException();
     }

     // https://www.redblobgames.com/grids/hexagons/ - cube coordinates
     public static readonly (string dir, (int dx, int dy, int dz) move)[] Directions =
     [
        ("se", (0, -1, 1)),
        ("sw", (-1, 0, 1)),
        ("nw", (0, 1, -1)),
        ("ne", (1, 0, -1)),
        ("e" , (1, -1, 0)),
        ("w" , (-1, 1, 0))
     ];
  }
}
