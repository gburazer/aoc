﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2020;

public class Day17(string input) : AoC(input)
{
  public override void Solve()
  {
     Part1 = Solve(useW: false);
     Part2 = Solve(useW: true);
  }

  string Solve(bool useW) // TODO PERF use bool [] instead of hashset
  {
     Init();
     return Enumerable.Range(0, 6).Aggregate(0L, (_, _) => DoRound(useW)).ToString();
  }

  void Init()
  {
     _lit = [];
     var l = InputLines.Value;
     var rowCount = l.Length; var colCount = l[0].Length;
     for (var row = 0; row < rowCount; row++)
        for (var col = 0; col < colCount; col++)
           if ('#' == l[row][col])
              _lit.Add(new Point { Row = row, Col = col });
  }

  long DoRound(bool useW)
  {
     List<Point> getNeighbours(Point p)
     {
        var res = new List<Point>();
        for (var dr = -1; dr <= 1; dr++)
           for (var dc = -1; dc <= 1; dc++)
              for (var dz = -1; dz <= 1; dz++)
                 if (useW)
                 {
                    for (var dw = -1; dw <= 1; dw++)
                       if (dr != 0 || dc != 0 || dz != 0 || dw != 0)
                          res.Add(new Point { Row = p.Row + dr, Col = p.Col + dc, Z = p.Z + dz, W = p.W + dw });
                 }
                 else if (dr != 0 || dc != 0 || dz != 0)
                    res.Add(new Point { Row = p.Row + dr, Col = p.Col + dc, Z = p.Z + dz });
        return res;
     }

     HashSet<Point> processed = [], newLit = [];
     void process(Point p)
     {
        if (!processed.Add(p))
           return;
        var litCount = getNeighbours(p).Count(n => _lit.Contains(n));
        if (litCount == 3 || (_lit.Contains(p) && litCount == 2))
           newLit.Add(p);
     }

     foreach (var p in _lit)
     {
        process(p);
        foreach (var n in getNeighbours(p))
           process(n);
     }

     _lit = newLit;
     return _lit.Count;
  }

  struct Point { public int Row, Col, Z, W; }

  HashSet<Point> _lit;
}
