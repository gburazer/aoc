﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2020;

public class Day20 : AoC
{
    readonly int _n, _dim;
    readonly List<Tile> _tiles = [];
    readonly Tile[,] _grid;
    readonly HashSet<int> _placed = [];

    public Day20(string input) : base(input)
    {
        var lines = InputLines.Value; var lineInd = 0; _n = lines[1].Length;
        while (lineInd < lines.Length)
        {
            var id = int.Parse(lines[lineInd++].Split(' ')[1][..^1]);
            var tileGrid = new bool[_n, _n];
            for (var row = 0; row < _n; row++)
                for (var col = 0; col < _n; col++)
                    tileGrid[row, col] = lines[lineInd + row][col] == '#';
            lineInd += _n + 1;

            void add()
            {
                _tiles.Add(new Tile(id, tileGrid));
                _tiles.Add(new Tile(id, FlipHorizontal(tileGrid)));
                _tiles.Add(new Tile(id, FlipVertical(tileGrid)));
            }

            add();
            for (var rot = 1; rot <= 3; rot++)
            {
                tileGrid = RotateCw(tileGrid);
                add();
            }
        }

        _dim = Convert.ToInt32(Math.Sqrt(_tiles.Count / 12));
        _grid = new Tile[_dim, _dim];
        foreach (var tile in _tiles)
        {
            tile.OptionsRight = [];
            tile.OptionsBottom = [];
            tile.OptionsLeft = [];
            tile.OptionsTop = [];
            foreach (var other in _tiles.Where(t => t.Id != tile.Id))
            {
                if (tile.Right == other.Left) tile.OptionsRight.Add(other);
                if (tile.Bottom == other.Top) tile.OptionsBottom.Add(other);
                if (tile.Left == other.Right) tile.OptionsLeft.Add(other);
                if (tile.Top == other.Bottom) tile.OptionsTop.Add(other);
            }
        }
    }

    public override void Solve()
    {
        Part1 = NoSolution;
        foreach (var tile in _tiles.Where(t => t.OptionsLeft.Count == 0 && t.OptionsTop.Count == 0))
            TryPlacing(tile, 0, 0);

        Part2 = SolvePart2();
    }

    void TryPlacing(Tile tile, int row, int col)
    {
        if (Part1 != NoSolution)
            return;
        _grid[row, col] = tile;
        _placed.Add(tile.Id);
        if (row == _dim - 1 && col == _dim - 1)
        {
            Part1 = (1L
                     * _grid[0, 0].Id
                     * _grid[0, _dim - 1].Id
                     * _grid[_dim - 1, 0].Id
                     * _grid[_dim - 1, _dim - 1].Id).ToString();
            return;
        }

        if (col == _dim - 1) // move down
            foreach (var option in _grid[row, 0].OptionsBottom.Where(o => !_placed.Contains(o.Id)))
                TryPlacing(option, row + 1, 0);
        else // move right
            foreach (var option in tile.OptionsRight.Where(o => !_placed.Contains(o.Id)))
                if (row == 0 || _grid[row - 1, col + 1].OptionsBottom.Contains(option))
                    TryPlacing(option, row, col + 1);
        _placed.Remove(tile.Id);
    }

    string SolvePart2()
    {
        var pattern = PreparePattern();
        foreach (var image in PrepareImages())
        {
            var res = Roughness(image, pattern);
            if (res > 0)
                return res.ToString();
        }
        return NoSolution;
    }

    List<bool[,]> PrepareImages()
    {
        var image = StripBorders();
        var images = new List<bool[,]> { image, FlipHorizontal(image), FlipVertical(image) };
        for (var rot = 1; rot <= 3; rot++)
        {
            image = RotateCw(image);
            images.Add(image); images.Add(FlipHorizontal(image)); images.Add(FlipVertical(image));
        }
        return images;
    }

    static bool[,] PreparePattern()
    {
        var strPattern = new[]
        {
            "                  # ",
            "#    ##    ##    ###",
            " #  #  #  #  #  #   "
        };
        var ph = strPattern.Length; var pw = strPattern[0].Length;
        var pattern = new bool[ph, pw];
        for (var r = 0; r < ph; r++)
            for (var c = 0; c < pw; c++)
                pattern[r, c] = strPattern[r][c] == '#';
        return pattern;
    }

    bool[,] StripBorders()
    {
        var res = new bool[_dim * (_n - 2), _dim * (_n - 2)];
        for (var row = 0; row < _dim; row++)
            for (var col = 0; col < _dim; col++)
                for (var r = 1; r < _n - 1; r++)
                    for (var c = 1; c < _n - 1; c++)
                        res[row * (_n - 2) + r - 1, col * (_n - 2) + c - 1] = _grid[row, col].Grid[r, c];
        return res;
    }

    static int Roughness(bool[,] image, bool[,] pattern)
    {
        var positions = FindPattern(pattern, image);
        return positions.Count == 0 ? 0 : Count(image) - positions.Count * Count(pattern);
    }

    static int Count(bool[,] grid)
    {
        var res = 0;
        for (var r = 0; r < grid.GetLength(0); r++)
            for (var c = 0; c < grid.GetLength(1); c++)
                if (grid[r, c])
                    res++;
        return res;
    }

    static List<(int r, int c)> FindPattern(bool[,] pattern, bool[,] image)
    {
        var res = new List<(int r, int c)>();
        for (var r = 0; r < image.GetLength(0) - pattern.GetLength(0); r++)
            for (var c = 0; c < image.GetLength(1) - pattern.GetLength(1); c++)
            {
                var isFound = true;
                for (var pr = 0; isFound && pr < pattern.GetLength(0); pr++)
                    for (var pc = 0; isFound && pc < pattern.GetLength(1); pc++)
                        if (pattern[pr, pc] && !image[r + pr, c + pc])
                            isFound = false;
                if (isFound)
                    res.Add((r, c));
            }
        return res;
    }

    static bool[,] FlipHorizontal(bool[,] grid)
    {
        var m = grid.GetLength(0); var n = grid.GetLength(1);
        var res = new bool[m, n];
        for (var r = 0; r < m; r++)
            for (var c = 0; c < n; c++)
                res[r, c] = grid[r, n - c - 1];
        return res;
    }

    static bool[,] FlipVertical(bool[,] grid)
    {
        var m = grid.GetLength(0); var n = grid.GetLength(1);
        var res = new bool[m, n];
        for (var r = 0; r < m; r++)
            for (var c = 0; c < n; c++)
                res[r, c] = grid[m - r - 1, c];
        return res;
    }

    static bool[,] RotateCw(bool[,] grid)
    {
        var m = grid.GetLength(0); var n = grid.GetLength(1);
        var res = new bool[m, n];
        for (var r = 0; r < m; r++)
            for (var c = 0; c < n; c++)
                res[r, c] = grid[m - c - 1, r];
        return res;
    }

    class Tile
    {
        public Tile(int id, bool[,] grid)
        {
            Id = id; Grid = grid;

            static int convert(char[] n) => Convert.ToInt32(new string(n), 2);

            var n = grid.GetLength(0); var num = new char[n];
            for (var i = 0; i < n; i++) num[i] = grid[0, i] ? '1' : '0';
            Top = convert(num);
            for (var i = 0; i < n; i++) num[i] = grid[i, 0] ? '1' : '0';
            Left = convert(num);
            for (var i = 0; i < n; i++) num[i] = grid[n - 1, i] ? '1' : '0';
            Bottom = convert(num);
            for (var i = 0; i < n; i++) num[i] = grid[i, n - 1] ? '1' : '0';
            Right = convert(num);
        }

        public readonly bool[,] Grid;
        public readonly int Id;
        public readonly int Left, Right, Top, Bottom;
        public HashSet<Tile> OptionsLeft, OptionsRight, OptionsTop, OptionsBottom;
    }
}
