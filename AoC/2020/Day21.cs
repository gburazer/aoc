﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2020;

public class Day21 : AoC
{
    readonly List<Food> _foods = [];
    readonly Dictionary<string, HashSet<string>> _ingredientCandidatesPerAllergen = [];

    public Day21(string input) : base(input)
    {
        var lines = InputLines.Value;
        var lineInd = 0;
        while (lineInd < lines.Length)
        {
            var line = lines[lineInd];
            var parts = line.Split(" (contains ");
            var food = new Food
            {
                Ingredients = [..parts[0].Split(' ')],
                Allergens = [..parts[1][..^1].Split(", ")]
            };
            _foods.Add(food);
            foreach (var allergen in food.Allergens)
                GetOrCreate(_ingredientCandidatesPerAllergen, allergen).UnionWith(food.Ingredients);
            lineInd++;
        }
    }

    static HashSet<string> GetOrCreate(Dictionary<string, HashSet<string>> dict, string item)
    {
        if (dict.TryGetValue(item, out var value))
            return value;
        var @new = new HashSet<string>();
        dict[item] = @new;
        return @new;
    }

    public override void Solve()
    {
        /* TODO PERF 5ms should be doable, no combining needed
           *    each allergen has multiple sets of possible ingredients (one from each food)
           *    intersect those sets, whichever allergen is left with one possible ingredient that's target mapping
           *    -> remove that ingredient from other allergens' sets and repeat
         */
        
        var attempt = new Dictionary<string, string>();
        if (!TryCombo(attempt))
            throw new InvalidOperationException();

        Part1 = _foods.Aggregate(0, (res, f) => 
            res + f.Ingredients.Count(ing => !attempt.ContainsValue(ing))).ToString();

        var ingredients = attempt.Values.ToArray();
        var lookup = attempt.ToDictionary(a => a.Value, a => a.Key);
        Array.Sort(ingredients, (i1, i2) => string.Compare(lookup[i1], lookup[i2], StringComparison.Ordinal));
        Part2 = string.Join(',', ingredients);
    }

    bool TryCombo(Dictionary<string, string> attempt)
    {
        if (attempt.Count == _ingredientCandidatesPerAllergen.Keys.Count)
            return true;
        foreach (var allergen in _ingredientCandidatesPerAllergen.Keys.Where(k => !attempt.ContainsKey(k)))
            foreach (var ingredient in _ingredientCandidatesPerAllergen[allergen]
                .Where(ing => !attempt.ContainsValue(ing) && IsValid(allergen, ing)))
            {
                attempt[allergen] = ingredient;
                if (TryCombo(attempt))
                    return true;
                attempt.Remove(allergen);
            }
        return false;
    }

    bool IsValid(string allergen, string ingredient) =>
        _foods.All(f => !f.Allergens.Contains(allergen) || f.Ingredients.Contains(ingredient));

    class Food
    {
        public HashSet<string> Ingredients;
        public HashSet<string> Allergens;
    }
}
