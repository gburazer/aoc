﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2020;

public class Day15(string input) : AoC(input)
{
  public override void Solve()
  {
     Part1 = Solve(2020L);
     Part2 = Solve(30000000L);
  }

  string Solve(long n) // TODO only use one dictionary, two are not needed
  {
     Dictionary<long, long> prev = [], prevPrev = [];
     for (var turn = 1; turn <= _ns.Length; turn++)
        prev[_ns[turn - 1]] = turn;
     var last = _ns.Last();
     long age() => prevPrev.TryGetValue(last, out var value) ? prev[last] - value : 0;
     void speak(long a, long t)
     {
        if (prev.TryGetValue(a, out var value))
           prevPrev[a] = value;
        prev[a] = t;
        last = a;
     }
     for (var turn = _ns.Length + 1L; turn <= n; turn++)
        speak(age(), turn);
     return last.ToString();
  }

  readonly long[] _ns = input.Split(',').Select(long.Parse).ToArray();
}
