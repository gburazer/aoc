﻿using System;
using System.Linq;
using System.Collections.Generic;

using Priority_Queue;

namespace AoC._2018;

public class Day23(string input) : AoC(input)
{
  public override void Solve()
  {
     ParseInput();
     Part1 = Enumerable.Range(0, _n).Count(i => Manhattan(i, _maxrind) <= _maxr).ToString();
     Part2 = SolvePart2();
  }

  void ParseInput()
  {
     var lines = InputLines.Value;
     _n = lines.Length;
     for (var ind = 0; ind < _n; ind++)
     {
        var parts = lines[ind].Split(", ");
        var r = int.Parse(parts[1].Split("=")[1]);
        if (r > _maxr) { _maxr = r; _maxrind = ind; }
        var coordBuild = parts[0].Split("<")[1];
        var coordParts = coordBuild[0..^1].Split(",");
        var v = coordParts.Take(Dim).Select(int.Parse).ToArray();
        Ns.Add((v, r));
     }
  }

  const int Dim = 3;
  static readonly List<(int[] v, int r)> Ns = []; // TODO get rid of static
  int _n, _maxr, _maxrind;

  static int Manhattan(int i1, int i2) => Manhattan(Ns[i1].v, Ns[i2].v);
  static int Manhattan(int[] v1, int[] v2) => Enumerable.Range(0, Dim).Sum(dim => Math.Abs(v1[dim] - v2[dim]));

  static string SolvePart2()
  {
     var startCube = new Cube(
        start: [int.MinValue / 4, int.MinValue / 4, int.MinValue / 4],
        size: (int.MaxValue / 2) + 1);
     var q = new FastPriorityQueue<Cube>(65536);
     q.Enqueue(startCube, startCube.Priority);
     while (q.Count > 0)
     {
        var cube = q.Dequeue();
        if (cube.Size == 1)
           return cube.OriginDist.ToString();
        foreach (var subCube in cube.ChopUp())
           q.Enqueue(subCube, subCube.Priority);
     }
     return NoSolution;
  }

  class Cube : FastPriorityQueueNode
  {
     public Cube(int[] start, int size)
     {
        _start = start;
        Size = size;

        var inRange = Ns.Count(n => Dist(n.v) <= n.r);
        OriginDist = Dist([0, 0, 0]);
        Priority = long.MaxValue - (1000000000000000L * inRange + 1000000L * OriginDist + Size);
     }

     readonly int[] _start;
     public readonly int Size;
     public readonly int OriginDist;

     public List<Cube> ChopUp()
     {
        var subCubes = new List<Cube>();
        var v = _start; var s = Size / 2;
        subCubes.Add(new Cube([v[0], v[1], v[2]], s));
        subCubes.Add(new Cube([v[0] + s, v[1], v[2]], s));
        subCubes.Add(new Cube([v[0], v[1] + s, v[2]], s));
        subCubes.Add(new Cube([v[0], v[1], v[2] + s], s));
        subCubes.Add(new Cube([v[0] + s, v[1] + s, v[2]], s));
        subCubes.Add(new Cube([v[0] + s, v[1], v[2] + s], s));
        subCubes.Add(new Cube([v[0], v[1] + s, v[2] + s], s));
        subCubes.Add(new Cube([v[0] + s, v[1] + s, v[2] + s], s));
        return subCubes;
     }

     int Dist(int[] v) => Enumerable.Range(0, Dim).Sum(dim =>
         v[dim] < Lo(dim) ? Lo(dim) - v[dim] : v[dim] > Hi(dim) ? v[dim] - Hi(dim) : 0);

     int Lo(int dim) => _start[dim];
     int Hi(int dim) => _start[dim] + Size - 1;
  }
}
