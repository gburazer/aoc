﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2018;

public class Day13 : AoC
{
   readonly (int x, int y) _size;
   readonly List<Cart> _carts = [];

  public Day13(string input) : base(input)
  {
     var lines = InputLines.Value;
     _size = (lines[0].Length, lines.Length);
     var chars = new char[_size.x, _size.y];
     for (var x = 0; x < _size.x; x++)
        for (var y = 0; y < _size.y; y++)
        {
           var c = lines[y][x];
           if (IsCart(c))
           {
              _carts.Add(new Cart(chars) { Pos = (x, y), Orientation = c, Turn = TurnLeft });
              c = c is Left or Right ? '-' : '|';
           }
           chars[x, y] = c;
        }
  }

  public override void Solve()
  {
     int[] getRemaining() => Enumerable.Range(0, _carts.Count)
         .Where(i => !_carts[i].IsRemoved)
         .OrderBy(i => (_carts[i].Pos.y * _size.x) + _carts[i].Pos.x)
         .ToArray();
     for (var remaining = getRemaining(); remaining.Length != 1; remaining = getRemaining())
        foreach (var ind in remaining)
           if (!_carts[ind].IsRemoved)
           {
              _carts[ind].Step();
              var (isCrash, pos) = CheckCrash();
              if (isCrash && Part1 is null)
                 Part1 = $"{pos.x},{pos.y}";
           }
     var last = _carts.Single(c => !c.IsRemoved);
     Part2 = $"{last.Pos.x},{last.Pos.y}";
  }

  static bool IsCart(char c) => c is Left or Right or Up or Down;
  const char Left = '<', Right = '>', Up = '^', Down = 'v', Intersection = '+', CurveTlBr = '/', CurveTrBl = '\\';

  class Cart(char[,] chars)
  {
     public (int x, int y) Pos;
     public char Orientation;
     public int Turn;
     public bool IsRemoved;

     public void Step()
     {
        var c = chars[Pos.x, Pos.y];
        if (c == Intersection)
           MakeTurn(c);

        Move();

        var newc = chars[Pos.x, Pos.y];
        if (newc is CurveTlBr or CurveTrBl)
           MakeTurn(newc);
     }

     void MakeTurn(char c)
     {
        switch (c)
        {
           case Intersection:
              if (Turn is not TurnStraight)
                 MakeTurn(Turn == TurnLeft);
              Turn = (Turn + 1) % 3;
              break;
           case CurveTlBr:
              MakeTurn(Orientation is Left or Right);
              break;
           case CurveTrBl:
              MakeTurn(Orientation is Up or Down);
              break;
        }
     }

     void MakeTurn(bool left) { Orientation = left ? ToLeft[Orientation] : ToRight[Orientation]; }

     void Move()
     {
        switch (Orientation)
        {
           case Left: Pos.x--; break;
           case Right: Pos.x++; break;
           case Up: Pos.y--; break;
           case Down: Pos.y++; break;
        }
     }
  }

  static readonly Dictionary<char, char> ToLeft = new() { [Left] = Down, [Down] = Right, [Right] = Up, [Up] = Left };
  static readonly Dictionary<char, char> ToRight = new() { [Left] = Up, [Up] = Right, [Right] = Down, [Down] = Left };

  const int TurnLeft = 0;
  const int TurnStraight = 1;
  //const int TurnRight = 2;

  (bool isCrash, (int x, int y) pos) CheckCrash()
  {
     for (var i1 = 0; i1 < _carts.Count - 1; i1++)
        if (!_carts[i1].IsRemoved)
           for (var i2 = i1 + 1; i2 < _carts.Count; i2++)
              if (!_carts[i2].IsRemoved && _carts[i1].Pos == _carts[i2].Pos)
              {
                 _carts[i1].IsRemoved = _carts[i2].IsRemoved = true;
                 return (true, _carts[i1].Pos);
              }
     return (false, (0, 0));
  }
}
