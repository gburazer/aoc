﻿using System;
using System.Collections.Generic;

namespace AoC._2018;

public class Day18 : AoC
{
  public Day18(string input) : base(input)
  {
     var lines = InputLines.Value;
     _n = lines.Length;
     _field = string.Join("", lines).ToCharArray();
     _oldField = new char[_n * _n];
     _newField = new char[_n * _n];
  }

  public override void Solve()
  {
     _field.CopyTo(_oldField, 0); Part1 = SolvePart1();
     _field.CopyTo(_oldField, 0); Part2 = SolvePart2();
  }

  readonly int _n;
  readonly char[] _field, _oldField, _newField;
  const char Open = '.', Trees = '|', Lumberyard = '#';

  string SolvePart1()
  {
     for (var i = 0; i < 10; i++)
        TickMinute();
     return ResourceValue(_oldField).ToString();
  }

  string SolvePart2()
  {
     var seen = new List<string>();
     int start;
     do
     {
        TickMinute();
        var str = new string(_oldField);
        start = seen.IndexOf(str);
        seen.Add(str);
     } while (start == -1);
     seen.RemoveAt(seen.Count - 1);
     var remaining = (1000000000 - start) % (seen.Count - start);
     for (var i = 0; i < remaining - 1; i++)
        TickMinute();
     return ResourceValue(_oldField).ToString();
  }

  void TickMinute() // oldField => newField => oldField
  {
     int neighbourCount(char[] field, int x, int y, char c)
     {
        var res = 0;
        for (var i = x - 1; i <= x + 1; i++)
           for (var j = y - 1; j <= y + 1; j++)
              if (i >= 0 && i < _n && j >= 0 && j < _n && !(i == x && j == y) && field[i * _n + j] == c)
                 res++;
        return res;
     }
     for (var i = 0; i < _n; i++)
        for (var j = 0; j < _n; j++)
        {
           var ind = (i * _n) + j;
           _newField[ind] = _oldField[ind] switch
           {
              Open => neighbourCount(_oldField, i, j, Trees) >= 3 ? Trees : Open,
              Trees => neighbourCount(_oldField, i, j, Lumberyard) >= 3 ? Lumberyard : Trees,
              Lumberyard =>
                 neighbourCount(_oldField, i, j, Lumberyard) >= 1 && neighbourCount(_oldField, i, j, Trees) >= 1
                    ? Lumberyard
                    : Open,
              _ => throw new InvalidOperationException(),
           };
        }
     _newField.CopyTo(_oldField, 0);
  }

  int ResourceValue(char[] field)
  {
     var treesCount = 0; var lumberyardCount = 0;
     for (var i = 0; i < _n; i++)
        for (var j = 0; j < _n; j++)
           if (field[i * _n + j] == Trees)
              treesCount++;
           else if (field[i * _n + j] == Lumberyard)
              lumberyardCount++;
     return treesCount * lumberyardCount;
  }
}
