﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2018;

public class Day24(string input) : AoC(input)
{

  // 100+, 50-, 75+, 62+, 55-, 60-
  const int BOOST = 61;

  public override void Solve()
  {
     ParseInput();

     while (_totalCount[IMMUNE_SYSTEM] > 0 && _totalCount[INFECTION] > 0)
        Fight();

     Part1 = _totalCount.Sum().ToString();
     Part2 = "bar"; // TODO function for immune system win/detect tie, bin search for min boost starting at 100
  }

  void ParseInput()
  {
     var lines = InputLines.Value;
     var armyInd = -1; var groupInd = 0;
     foreach (var line in lines.Where(l => l != string.Empty))
        if (line == _headlines[IMMUNE_SYSTEM])
        {
           armyInd = IMMUNE_SYSTEM;
           groupInd = 0;
        }
        else if (line == _headlines[INFECTION])
        {
           armyInd = INFECTION;
           groupInd = 0;
        }
        else
        {
           var group = ParseGroup(line, armyInd, ++groupInd);
           _totalCount[armyInd] += group.Count;
           _armies[armyInd].Add(group);
        }
  }

  static Group ParseGroup(string line, int armyInd, int groupInd)
  {
     string inner = null; string[] outer = null;
     var parts1 = line.Split("(");
     if (parts1.Length == 2)
     {
        var parts2 = parts1[1].Split(")");
        inner = parts2[0];
        outer = string.Join(" ", new[] { parts1[0], parts2[1] })
            .Split(" ").Where(s => s != string.Empty).ToArray();
     }
     else
        outer = parts1[0].Split(" ");

     var res = new Group
     {
        ArmyInd = armyInd,
        GroupInd = groupInd,

        Count = int.Parse(outer[0]),
        Hp = int.Parse(outer[4]),
        Damage = int.Parse(outer[12]) + (armyInd == IMMUNE_SYSTEM ? BOOST : 0),
        AttackType = outer[13],
        Initiative = int.Parse(outer[17]),

        WeakTo = [],
        ImmuneTo = []
     };

     if (inner != null)
     {
        var parts = inner.Split("| ");
        foreach (var part in parts)
        {
           var words = part.Split(" ");
           var set = words[0] == "weak" ? res.WeakTo : res.ImmuneTo;
           foreach (var word in string.Join(" ", words.Skip(2)).Split(", "))
              set.Add(word);
        }
     }

     return res;
  }

  class Group
  {
     public int ArmyInd;
     public int GroupInd;

     public int Count;
     public int Hp;
     public int Damage;
     public int Initiative;
     public string AttackType;

     public HashSet<string> WeakTo;
     public HashSet<string> ImmuneTo;

     public int EffectivePower() => Count * Damage;

     public Group TargetedBy;
     public Group Targets;
  }

  const int IMMUNE_SYSTEM = 0, INFECTION = 1;
  readonly string[] _headlines = ["Immune System:", "Infection:"];
  readonly List<Group>[] _armies = Enumerable.Range(0, 2).Select(i => new List<Group>()).ToArray();
  readonly int[] _totalCount = new int[2];

  void Fight()
  {
     TargetSelection();
     Attack();
  }

  void TargetSelection()
  {
     ResetTargeting();
     foreach (var group in ActiveGroupsOrderedForTargetSelection())
     {
        var potentialDamageTo = new Dictionary<Group, int>();
        foreach (var opposingGroup in ActiveGroups()
            .Where(g => g.ArmyInd != group.ArmyInd && g.TargetedBy == null))
        {
           var damage = CalcDamage(group, opposingGroup);
           potentialDamageTo[opposingGroup] = damage;
           var headline = _headlines[group.ArmyInd];
           var headlineNoColon = headline[0..^1];
        }
        var target = potentialDamageTo.Keys
            .Where(t => potentialDamageTo[t] > 0)
            .OrderByDescending(t => (potentialDamageTo[t] * 1000000L) + (t.EffectivePower() * 1000L) + t.Initiative)
            .FirstOrDefault();
        if (target != null)
        {
           group.Targets = target;
           target.TargetedBy = group;
        }
     }
  }

  void ResetTargeting()
  {
     foreach (var group in ActiveGroups())
        group.TargetedBy = group.Targets = null;
  }

  static int CalcDamage(Group from, Group to) => from.EffectivePower() *
     (to.ImmuneTo.Contains(from.AttackType)
        ? 0
        : (to.WeakTo.Contains(from.AttackType)
           ? 2
           : 1));

  void Attack()
  {
     foreach (var group in ActiveGroupsOrderedForAttack())
        if (group.Count > 0)
        {
           var target = group.Targets;
           if (target != null)
           {
              var unitsKilled = Math.Min(target.Count, CalcDamage(group, target) / target.Hp);
              target.Count -= unitsKilled;
              _totalCount[target.ArmyInd] -= unitsKilled;
           }
        }
  }

  IEnumerable<Group> ActiveGroups() =>
      _armies[IMMUNE_SYSTEM].Union(_armies[INFECTION]).Where(g => g.Count > 0);

  IEnumerable<Group> ActiveGroupsOrderedForTargetSelection() =>
      ActiveGroups().OrderByDescending(g => (g.EffectivePower() * 1000) + g.Initiative);

  IEnumerable<Group> ActiveGroupsOrderedForAttack() => ActiveGroups().OrderByDescending(g => g.Initiative);
}
