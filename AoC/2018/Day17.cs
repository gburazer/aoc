﻿using System;
using System.Collections.Generic;

namespace AoC._2018;

public class Day17(string input) : AoC(input)
{
  public override void Solve()
  {
     ParseInput();
     Init();
     Spread(ToRelX(500), 1);
     Part1 = CountInField([Wvert, Whoriz]).ToString();
     Part2 = CountInField([Whoriz]).ToString();
  }

  void ParseInput()
  {
     _minx = int.MaxValue; _miny = int.MaxValue;
     foreach (var line in InputLines.Value)
     {
        var parts = line.Split(", ");
        var lox = 0; var loy = 0; var hix = 0; var hiy = 0;
        foreach (var part in parts)
        {
           var innerParts = part.Split("=");
           int lo, hi;
           if (innerParts[1].Contains(".."))
           {
              var values = innerParts[1].Split("..");
              lo = int.Parse(values[0]); hi = int.Parse(values[1]);
           }
           else
              lo = hi = int.Parse(innerParts[1]);
           if (innerParts[0][0] == 'x') { lox = lo; hix = hi; } else { loy = lo; hiy = hi; }
        }
        _minx = Math.Min(_minx, lox); _miny = Math.Min(_miny, loy);
        _maxx = Math.Max(_maxx, hix); _maxy = Math.Max(_maxy, hiy);
        _xs.Add((lox, hix)); _ys.Add((loy, hiy));
     }
  }

  void Init()
  {
     _sizex = _maxx - _minx + 3; _sizey = _maxy + 3;
     _field = new char[_sizex, _sizey];
     for (var y = 0; y < _sizey; y++)
        for (var x = 0; x < _sizex; x++)
           _field[x, y] = Sand;
     for (var i = 0; i < _xs.Count; i++)
        for (var y = _ys[i].from; y <= _ys[i].to; y++)
           for (var x = ToRelX(_xs[i].from); x <= ToRelX(_xs[i].to); x++)
              _field[x, y] = Clay;
     _field[ToRelX(500), 0] = Spring;
  }

  readonly List<(int from, int to)> _xs = [], _ys = [];
  char[,] _field;
  int _minx, _maxx, _miny, _maxy, _sizex, _sizey;
  const char Sand = '.', Clay = '#', Spring = '+', Wvert = '|', Whoriz = '~';

  int ToRelX(int x) => x - _minx + 1;

  void Spread(int x, int y)
  {
     // out of (y) bounds, terminate
     if (y == 0 || y == _sizey - 1)
        return;

     // run through sand
     if (_field[x, y] == Sand)
        _field[x, y] = Wvert;

     // what's down?
     if (_field[x, y + 1] == Sand)
        Spread(x, y + 1);
     else if (_field[x, y + 1] is Clay or Whoriz)
     {
        var toLeft = SpreadHorizontal(x, y, -1);
        var toRight = SpreadHorizontal(x, y, 1);
        if (toLeft >= 0 && toRight >= 0) // bounded on both ends - gonna end up as still (horizontal) water
        {
           for (var col = x - toLeft; col <= x + toRight; col++)
              _field[col, y] = Whoriz;
           Spread(x, y - 1);
        }
        else // bounded on one end - gonna end up as running (vertical) water
        {
           for (var col = x - Math.Abs(toLeft); col <= x + Math.Abs(toRight); col++)
              _field[col, y] = Wvert;
           if (toLeft < 0)
              Spread(x + toLeft, y + 1);
           if (toRight < 0)
              Spread(x - toRight, y + 1);
        }
     }
  }

  // positive value for bounded with clay horizontally
  // negative value for bounded with sand or water below (will keep running)
  // reaching edge must mean there will be water running below, as clay xs are finite
  int SpreadHorizontal(int x, int y, int step)
  {
     var spread = 0;
     for (; x > 0 && x < _sizex - 1 && _field[x + step, y] != Clay; x += step, spread++)
        if (_field[x, y + 1] is Sand or Wvert)
           return -spread;
     return (x == 0 || x == _sizex - 1 ? -1 : 1) * spread;
  }

  int CountInField(char[] toCount)
  {
     var res = 0;
     for (var x = 0; x < _sizex; x++)
        for (var y = _miny; y <= _maxy; y++)
           foreach (var t in toCount)
              if (_field[x, y] == t)
                 res++;

     return res;
  }
}
