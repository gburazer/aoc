﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2018;

public class Day16(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value;
     int[] parse(string line)
     {
        var res = new int[R.Length];
        var rs = line.Split("[")[1][..10].Split(", ");
        for (var i = 0; i < R.Length; i++)
           res[i] = int.Parse(rs[i]);
        return res;
     }
     void set(int[] state) { for (var j = 0; j < R.Length; j++) R[j] = state[j]; }
     bool test(int[] state) => Enumerable.Range(0, R.Length).All(i => R[i] == state[i]);
     HashSet<string> behavesLike(int[] before, Instruction instruction, int[] after)
     {
        return _instructions.Where(i =>
        {
           set(before);
           new Instruction(instruction) { Action = i.Action }.Run();
           return test(after);
        }).Select(i => i.Opcode).ToHashSet();
     }
     var p1 = 0;
     var lineInd = 0;
     var matchesByOpInd = new Dictionary<int, HashSet<string>>();
     for (; lines[lineInd].StartsWith("Before: "); lineInd++)
     {
        var before = parse(lines[lineInd++]);
        var instruction = Instruction.Parse(lines[lineInd++]);
        var after = parse(lines[lineInd++]);

        var matches = behavesLike(before, instruction, after);
        if (matches.Count >= 3)
           p1++;

        var opind = instruction.Opind;
        if (!matchesByOpInd.TryGetValue(opind, out var value))
           matchesByOpInd[opind] = [..matches];
        else
           value.IntersectWith(matches);
     }
     var solved = new HashSet<int>();
     while (solved.Count < matchesByOpInd.Keys.Count)
     {
        var remaining = matchesByOpInd.Keys.Except(solved).ToList();
        var decidedOpInd = remaining.First(opind => matchesByOpInd[opind].Count == 1);
        var decidedOp = matchesByOpInd[decidedOpInd].Single();
        foreach (var key in remaining.Where(opind => opind != decidedOpInd))
           matchesByOpInd[key].Remove(decidedOp);
        solved.Add(decidedOpInd);
     }
     var instructions = Enumerable.Range(0, _instructions.Length)
         .Select(opind => _instructions.Single(i => matchesByOpInd[opind].Single() == i.Opcode))
         .ToArray();
     while (lines[lineInd++] == "") { }
     set([0, 0, 0, 0]);
     for (; lineInd < lines.Length; lineInd++)
     {
        var instruction = Instruction.Parse(lines[lineInd]);
        instruction.Action = instructions[instruction.Opind].Action;
        instruction.Run();
     }

     Part1 = p1.ToString();
     Part2 = R[0].ToString();
  }

  static readonly int[] R = new int[4]; // TODO get rid of static

  class Instruction(string opcode, Action<int, int, int> action)
  {
     public Instruction(Instruction i) : this(i.Opcode, i.Action)
     {
        Opind = i.Opind; _a = i._a; _b = i._b; _output = i._output;
     }

     public readonly string Opcode = opcode;
     public int Opind;
     int _a, _b, _output;

     public Action<int, int, int> Action = action;
     public void Run() { Action(_a, _b, _output); }

     public static Instruction Parse(string str)
     {
        var arr = str.Split(" ");
        return new Instruction("?", (_, _, _) => { })
        {
           Opind = int.Parse(arr[0]),
           _a = int.Parse(arr[1]),
           _b = int.Parse(arr[2]),
           _output = int.Parse(arr[3])
        };
     }
  }

  readonly Instruction[] _instructions =
  [
     new("addr", (a, b, c) => R[c] = R[a] + R[b]),
     new("addi", (a, b, c) => R[c] = R[a] + b),

     new("mulr", (a, b, c) => R[c] = R[a] * R[b]),
     new("muli", (a, b, c) => R[c] = R[a] * b),

     new("banr", (a, b, c) => R[c] = R[a] & R[b]),
     new("bani", (a, b, c) => R[c] = R[a] & b),

     new("borr", (a, b, c) => R[c] = R[a] | R[b]),
     new("bori", (a, b, c) => R[c] = R[a] | b),

     new("setr", (a, _, c) => R[c] = R[a]),
     new("seti", (a, _, c) => R[c] = a),

     new("gtir", (a, b, c) => R[c] = a > R[b] ? 1 : 0),
     new("gtri", (a, b, c) => R[c] = R[a] > b ? 1 : 0),
     new("gtrr", (a, b, c) => R[c] = R[a] > R[b] ? 1 : 0),

     new("eqir", (a, b, c) => R[c] = a == R[b] ? 1 : 0),
     new("eqri", (a, b, c) => R[c] = R[a] == b ? 1 : 0),
     new("eqrr", (a, b, c) => R[c] = R[a] == R[b] ? 1 : 0),
  ];
}
