﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2018;

public class Day04 : AoC
{
   readonly int _n;
   readonly (DateTime dt, int id, bool fallsAsleep)[] _entries;

  public Day04(string input) : base(input)
  {
     _n = InputLines.Value.Length;
     _entries = new (DateTime dt, int id, bool fallsAsleep)[_n];
  }

  public override void Solve()
  {
     ParseInput();
     Array.Sort(_entries, (e1, e2) => e1.dt < e2.dt ? -1 : 1);
     ProcessAll();
     Part1 = SolvePart1();
     Part2 = SolvePart2();
  }

  readonly Dictionary<int, int>[] _asleepAt = Enumerable.Range(0, 60).Select(_ => new Dictionary<int, int>()).ToArray();
  readonly Dictionary<int, int> _minutesPerId = [];

  void ParseInput()
  {
     for (var i = 0; i < _n; i++)
     {
        var line = InputLines.Value[i];
        var parts = line.Split(" ");
        var dtstr = parts[0][1..] + "T" + parts[1][0..^1] + ":00";
        _entries[i].dt = DateTime.Parse(dtstr);
        if (parts[2] == "Guard")
           _entries[i].id = int.Parse(parts[3][1..]);
        else
           _entries[i].fallsAsleep = parts[2] == "falls";
     }
  }

  void ProcessAll()
  {
     var id = -1;
     for (var i = 0; i < _n; i++)
     {
        var e = _entries[i];
        if (e.id > 0)
        {
           id = e.id;
           _minutesPerId.TryAdd(id, 0);
        }
        else if (e.fallsAsleep)
        {
           var minuteUntil = 59;
           if (i + 1 < _n)
           {
              var nexte = _entries[i + 1];
              if (nexte.dt.Date == e.dt.Date && !nexte.fallsAsleep)
              {
                 minuteUntil = nexte.dt.Minute;
                 i++;
              }
           }
           _minutesPerId[id] += minuteUntil - e.dt.Minute + 1;
           for (var minute = e.dt.Minute; minute <= minuteUntil; minute++) 
              _asleepAt[minute][id] = _asleepAt[minute].TryGetValue(id, out var value) ? ++value : 0;
        }
     }
  }

  string SolvePart1()
  {
     var idSleepingMax = _minutesPerId.Aggregate((i1, i2) => i1.Value > i2.Value ? i1 : i2).Key;
     var sleepingMaxMinute = -1; var sleepingMax = 0;
     for (var minute = 0; minute < 60; minute++)
        if (_asleepAt[minute].TryGetValue(idSleepingMax, out var value) && value > sleepingMax)
        {
           sleepingMaxMinute = minute;
           sleepingMax = value;
        }
     return (idSleepingMax * (sleepingMaxMinute + 1)).ToString();
  }

  string SolvePart2()
  {
     var maxTimesAsleep = 0; var idMaxTimesAsleep = -1; var maxTimesAsleepMinute = -1;
     for (var minute = 0; minute < 60; minute++)
        foreach (var timesAsleepInMinute in _asleepAt[minute])
           if (timesAsleepInMinute.Value > maxTimesAsleep)
           {
              idMaxTimesAsleep = timesAsleepInMinute.Key;
              maxTimesAsleep = timesAsleepInMinute.Value;
              maxTimesAsleepMinute = minute;
           }
     return (idMaxTimesAsleep * maxTimesAsleepMinute).ToString();
  }
}
