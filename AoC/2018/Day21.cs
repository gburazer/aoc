﻿using System;
using System.Collections.Generic;

namespace AoC._2018;

public class Day21(string input) : AoC(input)
{
  public override void Solve()
  {
     ParseInput();

     Test();

     // TODO
     Part1 = "foo";
     Part2 = "bar";
  }

  void ParseInput()
  {
     var lines = InputLines.Value;
     _ipr = int.Parse(lines[0].Split(" ")[1]);
     for (var i = 1; i < lines.Length; i++)
        _program.Add(Instruction.Parse(lines[i]));
  }

  // run 1 & 2 for many operations and make sure result is the same
  void Test()
  {
     void init() { for (var i = 0; i < R.Length; i++) R[i] = 0; }
     int[] clone() { var res = new int[R.Length]; R.CopyTo(res, 0); return res; }
     /*
     bool cmp(int[] r1, int[] r2) 
     {
         if (r1.Length != r2.Length)
             return false;
         for (var i = 0; i < r1.Length; i++)
             if (i != _ipr && r1[i] != r2[i])
                 return false;
         return true;
     }
     */
     const long OP_COUNT = 10000000000;
     //init(); Run(opCount); var t1 = clone();
     init(); Run2(OP_COUNT); var t2 = clone();
     //Console.WriteLine(cmp(t1, t2) ? "OK!" : "Error!");
  }

  /*
  void Run(long opCount)
  {
     //Console.WriteLine("Instruction count: " + _program.Count);
     //void writeState() { Console.Write($"[{string.Join(", ", R.Select(r => r.ToString()))}] "); }
     var opInd = 0;
     for (_ip = 0; opInd < opCount && _ip >= 0 && _ip < _program.Count; _ip++, opInd++)
     {
        var instruction = _program[_ip];
        //Console.Write($"opInd={opInd}, ip={_ip} "); writeState(); instruction.Write();
        R[_ipr] = _ip;
        instruction.Run();
        _ip = (int)R[_ipr];
        //writeState(); Console.WriteLine("Press backspace .."); //Console.ReadKey();
     }
  }
  */

  void Run2(long opCount)
  {
     var init = 0; var r1 = 0; var target = 0; var r4 = 0; var r5 = 0;
     var program2 = new Action[]
     {
            () => target = 123,                  // 00 seti 123 0 3
            () => target &= 456,                 // 01 bani 3 456 3
            () => target = target == 72 ? 1 : 0, // 02 eqri 3 72 3
            () => _ip += target,                 // 03 addr 3 2 2
            () => _ip = 0,                       // 04 seti 0 0 2
            () => target = 0,                    // 05 seti 0 0 3
            () => r4 = target | 65536,           // 06 bori 3 65536 4
            () => target = 10649702,             // 07 seti 10649702 3 3
            () => r5 = r4 & 255,                 // 08 bani 4 255 5
            () => target += r5,                  // 09 addr 3 5 3
            () => target &= 16777215,            // 10 bani 3 16777215 3
            () => target *= 65899,               // 11 muli 3 65899 3
            () => target &= 16777215,            // 12 bani 3 16777215 3
            () => r5 = 256 > r4 ? 1 : 0,         // 13 gtir 256 4 5
            () => _ip += r5,                     // 14 addr 5 2 2
            () => _ip++,                         // 15 addi 2 1 2
            () => _ip = 27,                      // 16 seti 27 7 2
            () => r5 = 0,                        // 17 seti 0 6 5
            () => r1 = r5 + 1,                   // 18 addi 5 1 1
            () => r1 *= 256,                     // 19 muli 1 256 1
            () => r1 = r1 > r4 ? 1 : 0,          // 20 gtrr 1 4 1
            () => _ip += r1,                     // 21 addr 1 2 2
            () => _ip++,                         // 22 addi 2 1 2
            () => _ip = 25,                      // 23 seti 25 9 2
            () => r5++,                          // 24 addi 5 1 5
            () => _ip = 17,                      // 25 seti 17 9 2
            () => r4 = r5,                       // 26 setr 5 7 4
            () => _ip = 7,                       // 27 seti 7 1 2
            () => r5 = target == init ? 1 : 0,   // 28 eqrr 3 0 5
            () => _ip += r5,                     // 29 addr 5 2 2
            () => _ip = 5                        // 30 seti 5 4 2
     };
     //Console.WriteLine("Instruction count: " + _program.Count);
     //void writeState() { Console.Write($"[{string.Join(", ", R.Select(r => r.ToString()))}] "); }
     var opInd = 0L;
     var solutions = new HashSet<int>();
     for (_ip = 0; opInd < opCount && _ip >= 0 && _ip < program2.Length; _ip++, opInd++)
     {
        //Console.Write($"opInd={opInd}, ip={_ip}, before: "); writeState();
        program2[_ip]();
        if (_ip == 28)
        {
           //if (!solutions.Contains(target))
           //; // Console.WriteLine($"opind {opInd}, new value found ({target}), count becomes {solutions.Count + 1}");
           solutions.Add(target);
        }
        //Console.Write(", after: "); writeState(); Console.WriteLine();
     }
     R[0] = init; R[1] = r1; R[3] = target; R[4] = r4; R[5] = r5;
  }

  class Instruction
  {
     public static Instruction Parse(string str)
     {
        var parts = str.Split(" ");
        return new Instruction
        {
           _opcode = parts[0],
           Action = Actions[parts[0]],
           _a = int.Parse(parts[1]),
           _b = int.Parse(parts[2]),
           _output = int.Parse(parts[3])
        };
     }

     public Action<int, int, int> Action;

     public void Run() { Action(_a, _b, _output); }
     public void Write() { Console.Write($"{_opcode} {_a} {_b} {_output} "); }

     string _opcode;
     int _a, _b, _output;
  }

  static readonly int[] R = new int[6]; // TODO get rid of static
  int _ip, _ipr;
  readonly List<Instruction> _program = [];
  static readonly Dictionary<string, Action<int, int, int>> Actions = new() // TODO get rid of static
  {
     ["addr"] = (a, b, c) => R[c] = R[a] + R[b],
     ["addi"] = (a, b, c) => R[c] = R[a] + b,

     ["mulr"] = (a, b, c) => R[c] = R[a] * R[b],
     ["muli"] = (a, b, c) => R[c] = R[a] * b,

     ["banr"] = (a, b, c) => R[c] = R[a] & R[b],
     ["bani"] = (a, b, c) => R[c] = R[a] & b,

     ["borr"] = (a, b, c) => R[c] = R[a] | R[b],
     ["bori"] = (a, b, c) => R[c] = R[a] | b,

     ["setr"] = (a, b, c) => R[c] = R[a],
     ["seti"] = (a, b, c) => R[c] = a,

     ["gtir"] = (a, b, c) => R[c] = a > R[b] ? 1 : 0,
     ["gtri"] = (a, b, c) => R[c] = R[a] > b ? 1 : 0,
     ["gtrr"] = (a, b, c) => R[c] = R[a] > R[b] ? 1 : 0,

     ["eqir"] = (a, b, c) => R[c] = a == R[b] ? 1 : 0,
     ["eqri"] = (a, b, c) => R[c] = R[a] == b ? 1 : 0,
     ["eqrr"] = (a, b, c) => R[c] = R[a] == R[b] ? 1 : 0,
  };
}
