﻿using System.Text;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2018;

public class Day02(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value;
     var n = lines.Length; var len = lines[0].Length;
     var n2S = 0; var n3S = 0;
     Part2 = NoSolution;
     for (var i = 0; i < n; i++)
     {
        var count = new Dictionary<char, int>();
        foreach (var c in lines[i]) 
           count[c] = count.TryGetValue(c, out var value) ? ++value : 1;

        if (count.Values.Any(v => v == 2)) n2S++;
        if (count.Values.Any(v => v == 3)) n3S++;

        if (Part2 != NoSolution) continue;
        
        for (var j = 0; j < i; j++)
        {
           var ndiffs = 0;
           for (var k = 0; k < len && ndiffs < 2; k++)
              if (lines[i][k] != lines[j][k])
                 ndiffs++;

           if (ndiffs != 1) continue;
           
           var sol = new StringBuilder();
           for (var k = 0; k < len; k++)
              if (lines[i][k] == lines[j][k])
                 sol.Append(lines[i][k]);
           Part2 = sol.ToString();
           break;
        }
     }

     Part1 = (n2S * n3S).ToString();
  }
}
