﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2018;

public class Day25(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value;
     var n = lines.Length;
     const int DIM = 4;
     var vs = new int[n][];
     var sameConstellation = new bool[n, n];
     int manhattan(int i1, int i2) => Enumerable.Range(0, DIM).Sum(i => Math.Abs(vs[i1][i] - vs[i2][i]));
     for (var ind = 0; ind < n; ind++)
     {
        var parts = lines[ind].Split(",");
        vs[ind] = Enumerable.Range(0, DIM).Select(i => int.Parse(parts[i])).ToArray();
        for (var i = 0; i < ind; i++)
           sameConstellation[ind, i] = sameConstellation[i, ind] = manhattan(ind, i) <= 3;
     }
     var constellations = new List<HashSet<int>>();
     for (var ind = 0; ind < n; ind++) // TODO PERF calc all on 1st pass
     {
        // new star belongs to some (number of) constellations
        var adjacentTo = new List<int>();
        for (var cind = 0; cind < constellations.Count; cind++)
           if (constellations[cind].Any(v => sameConstellation[v, ind]))
              adjacentTo.Add(cind);
        switch (adjacentTo.Count)
        {
           case 0: // new constellation is born
              constellations.Add([ind]);
              break;
           case 1: // add to only adjacent constellation
              constellations[adjacentTo.Single()].Add(ind);
              break;
           default: // merge all adjacent constellations + add to merged
              for (var i = adjacentTo.Count - 1; i > 0; i--)
              {
                 constellations[adjacentTo[0]].UnionWith(constellations[adjacentTo[i]]);
                 constellations.RemoveAt(adjacentTo[i]);
              }
              constellations[adjacentTo[0]].Add(ind);
              break;
        }
     }
     Part1 = constellations.Count.ToString();

     Part2 = NoSolution; // day 25 only has part1
  }
}
