﻿using System.Collections.Generic;
using System.Linq;

namespace AoC._2018;

public class Day08 : AoC
{
   readonly int[] _input;
   readonly Node _root;

  public Day08(string input) : base(input)
  {
     _input = InputLines.Value[0].Split(" ").Select(int.Parse).ToArray();

     var ind = 0;
     _root = ReadNode(ref ind);
  }

  class Node
  {
     public List<Node> Children;
     public List<int> Metadata;

     public int MetadataSum() => Metadata.Sum() + Children.Sum(c => c.MetadataSum());

     public int Value() => Children.Count == 0
        ? Metadata.Sum()
        : Metadata.Where(m => m > 0 && m <= Children.Count).Sum(m => Children[m - 1].Value());
  }

  Node ReadNode(ref int ind)
  {
     var node = new Node { Children = new List<Node>(_input[ind++]), Metadata = new List<int>(_input[ind++]) };
     for (var i = 0; i < node.Children.Capacity; i++) node.Children.Add(ReadNode(ref ind));
     for (var i = 0; i < node.Metadata.Capacity; i++) node.Metadata.Add(_input[ind++]);
     return node;
  }

  public override void Solve()
  {
     Part1 = _root.MetadataSum().ToString();
     Part2 = _root.Value().ToString();
  }
}
