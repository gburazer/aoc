﻿using System.Linq;

namespace AoC._2018;

public class Day05(string input) : AoC(input)
{
  public override void Solve()
  {
     var str = InputLines.Value[0];
     Part1 = Solve(str).ToString();
     Part2 = str.ToUpper().Distinct().Min(
        c => Solve(str.Replace(c.ToString(), "").Replace(char.ToLower(c).ToString(), "")))
        .ToString();
  }

  static int Solve(string str)
  {
     return str.ToCharArray().Aggregate((chars: new char[str.Length], len: 0), (res, c) =>
     {
        if (res.len > 0
               && char.ToUpper(res.chars[res.len - 1]) == char.ToUpper(c)
               && char.IsUpper(res.chars[res.len - 1]) != char.IsUpper(c))
           res.len--;
        else
           res.chars[res.len++] = c;
        return res;
     }).len;
  }
}
