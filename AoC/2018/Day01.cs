﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2018;

public class Day01 : AoC
{
   readonly int[] _ns;
   
  public Day01(string input) : base(input)
  {
     _ns = InputLines.Value.Select(int.Parse).ToArray();
  }

  public override void Solve()
  {
     Part1 = _ns.Aggregate(0, (a, n) => a + n).ToString();

     var seen = new HashSet<int>();
     var cur = 0; var dup = -1;
     for (var i = 0; dup == -1; i = (i + 1) % _ns.Length)
     {
        cur += _ns[i];
        if (!seen.Add(cur))
           dup = cur;
     }
     Part2 = dup.ToString();
  }
}
