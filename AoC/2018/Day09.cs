﻿using System.Linq;

namespace AoC._2018;

public class Day09(string input) : AoC(input)
{
  public override void Solve() // TODO use regex?
  {
     var parts = Input.Split("; ");
     var playerCount = int.Parse(parts[0].Split(" ")[0]);
     var lastMarble = int.Parse(parts[1].Split(" ")[4]);

     Part1 = HighScore(playerCount, lastMarble).ToString();
     Part2 = HighScore(playerCount, 100 * lastMarble).ToString();
  }

  static long HighScore(int playerCount, int lastMarble)
  {
     var scores = new long[playerCount];
     var list = new CircularList<int>();
     var cur = list.AddFirst(0);
     for (var marble = 1; marble <= lastMarble; marble++)
     {
        var player = marble % playerCount;
        if (marble % 23 == 0)
        {
           scores[player] += marble;
           for (var i = 0; i < 7; i++)
              cur = list.Previous(cur);
           scores[player] += cur.Value;
           var toRemove = cur;
           cur = list.Next(cur);
           list.Remove(toRemove);
        }
        else
        {
           cur = list.Next(cur);
           cur = list.AddAfter(cur, marble);
        }
     }
     return scores.Max();
  }
}
