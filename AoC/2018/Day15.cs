﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2018;

public class Day15(string input) : AoC(input)
{
  public override void Solve()
  {
     int outcome(int fullRoundsCompleted) => fullRoundsCompleted * Units.Where(u => !u.IsDead()).Sum(u => u.HitPoints);

     InitAndRunAndMaybeFailOnDeadElf(elvesPower: 3, failOnDeadElf: false, out var p1FullRounds);
     Part1 = outcome(p1FullRounds).ToString();

     int p2FullRounds;
     for (var elvesPower = 4;
         !InitAndRunAndMaybeFailOnDeadElf(elvesPower, failOnDeadElf: true, out p2FullRounds);
         elvesPower++)
     {
     }
     Part2 = outcome(p2FullRounds).ToString();
  }

  bool InitAndRunAndMaybeFailOnDeadElf(int elvesPower, bool failOnDeadElf, out int fullRoundsCompleted)
  {
     ParseInput(elvesPower);
     fullRoundsCompleted = 0;
     var outOfEnemies = false;
     while (!outOfEnemies)
     {
        //Display(fullRoundsCompleted);
        var orderedUnits = Units.Where(u => !u.IsDead()).OrderBy(u => Value(u.Pos)).ToList();
        foreach (var unit in orderedUnits)
        {
           var elfDied = Units.Any(u => !u.IsGoblin && u.IsDead());
           if (failOnDeadElf && elfDied)
              return false;
           var undead = orderedUnits.Where(u => !u.IsDead()).ToList();
           var goblins = undead.Where(u => u.IsGoblin).ToList();
           var elves = undead.Where(u => !u.IsGoblin).ToList();
           if (!goblins.Any() || !elves.Any())
           {
              outOfEnemies = true;
              break;
           }
           if (!unit.IsDead())
              unit.TakeTurn(elves, goblins);
        }
        if (!outOfEnemies)
           fullRoundsCompleted++;
     }
     return true;
  }

  void ParseInput(int elvesPower)
  {
     var lines = InputLines.Value;
     _sizex = lines[0].Length; _sizey = lines.Length;
     _field = new char[_sizex, _sizey];
     Units.Clear();
     for (var x = 0; x < _sizex; x++)
        for (var y = 0; y < _sizey; y++)
        {
           var c = lines[y][x];
           _field[x, y] = c;
           if (Unit.IsUnit(c))
              Units.Add(new Unit(c, x, y, c == Elf ? elvesPower : 3));
        }
  }

  /*
  static void Display(int round)
  {
     Console.WriteLine($"State after round {round} is fully completed:");
     for (var y = 0; y < _sizey; y++)
     {
        for (var x = 0; x < _sizex; x++)
           Console.Write(_field[x, y]);
        var rowStatus = string.Join(", ", _units
            .Where(u => !u.IsDead() && u.Pos.y == y)
            .Select(u => $"{_field[u.Pos.x, u.Pos.y]}({u.HitPoints})"));
        Console.WriteLine("  " + rowStatus);
     }
     Console.WriteLine("Press any key .."); Console.ReadLine();
  }
  */

  static int Value((int x, int y) pos) => pos.y * _sizex + pos.x;

  // TODO REFACTORING get rid of static fields
  static int _sizex, _sizey;
  static char[,] _field;
  static readonly List<Unit> Units = [];

  const char Goblin = 'G', Elf = 'E', Open = '.';

  class Unit(char c, int x, int y, int attackPower)
  {
     public static bool IsUnit(char c) => c is Goblin or Elf;

     public readonly bool IsGoblin = c == Goblin;
     public (int x, int y) Pos = (x, y);
     public int HitPoints = 200;

     public bool IsDead() => HitPoints <= 0;

     public void TakeTurn(IEnumerable<Unit> elves, IEnumerable<Unit> goblins)
     {
        var targets = (IsGoblin ? elves : goblins).ToList();
        if (targets.Count == 0) return;
        var targetsInRange = TargetsInRange(targets);
        if (targetsInRange.Count == 0)
        {
           Move(targets);
           targetsInRange = TargetsInRange(targets);
        }
        var target = targetsInRange.OrderBy(t => t.HitPoints * 1000 + Value(t.Pos)).FirstOrDefault();
        if (target != null)
           Attack(target);
     }

     IList<Unit> TargetsInRange(IList<Unit> targets) => targets.Where(t => IsInRange(t.Pos)).ToList();
     bool IsInRange((int x, int y) pos) =>
         (pos.x == Pos.x && (pos.y == Pos.y - 1 || pos.y == Pos.y + 1)) ||
         (pos.y == Pos.y && (pos.x == Pos.x - 1 || pos.x == Pos.x + 1));

     void Move(IEnumerable<Unit> targets)
     {
        var range = GetRange(targets);
        var shortestPaths = GenerateShortestPaths(range);
        var moveTo = shortestPaths.OrderBy(sp => Value(sp.TargetRange) * 1000 + Value(sp.FirstStep)).FirstOrDefault();
        if (moveTo == null) return;
        var (x, y) = Pos;
        Pos = moveTo.FirstStep;
        _field[Pos.x, Pos.y] = _field[x, y];
        _field[x, y] = Open;
     }

     static HashSet<(int x, int y)> GetRange(IEnumerable<Unit> targets)
     {
        var res = new HashSet<(int x, int y)>();
        void maybeAddToRange(int x, int y) { if (_field[x, y] == Open) res.Add((x, y)); }
        foreach (var t in targets)
        {
           maybeAddToRange(t.Pos.x - 1, t.Pos.y);
           maybeAddToRange(t.Pos.x + 1, t.Pos.y);
           maybeAddToRange(t.Pos.x, t.Pos.y - 1);
           maybeAddToRange(t.Pos.x, t.Pos.y + 1);
        }
        return res;
     }

     IEnumerable<ShortestPath> GenerateShortestPaths(HashSet<(int x, int y)> range)
     {
        var res = new List<ShortestPath>();
        foreach (var targetRange in range)
           res.AddRange(DeconstructShortestPaths(RunBfs(), targetRange));
        if (res.Count == 0) return res;
        var minLength = res.Min(r => r.Length);
        return res.Where(r => r.Length == minLength);
     }

     (int dist, (int x, int y) from)[,] RunBfs()
     {
        var auxField = new (int dist, (int x, int y) from)[_sizex, _sizey];
        var q = new Queue<(int x, int y)>();
        void add((int dist, (int x, int y) from) el, (int x, int y) pos)
        {
           q.Enqueue(pos);
           auxField[pos.x, pos.y] = el;
        }
        add((0, (0, 0)), (Pos.x, Pos.y));
        void tryEnqueue((int dist, (int x, int y) from) el, (int x, int y) pos)
        {
           var (dist, from) = auxField[pos.x, pos.y];
           int cmpFrom((int x, int y) from1, (int x, int y) from2) => Value(from1) - Value(from2);
           if (_field[pos.x, pos.y] == Open &&
               (dist == 0 || (dist == el.dist && cmpFrom(el.from, from) < 0)))
           {
              add(el, pos);
           }
        }
        while (q.Count > 0)
        {
           var cur = q.Dequeue();
           var newDist = auxField[cur.x, cur.y].dist + 1;
           tryEnqueue((newDist, cur), (cur.x + 1, cur.y));
           tryEnqueue((newDist, cur), (cur.x - 1, cur.y));
           tryEnqueue((newDist, cur), (cur.x, cur.y + 1));
           tryEnqueue((newDist, cur), (cur.x, cur.y - 1));
        }
        return auxField;
     }

     static List<ShortestPath> DeconstructShortestPaths(
        (int dist, (int x, int y) from)[,] auxField,
        (int x, int y) targetRange)
     {
        if (auxField[targetRange.x, targetRange.y].dist == 1)
           return [new ShortestPath { Length = 1, TargetRange = targetRange, FirstStep = targetRange }];

        var res = new List<ShortestPath>();
        var elements = new List<(int dist, (int x, int y) pos)>();
        void maybeAdd(int x, int y)
        {
           var d = auxField[x, y].dist;
           if (d > 0)
              elements.Add((d, (x, y)));
        }
        maybeAdd(targetRange.x - 1, targetRange.y);
        maybeAdd(targetRange.x + 1, targetRange.y);
        maybeAdd(targetRange.x, targetRange.y - 1);
        maybeAdd(targetRange.x, targetRange.y + 1);
        if (elements.Count == 0) return res;
        var minDist = elements.Min(el => el.dist);
        var best = elements.Where(el => el.dist == minDist);
        foreach (var el in best)
        {
           var sp = new ShortestPath { Length = minDist, TargetRange = targetRange };
           for (var cur = el; cur.dist > 0; cur = (cur.dist - 1, auxField[cur.pos.x, cur.pos.y].from))
              if (cur.dist == 1)
                 sp.FirstStep = cur.pos;
           res.Add(sp);
        }
        return res;
     }

     class ShortestPath
     {
        public (int x, int y) FirstStep;
        public int Length;
        public (int x, int y) TargetRange;
     }

     void Attack(Unit target)
     {
        target.HitPoints -= attackPower;
        if (target.HitPoints <= 0)
           _field[target.Pos.x, target.Pos.y] = Open;
     }
  }
}
