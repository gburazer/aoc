﻿// TODO optimise (dynamic programming)

namespace AoC._2018;

public class Day11(string input) : AoC(input)
{
  public override void Solve()
  {
     Init();
     //CalcAll(); // TODO

     var (x3, y3, _) = Calc(3);
     Part1 = $"{x3},{y3}";

     var bestx = 0; var besty = 0; var best = 0; var bestSquareSize = 0;
     for (var squareSize = 1; squareSize <= N; squareSize++)
     {
        var (x, y, sum) = Calc(squareSize);
        if (sum <= best) continue;
        bestx = x; besty = y; best = sum;
        bestSquareSize = squareSize;
     }
     Part2 = $"{bestx},{besty},{bestSquareSize}";
  }

  void Init()
  {
     var serial = int.Parse(InputLines.Value[0]);
     for (var y = 1; y <= N; y++)
        for (var x = 1; x <= N; x++)
        {
           var rackId = x + 10;
           _power[y - 1, x - 1] = (rackId * y + serial) * rackId / 100 % 10 - 5;
        }
  }

  (int bestx, int besty, int best) Calc(int squareSize)
  {
     var bestx = 0; var besty = 0; var best = 0;
     for (var y = 1; y <= N - squareSize; y++)
        for (var x = 1; x <= N - squareSize; x++)
        {
           var sum = 0;
           for (var i = 0; i < squareSize; i++)
              for (var j = 0; j < squareSize; j++)
                 sum += _power[y - 1 + i, x - 1 + j];
           if (sum <= best) continue;
           bestx = x; besty = y; best = sum;
        }
     return (bestx, besty, best);
  }

  const int N = 300;
  readonly int[,] _power = new int[N, N];
  //readonly int[,,] _sum = new int[N, N, N]; // TODO
}
