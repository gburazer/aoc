﻿using System;
using System.Collections.Generic;

namespace AoC._2018;

public class Day19(string input) : AoC(input)
{
  public override void Solve()
  {
     ParseInput();

     Run();
     Part1 = R[0].ToString();

     Part2 = DivisorsSum(10551264).ToString(); // constant calculated from input
  }

  void ParseInput()
  {
     var lines = InputLines.Value;
     _ipr = int.Parse(lines[0].Split(" ")[1]);
     for (var i = 1; i < lines.Length; i++)
        _program.Add(Instruction.Parse(lines[i]));
  }

  void Run()
  {
     for (_ip = 0; _ip >= 0 && _ip < _program.Count; _ip++)
     {
        var instruction = _program[_ip];
        R[_ipr] = _ip;
        instruction.Run();
        _ip = (int)R[_ipr];
     }
  }

  static long DivisorsSum(long limit)
  {
     var res = 0L;
     for (var i = 1; i * i <= limit; i++)
        if (limit % i == 0)
           res += i + limit / i;
     return res;
  }

  class Instruction
  {
     public static Instruction Parse(string str)
     {
        var parts = str.Split(" ");
        return new Instruction
        {
           _action = Actions[parts[0]],
           _a = long.Parse(parts[1]),
           _b = long.Parse(parts[2]),
           _output = long.Parse(parts[3])
        };
     }

     Action<long, long, long> _action;

     public void Run() { _action(_a, _b, _output); }

     long _a, _b, _output;
  }

  static readonly long[] R = new long[6]; // TODO get rid of static
  int _ip, _ipr;
  readonly List<Instruction> _program = [];
  static readonly Dictionary<string, Action<long, long, long>> Actions = new() // TODO get rid of static
  {
     ["addr"] = (a, b, c) => R[c] = R[a] + R[b],
     ["addi"] = (a, b, c) => R[c] = R[a] + b,

     ["mulr"] = (a, b, c) => R[c] = R[a] * R[b],
     ["muli"] = (a, b, c) => R[c] = R[a] * b,

     ["banr"] = (a, b, c) => R[c] = R[a] & R[b],
     ["bani"] = (a, b, c) => R[c] = R[a] & b,

     ["borr"] = (a, b, c) => R[c] = R[a] | R[b],
     ["bori"] = (a, b, c) => R[c] = R[a] | b,

     ["setr"] = (a, _, c) => R[c] = R[a],
     ["seti"] = (a, _, c) => R[c] = a,

     ["gtir"] = (a, b, c) => R[c] = a > R[b] ? 1 : 0,
     ["gtri"] = (a, b, c) => R[c] = R[a] > b ? 1 : 0,
     ["gtrr"] = (a, b, c) => R[c] = R[a] > R[b] ? 1 : 0,

     ["eqir"] = (a, b, c) => R[c] = a == R[b] ? 1 : 0,
     ["eqri"] = (a, b, c) => R[c] = R[a] == b ? 1 : 0,
     ["eqrr"] = (a, b, c) => R[c] = R[a] == R[b] ? 1 : 0,
  };
}
