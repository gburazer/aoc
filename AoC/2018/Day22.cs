﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2018;

public class Day22(string input) : AoC(input)
{
  public override void Solve()
  {
     ParseInput();
     Part1 = BuildCave().ToString();
     Part2 = FastestTripMinutes().ToString();
  }

  void ParseInput()
  {
     var lines = InputLines.Value;
     _depth = int.Parse(lines[0].Split(" ")[1]);
     var targetParts = lines[1].Split(" ")[1].Split(",");
     _target = (int.Parse(targetParts[0]), int.Parse(targetParts[1]));
  }

  int BuildCave()
  {
     // constants figured out by experimentation
     _limit = (_target.x * 7, _target.y * 1);

     int fromGi(int value) => (value + _depth) % 20183;

     var gi = new int[_limit.x + 1, _limit.y + 1];
     var el = new int[_limit.x + 1, _limit.y + 1];
     for (var x = 0; x <= _limit.x; x++) { gi[x, 0] = x * 16807; el[x, 0] = fromGi(gi[x, 0]); }
     for (var y = 0; y <= _limit.y; y++) { gi[0, y] = y * 48271; el[0, y] = fromGi(gi[0, y]); }
     void set(int x, int y) { gi[x, y] = el[x - 1, y] * el[x, y - 1]; el[x, y] = (gi[x, y] + _depth) % 20183; }
     gi[0, 0] = gi[_target.x, _target.y] = 0;
     for (var step = 1; step <= Math.Max(_limit.x, _limit.y); step++)
     {
        // left
        if (step <= _limit.y)
           for (var x = 1; x <= Math.Min(_limit.x, step - 1); x++)
              set(x, step);
        // up
        if (step <= _limit.x)
           for (var y = 1; y <= Math.Min(_limit.y, step - 1); y++)
              set(step, y);
        // it
        if (step <= _limit.x && step <= _limit.y)
           set(step, step);
     }

     _cave = new char[_limit.x + 1, _limit.y + 1];
     for (var x = 0; x <= _limit.x; x++)
        for (var y = 0; y <= _limit.y; y++)
           _cave[x, y] = (el[x, y] % 3) switch
           {
              0 => Rocky,
              1 => Wet,
              2 => Narrow,
              _ => throw new InvalidOperationException(),
           };
     _cave[0, 0] = 'M'; _cave[_target.x, _target.y] = 'T';

     return Enumerable.Range(0, _target.x + 1).Sum(x =>
         Enumerable.Range(0, _target.y + 1).Sum(y =>
             (x, y) != _target ? el[x, y] % 3 : 0));
  }

  int FastestTripMinutes()
  {
     var visitedInMinutes = new int[_limit.x + 1, _limit.y + 1, 3];
     for (var x = 0; x <= _limit.x; x++)
        for (var y = 0; y <= _limit.y; y++)
           for (var eq = 0; eq < 3; eq++)
              visitedInMinutes[x, y, eq] = int.MaxValue;

     bool checkLimits(int x, int y) => x >= 0 && y >= 0 && x <= _limit.x && y <= _limit.y;
     bool checkEquipment(State to)
     {
        var terrain = _cave[to.Pos.x, to.Pos.y];
        return !((terrain == Rocky && to.Equipped == Neither)
            || (terrain == Wet && to.Equipped == Torch)
            || (terrain == Narrow && to.Equipped == ClimbingGear));
     }
     bool checkVisited(State to) => visitedInMinutes[to.Pos.x, to.Pos.y, to.Equipped] > to.Minute;

     bool canMoveTo(State to) => checkLimits(to.Pos.x, to.Pos.y) && checkEquipment(to) && checkVisited(to);

     var q = new Queue<State>();
     void moveTo(State to)
     {
        visitedInMinutes[to.Pos.x, to.Pos.y, to.Equipped] = to.Minute;
        q.Enqueue(to);
     }
     void tryMove(State to) { if (canMoveTo(to)) moveTo(to); }

     var res = int.MaxValue;
     moveTo(new State { Pos = (0, 0), Minute = 0, Equipped = Torch });
     while (q.Count > 0)
     {
        // check where we are
        var state = q.Dequeue();

        // reached target, maybe found new solution
        if (state.Pos == _target)
           res = Math.Min(res, state.Minute + (state.Equipped != Torch ? 7 : 0));

        // generate/enqueue new valid states to explore further
        var newState = state;

        // try moves
        newState.Minute++;
        newState.Pos = state.Pos; newState.Pos.x++; tryMove(newState); // right
        newState.Pos = state.Pos; newState.Pos.y++; tryMove(newState); // down
        newState.Pos = state.Pos; newState.Pos.x--; tryMove(newState); // left
        newState.Pos = state.Pos; newState.Pos.y--; tryMove(newState); // up

        // try tool switches
        newState.Minute += 6; newState.Pos = state.Pos;
        newState.Equipped = (state.Equipped + 1) % 3; tryMove(newState); // switch to next
        newState.Equipped = (state.Equipped + 2) % 3; tryMove(newState); // switch to next next
     }
     return res;
  }

  struct State
  {
     public (int x, int y) Pos;
     public int Minute;
     public int Equipped;
  }

  int _depth;
  (int x, int y) _target, _limit;
  char[,] _cave;
  const char Rocky = '.', Wet = '=', Narrow = '|';
  const int Neither = 0, Torch = 1, ClimbingGear = 2;
}
