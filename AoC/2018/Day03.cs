﻿using System.Linq;

namespace AoC._2018;

public class Day03(string input) : AoC(input)
{
  public override void Solve()
  {
     const int N = 1000;
     var field = new int[N, N];

     var lines = InputLines.Value.Select(l => l.Split(" ")).ToArray();
     var pos = new (int left, int top)[lines.Length];
     var size = new (int width, int height)[lines.Length];
     for (var lineInd = 0; lineInd < lines.Length; lineInd++)
     {
        var part = lines[lineInd];
        var posstr = part[2]; var lt = posstr[0..^1].Split(",");
        pos[lineInd] = (int.Parse(lt[0]), int.Parse(lt[1]));

        var sizestr = part[3].Split("x");
        size[lineInd] = (int.Parse(sizestr[0]), int.Parse(sizestr[1]));

        for (var i = 0; i < size[lineInd].width; i++)
           for (var j = 0; j < size[lineInd].height; j++)
              field[pos[lineInd].left + i, pos[lineInd].top + j]++;
     }

     var p1 = 0;
     for (var i = 0; i < N; i++)
        for (var j = 0; j < N; j++)
           if (field[i, j] > 1)
              p1++;
     Part1 = p1.ToString();

     bool overlaps(int lineInd)
     {
        for (var i = 0; i < size[lineInd].width; i++)
           for (var j = 0; j < size[lineInd].height; j++)
              if (field[pos[lineInd].left + i, pos[lineInd].top + j] > 1)
                 return true;
        return false;
     }
     for (var lineInd = 0; lineInd < lines.Length; lineInd++)
        if (!overlaps(lineInd))
        {
           Part2 = lines[lineInd][0][1..];
           break;
        }
  }
}
