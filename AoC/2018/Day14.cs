﻿using System.Linq;
using System.Text;

namespace AoC._2018;

public class Day14 : AoC
{
   readonly CircularList<int> _list = [3, 7];
   readonly int _inputAsInt;
   readonly int[] _reversed;
   readonly int _len;

  public Day14(string input) : base(input)
  {
     _inputAsInt = int.Parse(Input);
     _reversed = Input.ToCharArray().Select(c => int.Parse(c.ToString())).Reverse().ToArray();
     _len = _reversed.Length;
  }

  public override void Solve()
  {
     var elf1 = _list.First; var elf2 = elf1.Next;
     while (Part1 == null || Part2 == null)
     {
        var newValue = elf1.Value + elf2.Value;
        if (newValue >= 10)
           AddAndCheckIsPart2Sol(1);
        AddAndCheckIsPart2Sol(newValue % 10);
        elf1 = _list.Advance(elf1, 1 + elf1.Value);
        elf2 = _list.Advance(elf2, 1 + elf2.Value);

        // check is Part1 sol
        if (Part1 != null || _list.Count < 10 + _inputAsInt) continue;
        var p1 = new StringBuilder();
        var cur = _list.Last;
        for (var i = 0; i < 10; i++, cur = cur.Previous)
           p1.Append(cur.Value);
        Part1 = new string(p1.ToString().ToCharArray().Reverse().ToArray());
     }
  }

  void AddAndCheckIsPart2Sol(int value)
  {
     _list.AddLast(value);
     if (Part2 != null || _list.Count < _len) return;
     var ind = 0;
     for (var cur = _list.Last; ind < _len && cur.Value == _reversed[ind]; ind++, cur = cur.Previous) { }
     if (ind == _len)
        Part2 = (_list.Count - _len).ToString();
  }
}
