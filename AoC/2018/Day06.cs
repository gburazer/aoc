﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2018;

public class Day06(string input) : AoC(input)
{
   int[] _minDistCountByInd;
   bool[] _exclude;
   int _regionSize;

  public override void Solve()
  {
     ParseInput();
     ProcessAll();

     Part1 = (_minDistCountByInd.Where((_, ind) => !_exclude[ind]).Max() + 1).ToString();
     Part2 = _regionSize.ToString();
  }

  int _n;
  int[] _xs, _ys;
  int _sizex, _sizey;

  void ParseInput()
  {
     var lines = InputLines.Value; _n = lines.Length;
     _xs = new int[_n]; _ys = new int[_n];
     var minx = int.MaxValue; var maxx = 0;
     var miny = int.MaxValue; var maxy = 0;
     for (var i = 0; i < _n; i++)
     {
        var parts = lines[i].Split(", ");
        _xs[i] = int.Parse(parts[0]); minx = Math.Min(minx, _xs[i]); maxx = Math.Max(maxx, _xs[i]);
        _ys[i] = int.Parse(parts[1]); miny = Math.Min(miny, _ys[i]); maxy = Math.Max(maxy, _ys[i]);
     }
     for (var i = 0; i < _n; i++) { _xs[i] -= minx; _ys[i] -= miny; }
     _sizex = maxx - minx + 1; _sizey = maxy - miny + 1;
  }

  void ProcessAll()
  {
     _minDistCountByInd = new int[_n];
     _exclude = new bool[_n];

     var indicesByDist = new Dictionary<int, List<int>>();
     for (var y = 0; y < _sizey; y++)
        for (var x = 0; x < _sizex; x++)
        {
           indicesByDist.Clear();
           var minDist = int.MaxValue;
           var totalDist = 0;
           for (var i = 0; i < _n; i++)
              if (_xs[i] != x || _ys[i] != y)
              {
                 var dist = Math.Abs(x - _xs[i]) + Math.Abs(y - _ys[i]);
                 minDist = Math.Min(minDist, dist);
                 if (!indicesByDist.TryGetValue(dist, out var value))
                    indicesByDist[dist] = [i];
                 else
                    value.Add(i);
                 totalDist += dist;
              }
           if (totalDist < 10000) _regionSize++;
           if (indicesByDist[minDist].Count != 1) continue;
           var minDistInd = indicesByDist[minDist].Single();
           _minDistCountByInd[minDistInd]++;
           if (x == 0 || x == _sizex - 1 || y == 0 || y == _sizey - 1)
              _exclude[minDistInd] = true;
        }
  }
}
