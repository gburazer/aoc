﻿using System.Text;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2018;

public class Day07(string input) : AoC(input)
{
  public override void Solve()
  {
     ParseInput();
     Part1 = CalcPart1();
     Part2 = CalcPart2();
  }

  void ParseInput()
  {
     foreach (var parts in InputLines.Value.Select(line => line.Split(" ")))
     {
        var prerequisite = parts[1][0]; var element = parts[7][0];
        if (!_prerequisites.ContainsKey(prerequisite))
           _prerequisites[prerequisite] = [];
        if (!_prerequisites.TryGetValue(element, out var value))
           _prerequisites[element] = [prerequisite];
        else
           value.Add(prerequisite);
        _elements.Add(element); _elements.Add(prerequisite);
     }
  }

  readonly SortedSet<char> _elements = [];
  readonly Dictionary<char, List<char>> _prerequisites = [];

  string CalcPart1()
  {
     var used = InitUsed();
     return _elements.Aggregate(new StringBuilder(), (res, _) =>
     {
        var pick = _elements.First(e => !used[e] && _prerequisites[e].All(p => used[p]));
        used[pick] = true;
        return res.Append(pick);
     }).ToString();
  }

  string CalcPart2()
  {
     var used = InitUsed();
     var workers = Enumerable.Range(0, 5).Select(_ => (element: Idle, startedAt: 0)).ToArray();
     var second = 0;
     for (var usedCount = 0; usedCount != _elements.Count; second++)
     {
        usedCount += Free(workers, second, used);
        Employ(workers, second, used);
     }
     return (second - 1).ToString();
  }

  const char Idle = '.';
  Dictionary<char, bool> InitUsed() => _elements.ToDictionary(e => e, _ => false);
  static int Free((char element, int startedAt)[] workers, int second, Dictionary<char, bool> used)
  {
     var freedCount = 0;
     for (var i = 0; i < workers.Length; i++)
        if (workers[i].element != Idle && second == workers[i].startedAt + 60 + workers[i].element - 'A' + 1)
        {
           used[workers[i].element] = true;
           freedCount++;
           workers[i].element = Idle;
        }
     return freedCount;
  }
  void Employ((char element, int startedAt)[] workers, int second, Dictionary<char, bool> used)
  {
     foreach (var availableElement in _elements
        .Where(e => !used[e]
           && _prerequisites[e].All(p => used[p])
           && workers.All(w => w.element != e)))
        for (var i = 0; i < workers.Length; i++)
           if (workers[i].element == Idle)
           {
              workers[i].element = availableElement;
              workers[i].startedAt = second;
              break;
           }
  }
}
