﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2018;

public class Day12(string input) : AoC(input)
{
  public override void Solve()
  {
     ParseInput();

     var sums = CalcUntilStable().Select(el => el.sum).ToArray();

     Part1 = sums[20].ToString();

     var lastInd = sums.Length - 1;
     Part2 = ((50000000000L - lastInd) * (sums[lastInd] - sums[lastInd - 1]) + sums[lastInd]).ToString();
  }

  void ParseInput()
  {
     _lines = InputLines.Value;
     ParseInitialState();
     ParseProjections();
  }

  void ParseInitialState()
  {
     var initialStateStr = _lines[0].Split(" ")[2];
     _initialStateLen = initialStateStr.Length;
     _initialState =
     [
        ..Enumerable.Range(0, _initialStateLen).Where(ind => initialStateStr[ind] == Plant)
     ];
  }

  void ParseProjections()
  {
     const int PROJECTIONS_START = 2;
     var len = _lines[PROJECTIONS_START].Length;
     foreach (var line in _lines.Skip(PROJECTIONS_START).Where(l => l[len - 1] == Plant))
        _projections.Add([..Enumerable.Range(-2, 5).Where(pos => line[pos + 2] == Plant)]);
  }

  const char Plant = '#';
  string[] _lines;
  int _initialStateLen;
  HashSet<int> _initialState;
  readonly List<HashSet<int>> _projections = [];

  List<(int count, int sum)> CalcUntilStable()
  {
     var res = new List<(int count, int sum)>([(_initialState.Count, _initialState.Sum())]);
     var oldState = _initialState; var newState = new HashSet<int>();
     var min = -2; var max = _initialStateLen + 1;
     for (var generation = 1; generation < 5
         || Enumerable.Range(1, 4).Any(i => res[generation - 1].count != res[generation - 1 - i].count);
         generation++, newState.Clear())
     {
        var newMax = max;
        for (var i = min; i <= max; i++)
           if (_projections.Any(p => IsMatch(oldState, i, p)))
           {
              newState.Add(i);
              min = Math.Min(min, i - 2); newMax = Math.Max(newMax, i + 2);
           }
        max = newMax;
        oldState = [..newState];
        res.Add((newState.Count, newState.Sum()));
     }
     return res;
  }

  static bool IsMatch(HashSet<int> state, int ind, HashSet<int> projection)
      => !Enumerable.Range(-2, 5).Any(i => state.Contains(ind + i) ^ projection.Contains(i));
}
