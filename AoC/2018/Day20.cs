﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2018;

public class Day20(string input) : AoC(input)
{
  public override void Solve()
  {
     BuildMap(Input[1..^1]);
     var roomDistances = Walk().Values;
     Part1 = roomDistances.Max().ToString();
     Part2 = roomDistances.Count(v => v >= 1000).ToString();
  }

  void BuildMap(string input)
  {
     (int x, int y) cur = (0, 0);
     var map = new Dictionary<(int x, int y), char> { [cur] = Start };
     var stack = new Stack<(int x, int y)>();
     foreach (var c in input)
        switch (c)
        {
           // pass through door into a room
           case 'N': map[(cur.x, cur.y - 1)] = Dhoriz; cur.y -= 2; map[cur] = Room; break;
           case 'S': map[(cur.x, cur.y + 1)] = Dhoriz; cur.y += 2; map[cur] = Room; break;
           case 'E': map[(cur.x + 1, cur.y)] = Dvert; cur.x += 2; map[cur] = Room; break;
           case 'W': map[(cur.x - 1, cur.y)] = Dvert; cur.x -= 2; map[cur] = Room; break;
           // handle branching using stack
           case '(': stack.Push(cur); break;
           case ')': cur = stack.Pop(); break;
           case '|': cur = stack.Peek(); break;
        }
     if (stack.Count != 0)
        throw new InvalidOperationException("Invalid input!");
     (int x, int y) min = (map.Keys.Min(k => k.x), map.Keys.Min(k => k.y));
     (int x, int y) max = (map.Keys.Max(k => k.x), map.Keys.Max(k => k.y));
     _size = (max.x - min.x + 3, max.y - min.y + 3);

     _map = new char[_size.x, _size.y];
     for (var x = 0; x < _size.x; x++)
        for (var y = 0; y < _size.y; y++)
        {
           var pos = (x + min.x - 1, y + min.y - 1);
           _map[x, y] = map.GetValueOrDefault(pos, Wall);
           if (_map[x, y] == Start)
              _start = (x, y);
        }
  }

  Dictionary<(int x, int y), int> Walk()
  {
     var visited = new Dictionary<(int x, int y), int>();
     var q = new Queue<(int x, int y)>();
     void visit((int x, int y) pos, int step)
     {
        visited[pos] = step;
        q.Enqueue(pos);
     }
     void tryVisit(int fromStep, (int x, int y) doorpos, char doorchar, (int x, int y) roompos)
     {
        if (_map[doorpos.x, doorpos.y] == doorchar && !visited.ContainsKey(roompos))
           visit(roompos, fromStep + 1);
     }
     visit(_start, 0);
     while (q.Count > 0)
     {
        var (x, y) = q.Dequeue();
        var fromStep = visited[(x, y)];
        tryVisit(fromStep, doorpos: (x - 1, y), Dvert, roompos: (x - 2, y)); // W
        tryVisit(fromStep, doorpos: (x + 1, y), Dvert, roompos: (x + 2, y)); // E
        tryVisit(fromStep, doorpos: (x, y - 1), Dhoriz, roompos: (x, y - 2)); // N
        tryVisit(fromStep, doorpos: (x, y + 1), Dhoriz, roompos: (x, y + 2)); // S
     }
     return visited;
  }

  const char Wall = '#', Room = '.', Dvert = '|', Dhoriz = '-', Start = 'X';
  char[,] _map;
  (int x, int y) _size, _start;
}
