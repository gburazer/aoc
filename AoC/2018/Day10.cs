﻿using System;
using System.Text;
using System.Linq;

namespace AoC._2018;

public class Day10 : AoC
{
   readonly (int x, int y, int vx, int vy)[] _points;

  public Day10(string input) : base(input)
  {
     _points = new (int x, int y, int vx, int vy)[InputLines.Value.Length];
     for (var i = 0; i < _points.Length; i++)
     {
        var parts = InputLines.Value[i].Split("> velocity=<");
        var xy = parts[0]["position=<".Length..].Split(", ");
        var v = parts[1][0..^1].Split(", ");
        _points[i] = (int.Parse(xy[0]), int.Parse(xy[1]), int.Parse(v[0]), int.Parse(v[1]));
     }
  }

  public override void Solve()
  {
     var second = 0;
     var diffx = int.MaxValue; var diffy = int.MaxValue;
     int lastDiffx; int lastDiffy;
     do
     {
        lastDiffx = diffx; lastDiffy = diffy;
        MoveForward();
        (_, _, diffx, diffy) = CalcDiff();
        second++;
     } while (diffx <= lastDiffx || diffy <= lastDiffy);
     MoveBackward(); second--;
     Part1 = Display(CalcDiff()); // TODO Ocr(Display(CalcDiff()));
     Part2 = second.ToString();
  }

  void MoveForward() => Move((n, v) => n + v);
  void MoveBackward() => Move((n, v) => n - v);
  void Move(Func<int, int, int> fn)
  {
     for (var i = 0; i < _points.Length; i++)
     {
        _points[i].x = fn(_points[i].x, _points[i].vx);
        _points[i].y = fn(_points[i].y, _points[i].vy);
     }
  }

  (int minx, int miny, int diffx, int diffy) CalcDiff()
  {
     var minx = int.MaxValue; var miny = int.MaxValue;
     var maxx = 0; var maxy = 0;
     for (var i = 0; i < _points.Length; i++)
     {
        minx = Math.Min(minx, _points[i].x); miny = Math.Min(miny, _points[i].y);
        maxx = Math.Max(maxx, _points[i].x); maxy = Math.Max(maxy, _points[i].y);
     }
     return (minx, miny, Math.Abs(maxx - minx), Math.Abs(maxy - miny));
  }

  string Display((int minx, int miny, int diffx, int diffy) diff)
  {
     var res = new StringBuilder();
     for (var row = 0; row <= diff.diffy; row++)
     {
        for (var col = 0; col <= diff.diffx; col++)
           res.Append(_points.Any(p => Math.Abs(p.x - diff.minx) == col && Math.Abs(p.y - diff.miny) == row) 
              ? '#'
              : '.');
        res.AppendLine();
     }
     return res.ToString();
  }
}
