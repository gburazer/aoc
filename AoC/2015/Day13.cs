﻿using System;

namespace AoC._2015;

public class Day13(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value;
     var n = (int)((1 + Math.Sqrt(1 + 4 * lines.Length)) / 2); var nn = n + 1;
     var change = new int[nn, nn];
     var lineInd = 0;
     for (var i = 0; i < n; i++)
        for (var j = 0; j < n; j++)
           if (j != i)
           {
              var parts = lines[lineInd++].Split(' ');
              change[i, j] = (parts[2] == "gain" ? 1 : -1) * int.Parse(parts[3]);
           }
     var perm = new int[nn];
     int diff(int i1, int i2) => change[i1, i2] + change[i2, i1];
     int value(int max)
     {
        var res = 0;
        for (var i = 0; i < max; i++)
           res += diff(perm[i], perm[(i + 1) % max]);
        return res;
     }
     var used = new bool[nn];
     void permutate(int i, int count, ref int res)
     {
        if (i == count)
           res = Math.Max(res, value(count));
        else
           for (var cur = 0; cur < n + 1; cur++)
              if (!used[cur])
              {
                 used[cur] = true;
                 perm[i] = cur; permutate(i + 1, count, ref res);
                 used[cur] = false;
              }
     }
     void init(int count) { for (var i = 0; i < count; i++) used[i] = false; }
     string solve(int count)
     {
        int res = 0;
        init(count);
        permutate(0, count, ref res);
        return res.ToString();
     }
     Part1 = solve(n); Part2 = solve(nn);
  }
}
