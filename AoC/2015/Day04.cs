﻿namespace AoC._2015;

public class Day04(string input) : AoC(input)
{
  public override void Solve()
  {
     var md5Generator = new Md5Generator();
     Part1 = Part2 = NoSolution;
     for (var n = 1; Part2 == NoSolution; n++)
     {
        var md5Hex = md5Generator.Md5Hex(Input, n);
        if (Part1 == NoSolution && md5Hex.StartsWith("00000")) Part1 = n.ToString();
        if (md5Hex.StartsWith("000000")) Part2 = n.ToString();
     }
  }
}
