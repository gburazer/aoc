﻿using System;
using System.Linq;

namespace AoC._2015;

public class Day02(string input) : AoC(input)
{
  public override void Solve()
  {
     var (paper, ribbon) = (0, 0);
     foreach (var el in InputLines.Value.Select(l => l.Split('x').Select(int.Parse).ToArray()))
     {
        var ar1 = el[0] * el[1]; var hp1 = el[0] + el[1];
        var ar2 = el[0] * el[2]; var hp2 = el[0] + el[2];
        var ar3 = el[1] * el[2]; var hp3 = el[1] + el[2];
        paper += (2 * (ar1 + ar2 + ar3)) + Math.Min(Math.Min(ar1, ar2), ar3);
        ribbon += (el[0] * el[1] * el[2]) + (2 * Math.Min(Math.Min(hp1, hp2), hp3));
     }
     (Part1, Part2) = (paper.ToString(), ribbon.ToString());
  }
}
