﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2015;

public class Day07(string input) : AoC(input)
{
  public override void Solve()
  {
     ParseInput();

     var a = Value("a");
     Part1 = a.ToString();

     _values.Clear();
     _values["b"] = a;
     Part2 = Value("a").ToString();
  }

  void ParseInput()
  {
     foreach (var statement in InputLines.Value.Select(l => l.Split(" -> ")))
        _expressions[statement[1]] = statement[0].Split(' ');
  }

  readonly Dictionary<string, string[]> _expressions = [];

  ushort Value(string id)
  {
     if (_values.TryGetValue(id, out ushort value))
        return value;
     var res = Eval(_expressions[id]);
     _values[id] = res;
     return res;
  }
  readonly Dictionary<string, ushort> _values = [];
  ushort Eval(string[] exp) =>
      exp.Length == 1
         ? Term(exp[0])
         : exp[0] == "NOT"
            ? unchecked((ushort)~Term(exp[1]))
            : ((exp[1]) switch
            {
               "AND" => (ushort)(Term(exp[0]) & Term(exp[2])),
               "OR" => (ushort)(Term(exp[0]) | Term(exp[2])),
               "LSHIFT" => (ushort)(Term(exp[0]) << Term(exp[2])),
               "RSHIFT" => (ushort)(Term(exp[0]) >> Term(exp[2])),
               _ => (ushort)0,
            });
  ushort Term(string t) => ushort.TryParse(t, out var res) ? res : Value(t);
}
