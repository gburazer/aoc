﻿using System;
using System.Linq;

using Props = System.Collections.Generic.Dictionary<string, int>;

namespace AoC._2015;

public class Day16(string input) : AoC(input)
{
  public override void Solve()
  {
     var props = new Props
     {
        {"children", 3}, {"cats", 7}, {"samoyeds", 2}, {"pomeranians", 3}, {"akitas", 0}, 
        {"vizslas", 0}, {"goldfish", 5}, {"trees", 3}, {"cars", 2}, {"perfumes", 1}, 
     };
     var lines = InputLines.Value; var n = lines.Length;
     const string SEPARATOR = ": ";
     Props parse(string line) => line[(line.IndexOf(SEPARATOR, StringComparison.Ordinal) + SEPARATOR.Length)..]
        .Split(", ")
        .Select(group => group.Split(SEPARATOR))
        .ToDictionary(parts => parts[0], parts => int.Parse(parts[1]));
     bool isMatch1(Props p) => p.Keys.All(key => p[key] == props[key]);
     bool isMatch2(Props p) => p.Keys.All(key => key switch
     {
        "cats" or "trees" => p[key] >= props[key],
        "pomeranians" or "goldfish" => p[key] <= props[key],
        _ => p[key] == props[key]
     });
     var p1 = 0; var p2 = 0;
     for (var res = 1; res <= n; res++)
     {
        var p = parse(lines[res - 1]);
        if (isMatch1(p)) p1 = res;
        if (isMatch2(p)) p2 = res;
     }
     Part1 = p1.ToString(); Part2 = p2.ToString();
  }
}
