﻿using System.Linq;

namespace AoC._2015;

public class Day17(string input) : AoC(input)
{
  public override void Solve()
  {
     var ns = InputLines.Value.Select(int.Parse).ToArray(); var n = ns.Length;
     const int TARGET = 150;
     var distinct = 0; var waysPerDistinct = new int[n];
     void combine(int sum, int ind)
     {
        if (sum == TARGET)
           waysPerDistinct[distinct]++;
        else if (sum <= TARGET && ind != n)
        {
           combine(sum, ind + 1);
           distinct++; combine(sum + ns[ind], ind + 1); distinct--;
        }
     }
     combine(sum: 0, ind: 0);
     Part1 = waysPerDistinct.Sum().ToString();
     Part2 = waysPerDistinct.First(wpd => wpd > 0).ToString();
  }
}
