﻿namespace AoC._2015;

public class Day01(string input) : AoC(input)
{
  public override void Solve()
  {
     var cur = 0; Part2 = NoSolution;
     for (var i = 0; i < Input.Length; i++)
     {
        cur += Input[i] == '(' ? 1 : -1;
        if (cur < 0 && Part2 == NoSolution)
           Part2 = (i + 1).ToString();
     }
     Part1 = cur.ToString();
  }
}
