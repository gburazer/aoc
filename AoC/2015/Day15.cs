﻿using System;

namespace AoC._2015;

public class Day15(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value; var n = lines.Length;
     const int TOTAL_AMOUNT = 100, TRAITS_COUNT = 5;
     var values = new int[n, TRAITS_COUNT];
     for (var i = 0; i < n; i++)
     {
        var parts = lines[i].Split(": ")[1].Split(", ");
        for (var j = 0; j < TRAITS_COUNT; j++)
           values[i, j] = int.Parse(parts[j].Split(' ')[1]);
     }
     var amounts = new int[n]; var remaining = TOTAL_AMOUNT;
     var p1 = 0L; var p2 = 0L;
     long calcTotalScore()
     {
        var res = 1L;
        for (var i = 0; i < TRAITS_COUNT - 1; i++)
        {
           var cur = 0L;
           for (var j = 0; j < n; j++)
              cur += amounts[j] * values[j, i];
           res *= Math.Max(cur, 0L);
        }
        return res;
     }
     long calcCalories()
     {
        var res = 0L;
        for (var i = 0; i < n; i++)
           res += amounts[i] * values[i, TRAITS_COUNT - 1];
        return res;
     }
     void combine(int ind)
     {
        if (ind == n - 1)
        {
           amounts[n - 1] = remaining;
           var totalScore = calcTotalScore();
           p1 = Math.Max(p1, totalScore);
           if (calcCalories() == 500) 
              p2 = Math.Max(p2, totalScore);
        }
        else
           for (var i = 1; i <= remaining - ind + 1; i++)
           {
              amounts[ind] = i;
              remaining -= i; combine(ind + 1); remaining += i;
           }
     }
     combine(0);
     Part1 = p1.ToString(); Part2 = p2.ToString();
  }
}
