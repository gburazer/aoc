﻿using System;

namespace AoC._2015;

public class Day18(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value; var mm = lines.Length; var nn = lines[0].Length;
     var init = new bool[mm, nn]; var cur = new bool[mm, nn]; var next = new bool[mm, nn];
     for (var r = 0; r < mm; r++)
        for (var c = 0; c < nn; c++)
           init[r, c] = lines[r][c] == '#';
     int countNeighbors(int r, int c)
     {
        var res = 0;
        for (var dr = -1; dr <= 1; dr++)
           for (var dc = -1; dc <= 1; dc++)
              if (dr != 0 || dc != 0)
              {
                 var rr = r + dr; var cc = c + dc;
                 if (rr < 0 || rr >= mm || cc < 0 || cc >= nn)
                    continue;
                 if (cur[rr, cc])
                    res++;
              }
        return res;
     }
     int totalCount()
     {
        var res = 0;
        for (var r = 0; r < mm; r++)
           for (var c = 0; c < nn; c++)
              if (cur[r, c])
                 res++;
        return res;
     }
     void run(bool fixedCorners)
     {
        bool isCorner(int r, int c) => (r == 0 && (c == 0 || c == nn - 1)) || (r == mm - 1 && (c == 0 || c == nn - 1));
        for (var step = 0; step < 100; step++)
        {
           for (var r = 0; r < mm; r++)
              for (var c = 0; c < nn; c++)
                 if (fixedCorners && isCorner(r, c))
                    next[r, c] = true;
                 else
                 {
                    var cnt = countNeighbors(r, c);
                    next[r, c] = cur[r, c] ? cnt == 2 || cnt == 3 : cnt == 3;
                 }
           Array.Copy(next, cur, mm * nn);
        }
     }
     string calc(bool fixedCorners)
     {
        Array.Copy(init, cur, mm * nn);
        if (fixedCorners) 
           cur[0, 0] = cur[0, nn - 1] = cur[mm - 1, 0] = cur[mm - 1, nn - 1] = true;
        run(fixedCorners);
        return totalCount().ToString();
     }
     Part1 = calc(fixedCorners: false); Part2 = calc(fixedCorners: true);
  }
}
