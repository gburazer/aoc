﻿using System.Collections.Generic;

namespace AoC._2015;

public class Day11(string input) : AoC(input)
{
  public override void Solve()
  {
     _cur = Input.ToCharArray();
     Part1 = NextValid(); Part2 = NextValid();
  }

  char[] _cur;
  readonly HashSet<char> _invalid = ['i', 'o', 'l'];

  string NextValid()
  {
     do Next(); while (!IsValid());
     return new string(_cur);
  }

  void Next()
  {
     static char nextChar(char c)
     {
        var res = (char)(c + 1);
        if (res > 'z') res = 'a';
        return res;
     }
     var skippedInvalid = false;
     for (var i = 0; i < _cur.Length; i++)
        if (_invalid.Contains(_cur[i]))
        {
           while (_invalid.Contains(_cur[i])) _cur[i] = nextChar(_cur[i]);
           skippedInvalid = true;
        }
     if (skippedInvalid)
        return;
     for (var i = _cur.Length - 1; i >= 0; i--)
     {
        _cur[i] = nextChar(_cur[i]);
        if (_cur[i] != 'a')
           break;
     }
  }

  bool IsValid() // TODO: use regex
  {
     var hasStraight = false;
     _pair1 = _pair2 = NoPair;
     for (var i = 0; i < _cur.Length; i++)
     {
        if (_invalid.Contains(_cur[i]))
           return false;
        if (i >= 2 && _cur[i - 1] == _cur[i] - 1 && _cur[i - 2] == _cur[i] - 2)
           hasStraight = true;
        if (i >= 1 && _cur[i - 1] == _cur[i])
           AddPair(_cur[i]);
     }
     return hasStraight && MinTwoPairs();
  }

  const char NoPair = '_';
  char _pair1, _pair2;
  bool MinTwoPairs() => _pair2 != NoPair;
  void AddPair(char p)
  {
     if (_pair1 == NoPair)
        _pair1 = p;
     else if (_pair2 == NoPair && p != _pair1)
        _pair2 = p;
  }
}
