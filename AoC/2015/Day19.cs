﻿using System;
using System.Collections.Generic;

namespace AoC._2015;

public class Day19(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value; var n = lines.Length - 2;
     var ts = new (string key, string value)[n];
     for (var i = 0; i < n; i++)
     {
        var parts = lines[i].Split(" => ");
        ts[i] = (parts[0], parts[1]);
     }
     var target = lines[^1];

     bool possiblyTransform(string str, int pos, string key, string value, out string res)
     {
        var len = key.Length;
        if (pos + len <= str.Length && str.AsSpan(pos, len).SequenceCompareTo(key) == 0)
        {
           res = $"{str[..pos]}{value}{str[(pos + len)..]}";
           return true;
        }
        else
        {
           res = string.Empty;
           return false;
        }
     }

     HashSet<string> transformAll(string str)
     {
        var res = new HashSet<string>();
        for (var pos = 0; pos < str.Length; pos++)
           for (var i = 0; i < n; i++)
              if (possiblyTransform(str, pos, ts[i].key, ts[i].value, out var transformed))
                 res.Add(transformed);
        return res;
     }
     Part1 = transformAll(target).Count.ToString();

     var p2 = 0;
     void reduce(string str, int steps)
     {
        if (p2 != 0)
           return;
        if (str == "e")
           p2 = steps;
        else
           for (var i = 0; i < n; i++)
              for (var pos = 0; pos < str.Length; pos++)
                 if (possiblyTransform(str, pos, ts[i].value, ts[i].key, out var transformed))
                    reduce(transformed, steps + 1);
     }
     reduce(target, 0);
     Part2 = p2.ToString();
  }
}
