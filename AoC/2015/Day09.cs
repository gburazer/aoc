﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2015;

public class Day09(string input) : AoC(input)
{
  public override void Solve()
  {
     ParseInput();

     _best = int.MaxValue;
     _worst = int.MinValue;
     _cur = 0;

     foreach (var dest in _allEdges.Keys)
        Travel(dest);

     Part1 = _best.ToString();
     Part2 = _worst.ToString();
  }

  void ParseInput()
  {
     foreach (var (from, to, dist) in InputLines.Value.Select(l =>
     {
        var parts = l.Split(' ');
        return (from: parts[0], to: parts[2], dist: int.Parse(parts[4]));
     }))
     {
        AddEdge(from, to, dist);
        AddEdge(to, from, dist);
     }
  }
  void AddEdge(string from, string to, int dist)
  {
     var edge = (to, dist);
     if (!_allEdges.TryGetValue(from, out var edges))
        _allEdges[from] = [edge];
     else 
        edges.Add(edge);
  }
  readonly Dictionary<string, List<(string to, int dist)>> _allEdges = [];

  int _best, _worst, _cur;
  void Travel(string dest)
  {
     _visited.Add(dest);
     if (_visited.Count == _allEdges.Keys.Count)
     {
        _best = Math.Min(_best, _cur);
        _worst = Math.Max(_worst, _cur);
     }
     else
        foreach (var (to, dist) in _allEdges[dest].Where(next => !_visited.Contains(next.to)))
        {
           _cur += dist;
           Travel(to);
           _cur -= dist;
        }
     _visited.Remove(dest);
  }
  readonly HashSet<string> _visited = [];
}
