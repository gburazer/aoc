﻿using System.Text;

namespace AoC._2015;

public class Day10(string input) : AoC(input)
{
  public override void Solve()
  {
     var str = Input;
     string sol() => str.Length.ToString();
     for (var i = 0; i < 50; i++)
     {
        if (i == 40) 
           Part1 = sol();
        str = LookAndSay(str);
     }
     Part2 = sol();
  }

  static string LookAndSay(string s)
  {
     var res = new StringBuilder();
     var cur = ' '; var count = 0;
     foreach (var c in s)
        if (c != cur)
        {
           if (count > 0)
              res.Append(count).Append(cur);
           cur = c; count = 1;
        }
        else
           count++;
     res.Append(count).Append(s[^1]);
     return res.ToString();
  }
}
