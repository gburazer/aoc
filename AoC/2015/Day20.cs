﻿namespace AoC._2015;

public class Day20(string input) : AoC(input)
{
    public override void Solve()
    {
        const int MAX = 1000000; 
        var count1 = new int[MAX]; var count2 = new int[MAX];
        for (var elf = 1; elf < MAX; elf++)
            for (var i = 1; i * elf < MAX; i++)
            {
                count1[i * elf] += 10 * elf;
                if (i <= 50) count2[i * elf] += 11 * elf;
            }
        var n = int.Parse(Input);
        var p1 = 0; var p2 = 0;
        for (var i = 1; p1 == 0 || p2 == 0; i++)
        {
            if (p1 == 0 && count1[i] >= n) p1 = i;
            if (p2 == 0 && count2[i] >= n) p2 = i;
        }
        Part1 = p1.ToString(); Part2 = p2.ToString();
    }
}