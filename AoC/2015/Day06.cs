﻿using System;

namespace AoC._2015;

public class Day06(string input) : AoC(input)
{
  public override void Solve()
  {
     Part1 = Solve(
        [
           ("turn on ", _ => 1),
           ("turn off ", _ => 0),
           ("toggle ", l => l == 0 ? 1 : 0)
        ]);
     Part2 = Solve(
        [
           ("turn on ", l => l + 1),
           ("turn off ", l => Math.Max(0, l - 1)),
           ("toggle ", l => l + 2)
        ]);
  }

  string Solve((string verb, Func<int, int> selector)[] options)
  {
     const int N = 1000;
     var grid = new int[N, N];
     foreach (var instruction in InputLines.Value)
     {
        var selector = ParseInstruction(instruction, options,
            out var left, out var top, out var right, out var bottom);

        for (var col = left; col <= right; col++)
           for (var row = top; row <= bottom; row++)
              grid[col, row] = selector(grid[col, row]);
     }

     var sol = 0;
     for (var col = 0; col < N; col++)
        for (var row = 0; row < N; row++)
           sol += grid[col, row];
     return sol.ToString();
  }

  static Func<int, int> ParseInstruction(string instruction, (string verb,
      Func<int, int> selector)[] options, out int left, out int top, out int right, out int bottom)
  {
     foreach (var (verb, selector) in options)
        if (instruction.StartsWith(verb))
        {
           var strings = instruction[verb.Length..].Split(' ');
           (left, top) = ParsePair(strings[0]);
           (right, bottom) = ParsePair(strings[2]);

           return selector;
        }
     throw new InvalidOperationException();
  }

  static (int v1, int v2) ParsePair(string s)
  {
     var parts = s.Split(',');
     return (int.Parse(parts[0]), int.Parse(parts[1]));
  }
}
