﻿namespace AoC._2015;

public class Day08(string input) : AoC(input)
{
  public override void Solve() // TODO use regex
  {
     var lines = InputLines.Value;
     var escaped = 2 * lines.Length; // quotes
     var encoded = 4 * lines.Length; // quotes
     foreach (var line in lines)
        for (var i = 1; i < line.Length - 1; i++)
           if (line[i] == '\\')
           {
              encoded++;
              if (line[i + 1] == '\\')
              {
                 escaped++;
                 encoded++;
                 i++;
              }
              else
                 escaped += line[i + 1] == 'x' ? 3 : 1;
           }
           else if (line[i] == '"')
              encoded++;

     Part1 = escaped.ToString();
     Part2 = encoded.ToString();
  }
}
