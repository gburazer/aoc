﻿using System;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Nodes;

namespace AoC._2015;

public class Day12(string input) : AoC(input)
{
  public override void Solve()
  {
     static int sumWhereNot(JsonNode node, Func<string, bool> predicate)
     {
        var res = 0;
        if (node is JsonObject jObj)
           foreach (var el in jObj)
           {
              if (el.Value is JsonValue && el.Value.AsValue().TryGetValue<string>(out var val) && predicate(val))
                 return 0;
              res += sumWhereNot(el.Value, predicate);
           }
        else if (node is JsonArray jArray)
           res += jArray.Sum(el => sumWhereNot(el, predicate));
        else if (int.TryParse(node.AsValue().ToString(), out var val))
           res += val;
        return res;
     }
     var data = JsonSerializer.Deserialize<JsonNode>(Input);
     Part1 = sumWhereNot(data, _ => false).ToString();
     Part2 = sumWhereNot(data, el => el == "red").ToString();
  }
}
