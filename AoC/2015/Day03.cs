﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2015;

public class Day03(string input) : AoC(input)
{
    public override void Solve()
    {
        Part1 = Solve(Input).Count.ToString();
        Part2 = Solve(Instructions(0)).Union(Solve(Instructions(1))).Count().ToString();
    }

    IEnumerable<char> Instructions(int parity) => Input.Where((_, i) => i % 2 == parity);

    static HashSet<(int, int)> Solve(IEnumerable<char> moves)
    {
        var (x, y) = (0, 0); var visited = new HashSet<(int, int)> { (x, y) };
        foreach (var (dx, dy) in moves.Select(ch => ch switch
            { '<' => (-1, 0), '>' => (1, 0), '^' => (0, -1), 'v' => (0, 1), _ => (0, 0) }))
        {
            x += dx; y += dy;
            visited.Add((x, y));
        }
        return visited;
    }
}