﻿using System;
using System.Linq;

namespace AoC._2015;

public class Day05(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value;
     string calc(Func<string, bool> predicate) => lines.Count(predicate).ToString();
     Part1 = calc(IsPart1); Part2 = calc(IsPart2);
  }

  static bool IsPart1(string s) // TODO use regex
  {
     var vowelCount = 0;
     var repeat = false;
     for (var i = 0; i < s.Length; i++)
     {
        var c = s[i];
        if (c is 'a' or 'e' or 'i' or 'o' or 'u')
           vowelCount++;
        if (i + 1 >= s.Length) continue;
        var cn = s[i + 1];
        if (cn == c)
           repeat = true;
        if ((c == 'a' && cn == 'b')
            || (c == 'c' && cn == 'd')
            || (c == 'p' && cn == 'q')
            || (c == 'x' && cn == 'y'))
           return false;
     }
     return vowelCount >= 3 && repeat;
  }

  static bool IsPart2(string s) // TODO use regex
  {
     var rule1 = false;
     for (var i = 0; i < s.Length - 3; i++)
     {
        for (var j = i + 2; j < s.Length - 1; j++)
           rule1 |= s[i] == s[j] && s[i + 1] == s[j + 1];
        if (rule1)
           break;
     }
     if (!rule1) return false;

     for (var i = 0; i < s.Length - 2; i++)
        if (s[i] == s[i + 2])
           return true;

     return false;
  }
}
