﻿using System;
using System.Linq;

namespace AoC._2015;

public class Day14(string input) : AoC(input)
{
  public override void Solve()
  {
     var lines = InputLines.Value; var n = lines.Length; const int N = 2503;
     var speeds = new int[n]; var flightSeconds = new int[n]; var restSeconds = new int[n]; 
     var fullPeriod = new int[n];
     var curDist = new int[n]; var points = new int[n];
     for (var i = 0; i < n; i++)
     {
        var parts = lines[i].Split(' ');
        speeds[i] = int.Parse(parts[3]);
        flightSeconds[i] = int.Parse(parts[6]); restSeconds[i] = int.Parse(parts[13]);
        fullPeriod[i] = flightSeconds[i] + restSeconds[i];
     }
     int dist(int ind, int seconds) => flightSeconds[ind] * speeds[ind] * (seconds / fullPeriod[ind]) 
        + Math.Min(flightSeconds[ind], seconds % fullPeriod[ind]) * speeds[ind];
     bool isMoving(int ind, int second) => second % fullPeriod[ind] < flightSeconds[ind];
     for (var second = 0; second < N; second++)
     {
        var bestDist = 0;
        for (var i = 0; i < n; i++)
        {
           if (isMoving(i, second))
              curDist[i] += speeds[i];
           bestDist = Math.Max(bestDist, curDist[i]);
        }
        for (var i = 0; i < n; i++)
           if (curDist[i] == bestDist)
              points[i]++;
     }
     string calc(Func<int, int> selector) => Enumerable.Range(0, n).Max(selector).ToString();
     Part1 = calc(i => dist(i, N)); Part2 = calc(i => points[i]);
  }
}
