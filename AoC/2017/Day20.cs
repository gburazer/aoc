﻿using System;
using System.Linq;

namespace AoC._2017;

public class Day20(string input) : AoC(input)
{
  public override void Solve()
  {
     ParseInput();

     Part1 = SolvePart1();
     Part2 = SolvePart2();
  }

  void ParseInput()
  {
     _n = InputLines.Value.Length;
     _ps = new long[_n][]; _vs = new long[_n][]; _as = new long[_n][];
     static long[] parsePart(string s, string p)
     {
        var parts = s.Split('=');
        return parts[0] != p
           ? throw new InvalidOperationException()
           : parts[1][1..^1].Split(',').Select(long.Parse).ToArray();
     }
     for (var i = 0; i < _n; i++)
     {
        var parts = InputLines.Value[i].Split(", ");
        _ps[i] = parsePart(parts[0], "p");
        _vs[i] = parsePart(parts[1], "v");
        _as[i] = parsePart(parts[2], "a");
     }
  }

  int _n;
  long[][] _ps, _vs, _as;

  string SolvePart1()
  {
     var sola = long.MaxValue; var solv = long.MaxValue; var solp = long.MaxValue;
     var soli = -1;
     for (var i = 0; i < _n; i++)
     {
        var a = _as[i].Sum(Math.Abs);
        var v = _vs[i].Sum(Math.Abs);
        var p = _ps[i].Sum(Math.Abs);
        if (a >= sola && (a != sola || (v >= solv && (v != solv || p > solp)))) continue;
        sola = a; solv = v; solp = p;
        soli = i;
     }
     return soli.ToString();
  }

  string SolvePart2()
  {
     var destroyed = new bool[_n];
     var dist = Enumerable.Range(0, _n).Select(Dist).ToArray();
     bool foundDecreasingDist;
     do
     {
        for (var i = 0; i < _n - 1; i++)
           if (!destroyed[i])
              for (var j = i + 1; j < _n; j++)
                 if (!destroyed[j] && AreEqual(i, j))
                    destroyed[i] = destroyed[j] = true;

        foundDecreasingDist = false;
        for (var i = 0; i < _n; i++)
        {
           if (destroyed[i])
              continue;

           Add(i, _vs, _as);
           Add(i, _ps, _vs);

           var d = Dist(i);
           if (d < dist[i])
              foundDecreasingDist = true;
           else
              dist[i] = d;
        }
     } while (foundDecreasingDist);

     return destroyed.Count(d => !d).ToString();
  }

  bool AreEqual(int i1, int i2) => _ps[i1].Zip(_ps[i2], (p1, p2) => p1 == p2).All(r => r);

  long Dist(int i) => _ps[i].Sum(Math.Abs);

  static void Add(int i, long[][] a1, long[][] a2)
  {
     for (var ind = 0; ind < 3; ind++)
        a1[i][ind] += a2[i][ind];
  }
}
