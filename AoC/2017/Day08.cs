﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;

namespace AoC._2017;

public class Day08(string input) : AoC(input)
{
  public override void Solve()
  {
     var max = int.MinValue;
     foreach (var instruction in InputLines.Value)
     {
        var parts = instruction.Split(" if ");

        var regInfo = parts[0].Split(' ');
        var target = regInfo[0];
        var op = regInfo[1];
        var amount = int.Parse(regInfo[2]);

        var cond = parts[1].Split(' ');
        var dep = cond[0];
        var cmp = cond[1];
        var depAmount = int.Parse(cond[2]);

        Rval(target); // init if not present yet
        if (!_ops[cmp](Rval(dep), depAmount)) continue;
        _rs[target] += (op == "inc" ? 1 : -1) * amount;
        max = Math.Max(max, _rs[target]);
     }

     Part1 = _rs.Values.Max().ToString();
     Part2 = max.ToString();
  }

  readonly Dictionary<string, int> _rs = [];
  readonly Dictionary<string, Func<int, int, bool>> _ops = new[] { "<", ">", "<=", ">=", "==", "!=" }
      .ToDictionary(op => op, op =>
      {
         var exp = $"a {op} b";
         var a = Expression.Parameter(typeof(int), "a");
         var b = Expression.Parameter(typeof(int), "b");
         var e = DynamicExpressionParser.ParseLambda(parameters: [a, b], null, expression: exp);
         return (Func<int, int, bool>)e.Compile();
      });

  int Rval(string r)
  {
     _rs.TryAdd(r, 0);
     return _rs[r];
  }
}
