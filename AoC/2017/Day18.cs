﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2017;

public class Day18(string input) : AoC(input)
{
  public override void Solve()
  {
     _ins = InputLines.Value.Select(@in => @in.Split(' ')).ToArray();
     _ops = new Dictionary<string, Action<int, string[]>>
     {
        ["set"] = (n, a) => Set(n, a[1], Get(n, a[2])),
        ["add"] = (n, a) => DoAtomic(n, '+', a),
        ["mul"] = (n, a) => DoAtomic(n, '*', a),
        ["mod"] = (n, a) => DoAtomic(n, '%', a),
        ["jgz"] = (n, a) =>
        {
           if (Get(n, a[1]) > 0)
              _ip[n] += Get(n, a[2]) - 1; // -1 is to counter adding to ip
        },
        ["snd"] = (n, a) =>
        {
           _q[Next(n)].Enqueue(Get(n, a[1]));
           _waiting[Next(n)] = false;
           if (n == 1)
              _p2++;
        },
        ["rcv"] = (n, a) =>
        {
           _waiting[n] = _q[n].Count == 0;
           if (!_waiting[n])
              Set(n, a[1], _q[n].Dequeue());
           else
              _ip[n]--; // to counter adding to ip and be back on blocking "rcv"
        }
     };
     _atomic = new Dictionary<char, Func<long, long, long>>
     {
        ['+'] = (a, b) => a + b,
        ['*'] = (a, b) => a * b,
        ['%'] = (a, b) => a % b
     };

     Init();
     Exec(0);
     Part1 = _q[1].Last().ToString();

     Init();
     for (var i = 0; _terminated.Count(t => t) + _waiting.Count(w => w) < N; i = Next(i))
        Exec(i);
     Part2 = _p2.ToString();
  }

  string[][] _ins;

  Dictionary<string, Action<int, string[]>> _ops;
  Dictionary<char, Func<long, long, long>> _atomic;

  void Set(int n, string s, long v) => _rs[n][s[0]] = v;
  long Get(int n, string s)
  {
     if (long.TryParse(s, out var v))
        return v;
     var c = s[0];
     if (!_rs[n].ContainsKey(c))
        _rs[n][c] = 0;
     return _rs[n][c];
  }

  const int N = 2;
  readonly Dictionary<char, long>[] _rs = new Dictionary<char, long>[N];

  void DoAtomic(int n, char op, string[] a) => Set(n, a[1], _atomic[op](Get(n, a[1]), Get(n, a[2])));

  static int Next(int i) => (i + 1) % N;

  void Init()
  {
     for (var i = 0; i < N; i++)
     {
        _rs[i] = [];
        Set(i, "p", i);
        _ip[i] = 0;
        _q[i] = new Queue<long>();
        _terminated[i] = _waiting[i] = false;
     }
  }

  readonly long[] _ip = new long[N];
  readonly Queue<long>[] _q = new Queue<long>[N];

  readonly bool[] _terminated = new bool[N];
  readonly bool[] _waiting = new bool[N];

  void Exec(int n)
  {
     while (!(_terminated[n] || _waiting[n]))
        if (_ip[n] < 0 || _ip[n] >= _ins.Length)
           _terminated[n] = true;
        else
        {
           var @in = _ins[_ip[n]];
           _ops[@in[0]](n, @in);
           _ip[n]++;
        }
  }

  int _p2;
}
