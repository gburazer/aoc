﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2017;

public class Day12(string input) : AoC(input)
{
  public override void Solve()
  {
     ParseInput();
     Init();

     var p1 = 0;
     for (var v = 0; v < _count; v++)
        if (_groups[v] == 0)
        {
           _groupCount++;
           Visit(v);
           if (v == 0)
              p1 = _visited.Count;
           _visited.Clear();
        }

     Part1 = p1.ToString();
     Part2 = _groupCount.ToString();
  }

  void ParseInput()
  {
     _count = InputLines.Value.Length;
     _edges = new List<int>[_count];
     foreach (var line in InputLines.Value)
     {
        var parts = line.Split(" <-> ");
        var v = int.Parse(parts[0]);
        _edges[v] = [.. parts[1].Split(", ").Select(int.Parse)];
     }
  }

  int _count;
  List<int>[] _edges;

  void Init()
  {
     _groups = new int[_count];
     _groupCount = 0;
     _visited = [];
     _q = new Queue<int>();
  }

  int[] _groups;
  int _groupCount;
  HashSet<int> _visited;
  Queue<int> _q;

  void Visit(int v)
  {
     _groups[v] = _groupCount;
     _visited.Add(v);
     foreach (var e in _edges[v].Where(e => !_visited.Contains(e)))
        _q.Enqueue(e);
     if (_q.Count > 0)
        Visit(_q.Dequeue());
  }
}
