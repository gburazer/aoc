﻿using System;
using System.Linq;

namespace AoC._2017;

public class Day02(string input) : AoC(input)
{
  public override void Solve()
  {
     var p1 = 0; var p2 = 0;
     foreach (var row in InputLines.Value)
     {
        var min = int.MaxValue; var max = 0;
        var elements = row.Split('\t').Select(int.Parse).ToArray();
        for (var i = 0; i < elements.Length; i++)
        {
           min = Math.Min(min, elements[i]);
           max = Math.Max(max, elements[i]);
           for (var j = i + 1; j < elements.Length; j++)
           {
              var lesser = Math.Min(elements[i], elements[j]);
              var greater = Math.Max(elements[i], elements[j]);
              if (greater % lesser != 0) continue;
              p2 += greater / lesser;
              break; // only one pair in a row
           }
        }
        p1 += max - min;
     }
     Part1 = p1.ToString(); Part2 = p2.ToString();
  }
}
