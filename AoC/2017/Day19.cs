﻿using System;
using System.Text;

namespace AoC._2017;

public class Day19(string input) : AoC(input)
{
  public override void Solve()
  {
     _grid = [.. Input[1..^1].Split(InputLineSeparator)];
     _m = _grid.Length; _n = _grid[0].Length;
     _cur = (0, _grid[0].IndexOf(Vert));
     _dir = 0; // down
  
     Part1 = Walk();
     Part2 = _p2.ToString();
  }

  string[] _grid;
  int _m, _n, _dir, _p2;
  (int r, int c) _cur;

  const char Empty = ' ', Vert = '|', Horiz = '-', Plus = '+';
  
  string Walk()
  {
     var res = new StringBuilder();
     while (IsWithinBounds())
     {
        var c = _grid[_cur.r][_cur.c];
        if (c is not Empty and not Vert and not Horiz and not Plus)
           res.Append(c);
        if (c == Plus)
        {
           var leftCandidate = Next(TurnLeft());
           var rightCandidate = Next(TurnRight());
           _dir = _grid[leftCandidate.r][leftCandidate.c] != Empty
              ? TurnLeft()
              : _grid[rightCandidate.r][rightCandidate.c] != Empty
                 ? TurnRight()
                 : throw new InvalidOperationException();
        }
        _cur = Next(_dir); _p2++;
     }
     return res.ToString();
  }

  bool IsWithinBounds() => _cur.r >= 0 && _cur.r < _m && _cur.c >= 0 && _cur.c < _n && _grid[_cur.r][_cur.c] != Empty;

  (int r, int c) Next(int d) => d switch
  {
     0 => (_cur.r + 1, _cur.c),// down
     1 => (_cur.r, _cur.c - 1),// left
     2 => (_cur.r - 1, _cur.c),// up
     3 => (_cur.r, _cur.c + 1),// right
     _ => throw new InvalidOperationException(),
  };
  int TurnLeft() => (_dir + 4 - 1) % 4;
  int TurnRight() => (_dir + 1) % 4;
}
