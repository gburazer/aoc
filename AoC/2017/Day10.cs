﻿using System.Linq;

namespace AoC._2017;

public class Day10(string input) : AoC(input)
{
    public override void Solve()
    {
        var list = Enumerable.Range(0, 256).Select(i => (byte)i).ToArray();
        var lengths = Input.Split(',').Select(s => (byte)int.Parse(s)).ToList();
        var cur = 0; var skipSize = 0;
        KnotHash.DoRound(list, lengths, ref cur, ref skipSize);
        Part1 = (list[0] * list[1]).ToString(); Part2 = KnotHash.CalcHash(Input);
    }
}