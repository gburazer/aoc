﻿using System;
using System.Linq;

namespace AoC._2017;

public class Day15(string input) : AoC(input)
{
  public override void Solve()
  {
     Part1 = Solve(40000000, _ => false, _ => false);
     Part2 = Solve(5000000, a => a % 4 > 0, b => b % 8 > 0);
  }

  string Solve(int count, Func<ulong, bool> whileA, Func<ulong, bool> whileB)
  {
     static ulong el(string line) => ulong.Parse(line.Split(' ').Last());
     var a = el(InputLines.Value[0]); var b = el(InputLines.Value[1]);
     var res = 0;
     const ulong MOD = 2147483647;
     for (var i = 0; i < count; i++)
     {
        do a = a * 16807 % MOD; while (whileA(a));
        do b = b * 48271 % MOD; while (whileB(b));
        if (unchecked((ushort)a) == unchecked((ushort)b))
           res++;
     }
     return res.ToString();
  }
}
