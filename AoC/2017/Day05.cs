﻿using System;
using System.Linq;

namespace AoC._2017;

public class Day05(string input) : AoC(input)
{
  public override void Solve()
  {
     int[] getArr() => Input.Split(' ').Select(int.Parse).ToArray();

     Part1 = Solve(getArr(), _ => 1);
     Part2 = Solve(getArr(), i => i >= 3 ? -1 : 1);
  }

  static string Solve(int[] arr, Func<int, int> increment)
  {
     var count = 0;
     for (var cur = 0; cur >= 0 && cur < arr.Length; count++)
     {
        var old = arr[cur];
        arr[cur] += increment(old);
        cur += old;
     }
     return count.ToString();
  }
}
