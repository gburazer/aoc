﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2017;

public class Day06(string input) : AoC(input)
{
  public override void Solve()
  {
     var (p1, last) = Walk(Input);
     var (p2, _) = Walk(last);

     Part1 = p1.ToString();
     Part2 = p2.ToString();
  }

  static (int sol, string final) Walk(string s)
  {
     var sol = 0; var final = "";
     var visited = new HashSet<string>();
     for (var cur = s; !visited.Contains(cur); cur = Next(cur), sol++, final = cur)
        visited.Add(cur);
     return (sol, final);
  }

  static string Next(string s)
  {
     var a = S2A(s);
     var maxi = MaxInd(a);
     var max = a[maxi];
     a[maxi] = 0;
     for (var i = 1; i <= max; i++)
        a[(maxi + i) % a.Length]++;
     return A2S(a);
  }

  static int[] S2A(string s) => s.Split(' ').Select(int.Parse).ToArray();

  static int MaxInd(int[] a)
  {
     var res = 0;
     var max = 0;
     for (var i = 0; i < a.Length; i++)
        if (a[i] > max)
        {
           max = a[i];
           res = i;
        }
     return res;
  }

  static string A2S(int[] a) => string.Join(' ', a);
}
