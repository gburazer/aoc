﻿using System.Linq;

namespace AoC._2017;

public class Day01(string input) : AoC(input)
{
  public override void Solve()
  {
     Part1 = Solve(1);
     Part2 = Solve(Input.Length / 2);
  }

  string Solve(int step) => Enumerable.Range(0, Input.Length)
     .Where(i => Input[i] == Input[(i + step) % Input.Length])
     .Sum(i => Input[i] - '0').ToString();
}
