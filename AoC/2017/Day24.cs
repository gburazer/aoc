﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2017;

public class Day24(string input) : AoC(input)
{
  public override void Solve()
  {
     _cs = InputLines.Value.Select(c =>
     {
        var parts = c.Split('/');
        return (p1: int.Parse(parts[0]), p2: int.Parse(parts[1]));
     }).ToArray();

     _statesByUsedCount = new Dictionary<int, List<(int last, int sum, HashSet<int> used)>>
     {
        [0] = [(0, 0, new HashSet<int>())]
     };

     for (var usedCount = 0; TryAdd(usedCount); usedCount++) { }

     Part1 = _statesByUsedCount.Values.Where(l => l.Count > 0).Max(l => l.Max(s => s.sum)).ToString();
     Part2 = _statesByUsedCount[_statesByUsedCount.Keys.Max()].Max(s => s.sum).ToString();
  }

  bool TryAdd(int usedCount)
  {
     static int other(int p1, int p2, int last) => last != p1 ? p1 : p2;

     var res = false;
     foreach (var (last, sum, used) in _statesByUsedCount[usedCount])
        for (var ci = 0; ci < _cs.Length; ci++)
        {
           var (p1, p2) = _cs[ci];
           if ((p1 != last && p2 != last) || used.Contains(ci)) continue;
           var el = (other(p1, p2, last), sum + p1 + p2, new HashSet<int>(used) { ci });
           if (!res)
           {
              _statesByUsedCount[usedCount + 1] = [el];
              res = true;
           }
           else
              _statesByUsedCount[usedCount + 1].Add(el);
        }
     return res;
  }

  (int p1, int p2)[] _cs;
  Dictionary<int, List<(int last, int sum, HashSet<int> used)>> _statesByUsedCount;
}
