﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2017;

public class Day25(string input) : AoC(input)
{
  public override void Solve()
  {
     ParseInput();

     var p1 = 0;
     var vs = new bool[2 * _n]; var pos = _n / 2;
     for (var step = 0; step < _n; step++)
     {
        var (write, move, continueWith) = _states[_c][vs[pos] ? 1 : 0];
        if (vs[pos] != write) { vs[pos] = write; p1 += write ? 1 : -1; }
        pos += move; _c = continueWith;
     }
     Part1 = p1.ToString();
     
     Part2 = NoSolution; // day 25 only has part1
  }

  void ParseInput()
  {
     var lines = InputLines.Value.Select(l => l.Trim()).Where(l => l.Length > 0).ToArray();

     _c = lines[0].Split(' ')[3][0];
     _n = int.Parse(lines[1].Split(' ')[5]);

     static (bool, int, char) parseInstruction(string[] lines, ref int ind)
     {
        ind++; // 0/1
        var write = lines[ind++].Split(' ')[4][0] == '1';
        var move = lines[ind++].Split(' ')[6] == "right." ? 1 : -1;
        var continueWith = lines[ind++].Split(' ')[4][0];
        return (write, move, continueWith);
     }

     for (var lineInd = 2; lineInd < lines.Length;)
        _states[lines[lineInd++].Split(' ')[2][0]] =
        [
           parseInstruction(lines, ref lineInd),
           parseInstruction(lines, ref lineInd)
        ];
  }

  int _n;
  char _c;
  readonly Dictionary<char, (bool write, int move, char continueWith)[]> _states = [];
}
