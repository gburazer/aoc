﻿using System;
using System.Collections.Generic;

namespace AoC._2017;

public class Day22(string input) : AoC(input)
{
  public override void Solve()
  {
     Part1 = Solve(burstCount: 10000, vadd: 2, dirAdd: cur => cur == 2 ? 1 : -1);
     Part2 = Solve(burstCount: 10000000, vadd: 1, dirAdd: cur => cur - 1);
  }

  string Solve(int burstCount, int vadd, Func<int, int> dirAdd)
  {
     ParseInput();

     (int r, int c) cur = (_n / 2, _n / 2);
     var dir = 0; // up
     var res = 0;
     for (var burst = 0; burst < burstCount; burst++)
     {
        var curv = Val(cur);
        dir += dirAdd(curv);
        dir = (dir + 4) % 4;
        curv += vadd;
        if (curv == 2)
           res++;
        _grid[cur] = curv % 4;
        switch (dir)
        {
           case 0: cur.r--; break;
           case 1: cur.c++; break;
           case 2: cur.r++; break;
           case 3: cur.c--; break;
        }
     }
     return res.ToString();
  }

  void ParseInput()
  {
     _grid = []; // 0 - clean, 1 - weakened, 2 - infected, 3 - flagged
     _n = InputLines.Value.Length;
     for (var i = 0; i < _n; i++)
        for (var j = 0; j < _n; j++)
           _grid[(i, j)] = InputLines.Value[i][j] == '#' ? 2 : 0;
  }

  Dictionary<(int, int), int> _grid;
  int _n;

  int Val((int, int) pos)
  {
     if (!_grid.TryGetValue(pos, out var value))
     {
        value = 0;
        _grid[pos] = value;
     }
     return value;
  }
}
