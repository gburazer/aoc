﻿using System.Linq;

namespace AoC._2017;

public class Day13(string input) : AoC(input)
{
  public override void Solve()
  {
     ParseInput();

     Part1 = _layers
         .Where((l, _) => l.depth % (2 * (l.range - 1)) == 0)
         .Sum(l => l.depth * l.range)
         .ToString();

     var delay = 0;
     while (IsCaught(delay))
        delay++;
     Part2 = delay.ToString();
  }

  void ParseInput()
  {
     var layerInfo = InputLines.Value;
     _layers = new (int, int)[layerInfo.Length];
     for (var i = 0; i < layerInfo.Length; i++)
     {
        var li = layerInfo[i].Split(": ");
        _layers[i] = (int.Parse(li[0]), int.Parse(li[1]));
     }
  }

  (int depth, int range)[] _layers;

  bool IsCaught(int d)
  {
     for (var i = 0; i < _layers.Length; i++)
        if ((_layers[i].depth + d) % (2 * (_layers[i].range - 1)) == 0)
           return true;
     return false;
  }
}
