﻿using System;

namespace AoC._2017;

public class Day09(string input) : AoC(input)
{
  public override void Solve()
  {
     Part1 = MatchGroupMember(1).ToString();
     Part2 = _garbageCount.ToString();
  }

  int _pos, _garbageCount;

  int MatchGroupMember(int score) => Input[_pos] == '<' ? MatchGarbage() : MatchGroup(score);

  int MatchGarbage()
  {
     Match('<');
     for (var isCanceled = false; isCanceled | Input[_pos] != '>'; _pos++)
        if (isCanceled)
           isCanceled = false;
        else if (Input[_pos] == '!')
           isCanceled = true;
        else
           _garbageCount++;
     Match('>');
     return 0;
  }

  int MatchGroup(int score)
  {
     Match('{');
     var res = score;
     res += MatchGroupInner(score + 1);
     Match('}');
     return res;
  }

  int MatchGroupInner(int score)
  {
     var memberCount = 0;
     var res = 0;
     while (Input[_pos] != '}')
     {
        if (memberCount > 0)
           Match(',');
        res += MatchGroupMember(score);
        memberCount++;
     }
     return res;
  }

  void Match(char c)
  {
     if (Input[_pos] == c)
        _pos++;
     else
        throw new InvalidOperationException($"'{c}' expected at pos {_pos}, '{Input[_pos]}' found");
  }
}
