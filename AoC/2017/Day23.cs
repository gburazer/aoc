﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2017;

public class Day23(string input) : AoC(input)
{
  public override void Solve()
  {
     Part1 = SolvePart1();
     Part2 = SolvePart2();
  }

  string SolvePart1()
  {
     var ins = InputLines.Value.Select(l => l.Split(' ')).ToArray();
     var insCount = ins.Length;
     var rs = new Dictionary<char, long>();
     void set(string s, long v) { rs[s[0]] = v; }
     long get(string s)
     {
        if (long.TryParse(s, out var res)) return res;
        var r = s[0];
        if (rs.TryGetValue(r, out var value)) return value;
        value = 0; rs[r] = value;
        return value;
     }
     var p1 = 0L;
     for (var ip = 0L; ip >= 0 && ip < insCount; ip++)
     {
        var cur = ins[ip];
        switch (cur[0])
        {
           case "set": set(cur[1], get(cur[2])); break;
           case "sub": set(cur[1], get(cur[1]) - get(cur[2])); break;
           case "mul": set(cur[1], get(cur[1]) * get(cur[2])); p1++; break;
           case "jnz": if (get(cur[1]) != 0) ip += get(cur[2]) - 1; break;
        }
     }
     return p1.ToString();
  }

  string SolvePart2()
  {
     static bool isPrime(int n)
     {
        if (n % 2 == 0)
           return false;
        for (var i = 3; i * i <= n; i += 2)
           if (n % i == 0)
              return false;
        return true;
     }
     var input = int.Parse(InputLines.Value[0].Split(' ')[2]);
     var min = input * 100 + 100000;
     var max = min + 17000;
     var res = 0;
     for (var n = min; n <= max; n += 17)
        if (!isPrime(n))
           res++;
     return res.ToString();
  }
}
