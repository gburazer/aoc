﻿using System;
using System.Collections.Generic;

namespace AoC._2017;

public class Day03(string input) : AoC(input)
{
  public override void Solve()
  {
     var n = int.Parse(Input);
     var left = 0; var right = 0; var up = 0; var down = 0; var x = -1; var y = 0;
     var sums = new Dictionary<(int, int), int> { [(0, 0)] = 1 };
     var dir = 0;
     var p2 = 0;
     for (var i = 1; i <= n; i++, dir %= 4)
     {
        switch (dir)
        {
           case 0: x++; if (x > right) { right++; dir++; } break;
           case 1: y--; if (y < 0 && Math.Abs(y) > up) { up++; dir++; } break;
           case 2: x--; if (x < 0 && Math.Abs(x) > left) { left++; dir++; } break;
           case 3: y++; if (y > down) { down++; dir++; } break;
        }
        if (i == 1) continue;

        if (p2 > 0) continue;
        var sum = 0;
        for (var col = -1; col <= 1; col++)
           for (var row = -1; row <= 1; row++)
              if (col != 0 || row != 0)
                 sum += sums.ContainsKey((x + col, y + row)) ? sums[(x + col, y + row)] : 0;
        sums[(x, y)] = sum;
        if (sums[(x, y)] > n)
           p2 = sums[(x, y)];
     }

     Part1 = (Math.Abs(x) + Math.Abs(y)).ToString();
     Part2 = p2.ToString();
  }
}
