﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2017;

public class Day11(string input) : AoC(input)
{

  // https://www.redblobgames.com/grids/hexagons/ - cube coordinates
  public override void Solve()
  {
     var directions = new Dictionary<string, (int dx, int dy, int dz)>
     {
        ["n"] = (1, -1, 0),
        ["ne"] = (0, -1, 1),
        ["se"] = (-1, 0, 1),
        ["s"] = (-1, 1, 0),
        ["sw"] = (0, 1, -1),
        ["nw"] = (1, 0, -1)
     };
     var x = 0; var y = 0; var z = 0;
     var curd = 0; var maxd = int.MinValue;
     static int dist((int x, int y, int z) a, (int x, int y, int z) b) =>
         (Math.Abs(a.x - b.x) + Math.Abs(a.y - b.y) + Math.Abs(a.z - b.z)) / 2;
     foreach (var (dx, dy, dz) in Input.Split(',').Select(d => directions[d]))
     {
        x += dx; y += dy; z += dz;
        curd = dist((0, 0, 0), (x, y, z));
        if (curd > maxd) maxd = curd;
     }
     Part1 = curd.ToString();
     Part2 = maxd.ToString();
  }
}
