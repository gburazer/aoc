﻿using System.Linq;
using System.Collections.Generic;

namespace AoC._2017;

public class Day07(string input) : AoC(input)
{
  public override void Solve()
  {
     ParseInput();

     var rootInd = Enumerable.Range(0, _programCount).Single(p => !_allChildren.Contains(p));
     var properWeight = FixIfUnbalanced(rootInd, 0);

     Part1 = _programs[rootInd].name;
     Part2 = properWeight.ToString();
  }

  void ParseInput()
  {
     _programCount = InputLines.Value.Length;
     var parsedChildren = new string[_programCount][];
     _programs = InputLines.Value.Select((item, ind) =>
     {
        var parts = item.Split(" -> ");
        var parts1 = parts[0].Split(' ');
        var name = parts1[0];
        var weight = int.Parse(parts1[1][1..^1]);
        if (parts.Length > 1)
           parsedChildren[ind] = parts[1].Split(", ");
        return (name, weight, new List<int>());
     }).ToArray();

     _allChildren = [];
     for (var i = 0; i < _programCount; i++)
        if (parsedChildren[i] != null)
           for (var j = 0; j < parsedChildren[i].Length; j++)
              for (var childInd = 0; childInd < _programCount; childInd++)
                 if (parsedChildren[i][j] == _programs[childInd].name)
                 {
                    _programs[i].children.Add(childInd);
                    _allChildren.Add(childInd);

                    break;
                 }

     _supporting = Enumerable.Range(0, _programCount).Select(CalcSupporting).ToArray();
  }

  int _programCount;
  (string name, int weight, List<int> children)[] _programs;
  HashSet<int> _allChildren;
  int[] _supporting;

  int CalcSupporting(int programInd)
  {
     var (_, weight, children) = _programs[programInd];
     return weight + Enumerable.Range(0, _programCount)
        .Where(p => children.IndexOf(p) != -1)
        .Sum(CalcSupporting);
  }

  int FixIfUnbalanced(int programInd, int target)
  {
     var children = _programs[programInd].children;
     if (children.Count == 0)
        return target;
     var weights = children.Select(c => _supporting[c]).ToArray();
     var sum = weights.Sum();
     if (target > 0 && sum == target)
        return 0;

     var referenceWeight = weights[0];
     for (var i = 0; i < children.Count; i++)
        if (weights[i] != referenceWeight)
           return FixIfUnbalanced(children[i], referenceWeight);

     return target - sum;
  }
}
