﻿using System;
using System.Collections.Generic;

namespace AoC._2017;

public class Day16(string input) : AoC(input)
{
  public override void Solve()
  {
     _moves = ParseMoves();

     Part1 = SolvePart1();
     Part2 = SolvePart2();
  }

  (int s, int i1, int i2, char c1, char c2)[] _moves;

  (int s, int i1, int i2, char c1, char c2)[] ParseMoves()
  {
     var strings = Input.Split(',');
     var res = new (int s, int i1, int i2, char c1, char c2)[strings.Length];
     for (var i = 0; i < strings.Length; i++)
        switch (strings[i][0])
        {
           case 's': res[i].s = int.Parse(strings[i][1..]); break;
           case 'x': (res[i].i1, res[i].i2) = Parse<int>(strings[i]); break;
           case 'p': res[i].i1 = -1; (res[i].c1, res[i].c2) = Parse<char>(strings[i]); break;
        }
     return res;
  }

  static (T v1, T v2) Parse<T>(string s)
  {
     var parseMethod = typeof(T).GetMethod("Parse", [typeof(string)]);
     if (parseMethod is null) throw new InvalidOperationException("No appropriate Parse method found.");
     var parts = s[1..].Split('/');
     return
         ((T)parseMethod.Invoke(null, [parts[0]]),
          (T)parseMethod.Invoke(null, [parts[1]]));
  }

  string SolvePart1()
  {
     Init();
     foreach (var move in _moves)
        Perform(move);
     return Current();
  }

  string SolvePart2()
  {
     Init();
     var visited = new Dictionary<string, int> { { Current(), 0 } };
     var ind = 0; var cycleLen = 0;
     _movesInd = 0;
     for (var i = 0; cycleLen < _moves.Length && i < 1000000000; i++)
     {
        ind++; PerformNext(); var cur = Current();
        if (visited.TryGetValue(cur, out var value))
        {
           var oldInd = value;
           cycleLen = ind - oldInd;
           if (cycleLen > _moves.Length)
              ind = oldInd;
        }
        else
           visited.Add(cur, ind);
     }
     for (var i = 0; i < (1000000000 - ind) % cycleLen; i++)
        PerformNext();
     return Current();
  }

  void Init()
  {
     for (var i = 0; i < Count; i++)
        _ps[i] = (char)('a' + i);
  }

  const int Count = 16;
  readonly char[] _ps = new char[Count];

  string Current() => new(_ps);

  void Perform((int s, int i1, int i2, char c1, char c2) move)
  {
     if (move.s == 0)
     {
        if (move.i1 >= 0)
           Swap(move.i1, move.i2);
        else
           Swap(Array.IndexOf(_ps, move.c1), Array.IndexOf(_ps, move.c2));
     }
     else
        for (var n = 0; n < move.s; n++)
        {
           var last = _ps[Count - 1];
           for (var i = Count - 1; i > 0; i--)
              _ps[i] = _ps[i - 1];
           _ps[0] = last;
        }
  }

  void Swap(int i1, int i2)
  {
     _ps[i1] ^= _ps[i2];
     _ps[i2] ^= _ps[i1];
     _ps[i1] ^= _ps[i2];
  }

  int _movesInd;
  void PerformNext()
  {
     Perform(_moves[_movesInd++]);
     if (_movesInd == _moves.Length)
        _movesInd = 0;
  }
}
