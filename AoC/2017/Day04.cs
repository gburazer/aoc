﻿using System;
using System.Linq;

namespace AoC._2017;

public class Day04(string input) : AoC(input)
{
  public override void Solve()
  {
     Part1 = Solve(w => w);
     Part2 = Solve(w => string.Concat(w.OrderBy(c => c)));
  }

  // TODO use REGEX
  string Solve(Func<string, string> selector) => InputLines.Value.Count(p =>
  {
     var words = p.Split(' ').Select(selector).ToArray();
     for (var i = 0; i < words.Length - 1; i++)
        for (var j = i + 1; j < words.Length; j++)
           if (words[i] == words[j])
              return false;
     return true;
  }).ToString();
}
