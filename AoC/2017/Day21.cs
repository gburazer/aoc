﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AoC._2017;

public class Day21(string input) : AoC(input)
{
  public override void Solve()
  {
     ParseInput();
     var grid = ParseGrid(@".#./..#/###");
     for (var iteration = 1; iteration <= 18; iteration++)
     {
        grid = Enhance(grid);
        if (iteration == 5)
           Part1 = Count(grid).ToString();
     }
     Part2 = Count(grid).ToString();
  }

  void ParseInput()
  {
     foreach (var line in InputLines.Value)
     {
        var grids = line.Split(" => ");

        var original = ParseGrid(grids[0]);
        var @out = ParseGrid(grids[1]);

        var @in = new List<bool[,]> { original, Rotate(original) };
        @in.Add(Rotate(@in[1]));
        @in.Add(Rotate(@in[2]));
        @in.Add(Flip(@in[0]));
        @in.Add(Flip(@in[1]));
        @in.Add(Flip(@in[2]));
        @in.Add(Flip(@in[3]));

        if (@out.GetLength(0) != original.GetLength(0) + 1)
           throw new InvalidOperationException();

        _rules.Add((@in, @out));
     }
  }

  readonly List<(List<bool[,]> @in, bool[,] @out)> _rules = [];
  static bool[,] ParseGrid(string s)
  {
     var lines = s.Split('/');
     var dim = lines.Length;
     if (dim is < 2 or > 4)
        throw new InvalidOperationException();
     var grid = new bool[dim, dim];
     for (var i = 0; i < dim; i++)
        for (var j = 0; j < dim; j++)
           grid[i, j] = lines[i][j] == '#';
     return grid;
  }
  static bool[,] Rotate(bool[,] grid)
  {
     var dim = Dim(grid);
     var rotated = new bool[dim, dim];
     for (var i = 0; i < dim; i++)
        for (var j = 0; j < dim; j++)
           rotated[i, j] = grid[dim - j - 1, i];
     return rotated;
  }
  static bool[,] Flip(bool[,] grid)
  {
     var dim = Dim(grid);
     var flipped = new bool[dim, dim];
     for (var i = 0; i < dim; i++)
        for (var j = 0; j < dim; j++)
           flipped[i, j] = grid[dim - i - 1, j];
     return flipped;
  }
  static int Dim(bool[,] grid)
  {
     var dim = grid.GetLength(0);
     return dim != grid.GetLength(1) ? throw new NotImplementedException() : dim;
  }

  bool[,] Enhance(bool[,] grid)
  {
     var dim = Dim(grid);
     var pieceDim = dim % 2 == 0 ? 2 : 3;
     var piecesDim = dim / pieceDim;
     var newDim = (pieceDim + 1) * piecesDim;
     var newGrid = new bool[newDim, newDim];
     for (var row = 0; row < piecesDim; row++)
        for (var col = 0; col < piecesDim; col++)
           CopyPiece(newGrid, Match(grid, pieceDim, row, col), pieceDim + 1, row, col);
     return newGrid;
  }
  bool[,] Match(bool[,] grid, int pieceDim, int row, int col)
  {
     for (var ruleInd = 0; ruleInd < _rules.Count; ruleInd++)
     {
        var (@in, @out) = _rules[ruleInd];
        if (Dim(@in.First()) != pieceDim)
           continue;
        for (var transformationInd = 0; transformationInd < @in.Count; transformationInd++)
           if (AreEqual(grid, row, col, pieceDim, @in[transformationInd]))
              return @out;
     }
     throw new InvalidOperationException();
  }
  static bool AreEqual(bool[,] grid, int row, int col, int pieceDim, bool[,] piece)
  {
     for (var i = 0; i < pieceDim; i++)
        for (var j = 0; j < pieceDim; j++)
           if (grid[(row * pieceDim) + i, (col * pieceDim) + j] != piece[i, j])
              return false;
     return true;
  }
  static void CopyPiece(bool[,] toGrid, bool[,] fromGrid, int pieceDim, int row, int col)
  {
     for (var i = 0; i < pieceDim; i++)
        for (var j = 0; j < pieceDim; j++)
           toGrid[(row * pieceDim) + i, (col * pieceDim) + j] = fromGrid[i, j];
  }

  static int Count(bool[,] grid)
  {
     var dim = Dim(grid);
     return Enumerable.Range(0, dim * dim).Count(i => grid[i / dim, i % dim]);
  }
}
