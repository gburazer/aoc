﻿using System;
using System.Collections.Generic;

namespace AoC._2017;

public class Day17 : AoC
{
  public Day17(string input) : base(input)
  {
     _n = int.Parse(Input);
  }

  public override void Solve()
  {
     const int TARGET1 = 2017;
     Part1 = Solve(TARGET1,
         () => new List<int> { 0 },
         (acc, el, curAfter) => { acc.Insert(curAfter + 1, el); return acc; },
         (acc, curAfter) => acc[(curAfter + 1) % TARGET1]);

     Part2 = Solve(50000000, () => -1, (acc, el, curAfter) => curAfter == 0 ? el : acc, (acc, _) => acc);
  }

  readonly int _n;

  string Solve(int target,
      Func<dynamic> init, Func<dynamic, int, int, dynamic> update, Func<dynamic, int, dynamic> @return)
  {
     var acc = init();
     var curAfter = 0;
     for (var el = 1; el <= target; el++)
     {
        curAfter = (curAfter + _n) % el;
        acc = update(acc, el, curAfter);
        curAfter = (curAfter + 1) % (el + 1);
     }
     return @return(acc, curAfter).ToString();
  }
}
