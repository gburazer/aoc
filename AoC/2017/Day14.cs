﻿using System.Collections;
using System.Globalization;

namespace AoC._2017;

public class Day14(string input) : AoC(input)
{
  public override void Solve()
  {
     var p1 = 0;
     for (var rowInd = 0; rowInd < N; rowInd++)
     {
        var row = $"{Input}-{rowInd}";
        var knot = KnotHash.CalcHash(row);
        var (count, ba) = ExtractBits(knot);
        p1 += count;
        for (var j = 0; j < N; j++)
           _grid[rowInd, j] = ba[j] ? 0 : -1;
     }

     Part1 = p1.ToString();
     Part2 = CountGroups().ToString();
  }

  const int N = 128;
  readonly int[,] _grid = new int[N, N];

  static (int count, BitArray ba) ExtractBits(string hexData)
  {
     var ba = new BitArray(4 * hexData.Length);
     for (var i = 0; i < hexData.Length; i++)
     {
        var b = byte.Parse(hexData[i].ToString(), NumberStyles.HexNumber);
        for (var j = 0; j < 4; j++)
           ba.Set(i * 4 + j, (b & (1 << (3 - j))) != 0);
     }
     var count = 0;
     for (var i = 0; i < ba.Length; i++)
        count += ba[i] ? 1 : 0;
     return (count, ba);
  }

  int CountGroups()
  {
     var res = 0;
     for (var i = 0; i < N; i++)
        for (var j = 0; j < N; j++)
           if (_grid[i, j] == 0)
              FloodFill(i, j, ++res);
     return res;
  }

  void FloodFill(int row, int col, int group)
  {
     _grid[row, col] = group;
     if (row > 0 && _grid[row - 1, col] == 0)
        FloodFill(row - 1, col, group);
     if (row < N - 1 && _grid[row + 1, col] == 0)
        FloodFill(row + 1, col, group);
     if (col > 0 && _grid[row, col - 1] == 0)
        FloodFill(row, col - 1, group);
     if (col < N - 1 && _grid[row, col + 1] == 0)
        FloodFill(row, col + 1, group);
  }
}
