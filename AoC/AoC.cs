﻿using System;

namespace AoC;

public abstract class AoC
{
    protected AoC(string input)
    {
        Input = input;

        SetYearAndDayFromTypeFullName();
    }

    public const char InputLineSeparator = ';';
    public const string NoSolution = "_";

    protected readonly string Input;
    protected Lazy<string[]> InputLines => new(() => Input.Split(InputLineSeparator));

    public static (int y, int d) ParseTypeFullName(string typeFullName)
    {
        const string ERROR_MSG = "Expected solution (AoC-inherited) Type FullName: 'AoC._<Year>.Day<Day>'.",
            YEAR_PREFIX = "_",
            DAY_PREFIX = "Day";
        var parts = typeFullName.Split('.');
        return parts.Length != 3 || parts[0] != "AoC"
            || !parts[1].StartsWith(YEAR_PREFIX) || !int.TryParse(parts[1][YEAR_PREFIX.Length..], out var y)
            || !parts[2].StartsWith(DAY_PREFIX) || !int.TryParse(parts[2][DAY_PREFIX.Length..], out var d)
            ? throw new InvalidOperationException(ERROR_MSG)
            : (y, d);
    }

    public static bool IsValidFullTypeName(string typeName)
    {
        try { ParseTypeFullName(typeName); }
        catch (InvalidOperationException) { return false; }
        return true;
    }

    public abstract void Solve();

    public string Part1 { get; protected set; }
    public string Part2 { get; protected set; }

    public int Year { get; private set; }
    public int Day { get; private set; }

    void SetYearAndDayFromTypeFullName() => (Year, Day) = ParseTypeFullName(GetType().FullName);
}