﻿/* TODO
 * solve remaining (get all stars)
 * optimise (to below threshold): (2016,5), (2018,11), (2022,19), (2023,5), (2023,23)
 * try using regex (2015,5), (2015,8), (2015,11), (2016,7), (2017,4), (2018,9)?, (2020,4)
 * OCR (https://github.com/bsoyka/advent-of-code-ocr/blob/main/advent_of_code_ocr/characters.py)
*    (2016,8), (2018,10), (2021,13), (2022,10)
 * fix broken solutions so they can be ran and work again
 * when asked to include certain day, if it's not in dictionary print error message
 * upgrade cmdline processing (to mainstream lib) + recheck dependencies
 * add proper benchmarking: https://github.com/dotnet/BenchmarkDotNet
 * 2018 refactor common code for "assembler" problems (3 or 4 of them)
 * 2019 refactor to reuse Intcode class across tasks
 */

using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Collections.Generic;

using McMaster.Extensions.CommandLineUtils;

using TestDictionary = System.Collections.Generic.Dictionary<(int y, int d),
    System.Collections.Generic.List<(string input, string p1, string p2)>>;
using OutputDictionary = System.Collections.Concurrent.ConcurrentDictionary<(int y, int d),
    System.Collections.Generic.List<(string p1, string p2, (long ms, long ticks) elapsed)>>;

namespace AoC;

static class Program
{
    static Dictionary<(int y, int d), Type> SolutionTypes => Assembly.GetExecutingAssembly().GetTypes()
        .Where(t => t.IsClass && !t.IsAbstract && t.IsSubclassOf(typeof(AoC)) && AoC.IsValidFullTypeName(t.FullName))
        .ToDictionary(t => AoC.ParseTypeFullName(t.FullName));

    static int MaxYear => SolutionTypes.Keys.Max(k => k.y);

    static readonly TestDictionary Tests = LoadTests("Tests.txt");

    static TestDictionary LoadTests(string fileName)
    {
        Console.Write($"Loading from {fileName} .. ");
        var tests = new TestDictionary();
        var count = 0;
        foreach (var test in File.ReadAllLines(fileName)
            .Where(l => l.Length > 0 && l[0] != '#').Select(l => l.Split(' ')))
        {
            var (y, d, p1, p2) = (int.Parse(test[0]), int.Parse(test[1]), test[2], test[3]);
            var list = tests.ContainsKey((y, d)) ? tests[(y, d)] : new List<(string, string, string)>();
            list.Add((string.Join(' ', test.Skip(4)), p1, p2));
            tests[(y, d)] = list;
            count++;
        }
        Console.WriteLine($"done. {count} test(s) loaded, total {tests.Keys.Count} problem(s)/day(s).");
        return tests;
    }

    static readonly OutputDictionary Output = new();

    static int Main(string[] args)
    {
        InitCommandLineOptions();
        CommandLineApplication.OnExecute(Exec);
        return CommandLineApplication.Execute(args);
    }

    static void InitCommandLineOptions()
    {
        CommandLineApplication.HelpOption();

        _listOption = CommandLineApplication.Option("-l|--list",
            "List available solutions, inputs and tests.", CommandOptionType.NoValue);

        _yearOption = CommandLineApplication.Option("-y|--year <y>",
            $"Run year (2015-{MaxYear})", CommandOptionType.SingleValue);
        _dayOption = CommandLineApplication.Option("-d|--day <d>",
            "Run day (1-25)", CommandOptionType.SingleValue);
        _partOption = CommandLineApplication.Option("-p|--part <p>",
            "Test part (1-2)", CommandOptionType.SingleValue);

        _includeOption = CommandLineApplication.Option("-i|--include <(y1,d1)[;(y2,d2)...]>",
            "Semicolon-separated list of problems to run.", CommandOptionType.SingleValue);
        _excludeOption = CommandLineApplication.Option("-x|--exclude <(y1,d1)[;(y2,d2)...]>",
            "Semicolon-separated list of problems to exclude.", CommandOptionType.SingleValue);

        _runInputsOption = CommandLineApplication.Option("-ri|--runInputs",
            "Run official inputs only", CommandOptionType.NoValue);
        _runTestsOption = CommandLineApplication.Option("-rt|--runTests",
            "Run tests only", CommandOptionType.NoValue);
    }

    static readonly CommandLineApplication CommandLineApplication = new()
    {
        Description = "adventofcode.com (AoC) solutions runner for gb@snowfalltravel.com"
    };

    static CommandOption _listOption,
        _yearOption, _dayOption, _partOption,
        _includeOption, _excludeOption,
        _runInputsOption, _runTestsOption;

    const int OptimizationThresholdMs = 5000; // 5s

    static int Exec()
    {
        if (DevAndMaybeExit()) return 0;

        if (_listOption.HasValue())
        {
            ListAvailable();
            return 0;
        }

        _year = ParseAndDisplayIntOption(_yearOption, "Year", 2015, MaxYear);
        _day = ParseAndDisplayIntOption(_dayOption, "Day", 1, 25);
        _part = ParseAndDisplayIntOption(_partOption, "Part", 1, 2);

        _includeKeyList = ParseAndDisplayKeyListOption(_includeOption, "Include", SolutionTypes.Keys);
        _excludeKeyList = ParseAndDisplayKeyListOption(_excludeOption, "Exclude", []);

        _keysToRun = Filter();

        switch (RunInputs)
        {
            case true when !RunTests:
                Console.WriteLine("Running official inputs only.");
                break;
            case false when RunTests:
                Console.WriteLine("Running tests only.");
                break;
            default:
                Console.WriteLine("Running both official inputs and tests.");
                break;
        }

        DisplayTimerProperties();

        Console.WriteLine($"Optimization threshold: {OptimizationThresholdMs}ms.");

        DisplayOutput(BenchmarkMs(
            $"requested tasks (parallel execution on {Environment.ProcessorCount} logical processor(s), by day)",
            () => Run(_keysToRun)));

        return 0;
    }

    static void DisplayTimerProperties()
    {
        Console.WriteLine(Stopwatch.IsHighResolution 
            ? "Operations timed using the system's high-resolution performance counter." 
            : "Operations timed using the DateTime class.");
        var frequency = Stopwatch.Frequency;
        Console.WriteLine("  Timer frequency in ticks per second = {0}", frequency);
        var nanosecPerTick = 1000L * 1000L * 1000L / frequency;
        Console.WriteLine("  Timer is accurate within {0} nanoseconds", nanosecPerTick);
    }

    static int _year, _day, _part;

    static int ParseAndDisplayIntOption(CommandOption option, string name, int min, int max)
    {
        var res = default(int);
        if (option.HasValue())
            if (!int.TryParse(option.Value(), out res) || res < min || res > max)
                Console.WriteLine($"{name} should be a number between {min} and {max} (incl).");
        Console.WriteLine(name + ": " + (res == default ? "all" : res.ToString()));
        return res;
    }

    static IEnumerable<(int y, int d)> _includeKeyList, _excludeKeyList;

    static IEnumerable<(int y, int d)> ParseAndDisplayKeyListOption(CommandOption option, string name,
        IEnumerable<(int y, int d)> @default)
    {
        var res = @default;
        if (!option.HasValue()) return res;
        const string EXCEPTION_STR = "List element format should be '(y,d)'.";
        static void assert(Func<bool> cond) { if (!cond()) throw new InvalidOperationException(EXCEPTION_STR); }
        res = option.Value()?.Split(AoC.InputLineSeparator).Select(it =>
        {
            assert(() => it.Length >= 2 && it[0] == '(' && it[^1] == ')');
            var parts = it[1..^1].Split(',');
            assert(() => parts.Length == 2);
            return !int.TryParse(parts[0], out var y) || !int.TryParse(parts[1], out var d)
                ? throw new InvalidOperationException(EXCEPTION_STR)
                : (y, d);
        });
        Console.WriteLine($"{name}: {option.Value()?.Replace(AoC.InputLineSeparator, ' ')}");
        return res;
    }

    static bool DevAndMaybeExit()
    {
        const bool DO_EXIT = true;
        Console.WriteLine("Dev" + (DO_EXIT ? " then exit" : ""));
        Test(key: (2024, 20));
        return DO_EXIT;
    }

    static void Test((int y, int d) key)
    {
        BenchmarkMs($"Year: {key.y}, Day: {key.d}", () =>
        {
            Run([key]);
            VerifyAndDisplayResult(key);
        });
    }

    static void ListAvailable()
    {
        Console.WriteLine("Available:");
        Console.WriteLine("Year|Day|Solution|Input|Test count");
        Console.WriteLine("----------------------------------");
        var ylast = 0;
        foreach (var (y, d) in SolutionTypes.Keys.Union(Tests.Keys).OrderBy(KeySelector))
        {
            var solution = SolutionTypes.ContainsKey((y, d)).ToString();
            var input = Tests.ContainsKey((y, d)).ToString();
            var testCount = Tests.ContainsKey((y, d)) ? (Tests[(y, d)].Count - 1).ToString() : "";
            var ydisplay = y != ylast ? y.ToString() : "";
            ylast = y;
            Console.WriteLine($"{ydisplay,4}|{d,3}|{solution,8}|{input,5}|{testCount,10}");
        }
    }

    static readonly Func<(int y, int d), int> KeySelector = k => 25 * (k.y - 2015) + k.d;

    static IEnumerable<(int y, int d)> Filter() => _includeKeyList.Except(_excludeKeyList).Intersect(Tests.Keys)
        .Where(key => (_year == default || key.y == _year) && (_day == default || key.d == _day));

    static IEnumerable<(int y, int d)> _keysToRun;

    static void Run(IEnumerable<(int y, int d)> keys)
    {
        Parallel.ForEach(keys, key =>
        {
            Output[key] = [];
            for (var testInd = 0; testInd < Tests[key].Count; testInd++)
                if ((testInd == 0 && !RunInputs) || (testInd > 0 && !RunTests))
                    Output[key].Add((AoC.NoSolution, AoC.NoSolution, (0, 0)));
                else
                {
                    Output[key].Add(Solve(key, Tests[key][testInd].input));
                    var (ms, ticks) = Output[key][testInd].elapsed;
                    Console.WriteLine(
                        $".. done with ({key.y}, day {key.d}, {InputName(testInd)}) in {ms}ms ({ticks} tick(s)).");
                }
        });
    }

    static bool RunInputs => _runInputsOption.HasValue() || !_runTestsOption.HasValue();
    static bool RunTests => _runTestsOption.HasValue() || !_runInputsOption.HasValue();
    static string InputName(int testInd) => testInd == 0 ? "input" : $"test {testInd}";

    static (string s1, string s2, (long ms, long ticks) elapsed) Solve((int y, int d) key, string testsInput)
    {
        if (!SolutionTypes.TryGetValue(key, out Type value))
            return (AoC.NoSolution, AoC.NoSolution, (0, 0));
        var sol = (AoC)Activator.CreateInstance(value, GetInput(key.y, key.d, testsInput));
        if (sol is null) throw new InvalidOperationException($"No solution found for {key}!");
        var elapsed = Measure(sol.Solve);
        return (sol.Part1, sol.Part2, elapsed);
    }

    const string InputFromFileMarker = "$FILE";

    static string GetInput(int y, int d, string inputFromTests) => inputFromTests.StartsWith(InputFromFileMarker)
        ? string.Join(AoC.InputLineSeparator, 
            File.ReadAllLines(GetInputFileName(y, d, inputFromTests[InputFromFileMarker.Length..])))
        : inputFromTests;

    static string GetInputFileName(int y, int d, string fileNameInParentheses) => fileNameInParentheses.Length == 0
        ? $"{y}{Path.DirectorySeparatorChar}Day{d.ToString().PadLeft(2, '0')}.txt"
        : fileNameInParentheses[0] != '(' || fileNameInParentheses[^1] != ')'
            ? throw new InvalidOperationException(
                $"Input file name must be given inside parentheses! ({fileNameInParentheses})")
            : fileNameInParentheses[1..^1];

    static int VerifyAndDisplayResult((int y, int d) key)
    {
        var output = Output[key];
        var res = 0;
        for (var testInd = 0; testInd < output.Count; testInd++)
        {
            if ((testInd == 0 && !RunInputs) || (testInd > 0 && !RunTests))
                continue;
            for (var p = 1; p <= 2; p++)
            {
                if (_part != default && _part != p)
                    continue;
                if (VerifyAndDisplayResult(key, testInd, p))
                    res++;
            }

            if (output.Count <= 1) continue;
            var (ms, ticks) = output[testInd].elapsed;
            Console.WriteLine($"\tTotal for {InputName(testInd)}: {ms}ms ({ticks} tick(s)).");
        }
        return res;
    }

    static bool VerifyAndDisplayResult((int y, int d) key, int testInd, int p)
    {
        var actual = p == 1 ? Output[key][testInd].p1 : Output[key][testInd].p2;
        var (_, p1, p2) = Tests[key][testInd];
        var expected = p == 1 ? p1 : p2;
        var success = expected == actual;
        var testOutput = success ? "OK." : $"{expected} expected!";
        var inputName = InputName(testInd);
        Console.WriteLine($"\t{inputName[0] + inputName[1..]}, Part {p}: {actual} ({testOutput})");
        return success;
    }

    static long BenchmarkMs(string caption, Action action)
    {
        Console.WriteLine($"Starting {caption} .. ");
        var elapsed = Measure(action);
        Console.WriteLine($"Done with {caption} in {elapsed.ms}ms ({elapsed.ticks} tick(s)).");
        return elapsed.ms;
    }

    static (long ms, long ticks) Measure(Action action)
    {
        var sw = new Stopwatch();
        sw.Start();
        action();
        return (sw.ElapsedMilliseconds, sw.ElapsedTicks);
    }

    static void DisplayOutput(long totalMs)
    {
        // per day/problem
        var count = 0; var successCount = 0; var slowCount = 0;
        foreach (var key in Output.Keys.Intersect(_keysToRun).OrderBy(KeySelector))
        {
            var ms = Output[key].Sum(o => o.elapsed.ms);
            var optimize = ms >= OptimizationThresholdMs ? " - OPTIMIZE!" : "";
            if (optimize != "") slowCount++;
            Console.WriteLine(
                $"#{++count} Year: {key.y}, Day: {key.d}, " +
                $"Total: {ms}ms ({Output[key].Sum(o => o.elapsed.ticks)} tick(s))" +
                $"{optimize}");
            successCount += VerifyAndDisplayResult(key);
        }

        // summary
        var testKeys = Tests.Keys.Intersect(_keysToRun).ToList();
        var partMultiplier = _part == default ? 2 : 1;
        var tryCount = partMultiplier * testKeys.Sum(t => Tests[t].Count);
        if (RunInputs && !RunTests)
            tryCount -= partMultiplier * testKeys.Sum(t => Tests[t].Count - 1);
        else if (!RunInputs && RunTests)
            tryCount -= partMultiplier * testKeys.Count;
        Console.WriteLine($"{count} day(s) ({tryCount} case(s)) tried, " +
            $"{successCount} case(s) succeeded ({(double)successCount * 100 / tryCount:0.00}%), " +
            $"{slowCount} need(s) optimizing.");
        var totalMsInCalc = Output.Values.Sum(v => v.Sum(o => o.elapsed.ms));
        var percentageInCalc = Math.Round(Convert.ToDecimal(100 * (double)totalMsInCalc / totalMs), 2);
        Console.WriteLine(
            $"Total time spent calculating: {totalMsInCalc}ms (/{totalMs}ms = {percentageInCalc}%).");
    }
}