#include <stdio.h>
#include <stdlib.h>
#include <time.h>

char *buf; long bufLen;

int calc(int start, int len)
{
	for (int pos = start + len - 1; pos < bufLen; pos++)
	{
		for (int i = 0; i < len; i++)
			for (int j = i + 1; j < len; j++)
				if (buf[pos-i] == buf[pos-j])
				{
					pos = pos - j + len - 1;
					goto next;
				}
		return pos + 1;
	next:
		;
	}
	return -1;
}

struct timespec timer_start()
{
    struct timespec start_time;
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start_time);
    return start_time;
}
long timer_end(struct timespec start_time)
{
    struct timespec end_time;
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end_time);
    long diffInNanos = (end_time.tv_sec - start_time.tv_sec) * (long)1e9 + (end_time.tv_nsec - start_time.tv_nsec);
    return diffInNanos;
}

int main(int argc, char *argv[])
{
	FILE *f = fopen(argc <= 1 ? "y2022d06.in" : argv[1], "rb");
	if (!f) return 1;
	fseek(f, 0, SEEK_END);
	bufLen = ftell(f);
	fseek(f, 0, SEEK_SET);
	buf = malloc(bufLen);
	if (!buf) return 1;
	fread(buf, 1, bufLen, f);
	long total = 0, best = 0; int count = 1000000;
	int p1 = 0; int p2 = 0;
	for (int i = 0; i < count; i++)
	{
		struct timespec start_time = timer_start();
		p1 = calc(0, 4);
		p2 = calc(p1, 14);
		long ns = timer_end(start_time);
		if (ns > best) best = ns;
		total += ns;
	}
	printf("%d %d\n", p1, p2);
	printf("Took %ld ns (avg of %d runs), best %ld ns.", total / count, count, best);
	return 0;
}