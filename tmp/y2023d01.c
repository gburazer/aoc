#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

char *DIGITS[10] = { "", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
int LEN[10];
const int MAX_LINE_LEN = 255;
char line[MAX_LINE_LEN+1];
int len;

int digitOrZero(bool useLetters, int ind)
{
    if (isdigit(line[ind])) return line[ind] - '0';
    if (!useLetters) return 0;
    for (int d = 1; d <= 9; d++)
        if (ind + LEN[d] <= len && !strncmp(line + ind, DIGITS[d], LEN[d]))
            return d;
    return 0;
}

int firstDigit(bool useLetters, int startInd, int step)
{
    for (int i = startInd; i >= 0 && i < len; i += step)
    {
        int d = digitOrZero(useLetters, i);
        if (d > 0)
            return d;
    }
    return 0;
}

int value(bool useLetters)
{
    return 10 * firstDigit(useLetters, 0, 1) + firstDigit(useLetters, len - 1, -1);
}

int main(void) 
{
    for (int d = 1; d <= 9; d++) LEN[d] = strlen(DIGITS[d]);

    FILE *fp = fopen("y2023d01.in", "r");
    if (fp == NULL) return -1;

    int p1 = 0; int p2 = 0;
    while (fgets(line, sizeof(line), fp))
    {
        len = strlen(line);
        if (line[len - 1] == '\n') line[--len] = '\0'; // Remove newline if present
        p1 += value(false);
        p2 += value(true);
    }
    fclose(fp);
    printf("p1 = %d; p2 = %d\n", p1, p2);
    return 0;
}
